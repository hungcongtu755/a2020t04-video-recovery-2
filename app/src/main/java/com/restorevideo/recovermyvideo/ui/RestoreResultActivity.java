package com.restorevideo.recovermyvideo.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.measurement.api.AppMeasurementSdk;
import com.restorevideo.recovermyvideo.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.kzvideo2004.sdk.ads.AdsCompat;
import com.kzvideo2004.sdk.ads.funtion.Rate;

public class RestoreResultActivity extends AppCompatActivity {
    Toolbar toolbar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_restore_result);
        intView();
        intData();
        AdsCompat.getInstance(this).loadNative(true);
    }

    public void intView() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle((CharSequence) getString(R.string.photo_recovery));
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void intData() {
        ((TextView) findViewById(R.id.tvStatus)).setText(String.valueOf(getIntent().getIntExtra(AppMeasurementSdk.ConditionalUserProperty.VALUE, 0)));
        ((TextView) findViewById(R.id.tvPath)).setText("File Restored to\n/" + getString(R.string.restore_folder_path));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onBackPressed() {
        Rate.Show(this);
    }
}
