package com.restorevideo.recovermyvideo.ui.dialog;

import android.app.Dialog;
import android.content.Context;

import com.restorevideo.recovermyvideo.R;

public class LoadingDialog extends Dialog {
    private Context mContext;

    public LoadingDialog(Context context) {
        super(context, R.style.DialogLoadingTheme);
        this.mContext = context;
        requestWindowFeature(1);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.layout_loading_dialog);
    }
}
