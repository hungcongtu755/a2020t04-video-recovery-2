package com.restorevideo.recovermyvideo.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.restorevideo.recovermyvideo.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.kzvideo2004.sdk.ads.AdsCompat;

public class NoFileActiviy extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;

    public void intEvent() {
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_no_file_activiy);
        intView();
        intEvent();
        intData();
    }

    public void intView() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.toolbar.setTitle((CharSequence) getString(R.string.app_name));
    }

    public void intData() {
        AdsCompat.getInstance(this).loadNative(true);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onBackPressed() {
        AdsCompat.getInstance(this).showFullScreen(new AdsCompat.AdCloseListener() {
            public void onAdClosed() {
                NoFileActiviy.this.finish();
            }
        }, true);
    }
}
