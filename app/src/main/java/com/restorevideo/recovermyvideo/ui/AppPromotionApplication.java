package com.restorevideo.recovermyvideo.ui;

import android.content.Context;

import com.bzvideo2004.sdk.android.fcm.ImbaMessaging;
import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.utils.FirebaseUtil;
import com.restorevideo.recovermyvideo.utils.PrefManager;

import androidx.multidex.MultiDexApplication;

public class AppPromotionApplication extends MultiDexApplication {

    private static Context applicationContext;
    private static PrefManager prefManager;
    private static boolean isVipAccount = false;

    public static Context getGlobalContext() {
        return applicationContext;
    }

    public static PrefManager getPrefManager() {
        return prefManager;
    }

    public static boolean isVipAccount() {
        return isVipAccount;
    }

    public static void setVipAccount(boolean isVipAccount) {
        AppPromotionApplication.isVipAccount = isVipAccount;
        prefManager.setVipAccount(isVipAccount);
    }

    public void onCreate() {
        super.onCreate();

        applicationContext = getApplicationContext();
        prefManager = new PrefManager(applicationContext);
        isVipAccount = prefManager.getVipAccount();

        initApp();
    }

    private void initApp() {
        com.google.firebase.FirebaseApp.initializeApp(this);
        com.google.android.gms.ads.MobileAds.initialize(this, getString(R.string.google_admob_pub_id));
        ImbaMessaging.initialize(this);
        com.facebook.ads.AudienceNetworkAds.initialize(this);
        FirebaseUtil.setContextFa(applicationContext);
    }
}
