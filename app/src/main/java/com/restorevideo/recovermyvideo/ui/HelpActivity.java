package com.restorevideo.recovermyvideo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.utils.PrefManager;

import androidx.appcompat.app.AppCompatActivity;

public class HelpActivity extends AppCompatActivity {

    private PrefManager prefManager;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_help);

        prefManager = new PrefManager(this);
    }

    public void onStartClick(View view) {
        if (AppPromotionApplication.isVipAccount()) {
            prefManager.setCountScanDuplicateFiles(0);
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            startActivity(new Intent(this, PurchaseFirstActivity.class));
            finish();
        }
    }
}
