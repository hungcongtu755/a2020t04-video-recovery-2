package com.restorevideo.recovermyvideo.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kzvideo2004.sdk.ads.AdsCompat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.model.VideoModel;
import com.restorevideo.recovermyvideo.task.RecoverOneVideosAsyncTask;
import com.restorevideo.recovermyvideo.ui.dialog.DialogUpgradeVip;
import com.restorevideo.recovermyvideo.utils.FirebaseUtil;
import com.restorevideo.recovermyvideo.utils.Utils;

import java.io.File;
import java.security.AccessController;
import java.text.DateFormat;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;

public class FileInfoActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnOpen;
    Button btnRestore;
    Button btnShare;
    ImageView ivVideo;
    RecoverOneVideosAsyncTask mRecoverOneVideosAsyncTask;
    VideoModel mVideoModel;
    SharedPreferences sharedPreferences;
    Toolbar toolbar;
    TextView tvDate;
    TextView tvSize;
    TextView tvType;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_file_info);
        intView();
        intData();
        intEvent();
        if (!AppPromotionApplication.isVipAccount()) {
            AdsCompat.getInstance(this).loadNative(true);
        }
    }

    public void intView() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle((CharSequence) getString(R.string.restore_photo));
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.btnOpen = (Button) findViewById(R.id.btnOpen);
        this.btnShare = (Button) findViewById(R.id.btnShare);
        this.btnRestore = (Button) findViewById(R.id.btnRestore);
        this.tvDate = (TextView) findViewById(R.id.tvDate);
        this.tvSize = (TextView) findViewById(R.id.tvSize);
        this.tvType = (TextView) findViewById(R.id.tvType);
        this.ivVideo = (ImageView) findViewById(R.id.ivVideo);
    }

    public void intData() {
        this.mVideoModel = (VideoModel) getIntent().getSerializableExtra("ojectVideo");
        TextView textView = this.tvDate;
        textView.setText(DateFormat.getDateInstance().format(Long.valueOf(this.mVideoModel.getLastModified())) + "  " + this.mVideoModel.getTimeDuration());
        this.tvSize.setText(Utils.formatSize(this.mVideoModel.getSizePhoto()));
        this.tvType.setText(this.mVideoModel.getTypeFile());
        RequestManager with = Glide.with((FragmentActivity) this);
        ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) with.load("file://" + this.mVideoModel.getPathPhoto()).diskCacheStrategy(DiskCacheStrategy.ALL)).priority(Priority.HIGH)).centerCrop()).error((int) R.drawable.ic_error)).into(this.ivVideo);
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public void intEvent() {
        this.btnOpen.setOnClickListener(this);
        this.btnShare.setOnClickListener(this);
        this.btnRestore.setOnClickListener(this);
        this.ivVideo.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnOpen:
                openFile(this.mVideoModel.getPathPhoto());
                return;
            case R.id.btnRestore:
                if (AppPromotionApplication.isVipAccount()) {
                    this.mRecoverOneVideosAsyncTask = new RecoverOneVideosAsyncTask(this, this.mVideoModel, new RecoverOneVideosAsyncTask.OnRestoreListener() {
                        public void onComplete() {
                            Intent intent = new Intent(FileInfoActivity.this.getApplicationContext(), RestoreResultActivity.class);
                            intent.putExtra(AppMeasurementSdk.ConditionalUserProperty.VALUE, 1);
                            FileInfoActivity.this.startActivity(intent);
                            FileInfoActivity.this.finish();
                        }
                    });
                    this.mRecoverOneVideosAsyncTask.execute(new String[0]);
                } else {
                    FirebaseUtil.logEvent("dialog_vip_open");
                    DialogUpgradeVip dialogUpgradeVip = new DialogUpgradeVip(FileInfoActivity.this);
                    dialogUpgradeVip.show();
                }
                return;
            case R.id.btnShare:
                shareVideo(this.mVideoModel.getPathPhoto());
                return;
            case R.id.ivVideo:
                openFile(this.mVideoModel.getPathPhoto());
                return;
            default:
                return;
        }
    }

    public void cancleUIUPdate() {
        RecoverOneVideosAsyncTask recoverOneVideosAsyncTask = this.mRecoverOneVideosAsyncTask;
        if (recoverOneVideosAsyncTask != null && recoverOneVideosAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            this.mRecoverOneVideosAsyncTask.cancel(true);
            this.mRecoverOneVideosAsyncTask = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public boolean SDCardCheck() {
        File[] externalFilesDirs = ContextCompat.getExternalFilesDirs(this, (String) null);
        return (externalFilesDirs.length <= 1 || externalFilesDirs[0] == null || externalFilesDirs[1] == null) ? false : true;
    }

    public void fileSearch() {
        startActivityForResult(new Intent("android.intent.action.OPEN_DOCUMENT_TREE"), 100);
    }

    public void openFile(String str) {
        Intent intent;
        if (Build.VERSION.SDK_INT < 24) {
            Intent intent2 = new Intent("android.intent.action.VIEW");
            intent2.setDataAndType(Uri.fromFile(new File(str)), "video/*");
            intent = Intent.createChooser(intent2, "Complete action using");
        } else {
            File file = new File(str);
            Intent intent3 = new Intent("android.intent.action.VIEW");
            Uri uriForFile = FileProvider.getUriForFile(this, getPackageName() + ".provider", file);
            grantUriPermission(getPackageName(), uriForFile, Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent3.setType("*/*");
            if (Build.VERSION.SDK_INT < 24) {
                uriForFile = Uri.fromFile(file);
            }
            intent3.setData(uriForFile);
            intent3.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent = Intent.createChooser(intent3, "Complete action using");
        }
        startActivity(intent);
    }

    private void shareVideo(String str) {
        try {
            startActivity(Intent.createChooser(new Intent().setAction("android.intent.action.SEND").setType("video/*").setFlags(1).putExtra("android.intent.extra.STREAM", FileProvider.getUriForFile(this, getPackageName() + ".provider", new File(str))), ""));
        } catch (Exception unused) {
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        cancleUIUPdate();
    }

    @TargetApi(21)
    private static boolean checkIfSDCardRoot(Uri uri) {
        return isExternalStorageDocument(uri) && isRootUri(uri) && !isInternalStorage(uri);
    }

    @RequiresApi(api = 21)
    private static boolean isRootUri(Uri uri) {
        return DocumentsContract.getTreeDocumentId(uri).endsWith(":");
    }

    @RequiresApi(api = 21)
    public static boolean isInternalStorage(Uri uri) {
        return isExternalStorageDocument(uri) && DocumentsContract.getTreeDocumentId(uri).contains("primary");
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    @Override
    public void onActivityResult(int i, int i2, Intent intent) {
        boolean z;
        super.onActivityResult(i, i2, intent);
        if (i == 100 && i2 == -1) {
            SharedPreferences.Editor edit = this.sharedPreferences.edit();
            if (intent != null) {
                Uri data = intent.getData();
                if (Build.VERSION.SDK_INT >= 19 && AccessController.getContext() != null) {
                    getContentResolver().takePersistableUriPermission(data, 3);
                }
                z = true;
                if (checkIfSDCardRoot(data)) {
                    edit.putString("sdCardUri", data.toString());
                    edit.putBoolean("storagePermission", true);
                    if (edit.commit()) {
                        edit.apply();
                        if (z) {
                            this.mRecoverOneVideosAsyncTask = new RecoverOneVideosAsyncTask(this, this.mVideoModel, new RecoverOneVideosAsyncTask.OnRestoreListener() {
                                public void onComplete() {
                                    Intent intent = new Intent(FileInfoActivity.this.getApplicationContext(), RestoreResultActivity.class);
                                    intent.putExtra(AppMeasurementSdk.ConditionalUserProperty.VALUE, 1);
                                    intent.putExtra("type", 1);
                                    FileInfoActivity.this.startActivity(intent);
                                    FileInfoActivity.this.finish();
                                }
                            });
                            this.mRecoverOneVideosAsyncTask.execute(new String[0]);
                        }
                    }
                } else {
                    Toast.makeText(this, "Please Select Right SD Card.", Toast.LENGTH_SHORT).show();
                    edit.putBoolean("storagePermission", false);
                    edit.putString("sdCardUri", "");
                }
            } else {
                Toast.makeText(this, "Please Select Right SD Card.", Toast.LENGTH_SHORT).show();
                edit.putString("sdCardUri", "");
            }
            z = false;
            if (edit.commit()) {
            }
        }
        if (i == 200 && i2 == -1) {
            finish();
        }
    }
}
