package com.restorevideo.recovermyvideo.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.utils.Config;
import com.restorevideo.recovermyvideo.utils.SharePreferenceUtils;

import java.util.Locale;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.kzvideo2004.sdk.ads.AdsCompat;
import com.kzvideo2004.sdk.ads.funtion.Rate;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {
    RelativeLayout rlLanguage;
    RelativeLayout rlMoreApp;
    RelativeLayout rlPolicy;
    RelativeLayout rlRate;
    RelativeLayout rlShare;
    Toolbar toolbar;
    TextView tvDir;
    TextView tvLanguage;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_setting);
        intView();
        intData();
        intEvent();
    }

    public void intView() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle((CharSequence) getString(R.string.setting));
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.tvDir = (TextView) findViewById(R.id.tvDir);
        this.tvLanguage = (TextView) findViewById(R.id.tvLanguage);
        this.rlMoreApp = (RelativeLayout) findViewById(R.id.rlMoreApp);
        this.rlRate = (RelativeLayout) findViewById(R.id.rlRate);
        this.rlShare = (RelativeLayout) findViewById(R.id.rlShare);
        this.rlPolicy = (RelativeLayout) findViewById(R.id.rlPolicy);
        this.rlLanguage = (RelativeLayout) findViewById(R.id.rlLanguage);
    }

    public void intData() {
        this.tvDir.setText(Config.IMAGE_RECOVER_DIRECTORY);
        this.tvLanguage.setText(getResources().getStringArray(R.array.arrLanguage)[SharePreferenceUtils.getInstance(this).getLanguageIndex()]);
    }

    public void intEvent() {
        this.rlMoreApp.setOnClickListener(this);
        this.rlRate.setOnClickListener(this);
        this.rlShare.setOnClickListener(this);
        this.rlPolicy.setOnClickListener(this);
        this.rlLanguage.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlLanguage /*2131362039*/:
                showDialogLanguage();
                return;
            case R.id.rlMoreApp /*2131362040*/:
                AdsCompat.getInstance(this).showStoreAds(new AdsCompat.AdCloseListener() {
                    public void onAdClosed() {
                    }
                });
                return;
            case R.id.rlPolicy /*2131362041*/:
                UtilsCombat.OpenBrower(this, getString(R.string.link_policy));
                return;
            case R.id.rlRate /*2131362042*/:
                Rate.ShowNoExit(this);
                return;
            case R.id.rlShare /*2131362043*/:
                UtilsCombat.OpenBrower(this, getString(R.string.link_more_app));
                return;
            default:
                return;
        }
    }

    public void showDialogLanguage() {
        final String[] stringArray = getResources().getStringArray(R.array.arrLanguage);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.language_setting);
        builder.setSingleChoiceItems((CharSequence[]) getResources().getStringArray(R.array.arrLanguage), SharePreferenceUtils.getInstance(this).getLanguageIndex(), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                SettingActivity.this.tvLanguage.setText(stringArray[i]);
                SettingActivity.this.setSaveLanguage(i);
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton((CharSequence) getString(17039360), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.create().show();
    }

    public void setSaveLanguage(int i) {
        if (i == 0) {
            SharePreferenceUtils.getInstance(this).saveLanguageIndex(0);
            setLocale("en");
        } else if (i == 1) {
            SharePreferenceUtils.getInstance(this).saveLanguageIndex(1);
            setLocale("pt");
        } else if (i == 2) {
            SharePreferenceUtils.getInstance(this).saveLanguageIndex(2);
            setLocale("vi");
        } else if (i == 3) {
            SharePreferenceUtils.getInstance(this).saveLanguageIndex(3);
            setLocale("ru");
        } else if (i == 4) {
            SharePreferenceUtils.getInstance(this).saveLanguageIndex(4);
            setLocale("fr");
        } else if (i == 5) {
            SharePreferenceUtils.getInstance(this).saveLanguageIndex(5);
            setLocale("ar");
        } else if (i == 6) {
            SharePreferenceUtils.getInstance(this).saveLanguageIndex(6);
            setLocale("es");
        }
    }

    public void setLocale(String str) {
        Locale locale = new Locale(str);
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, displayMetrics);
        startActivity(new Intent(this, SplashActivity.class));
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onBackPressed() {
        AdsCompat.getInstance(this).showFullScreen(new AdsCompat.AdCloseListener() {
            public void onAdClosed() {
                SettingActivity.this.finish();
            }
        }, true);
    }
}
