package com.restorevideo.recovermyvideo.ui;

import android.content.Intent;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.kzvideo2004.sdk.ads.AdsCompat;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.adapter.VideoAdapter;
import com.restorevideo.recovermyvideo.model.VideoModel;
import com.restorevideo.recovermyvideo.task.RecoverVideoAsyncTask;
import com.restorevideo.recovermyvideo.ui.dialog.DialogUpgradeVip;
import com.restorevideo.recovermyvideo.utils.FirebaseUtil;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class VideoActivity extends AppCompatActivity {
    VideoAdapter adapter;
    Button btnRestore;
    int int_position;
    ArrayList<VideoModel> mList = new ArrayList<>();
    RecoverVideoAsyncTask mRecoverVideoAsyncTask;
    RecyclerView recyclerView;
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_video);
        intView();
        intData();
        if (!AppPromotionApplication.isVipAccount()) {
            AdsCompat.getInstance(this).loadBanner(true);
        }
    }

    public void intView() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle((CharSequence) getString(R.string.photo_recovery));
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.btnRestore = (Button) findViewById(R.id.btnRestore);
        this.recyclerView = (RecyclerView) findViewById(R.id.gv_folder);
        this.recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        this.recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(5), true));
        this.recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void intData() {
        this.int_position = getIntent().getIntExtra(AppMeasurementSdk.ConditionalUserProperty.VALUE, 0);
        if (MainActivity.mAlbumVideo != null && MainActivity.mAlbumVideo.size() > this.int_position) {
            this.mList.addAll((ArrayList) MainActivity.mAlbumVideo.get(this.int_position).getListPhoto().clone());
        }
        this.adapter = new VideoAdapter(this, this.mList);
        this.recyclerView.setAdapter(this.adapter);
        this.btnRestore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (AppPromotionApplication.isVipAccount()) {
                    final ArrayList<VideoModel> selectedItem = VideoActivity.this.adapter.getSelectedItem();
                    if (selectedItem.size() == 0) {
                        Toast.makeText(VideoActivity.this, "Cannot restore, all items are unchecked!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    VideoActivity videoActivity = VideoActivity.this;
                    videoActivity.mRecoverVideoAsyncTask = new RecoverVideoAsyncTask(videoActivity, videoActivity.adapter.getSelectedItem(), new RecoverVideoAsyncTask.OnRestoreListener() {
                        public void onComplete() {
                            Intent intent = new Intent(VideoActivity.this.getApplicationContext(), RestoreResultActivity.class);
                            intent.putExtra(AppMeasurementSdk.ConditionalUserProperty.VALUE, selectedItem.size());
                            intent.putExtra("type", 1);
                            VideoActivity.this.startActivity(intent);
                            VideoActivity.this.adapter.setAllImagesUnseleted();
                            VideoActivity.this.adapter.notifyDataSetChanged();
                        }
                    });
                    VideoActivity.this.mRecoverVideoAsyncTask.execute(new String[0]);
                } else {
                    FirebaseUtil.logEvent("dialog_vip_open");
                    DialogUpgradeVip dialogUpgradeVip = new DialogUpgradeVip(VideoActivity.this);
                    dialogUpgradeVip.show();
                }
            }
        });
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {
        private boolean includeEdge;
        private int spacing;
        private int spanCount;

        public GridSpacingItemDecoration(int i, int i2, boolean z) {
            this.spanCount = i;
            this.spacing = i2;
            this.includeEdge = z;
        }

        public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
            int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
            int i = this.spanCount;
            int i2 = childAdapterPosition % i;
            if (this.includeEdge) {
                int i3 = this.spacing;
                rect.left = i3 - ((i2 * i3) / i);
                rect.right = ((i2 + 1) * i3) / i;
                if (childAdapterPosition < i) {
                    rect.top = i3;
                }
                rect.bottom = this.spacing;
                return;
            }
            int i4 = this.spacing;
            rect.left = (i2 * i4) / i;
            rect.right = i4 - (((i2 + 1) * i4) / i);
            if (childAdapterPosition >= i) {
                rect.top = i4;
            }
        }
    }

    private int dpToPx(int i) {
        return Math.round(TypedValue.applyDimension(1, (float) i, getResources().getDisplayMetrics()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void cancleUIUPdate() {
        RecoverVideoAsyncTask recoverVideoAsyncTask = this.mRecoverVideoAsyncTask;
        if (recoverVideoAsyncTask != null && recoverVideoAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            this.mRecoverVideoAsyncTask.cancel(true);
            this.mRecoverVideoAsyncTask = null;
        }
    }

    @Override
    public void onBackPressed() {
        if (AppPromotionApplication.isVipAccount()) {
            super.onBackPressed();
        } else {
            AdsCompat.getInstance(this).showFullScreen(new AdsCompat.AdCloseListener() {
                public void onAdClosed() {
                    VideoActivity.this.finish();
                }
            }, true);
        }
    }
}
