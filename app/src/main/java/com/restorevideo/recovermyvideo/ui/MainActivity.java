package com.restorevideo.recovermyvideo.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kzvideo2004.sdk.ads.AdsCompat;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;
import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.navigation.NavigationView;
import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.model.AlbumVideo;
import com.restorevideo.recovermyvideo.model.VideoModel;
import com.restorevideo.recovermyvideo.utils.PrefManager;
import com.restorevideo.recovermyvideo.utils.Utils;
import com.romainpiel.shimmer.Shimmer;
import com.skyfishjy.library.RippleBackground;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.os.EnvironmentCompat;
import androidx.drawerlayout.widget.DrawerLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_PERMISSIONS = 100;
    public static ArrayList<AlbumVideo> mAlbumVideo = new ArrayList<>();
    private ArrayList<String> arrPermission;
    private PrefManager prefManager;
    ImageButton btnScan;
    CardView cvVideo;
    CardView cvSetting;
    CardView cvViewVideo;
    LottieAnimationView ivSearch;
    public DrawerLayout mDrawerLayout;
    ScanAsyncTask mScanAsyncTask;
    String[] projection = {"_data", "mime_type"};
    RippleBackground rippleBackground;
    Shimmer shimmer;
    Toolbar toolbar;
    TextView tvNumber;

    public void intData() {
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        prefManager = new PrefManager(this);
        intDrawer();
        intView();
        intData();
        intEvent();
        checkPermission();
    }

    public void requestPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), "android.permission.WRITE_EXTERNAL_STORAGE") == 0 || ContextCompat.checkSelfPermission(getApplicationContext(), "android.permission.READ_EXTERNAL_STORAGE") == 0) {
            File file = new File(Utils.getPathSave(this, "RestorePhoto"));
            if (!file.exists()) {
                file.mkdirs();
            }
        } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.WRITE_EXTERNAL_STORAGE") || !ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.READ_EXTERNAL_STORAGE")) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"}, 100);
        }
    }

    public void intDrawer() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle((CharSequence) getString(R.string.app_name));
        setSupportActionBar(this.toolbar);
        this.mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, this.mDrawerLayout, this.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        this.mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        ((NavigationView) findViewById(R.id.nav_view)).setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                switch (menuItem.getItemId()) {
                    case R.id.nav_setting:
                        startActivity(new Intent(MainActivity.this, SettingActivity.class));
                        break;
                    case R.id.nav_policy:
                        MainActivity mainActivity = MainActivity.this;
                        UtilsCombat.OpenBrower(mainActivity, mainActivity.getString(R.string.link_policy));
                        break;
                    case R.id.nav_send:
                        MainActivity mainActivity2 = MainActivity.this;
                        UtilsCombat.SendFeedBack(mainActivity2, mainActivity2.getString(R.string.Title_email), MainActivity.this.getString(R.string.email_feedback));
                        break;
                    case R.id.nav_share:
                        UtilsCombat.shareApp(MainActivity.this);
                        break;
                    case R.id.nav_view_restored_photos:
                        MainActivity.this.startActivity(new Intent(MainActivity.this.getApplicationContext(), ViewVideoActivity.class));
                        break;
                }
                MainActivity.this.mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    public void intView() {
        this.btnScan = (ImageButton) findViewById(R.id.btnScan);
        this.tvNumber = (TextView) findViewById(R.id.tvNumber);
        this.ivSearch = (LottieAnimationView) findViewById(R.id.ivSearch);
        this.rippleBackground = (RippleBackground) findViewById(R.id.im_scan_bg);
        this.cvVideo = (CardView) findViewById(R.id.cvVideo);
        this.cvViewVideo = (CardView) findViewById(R.id.cvViewVideo);
        this.cvSetting = (CardView) findViewById(R.id.cvSetting);
    }

    public void intEvent() {
        this.cvVideo.setOnClickListener(this);
        this.cvViewVideo.setOnClickListener(this);
        this.cvSetting.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cvVideo:
                prefManager.setCountScanDuplicateFiles(prefManager.getCountScanDuplicateFiles() + 1);
                if (!AppPromotionApplication.isVipAccount() && prefManager.getCountScanDuplicateFiles() > 5) {
                    startActivity(new Intent(this, PurchaseFirstActivity.class));
                    return;
                }
                scanType();
                return;
            case R.id.cvSetting:
                startActivity(new Intent(this, SettingActivity.class));
                return;
            case R.id.cvViewVideo:
                startActivity(new Intent(getApplicationContext(), ViewVideoActivity.class));
                return;
            default:
                return;
        }
    }

    public void scanType() {
        ScanAsyncTask scanAsyncTask = this.mScanAsyncTask;
        if (scanAsyncTask == null || scanAsyncTask.getStatus() != AsyncTask.Status.RUNNING) {
            mAlbumVideo.clear();
            this.tvNumber.setVisibility(View.VISIBLE);
            this.tvNumber.setText(getString(R.string.analyzing));
            this.ivSearch.playAnimation();
            this.rippleBackground.startRippleAnimation();
            this.mScanAsyncTask = new ScanAsyncTask();
            this.mScanAsyncTask.execute(new Void[0]);
            return;
        }
        Toast.makeText(this, getString(R.string.scan_wait), 1).show();
    }

    public class ScanAsyncTask extends AsyncTask<Void, Integer, Void> {
        ArrayList<VideoModel> listVideo = new ArrayList<>();
        int number = 0;

        public ScanAsyncTask() {
        }

        @Override
        public void onPreExecute() {
            super.onPreExecute();
            this.number = 0;
        }

        @Override
        public void onPostExecute(Void voidR) {
            super.onPostExecute(voidR);
            MainActivity.this.rippleBackground.stopRippleAnimation();
            MainActivity.this.ivSearch.pauseAnimation();
            MainActivity.this.ivSearch.setProgress(0.0f);
            MainActivity.this.tvNumber.setText("");
            MainActivity.this.tvNumber.setVisibility(View.INVISIBLE);
            if (MainActivity.mAlbumVideo.size() == 0) {
                MainActivity.this.startActivity(new Intent(MainActivity.this.getApplicationContext(), NoFileActiviy.class));
            } else {
                MainActivity.this.startActivity(new Intent(MainActivity.this.getApplicationContext(), AlbumVideoActivity.class));
            }
        }

        @Override
        public void onProgressUpdate(Integer... numArr) {
            super.onProgressUpdate(numArr);
            TextView textView = MainActivity.this.tvNumber;
            textView.setText("Files: " + String.valueOf(numArr[0]));
        }

        @Override
        public Void doInBackground(Void... voidArr) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            StringBuilder sb = new StringBuilder();
            sb.append("root = ");
            sb.append(absolutePath);
            try {
                getSdCardVideo();
                checkFileOfDirectoryVideo(absolutePath, Utils.getFileList(absolutePath));
            } catch (Exception unused2) {
            }
            try {
                Thread.sleep(3000);
                return null;
            } catch (InterruptedException e2) {
                e2.printStackTrace();
                return null;
            }
        }

        public void getSdCardVideo() {
            String[] externalStorageDirectories = MainActivity.this.getExternalStorageDirectories();
            if (externalStorageDirectories != null && externalStorageDirectories.length > 0) {
                for (String str : externalStorageDirectories) {
                    File file = new File(str);
                    if (file.exists()) {
                        checkFileOfDirectoryVideo(str, file.listFiles());
                    }
                }
            }
        }

        public void checkFileOfDirectoryVideo(String str, File[] fileArr) {
            String str2 = str;
            File[] fileArr2 = fileArr;
            for (int i = 0; i < fileArr2.length; i++) {
                if (fileArr2[i].isDirectory()) {
                    String path = fileArr2[i].getPath();
                    File[] fileList = Utils.getFileList(fileArr2[i].getPath());
                    if (!(path == null || fileList == null || fileList.length <= 0)) {
                        checkFileOfDirectoryVideo(path, fileList);
                    }
                } else if (fileArr2[i].getPath().endsWith(".3gp") || fileArr2[i].getPath().endsWith(".mp4") || fileArr2[i].getPath().endsWith(".mkv") || fileArr2[i].getPath().endsWith(".flv")) {
                    File file = new File(fileArr2[i].getPath());
                    String substring = fileArr2[i].getPath().substring(fileArr2[i].getPath().lastIndexOf(".") + 1);
                    long j = 0;
                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                    try {
                        mediaMetadataRetriever.setDataSource(file.getPath());
                        j = Long.parseLong(mediaMetadataRetriever.extractMetadata(9));
                        mediaMetadataRetriever.release();
                    } catch (Exception unused) {
                        unused.printStackTrace();
                    }
                    ArrayList<VideoModel> arrayList = this.listVideo;
//                    VideoModel videoModel = arrayList.get(i);
                    VideoModel videoModel2 = new VideoModel(fileArr2[i].getPath(), file.lastModified(), file.length(), substring, Utils.convertDuration(j));
                    arrayList.add(videoModel2);
                    this.number++;
                    publishProgress(new Integer[]{Integer.valueOf(this.number)});
                }
            }
            if (this.listVideo.size() != 0) {
                MainActivity mainActivity = MainActivity.this;
                if (!str2.contains(Utils.getPathSave(mainActivity, mainActivity.getString(R.string.restore_folder_path)))) {
                    AlbumVideo albumVideo = new AlbumVideo();
                    albumVideo.setStr_folder(str2);
                    albumVideo.setLastModified(new File(str2).lastModified());
                    Collections.sort(this.listVideo, new Comparator<VideoModel>() {
                        public int compare(VideoModel videoModel, VideoModel videoModel2) {
                            return Long.valueOf(videoModel2.getLastModified()).compareTo(Long.valueOf(videoModel.getLastModified()));
                        }
                    });
                    albumVideo.setListPhoto((ArrayList) this.listVideo.clone());
                    MainActivity.mAlbumVideo.add(albumVideo);
                }
            }
            this.listVideo.clear();
        }
    }

    @Override
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        super.onRequestPermissionsResult(i, strArr, iArr);
        if (i == 100) {
            for (int i2 = 0; i2 < iArr.length; i2++) {
                if (iArr.length <= 0 || iArr[i2] != 0) {
                    Toast.makeText(this, "The app was not allowed to read or write to your storage. Hence, it cannot function properly. Please consider granting it this permission", 1).show();
                    finish();
                } else {
                    File file = new File(Utils.getPathSave(this, "RestorePhoto"));
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                }
            }
        }
    }

    private void showNotFoundDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((CharSequence) getString(R.string.not_found));
        builder.setPositiveButton((CharSequence) getString(17039370), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem findItem = menu.findItem(R.id.mnAds);
        findItem.setActionView(R.layout.layout_gif_ads);
        ((LottieAnimationView) findItem.getActionView().findViewById(R.id.lvAds)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdsCompat.getInstance(MainActivity.this).showStoreAds(new AdsCompat.AdCloseListener() {
                    public void onAdClosed() {
                    }
                });
            }
        });
        return true;
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.String, java.io.InputStream] */
    public String[] getExternalStorageDirectories() {
        File[] externalFilesDirs;
        String[] split;
        boolean z;
        ArrayList arrayList = new ArrayList();
//        ?r2 = 0;
        String r2 = String.format("%s", Environment.getExternalStorageDirectory());
        if (Build.VERSION.SDK_INT >= 19 && (externalFilesDirs = getExternalFilesDirs(r2)) != null && externalFilesDirs.length > 0) {
            for (File file : externalFilesDirs) {
                if (!(file == null || (split = file.getPath().split("/Android")) == null || split.length <= 0)) {
                    String str = split[0];
                    if (Build.VERSION.SDK_INT >= 21) {
                        z = Environment.isExternalStorageRemovable(file);
                    } else {
                        z = "mounted".equals(EnvironmentCompat.getStorageState(file));
                    }
                    if (z) {
                        arrayList.add(str);
                    }
                }
            }
        }
        if (arrayList.isEmpty()) {
            String str2 = "";
            try {
                Process start = new ProcessBuilder(new String[0]).command(new String[]{"mount | grep /dev/block/vold"}).redirectErrorStream(true).start();
                start.waitFor();
                InputStream inputStream = start.getInputStream();
                byte[] bArr = new byte[1024];
                while (inputStream.read(bArr) != -1) {
                    str2 = str2 + new String(bArr);
                }
                inputStream.close();
            } catch (Exception unused) {
//                if (r2 != 0) {
//                    try {
//                        r2.close();
//                    } catch (IOException unused2) {
//                    }
//                }
            }
            if (!str2.trim().isEmpty()) {
                String[] split2 = str2.split(IOUtils.LINE_SEPARATOR_UNIX);
                if (split2.length > 0) {
                    for (String split3 : split2) {
                        arrayList.add(split3.split(" ")[2]);
                    }
                }
            }
        }
        String[] strArr = new String[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            strArr[i] = (String) arrayList.get(i);
        }
        return strArr;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.lvAds) {
            return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }

    private void checkPermission() {
        this.arrPermission = new ArrayList<>();
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Utils.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                this.arrPermission.add("android.permission.WRITE_EXTERNAL_STORAGE");
            }
            if (!Utils.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE")) {
                this.arrPermission.add("android.permission.READ_EXTERNAL_STORAGE");
            }
            if (!this.arrPermission.isEmpty()) {
                requestPermissions((String[]) this.arrPermission.toArray(new String[0]), 100);
            }
        }
    }

    @Override
    public void onBackPressed() {
        ScanAsyncTask scanAsyncTask = this.mScanAsyncTask;
        if (scanAsyncTask == null || scanAsyncTask.getStatus() != AsyncTask.Status.RUNNING) {
            AdsCompat.getInstance(this).showDialogExit();
        } else {
            Toast.makeText(this, getString(R.string.scan_wait), 1).show();
        }
    }
}
