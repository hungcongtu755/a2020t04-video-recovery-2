package com.restorevideo.recovermyvideo.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.android.vending.billing.IInAppBillingService;
import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.purchase.IabHelper;
import com.restorevideo.recovermyvideo.purchase.IabResult;
import com.restorevideo.recovermyvideo.purchase.Purchase;
import com.restorevideo.recovermyvideo.utils.CommonUtils;
import com.restorevideo.recovermyvideo.utils.Constants;
import com.restorevideo.recovermyvideo.utils.FirebaseUtil;
import com.restorevideo.recovermyvideo.utils.PrefManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PurchaseSecondActivity extends AppCompatActivity {

    private PrefManager prefManager;
    private String packagePurchase = Constants.GOOGLE_PLAY_PACKAGE_FREE_TRIAL;
    private IabHelper mHelper;
    private boolean isIabHelperSetup = false;
    private IInAppBillingService mService;
    private String payload = "";

    ServiceConnection mServiceConn = new ServiceConnection() {
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = IInAppBillingService.Stub.asInterface(iBinder);
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult iabResult, Purchase purchase) {
            if (iabResult.isFailure()) {
                if (iabResult.getResponse() == -1005) {
                    FirebaseUtil.logEventClose(packagePurchase);
                } else {
                    FirebaseUtil.logEventPurchaseFailed(purchase, iabResult.getResponse(), iabResult.getMessage());
                }
                Toast.makeText(getApplicationContext(), "Purchase error. Please try again later!", Toast.LENGTH_LONG).show();
            } else if (verifyDeviceBuyPayload(purchase)) {
                try {
                    if (purchase.getSku().equals(Constants.GOOGLE_PLAY_PACKAGE_FREE_TRIAL) || purchase.getSku().equals(Constants.GOOGLE_PLAY_PACKAGE_BUY_NOW)) {
                        FirebaseUtil.logEventPurchase(purchase);
                        Toast.makeText(getApplicationContext(), "Upgrade Premium Version successfully!", Toast.LENGTH_LONG).show();
                        AppPromotionApplication.setVipAccount(true);
                        AppPromotionApplication.getPrefManager().setVipAccount(true);
                        startMainActivity();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    FirebaseUtil.logEventPurchaseFailed(purchase, e.hashCode(), e.getMessage());
                }
            } else {
                FirebaseUtil.logEventPurchaseFailed(purchase, 1001, "Error purchasing. Authenticity verification failed!");
                Toast.makeText(getApplicationContext(), "Error purchasing. Authenticity verification failed!", Toast.LENGTH_LONG).show();
            }
        }
    };

    @BindView(R.id.txtContinueLimited)
    TextView txtContinueLimited;

    @BindView(R.id.btnContinueLimited)
    RippleView btnContinueLimited;

    @BindView(R.id.btnTrial3Days)
    RippleView btnTrial3Days;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_second);
        ButterKnife.bind(this);

        prefManager = new PrefManager(this);
        prefManager.setHasClickCancelPurchase(true);

        SpannableString content = new SpannableString("continue with limited version");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        txtContinueLimited.setText(content);

        // IAP
        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        bindService(intent, this.mServiceConn, Context.BIND_AUTO_CREATE);
        this.mHelper = new IabHelper(this, Constants.GOOGLE_PLAY_BASE64_KEY);
        this.mHelper.enableDebugLogging(true);
        this.mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult iabResult) {
                if (iabResult.isSuccess()) {
                    isIabHelperSetup = true;
                }
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("In-app Billing setup failed: ");
                stringBuilder.append(iabResult);
                isIabHelperSetup = false;
            }
        });
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        this.payload = androidId;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (this.mService != null) {
            unbindService(this.mServiceConn);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(PurchaseSecondActivity.this, PurchaseFirstActivity.class));
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onActivityResult(");
        stringBuilder.append(requestCode);
        stringBuilder.append(",");
        stringBuilder.append(resultCode);
        stringBuilder.append(",");
        stringBuilder.append(data);
        if (this.mHelper != null) {
            if (this.mHelper.handleActivityResult(requestCode, resultCode, data)) {
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @OnClick({R.id.btnContinueLimited})
    public void btnContinueLimitedClicked() {
        btnContinueLimited.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                startActivity(new Intent(PurchaseSecondActivity.this, PurchaseFirstActivity.class));
                finish();
            }
        });
    }

    @OnClick({R.id.btnTrial3Days})
    public void btnTrial3DaysClicked() {
        btnTrial3Days.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                try {
                    packagePurchase =  Constants.GOOGLE_PLAY_PACKAGE_FREE_TRIAL;
                    FirebaseUtil.logEventOpen(packagePurchase);
                    mHelper.launchSubscriptionPurchaseFlow(PurchaseSecondActivity.this, packagePurchase, 10001, mPurchaseFinishedListener, payload);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void startMainActivity() {
        if (AppPromotionApplication.isVipAccount()) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            if (CommonUtils.isNetworkAvailable(this)) {
                startActivity(new Intent(this, PurchaseFirstActivity.class));
            } else {
                startActivity(new Intent(this, MainActivity.class));
            }
        }
        finish();
    }

    boolean verifyDeviceBuyPayload(Purchase purchase) {
        String developerPayload = purchase.getDeveloperPayload();
        String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        if (developerPayload.equals(androidId)) {
            return true;
        }
        return false;
    }
}
