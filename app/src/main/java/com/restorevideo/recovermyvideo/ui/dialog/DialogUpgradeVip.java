package com.restorevideo.recovermyvideo.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.andexert.library.RippleView;
import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.ui.PurchaseFirstActivity;
import com.restorevideo.recovermyvideo.utils.FirebaseUtil;

public class DialogUpgradeVip extends Dialog {

    private Activity activity;

    public DialogUpgradeVip(Activity a) {
        super(a, R.style.DialogPurchaseTheme);
        this.activity = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_purchase);

        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_purchase, null);
        setContentView(view);
        setCancelable(true);

        RippleView btnFreeTrial = findViewById(R.id.btnFreeTrial);

        btnFreeTrial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnFreeTrial.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                    @Override
                    public void onComplete(RippleView rippleView) {
                        FirebaseUtil.logEvent("btn_free_trial_dialog_vip_click");
                        activity.startActivity(new Intent(activity, PurchaseFirstActivity.class));
                        dismiss();
                    }
                });
            }
        });
    }
}