package com.restorevideo.recovermyvideo.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.kzvideo2004.sdk.ads.AdsCompat;
import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.purchase.IabHelper;
import com.restorevideo.recovermyvideo.purchase.IabResult;
import com.restorevideo.recovermyvideo.purchase.Inventory;
import com.restorevideo.recovermyvideo.purchase.Purchase;
import com.restorevideo.recovermyvideo.utils.Constants;
import com.restorevideo.recovermyvideo.utils.PrefManager;
import com.restorevideo.recovermyvideo.utils.SharePreferenceUtils;

import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    private Handler mHandler;
    private Runnable r;
    private PrefManager prefManager;
    private IInAppBillingService mService;
    private IabHelper mHelper;

    ServiceConnection mServiceConn = new ServiceConnection() {
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = IInAppBillingService.Stub.asInterface(iBinder);
        }
    };

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                return;
            }

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the premium upgrade?
            Purchase purchaseFreeTrial = inventory.getPurchase(Constants.GOOGLE_PLAY_PACKAGE_FREE_TRIAL);
            Purchase purchasePayNow = inventory.getPurchase(Constants.GOOGLE_PLAY_PACKAGE_BUY_NOW);
            AppPromotionApplication.setVipAccount(false);
            if ((purchaseFreeTrial != null && (purchaseFreeTrial.getOrderId().contains("GPA.33")) || (purchasePayNow != null && purchasePayNow.getOrderId().contains("GPA.33")))) {
                AppPromotionApplication.setVipAccount(true);
            }
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setLocale();
        setContentView((int) R.layout.activity_splash);
        intFirebaseConfig();
        AdsCompat.getInstance(this).loadData();

        prefManager = new PrefManager(this);
        prefManager.setHasClickCancelPurchase(false);

        purchaseSetup();

        this.mHandler = new Handler();
        this.r = new Runnable() {
            public void run() {
                SplashActivity splashActivity = SplashActivity.this;
                splashActivity.startActivity(new Intent(splashActivity, HelpActivity.class));
                SplashActivity.this.finish();
            }
        };
        this.mHandler.postDelayed(this.r, 4000);
    }

    @Override
    public void onDestroy() {
        Runnable runnable;
        Handler handler = this.mHandler;
        if (!(handler == null || (runnable = this.r) == null)) {
            handler.removeCallbacks(runnable);
        }
        super.onDestroy();
    }

    public void setLocale() {
        String str = "en";
        int languageIndex = SharePreferenceUtils.getInstance(this).getLanguageIndex();
        if (languageIndex != 0) {
            if (languageIndex == 1) {
                str = "pt";
            } else if (languageIndex == 2) {
                str = "vi";
            } else if (languageIndex == 3) {
                str = "ru";
            } else if (languageIndex == 4) {
                str = "fr";
            } else if (languageIndex == 5) {
                str = "ar";
            } else if (languageIndex == 6) {
                str = "es";
            }
            if (SharePreferenceUtils.getInstance(this).getFirstRun()) {
                str = Locale.getDefault().getLanguage();
                if (str.equalsIgnoreCase("en")) {
                    SharePreferenceUtils.getInstance(this).saveLanguageIndex(0);
                }
                if (str.equalsIgnoreCase("pt")) {
                    SharePreferenceUtils.getInstance(this).saveLanguageIndex(1);
                }
                if (str.equalsIgnoreCase("vi")) {
                    SharePreferenceUtils.getInstance(this).saveLanguageIndex(2);
                }
                if (str.equalsIgnoreCase("ru")) {
                    SharePreferenceUtils.getInstance(this).saveLanguageIndex(3);
                }
                if (str.equalsIgnoreCase("fr")) {
                    SharePreferenceUtils.getInstance(this).saveLanguageIndex(4);
                }
                if (str.equalsIgnoreCase("ar")) {
                    SharePreferenceUtils.getInstance(this).saveLanguageIndex(5);
                }
                if (str.equalsIgnoreCase("es")) {
                    SharePreferenceUtils.getInstance(this).saveLanguageIndex(6);
                }
            }
            Locale locale = new Locale(str);
            Resources resources = getResources();
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            Configuration configuration = resources.getConfiguration();
            configuration.locale = locale;
            resources.updateConfiguration(configuration, displayMetrics);
        }
        if (SharePreferenceUtils.getInstance(this).getFirstRun()) {
            str = Locale.getDefault().getLanguage();
            if (str.equalsIgnoreCase("en")) {
                SharePreferenceUtils.getInstance(this).saveLanguageIndex(0);
            }
            if (str.equalsIgnoreCase("pt")) {
                SharePreferenceUtils.getInstance(this).saveLanguageIndex(1);
            }
            if (str.equalsIgnoreCase("vi")) {
                SharePreferenceUtils.getInstance(this).saveLanguageIndex(2);
            }
            if (str.equalsIgnoreCase("ru")) {
                SharePreferenceUtils.getInstance(this).saveLanguageIndex(3);
            }
            if (str.equalsIgnoreCase("fr")) {
                SharePreferenceUtils.getInstance(this).saveLanguageIndex(4);
            }
            if (str.equalsIgnoreCase("ar")) {
                SharePreferenceUtils.getInstance(this).saveLanguageIndex(5);
            }
            if (str.equalsIgnoreCase("es")) {
                SharePreferenceUtils.getInstance(this).saveLanguageIndex(6);
            }
        }
        Locale locale2 = new Locale(str);
        Resources resources2 = getResources();
        DisplayMetrics displayMetrics2 = resources2.getDisplayMetrics();
        Configuration configuration2 = resources2.getConfiguration();
        configuration2.locale = locale2;
        resources2.updateConfiguration(configuration2, displayMetrics2);
    }

    private void purchaseSetup() {
        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        bindService(intent, this.mServiceConn, Context.BIND_AUTO_CREATE);
        mHelper = new IabHelper(this, Constants.GOOGLE_PLAY_BASE64_KEY);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult iabResult) {
                try {
                    if (iabResult == null) {
                        return;
                    }
                    if (!iabResult.isSuccess()) {
                        return;
                    }
                    if (mHelper == null) return;
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                } catch (Exception e) {

                }
            }
        });
    }

    public void intFirebaseConfig() {
        final FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .build();
        mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
        mFirebaseRemoteConfig.setDefaultsAsync(com.bzvideo2004.sdk.R.xml.remote_config_defaults);
        mFirebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(this, new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                        if (task.isSuccessful()) {
                            boolean updated = task.getResult();
                            Log.d("Ads", "Config params updated: " + updated);
                        } else {
                        }
                    }
                });
    }
}
