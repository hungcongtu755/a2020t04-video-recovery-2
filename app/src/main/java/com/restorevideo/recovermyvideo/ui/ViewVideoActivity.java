package com.restorevideo.recovermyvideo.ui;

import android.graphics.Rect;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.kzvideo2004.sdk.ads.AdsCompat;
import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.adapter.ViewVideoAdapter;
import com.restorevideo.recovermyvideo.model.VideoModel;
import com.restorevideo.recovermyvideo.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ViewVideoActivity extends AppCompatActivity {
    ViewVideoAdapter adapter;
    LottieAnimationView lvLoading;
    ScanViewVideo mScanViewVideo;
    RecyclerView recyclerView;
    Toolbar toolbar;
    TextView tvStatus;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_restored_video);
        intView();
        intData();
    }

    public void intView() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setTitle((CharSequence) getString(R.string.view_restored_photos));
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.lvLoading = (LottieAnimationView) findViewById(R.id.loading);
        this.tvStatus = (TextView) findViewById(R.id.tvStatus);
        this.recyclerView = (RecyclerView) findViewById(R.id.gv_folder);
        this.recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        this.recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        this.recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void intData() {
        this.mScanViewVideo = new ScanViewVideo();
        this.mScanViewVideo.execute(new String[0]);
    }

    public class ScanViewVideo extends AsyncTask<String, Integer, ArrayList<VideoModel>> {
        ArrayList<VideoModel> listVideo = new ArrayList<>();

        public ScanViewVideo() {
        }

        @Override
        public void onPreExecute() {
            super.onPreExecute();
            ViewVideoActivity.this.lvLoading.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPostExecute(ArrayList<VideoModel> arrayList) {
            super.onPostExecute(arrayList);
            ViewVideoActivity.this.lvLoading.setVisibility(View.GONE);
            if (this.listVideo.size() == 0) {
                ViewVideoActivity.this.tvStatus.setVisibility(View.VISIBLE);
                return;
            }
            ViewVideoActivity viewVideoActivity = ViewVideoActivity.this;
            viewVideoActivity.adapter = new ViewVideoAdapter(viewVideoActivity, this.listVideo);
            ViewVideoActivity.this.recyclerView.setAdapter(ViewVideoActivity.this.adapter);
        }

        @Override
        public void onProgressUpdate(Integer... numArr) {
            super.onProgressUpdate(numArr);
        }

        
        public ArrayList<VideoModel> doInBackground(String... strArr) {
            String str = Environment.getExternalStorageDirectory().getAbsolutePath() + "/RestoredVideos";
            StringBuilder sb = new StringBuilder();
            sb.append("root = ");
            sb.append(str);
            checkFileOfDirectory(Utils.getFileList(str));
            if (this.listVideo.size() == 0) {
                return null;
            }
            Collections.sort(this.listVideo, new Comparator<VideoModel>() {
                public int compare(VideoModel photoModel, VideoModel photoModel2) {
                    return Long.valueOf(photoModel2.getLastModified()).compareTo(Long.valueOf(photoModel.getLastModified()));
                }
            });
            return null;
        }

        public void checkFileOfDirectory(File[] fileArr) {
            for (int i = 0; i < fileArr.length; i++) {
                if (fileArr[i].isDirectory()) {
                    fileArr[i].getPath();
                    checkFileOfDirectory(Utils.getFileList(fileArr[i].getPath()));
                } else if (fileArr[i].getPath().endsWith(".3gp") || fileArr[i].getPath().endsWith(".mp4") || fileArr[i].getPath().endsWith(".mkv") || fileArr[i].getPath().endsWith(".flv")) {
                    File file = new File(fileArr[i].getPath());
                    String substring = fileArr[i].getPath().substring(fileArr[i].getPath().lastIndexOf(".") + 1);
                    long j = 0;
                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                    try {
                        mediaMetadataRetriever.setDataSource(file.getPath());
                        j = Long.parseLong(mediaMetadataRetriever.extractMetadata(9));
                        mediaMetadataRetriever.release();
                    } catch (Exception unused) {
                        unused.printStackTrace();
                    }
                    VideoModel videoModel = new VideoModel(fileArr[i].getPath(), file.lastModified(), file.length(), substring, Utils.convertDuration(j));
                    this.listVideo.add(videoModel);
                }
            }
        }
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {
        private boolean includeEdge;
        private int spacing;
        private int spanCount;

        public GridSpacingItemDecoration(int i, int i2, boolean z) {
            this.spanCount = i;
            this.spacing = i2;
            this.includeEdge = z;
        }

        public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
            int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
            int i = this.spanCount;
            int i2 = childAdapterPosition % i;
            if (this.includeEdge) {
                int i3 = this.spacing;
                rect.left = i3 - ((i2 * i3) / i);
                rect.right = ((i2 + 1) * i3) / i;
                if (childAdapterPosition < i) {
                    rect.top = i3;
                }
                rect.bottom = this.spacing;
                return;
            }
            int i4 = this.spacing;
            rect.left = (i2 * i4) / i;
            rect.right = i4 - (((i2 + 1) * i4) / i);
            if (childAdapterPosition >= i) {
                rect.top = i4;
            }
        }
    }

    private int dpToPx(int i) {
        return Math.round(TypedValue.applyDimension(1, (float) i, getResources().getDisplayMetrics()));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onBackPressed() {
        AdsCompat.getInstance(this).showFullScreen(new AdsCompat.AdCloseListener() {
            public void onAdClosed() {
                ViewVideoActivity.this.finish();
            }
        }, true);
    }
}
