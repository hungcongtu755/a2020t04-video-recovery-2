package com.restorevideo.recovermyvideo.utils;

import android.os.Environment;

import java.io.File;

public class Config {
    public static final int DATA = 1000;
    public static final String IMAGE_RECOVER_DIRECTORY = (Environment.getExternalStorageDirectory() + File.separator + "RestoredVideos");
    public static final int REPAIR = 2000;
    public static final int UPDATE = 3000;
}
