package com.restorevideo.recovermyvideo.utils;

public class Constants {

    public static final String GOOGLE_PLAY_BASE64_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq3BJxEmyLvJ2OyfSgKO/Bc8DsaE73J3CBMl5QM6paW1u14rJED22p3bsJswGUn8DVEhzyQPkzQtKCRW2oMrxOEycu4WxaaMqPUFs4EDc/cznt8WK02u7gq5YYI79gEr6a3ZEDmbUC7wjYfCTadxolmXhfw1LK72t6WqzgAyALFXheL6iovUhBm+xCgoqy9bLSTsESLE2y/aXQtKGuNkDwuTxm8E9deFI6e33Kxo5bSJfLHJVH01hl4/BX05BBGKEB55HBRrtaqJcboBZdNx/ot+tg2j+YBlMCGo+ncgJFb3Xi0OdxonZuAN0heu7MqpC/kKRwjzfT5PJyLVtOyklRQIDAQAB";
    public static final String GOOGLE_PLAY_PACKAGE_BUY_NOW = "buy_now";
    public static final String GOOGLE_PLAY_PACKAGE_FREE_TRIAL = "free_trial_3days";
    public static long CONFIG_EXPIRE_SECOND = 43200;
}
