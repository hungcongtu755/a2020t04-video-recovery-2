package com.restorevideo.recovermyvideo.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "com.recovermyvideos.restoredeletedvideos";
    private static final String IS_VIP_ACCOUNT = "IS_VIP_ACCOUNT";
    private static final String HAS_CLICK_CANCEL_PURCHASE = "HAS_CLICK_CANCEL_PURCHASE";
    private static final String COUNT_SCAN_DUPLICATE_FILES = "COUNT_SCAN_DUPLICATE_FILES";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setVipAccount(boolean isVipAccount) {
        editor.putBoolean(IS_VIP_ACCOUNT, isVipAccount);
        editor.commit();
    }

    public boolean getVipAccount() {
        return pref.getBoolean(IS_VIP_ACCOUNT, false);
    }

    public boolean getHasClickCancelPurchase() {
        return pref.getBoolean(HAS_CLICK_CANCEL_PURCHASE, false);
    }

    public void setHasClickCancelPurchase(boolean hasClickCancelPurchase) {
        editor.putBoolean(HAS_CLICK_CANCEL_PURCHASE, hasClickCancelPurchase);
        editor.commit();
    }

    public void setCountScanDuplicateFiles(int count) {
        editor.putInt(COUNT_SCAN_DUPLICATE_FILES, count);
        editor.commit();
    }

    public int getCountScanDuplicateFiles() {
        return pref.getInt(COUNT_SCAN_DUPLICATE_FILES, 0);
    }
}
