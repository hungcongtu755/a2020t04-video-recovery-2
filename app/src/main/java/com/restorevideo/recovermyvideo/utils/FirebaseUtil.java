package com.restorevideo.recovermyvideo.utils;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.restorevideo.recovermyvideo.purchase.Purchase;
import com.restorevideo.recovermyvideo.purchase.SkuDetails;

import androidx.core.provider.FontsContractCompat;

public class FirebaseUtil {

    public static String PURCHASE = "purchaseAndroid";
    public static FirebaseAnalytics firebaseAnalytics;

    public static void setContextFa(Context context) {
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public static DatabaseReference getBaseRef() {
        return FirebaseDatabase.getInstance().getReference();
    }

    public static DatabaseReference getPurchaseRef() {
        return getBaseRef().child(PURCHASE);
    }

    public static DatabaseReference getAppConfigRef() {
        return getBaseRef().child("config");
    }

    public static DatabaseReference getAppConfigVerRef() {
        return getBaseRef().child("configVer");
    }

    public static void logEvent(String str) {
        Bundle bundle = new Bundle();
        bundle.putString(str, str);
        firebaseAnalytics.logEvent(str, bundle);
    }

    public static void logEvent(String str, Bundle bundle) {
        firebaseAnalytics.logEvent(str, bundle);
    }

    public static void logEventClickBuy(SkuDetails skuDetails) {
        Bundle bundle = new Bundle();
        if (skuDetails != null) {
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, skuDetails.getSku());
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, skuDetails.getTitle());
            bundle.putString(FirebaseAnalytics.Param.PRICE, skuDetails.getPrice());
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, skuDetails.getType());
        }
        logEvent("checkout", bundle);
    }

    public static void logEventPurchase(Purchase purchase) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, purchase.getSku());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, purchase.getPackageName());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, purchase.getItemType());
        bundle.putString("auto_renewming", String.valueOf(purchase.isAutoRenewing()));
        logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, bundle);
    }

    public static void logEventPurchaseFailed(SkuDetails skuDetails, int code, String message) {
        Bundle bundle = new Bundle();
        if (skuDetails != null) {
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, skuDetails.getSku());
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, skuDetails.getTitle());
            bundle.putString(FirebaseAnalytics.Param.PRICE, skuDetails.getPrice());
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, skuDetails.getType());
        }
        bundle.putString(FontsContractCompat.Columns.RESULT_CODE, String.valueOf(code));
        bundle.putString("result_message", message);
        logEvent("purchase_failed", bundle);
    }

    public static void logEventPurchaseFailed(Purchase purchase, int code, String message) {
        Bundle bundle = new Bundle();
        if (purchase != null) {
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, purchase.getSku());
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, purchase.getPackageName());
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, purchase.getItemType());
        }
        bundle.putString(FontsContractCompat.Columns.RESULT_CODE, String.valueOf(code));
        bundle.putString("result_message", message);
        logEvent("purchase_failed", bundle);
    }

    public static void logEventClose(String productId) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, productId);
        logEvent("iab_close", bundle);
    }

    public static void logEventOpen(String productId) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, productId);
        logEvent("iab_open", bundle);
    }
}
