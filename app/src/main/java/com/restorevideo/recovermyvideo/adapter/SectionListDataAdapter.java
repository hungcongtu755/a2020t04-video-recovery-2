package com.restorevideo.recovermyvideo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.measurement.api.AppMeasurementSdk;
import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.ui.VideoActivity;
import com.restorevideo.recovermyvideo.model.VideoModel;
import com.restorevideo.recovermyvideo.ui.view.SquareImageView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {
    private ArrayList<VideoModel> itemsList;
    public Context mContext;
    int postion;
    int size;

    public SectionListDataAdapter(Context context, ArrayList<VideoModel> arrayList, int i) {
        this.itemsList = arrayList;
        this.mContext = context;
        this.postion = i;
        if (arrayList.size() >= 5) {
            this.size = 5;
        } else {
            this.size = arrayList.size();
        }
    }

    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new SingleItemRowHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_video, (ViewGroup) null));
    }

    public void onBindViewHolder(SingleItemRowHolder singleItemRowHolder, int i) {
        VideoModel videoModel = this.itemsList.get(i);
        try {
            RequestManager with = Glide.with(this.mContext);
            ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) with.load("file://" + videoModel.getPathPhoto()).diskCacheStrategy(DiskCacheStrategy.ALL)).priority(Priority.HIGH)).centerCrop()).error((int) R.drawable.ic_error)).into((ImageView) singleItemRowHolder.itemImage);
        } catch (Exception e) {
            Context context = this.mContext;
            Toast.makeText(context, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        singleItemRowHolder.itemImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(SectionListDataAdapter.this.mContext, VideoActivity.class);
                intent.putExtra(AppMeasurementSdk.ConditionalUserProperty.VALUE, SectionListDataAdapter.this.postion);
                SectionListDataAdapter.this.mContext.startActivity(intent);
            }
        });
    }

    public int getItemCount() {
        return this.size;
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
        protected SquareImageView itemImage;

        public SingleItemRowHolder(View view) {
            super(view);
            this.itemImage = (SquareImageView) view.findViewById(R.id.ivImage);
        }
    }
}
