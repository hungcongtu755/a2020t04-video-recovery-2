package com.restorevideo.recovermyvideo.task;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.TextView;

import com.restorevideo.recovermyvideo.R;
import com.restorevideo.recovermyvideo.model.VideoModel;
import com.restorevideo.recovermyvideo.ui.dialog.LoadingDialog;
import com.restorevideo.recovermyvideo.utils.MediaScanner;
import com.restorevideo.recovermyvideo.utils.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

public class RecoverVideoAsyncTask extends AsyncTask<String, Integer, String> {
    private final String TAG = getClass().getName();
    int count = 0;
    private ArrayList<VideoModel> listPhoto;
    private Context mContext;
    private OnRestoreListener onRestoreListener;
    private LoadingDialog progressDialog;
    TextView tvNumber;

    public interface OnRestoreListener {
        void onComplete();
    }

    public RecoverVideoAsyncTask(Context context, ArrayList<VideoModel> arrayList, OnRestoreListener onRestoreListener2) {
        this.mContext = context;
        this.listPhoto = arrayList;
        this.onRestoreListener = onRestoreListener2;
    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
        this.progressDialog = new LoadingDialog(this.mContext);
        this.progressDialog.setCancelable(false);
        this.progressDialog.show();
    }

    @Override
    public String doInBackground(String... strArr) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < this.listPhoto.size(); i++) {
            File file = new File(this.listPhoto.get(i).getPathPhoto());
            Context context = this.mContext;
            File file2 = new File(Utils.getPathSave(context, context.getString(R.string.restore_folder_path)));
            StringBuilder sb = new StringBuilder();
            Context context2 = this.mContext;
            sb.append(Utils.getPathSave(context2, context2.getString(R.string.restore_folder_path)));
            sb.append(File.separator);
            sb.append(getFileName(this.listPhoto.get(i).getPathPhoto()));
            File file3 = new File(sb.toString());
            try {
                if (!file3.exists()) {
                    file2.mkdirs();
                }
                copy(file, file3);
                if (Build.VERSION.SDK_INT >= 19) {
                    Intent intent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
                    intent.setData(Uri.fromFile(file3));
                    this.mContext.sendBroadcast(intent);
                }
                new MediaScanner(this.mContext, file3);
                this.count = i + 1;
                publishProgress(new Integer[]{Integer.valueOf(this.count)});
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
        }
        return null;
    }

    public void copy(File file, File file2) throws IOException {
        FileChannel channel = new FileInputStream(file).getChannel();
        FileChannel channel2 = new FileOutputStream(file2).getChannel();
        channel.transferTo(0, channel.size(), channel2);
        if (channel != null) {
            channel.close();
        }
        if (channel2 != null) {
            channel2.close();
        }
    }

    public String getFileName(String str) {
        String substring = str.substring(str.lastIndexOf("/") + 1);
        if (substring.endsWith(".3gp") || substring.endsWith(".mp4") || substring.endsWith(".mkv") || substring.endsWith(".flv")) {
            return substring;
        }
        return substring + ".mp4";
    }

    @Override
    public void onPostExecute(String str) {
        super.onPostExecute(str);
        try {
            if (this.progressDialog != null && this.progressDialog.isShowing()) {
                this.progressDialog.dismiss();
                this.progressDialog = null;
            }
        } catch (Exception unused) {
        }
        OnRestoreListener onRestoreListener2 = this.onRestoreListener;
        if (onRestoreListener2 != null) {
            onRestoreListener2.onComplete();
        }
    }

    @Override
    public void onProgressUpdate(Integer... numArr) {
        super.onProgressUpdate(numArr);
        this.tvNumber = (TextView) this.progressDialog.findViewById(R.id.tvNumber);
        this.tvNumber.setText(String.format(this.mContext.getResources().getString(R.string.restoring_number_format), new Object[]{numArr[0]}));
    }
}
