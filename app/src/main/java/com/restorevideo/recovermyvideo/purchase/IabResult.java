package com.restorevideo.recovermyvideo.purchase;

public class IabResult {
    String mMessage;
    int mResponse;

    public IabResult(int i, String str) {
        this.mResponse = i;
        if (str == null || str.trim().length() == 0) {
            this.mMessage = IabHelper.getResponseDesc(i);
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str);
        stringBuilder.append(" (response: ");
        stringBuilder.append(IabHelper.getResponseDesc(i));
        stringBuilder.append(")");
        this.mMessage = stringBuilder.toString();
    }

    public int getResponse() {
        return this.mResponse;
    }

    public String getMessage() {
        return this.mMessage;
    }

    public boolean isSuccess() {
        return this.mResponse == 0;
    }

    public boolean isFailure() {
        return isSuccess() ^ true;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("IabResult: ");
        stringBuilder.append(getMessage());
        return stringBuilder.toString();
    }
}
