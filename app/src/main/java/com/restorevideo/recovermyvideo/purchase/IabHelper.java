package com.restorevideo.recovermyvideo.purchase;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.android.vending.billing.IInAppBillingService.Stub;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IabHelper {
    public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
    public static final int BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = 5;
    public static final int BILLING_RESPONSE_RESULT_ERROR = 6;
    public static final int BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = 7;
    public static final int BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = 8;
    public static final int BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = 4;
    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    public static final int BILLING_RESPONSE_RESULT_SERVICE_UNAVAILABLE = 2;
    public static final int BILLING_RESPONSE_RESULT_USER_CANCELED = 1;
    public static final String GET_SKU_DETAILS_ITEM_LIST = "ITEM_ID_LIST";
    public static final String GET_SKU_DETAILS_ITEM_TYPE_LIST = "ITEM_TYPE_LIST";
    public static final int IABHELPER_BAD_RESPONSE = -1002;
    public static final int IABHELPER_ERROR_BASE = -1000;
    public static final int IABHELPER_INVALID_CONSUMPTION = -1010;
    public static final int IABHELPER_MISSING_TOKEN = -1007;
    public static final int IABHELPER_REMOTE_EXCEPTION = -1001;
    public static final int IABHELPER_SEND_INTENT_FAILED = -1004;
    public static final int IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE = -1009;
    public static final int IABHELPER_SUBSCRIPTION_UPDATE_NOT_AVAILABLE = -1011;
    public static final int IABHELPER_UNKNOWN_ERROR = -1008;
    public static final int IABHELPER_UNKNOWN_PURCHASE_RESPONSE = -1006;
    public static final int IABHELPER_USER_CANCELLED = -1005;
    public static final int IABHELPER_VERIFICATION_FAILED = -1003;
    public static final String INAPP_CONTINUATION_TOKEN = "INAPP_CONTINUATION_TOKEN";
    public static final String ITEM_TYPE_INAPP = "inapp";
    public static final String ITEM_TYPE_SUBS = "subs";
    public static final String RESPONSE_BUY_INTENT = "BUY_INTENT";
    public static final String RESPONSE_CODE = "RESPONSE_CODE";
    public static final String RESPONSE_GET_SKU_DETAILS_LIST = "DETAILS_LIST";
    public static final String RESPONSE_INAPP_ITEM_LIST = "INAPP_PURCHASE_ITEM_LIST";
    public static final String RESPONSE_INAPP_PURCHASE_DATA = "INAPP_PURCHASE_DATA";
    public static final String RESPONSE_INAPP_PURCHASE_DATA_LIST = "INAPP_PURCHASE_DATA_LIST";
    public static final String RESPONSE_INAPP_SIGNATURE = "INAPP_DATA_SIGNATURE";
    public static final String RESPONSE_INAPP_SIGNATURE_LIST = "INAPP_DATA_SIGNATURE_LIST";
    boolean mAsyncInProgress = false;
    private final Object mAsyncInProgressLock = new Object();
    String mAsyncOperation = "";
    Context mContext;
    boolean mDebugLog = false;
    String mDebugTag = "IabHelper";
    boolean mDisposeAfterAsync = false;
    boolean mDisposed = false;
    OnIabPurchaseFinishedListener mPurchaseListener;
    String mPurchasingItemType;
    int mRequestCode;
    IInAppBillingService mService;
    ServiceConnection mServiceConn;
    boolean mSetupDone = false;
    String mSignatureBase64 = null;
    boolean mSubscriptionUpdateSupported = false;
    boolean mSubscriptionsSupported = false;

    public static class IabAsyncInProgressException extends Exception {
        public IabAsyncInProgressException(String str) {
            super(str);
        }
    }

    public interface OnConsumeFinishedListener {
        void onConsumeFinished(Purchase purchase, IabResult iabResult);
    }

    public interface OnConsumeMultiFinishedListener {
        void onConsumeMultiFinished(List<Purchase> list, List<IabResult> list2);
    }

    public interface OnIabPurchaseFinishedListener {
        void onIabPurchaseFinished(IabResult iabResult, Purchase purchase);
    }

    public interface OnIabSetupFinishedListener {
        void onIabSetupFinished(IabResult iabResult);
    }

    public interface QueryInventoryFinishedListener {
        void onQueryInventoryFinished(IabResult iabResult, Inventory inventory);
    }

    public IabHelper(Context context, String str) {
        this.mContext = context.getApplicationContext();
        this.mSignatureBase64 = str;
        logDebug("IAB helper created.");
    }

    public void enableDebugLogging(boolean z, String str) {
        checkNotDisposed();
        this.mDebugLog = z;
        this.mDebugTag = str;
    }

    public void enableDebugLogging(boolean z) {
        checkNotDisposed();
        this.mDebugLog = z;
    }

    public void startSetup(final OnIabSetupFinishedListener onIabSetupFinishedListener) {
        checkNotDisposed();
        if (this.mSetupDone) {
            throw new IllegalStateException("IAB helper is already set up.");
        }
        logDebug("Starting in-app billing setup.");
        this.mServiceConn = new ServiceConnection() {
            public void onServiceDisconnected(ComponentName componentName) {
                IabHelper.this.logDebug("Billing service disconnected.");
                IabHelper.this.mService = null;
            }

            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                if (!IabHelper.this.mDisposed) {
                    IabHelper.this.logDebug("Billing service connected.");
                    IabHelper.this.mService = Stub.asInterface(iBinder);
                    String packageName = IabHelper.this.mContext.getPackageName();
                    try {
                        IabHelper.this.logDebug("Checking for in-app billing 3 support.");
                        int isBillingSupported = IabHelper.this.mService.isBillingSupported(3, packageName, "inapp");
                        if (isBillingSupported != 0) {
                            if (onIabSetupFinishedListener != null) {
                                onIabSetupFinishedListener.onIabSetupFinished(new IabResult(isBillingSupported, "Error checking for billing v3 support."));
                            }
                            IabHelper.this.mSubscriptionsSupported = false;
                            IabHelper.this.mSubscriptionUpdateSupported = false;
                            return;
                        }
                        IabHelper iabHelper = IabHelper.this;
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("In-app billing version 3 supported for ");
                        stringBuilder.append(packageName);
                        iabHelper.logDebug(stringBuilder.toString());
                        if (IabHelper.this.mService.isBillingSupported(5, packageName, "subs") == 0) {
                            IabHelper.this.logDebug("Subscription re-signup AVAILABLE.");
                            IabHelper.this.mSubscriptionUpdateSupported = true;
                        } else {
                            IabHelper.this.logDebug("Subscription re-signup not available.");
                            IabHelper.this.mSubscriptionUpdateSupported = false;
                        }
                        if (IabHelper.this.mSubscriptionUpdateSupported) {
                            IabHelper.this.mSubscriptionsSupported = true;
                        } else {
                            int isBillingSupported2 = IabHelper.this.mService.isBillingSupported(3, packageName, "subs");
                            if (isBillingSupported2 == 0) {
                                IabHelper.this.logDebug("Subscriptions AVAILABLE.");
                                IabHelper.this.mSubscriptionsSupported = true;
                            } else {
                                iabHelper = IabHelper.this;
                                StringBuilder stringBuilder2 = new StringBuilder();
                                stringBuilder2.append("Subscriptions NOT AVAILABLE. Response: ");
                                stringBuilder2.append(isBillingSupported2);
                                iabHelper.logDebug(stringBuilder2.toString());
                                IabHelper.this.mSubscriptionsSupported = false;
                                IabHelper.this.mSubscriptionUpdateSupported = false;
                            }
                        }
                        IabHelper.this.mSetupDone = true;
                        if (onIabSetupFinishedListener != null) {
                            onIabSetupFinishedListener.onIabSetupFinished(new IabResult(0, "Setup successful."));
                        }
                    } catch (RemoteException e) {
                        if (onIabSetupFinishedListener != null) {
                            onIabSetupFinishedListener.onIabSetupFinished(new IabResult(IabHelper.IABHELPER_REMOTE_EXCEPTION, "RemoteException while setting up in-app billing."));
                        }
                        e.printStackTrace();
                    }
                }
            }
        };
        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        intent.setPackage("com.android.vending");
        List queryIntentServices = this.mContext.getPackageManager().queryIntentServices(intent, 0);
        if (queryIntentServices != null && !queryIntentServices.isEmpty()) {
            this.mContext.bindService(intent, this.mServiceConn, (int) 1);
        } else if (onIabSetupFinishedListener != null) {
            onIabSetupFinishedListener.onIabSetupFinished(new IabResult(3, "Billing service unavailable on device."));
        }
    }

    public void dispose() throws IabAsyncInProgressException {
        synchronized (this.mAsyncInProgressLock) {
            if (this.mAsyncInProgress) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Can't dispose because an async operation (");
                stringBuilder.append(this.mAsyncOperation);
                stringBuilder.append(") is in progress.");
                throw new IabAsyncInProgressException(stringBuilder.toString());
            }
        }
        logDebug("Disposing.");
        this.mSetupDone = false;
        if (this.mServiceConn != null) {
            logDebug("Unbinding from service.");
            if (this.mContext != null) {
                this.mContext.unbindService(this.mServiceConn);
            }
        }
        this.mDisposed = true;
        this.mContext = null;
        this.mServiceConn = null;
        this.mService = null;
        this.mPurchaseListener = null;
    }

    public void disposeWhenFinished() {
        synchronized (this.mAsyncInProgressLock) {
            if (this.mAsyncInProgress) {
                logDebug("Will dispose after async operation finishes.");
                this.mDisposeAfterAsync = true;
            } else {
//                dispose();
            }
        }
    }

    private void checkNotDisposed() {
        if (this.mDisposed) {
            throw new IllegalStateException("IabHelper was disposed of, so it cannot be used.");
        }
    }

    public boolean subscriptionsSupported() {
        checkNotDisposed();
        return this.mSubscriptionsSupported;
    }

    public void launchPurchaseFlow(Activity activity, String str, int i, OnIabPurchaseFinishedListener onIabPurchaseFinishedListener) throws IabAsyncInProgressException {
        launchPurchaseFlow(activity, str, i, onIabPurchaseFinishedListener, "");
    }

    public void launchPurchaseFlow(Activity activity, String str, int i, OnIabPurchaseFinishedListener onIabPurchaseFinishedListener, String str2) throws IabAsyncInProgressException {
        launchPurchaseFlow(activity, str, "inapp", null, i, onIabPurchaseFinishedListener, str2);
    }

    public void launchSubscriptionPurchaseFlow(Activity activity, String str, int i, OnIabPurchaseFinishedListener onIabPurchaseFinishedListener) throws IabAsyncInProgressException {
        launchSubscriptionPurchaseFlow(activity, str, i, onIabPurchaseFinishedListener, "");
    }

    public void launchSubscriptionPurchaseFlow(Activity activity, String str, int i, OnIabPurchaseFinishedListener onIabPurchaseFinishedListener, String str2) throws IabAsyncInProgressException {
        launchPurchaseFlow(activity, str, "subs", null, i, onIabPurchaseFinishedListener, str2);
    }

    public void launchPurchaseFlow(Activity activity, String str, String str2, List<String> list, int i, OnIabPurchaseFinishedListener onIabPurchaseFinishedListener, String str3) throws IabAsyncInProgressException {
        String str4 = str;
        String str5 = str2;
        int i2 = i;
        OnIabPurchaseFinishedListener onIabPurchaseFinishedListener2 = onIabPurchaseFinishedListener;
        checkNotDisposed();
        checkSetupDone("launchPurchaseFlow");
        flagStartAsync("launchPurchaseFlow");
        IabResult iabResult;
        if (!str5.equals("subs") || this.mSubscriptionsSupported) {
            StringBuilder stringBuilder;
            try {
                Bundle buyIntent;
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append("Constructing buy intent for ");
                stringBuilder2.append(str4);
                stringBuilder2.append(", item type: ");
                stringBuilder2.append(str5);
                logDebug(stringBuilder2.toString());
                if (list == null || list.isEmpty()) {
                    buyIntent = this.mService.getBuyIntent(3, this.mContext.getPackageName(), str4, str5, str3);
                } else if (this.mSubscriptionUpdateSupported) {
                    buyIntent = this.mService.getBuyIntentToReplaceSkus(5, this.mContext.getPackageName(), list, str4, str5, str3);
                } else {
                    iabResult = new IabResult(IABHELPER_SUBSCRIPTION_UPDATE_NOT_AVAILABLE, "Subscription updates are not available.");
                    flagEndAsync();
                    if (onIabPurchaseFinishedListener2 != null) {
                        onIabPurchaseFinishedListener2.onIabPurchaseFinished(iabResult, null);
                    }
                    return;
                }
                int responseCodeFromBundle = getResponseCodeFromBundle(buyIntent);
                if (responseCodeFromBundle != 0) {
                    stringBuilder2 = new StringBuilder();
                    stringBuilder2.append("Unable to buy item, Error response: ");
                    stringBuilder2.append(getResponseDesc(responseCodeFromBundle));
                    logError(stringBuilder2.toString());
                    flagEndAsync();
                    iabResult = new IabResult(responseCodeFromBundle, "Unable to buy item");
                    if (onIabPurchaseFinishedListener2 != null) {
                        onIabPurchaseFinishedListener2.onIabPurchaseFinished(iabResult, null);
                    }
                    return;
                }
                PendingIntent pendingIntent = (PendingIntent) buyIntent.getParcelable("BUY_INTENT");
                stringBuilder = new StringBuilder();
                stringBuilder.append("Launching buy intent for ");
                stringBuilder.append(str4);
                stringBuilder.append(". Request code: ");
                stringBuilder.append(i2);
                logDebug(stringBuilder.toString());
                this.mRequestCode = i2;
                this.mPurchaseListener = onIabPurchaseFinishedListener2;
                this.mPurchasingItemType = str5;
                activity.startIntentSenderForResult(pendingIntent.getIntentSender(), i2, new Intent(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue());
            } catch (SendIntentException e) {
                SendIntentException sendIntentException = e;
                stringBuilder = new StringBuilder();
                stringBuilder.append("SendIntentException while launching purchase flow for sku ");
                stringBuilder.append(str4);
                logError(stringBuilder.toString());
                sendIntentException.printStackTrace();
                flagEndAsync();
                iabResult = new IabResult(IABHELPER_SEND_INTENT_FAILED, "Failed to send intent.");
                if (onIabPurchaseFinishedListener2 != null) {
                    onIabPurchaseFinishedListener2.onIabPurchaseFinished(iabResult, null);
                }
            } catch (RemoteException e2) {
                RemoteException remoteException = e2;
                stringBuilder = new StringBuilder();
                stringBuilder.append("RemoteException while launching purchase flow for sku ");
                stringBuilder.append(str4);
                logError(stringBuilder.toString());
                remoteException.printStackTrace();
                flagEndAsync();
                iabResult = new IabResult(IABHELPER_REMOTE_EXCEPTION, "Remote exception while starting purchase flow");
                if (onIabPurchaseFinishedListener2 != null) {
                    onIabPurchaseFinishedListener2.onIabPurchaseFinished(iabResult, null);
                }
            }
        } else {
            iabResult = new IabResult(IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE, "Subscriptions are not available.");
            flagEndAsync();
            if (onIabPurchaseFinishedListener2 != null) {
                onIabPurchaseFinishedListener2.onIabPurchaseFinished(iabResult, null);
            }
        }
    }

    public boolean handleActivityResult(int i, int i2, Intent intent) {
        if (i != this.mRequestCode) {
            return false;
        }
        checkNotDisposed();
        checkSetupDone("handleActivityResult");
        flagEndAsync();
        IabResult iabResult;
        if (intent == null) {
            logError("Null data in IAB activity result.");
            iabResult = new IabResult(IABHELPER_BAD_RESPONSE, "Null data in IAB result");
            if (this.mPurchaseListener != null) {
                this.mPurchaseListener.onIabPurchaseFinished(iabResult, null);
            }
            return true;
        }
        int responseCodeFromIntent = getResponseCodeFromIntent(intent);
        String stringExtra = intent.getStringExtra(RESPONSE_INAPP_PURCHASE_DATA);
        String stringExtra2 = intent.getStringExtra(RESPONSE_INAPP_SIGNATURE);
        StringBuilder stringBuilder;
        IabResult iabResult2;
        if (i2 == -1 && responseCodeFromIntent == 0) {
            logDebug("Successful resultcode from purchase activity.");
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Purchase data: ");
            stringBuilder2.append(stringExtra);
            logDebug(stringBuilder2.toString());
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Data signature: ");
            stringBuilder2.append(stringExtra2);
            logDebug(stringBuilder2.toString());
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Extras: ");
            stringBuilder2.append(intent.getExtras());
            logDebug(stringBuilder2.toString());
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Expected item type: ");
            stringBuilder2.append(this.mPurchasingItemType);
            logDebug(stringBuilder2.toString());
            if (stringExtra == null || stringExtra2 == null) {
                logError("BUG: either purchaseData or dataSignature is null.");
                stringBuilder = new StringBuilder();
                stringBuilder.append("Extras: ");
                stringBuilder.append(intent.getExtras().toString());
                logDebug(stringBuilder.toString());
                iabResult2 = new IabResult(IABHELPER_UNKNOWN_ERROR, "IAB returned null purchaseData or dataSignature");
                if (this.mPurchaseListener != null) {
                    this.mPurchaseListener.onIabPurchaseFinished(iabResult2, null);
                }
                return true;
            }
            try {
                Purchase purchase = new Purchase(this.mPurchasingItemType, stringExtra, stringExtra2);
                String sku = purchase.getSku();
                if (Security.verifyPurchase(this.mSignatureBase64, stringExtra, stringExtra2)) {
                    logDebug("Purchase signature successfully verified.");
                    if (this.mPurchaseListener != null) {
                        this.mPurchaseListener.onIabPurchaseFinished(new IabResult(0, "Success"), purchase);
                    }
                } else {
                    StringBuilder stringBuilder3 = new StringBuilder();
                    stringBuilder3.append("Purchase signature verification FAILED for sku ");
                    stringBuilder3.append(sku);
                    logError(stringBuilder3.toString());
                    StringBuilder stringBuilder4 = new StringBuilder();
                    stringBuilder4.append("Signature verification failed for sku ");
                    stringBuilder4.append(sku);
                    IabResult iabResult3 = new IabResult(IABHELPER_VERIFICATION_FAILED, stringBuilder4.toString());
                    if (this.mPurchaseListener != null) {
                        this.mPurchaseListener.onIabPurchaseFinished(iabResult3, purchase);
                    }
                    return true;
                }
            } catch (JSONException e) {
                logError("Failed to parse purchase data.");
                e.printStackTrace();
                iabResult = new IabResult(IABHELPER_BAD_RESPONSE, "Failed to parse purchase data.");
                if (this.mPurchaseListener != null) {
                    this.mPurchaseListener.onIabPurchaseFinished(iabResult, null);
                }
                return true;
            }
        } else if (i2 == -1) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Result code was OK but in-app billing response was not OK: ");
            stringBuilder.append(getResponseDesc(responseCodeFromIntent));
            logDebug(stringBuilder.toString());
            if (this.mPurchaseListener != null) {
                this.mPurchaseListener.onIabPurchaseFinished(new IabResult(responseCodeFromIntent, "Problem purchashing item."), null);
            }
        } else if (i2 == 0) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Purchase canceled - Response: ");
            stringBuilder.append(getResponseDesc(responseCodeFromIntent));
            logDebug(stringBuilder.toString());
            iabResult2 = new IabResult(IABHELPER_USER_CANCELLED, "User canceled.");
            if (this.mPurchaseListener != null) {
                this.mPurchaseListener.onIabPurchaseFinished(iabResult2, null);
            }
        } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Purchase failed. Result code: ");
            stringBuilder.append(Integer.toString(i2));
            stringBuilder.append(". Response: ");
            stringBuilder.append(getResponseDesc(responseCodeFromIntent));
            logError(stringBuilder.toString());
            iabResult2 = new IabResult(IABHELPER_UNKNOWN_PURCHASE_RESPONSE, "Unknown purchase response.");
            if (this.mPurchaseListener != null) {
                this.mPurchaseListener.onIabPurchaseFinished(iabResult2, null);
            }
        }
        return true;
    }

    public Inventory queryInventory() throws IabException {
        return queryInventory(false, null, null);
    }

    public Inventory queryInventory(boolean z, List<String> list, List<String> list2) throws IabException {
        checkNotDisposed();
        checkSetupDone("queryInventory");
        try {
            Inventory inventory = new Inventory();
            int queryPurchases = queryPurchases(inventory, "inapp");
            if (queryPurchases != 0) {
                throw new IabException(queryPurchases, "Error refreshing inventory (querying owned items).");
            }
            int querySkuDetails;
            if (z) {
                querySkuDetails = querySkuDetails("inapp", inventory, list);
                if (querySkuDetails != 0) {
                    throw new IabException(querySkuDetails, "Error refreshing inventory (querying prices of items).");
                }
            }
            if (this.mSubscriptionsSupported) {
                querySkuDetails = queryPurchases(inventory, "subs");
                if (querySkuDetails != 0) {
                    throw new IabException(querySkuDetails, "Error refreshing inventory (querying owned subscriptions).");
                } else if (z) {
                    int querySkuDetails2 = querySkuDetails("subs", inventory, list2);
                    if (querySkuDetails2 != 0) {
                        throw new IabException(querySkuDetails2, "Error refreshing inventory (querying prices of subscriptions).");
                    }
                }
            }
            return inventory;
        } catch (RemoteException e) {
            throw new IabException(IABHELPER_REMOTE_EXCEPTION, "Remote exception while refreshing inventory.", e);
        } catch (Exception e2) {
            throw new IabException(IABHELPER_BAD_RESPONSE, "Error parsing JSON response while refreshing inventory.", e2);
        }
    }

    IabResult iabResult;
    Inventory queryInventory;

    public void queryInventoryAsync(boolean z, List<String> list, List<String> list2, QueryInventoryFinishedListener queryInventoryFinishedListener) throws IabAsyncInProgressException {
        final Handler handler = new Handler();
        checkNotDisposed();
        checkSetupDone("queryInventory");
        flagStartAsync("refresh inventory");
        final boolean z2 = z;
        final List<String> list3 = list;
        final List<String> list4 = list2;
        final QueryInventoryFinishedListener queryInventoryFinishedListener2 = queryInventoryFinishedListener;

        new Thread(new Runnable() {
            public void run() {
                iabResult = new IabResult(0, "Inventory refresh successful.");
                try {
                    queryInventory = IabHelper.this.queryInventory(z2, list3, list4);
                } catch (IabException e) {
                    iabResult = e.getResult();
                    queryInventory = null;
                }
                IabHelper.this.flagEndAsync();
                if (!IabHelper.this.mDisposed && queryInventoryFinishedListener2 != null) {
                    handler.post(new Runnable() {
                        public void run() {
                            queryInventoryFinishedListener2.onQueryInventoryFinished(iabResult, queryInventory);
                        }
                    });
                }
            }
        }).start();
    }

    public void queryInventoryAsync(QueryInventoryFinishedListener queryInventoryFinishedListener) throws IabAsyncInProgressException {
        queryInventoryAsync(false, null, null, queryInventoryFinishedListener);
    }

    void consume(Purchase purchase) throws IabException {
        checkNotDisposed();
        checkSetupDone("consume");
        StringBuilder stringBuilder;
        if (purchase.mItemType.equals("inapp")) {
            StringBuilder stringBuilder2;
            try {
                String token = purchase.getToken();
                String sku = purchase.getSku();
                StringBuilder stringBuilder3;
                if (token == null || token.equals("")) {
                    stringBuilder3 = new StringBuilder();
                    stringBuilder3.append("Can't consume ");
                    stringBuilder3.append(sku);
                    stringBuilder3.append(". No token.");
                    logError(stringBuilder3.toString());
                    stringBuilder2 = new StringBuilder();
                    stringBuilder2.append("PurchaseInfo is missing token for sku: ");
                    stringBuilder2.append(sku);
                    stringBuilder2.append(" ");
                    stringBuilder2.append(purchase);
                    throw new IabException((int) IABHELPER_MISSING_TOKEN, stringBuilder2.toString());
                }
                stringBuilder = new StringBuilder();
                stringBuilder.append("Consuming sku: ");
                stringBuilder.append(sku);
                stringBuilder.append(", token: ");
                stringBuilder.append(token);
                logDebug(stringBuilder.toString());
                int consumePurchase = this.mService.consumePurchase(3, this.mContext.getPackageName(), token);
                if (consumePurchase == 0) {
                    stringBuilder3 = new StringBuilder();
                    stringBuilder3.append("Successfully consumed sku: ");
                    stringBuilder3.append(sku);
                    logDebug(stringBuilder3.toString());
                    return;
                }
                stringBuilder = new StringBuilder();
                stringBuilder.append("Error consuming consuming sku ");
                stringBuilder.append(sku);
                stringBuilder.append(". ");
                stringBuilder.append(getResponseDesc(consumePurchase));
                logDebug(stringBuilder.toString());
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("Error consuming sku ");
                stringBuilder2.append(sku);
                throw new IabException(consumePurchase, stringBuilder2.toString());
            } catch (Exception e) {
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("Remote exception while consuming. PurchaseInfo: ");
                stringBuilder2.append(purchase);
                throw new IabException(IABHELPER_REMOTE_EXCEPTION, stringBuilder2.toString(), e);
            }
        }
        stringBuilder = new StringBuilder();
        stringBuilder.append("Items of type '");
        stringBuilder.append(purchase.mItemType);
        stringBuilder.append("' can't be consumed.");
        throw new IabException((int) IABHELPER_INVALID_CONSUMPTION, stringBuilder.toString());
    }

    public void consumeAsync(Purchase purchase, OnConsumeFinishedListener onConsumeFinishedListener) throws IabAsyncInProgressException {
        checkNotDisposed();
        checkSetupDone("consume");
        List arrayList = new ArrayList();
        arrayList.add(purchase);
        consumeAsyncInternal(arrayList, onConsumeFinishedListener, null);
    }

    public void consumeAsync(List<Purchase> list, OnConsumeMultiFinishedListener onConsumeMultiFinishedListener) throws IabAsyncInProgressException {
        checkNotDisposed();
        checkSetupDone("consume");
        consumeAsyncInternal(list, null, onConsumeMultiFinishedListener);
    }

    public static String getResponseDesc(int i) {
        String[] split = "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned".split("/");
        String[] split2 = "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt".split("/");
        StringBuilder stringBuilder;
        if (i <= -1000) {
            int i2 = -1000 - i;
            if (i2 >= 0 && i2 < split2.length) {
                return split2[i2];
            }
            stringBuilder = new StringBuilder();
            stringBuilder.append(String.valueOf(i));
            stringBuilder.append(":Unknown IAB Helper Error");
            return stringBuilder.toString();
        } else if (i >= 0 && i < split.length) {
            return split[i];
        } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append(String.valueOf(i));
            stringBuilder.append(":Unknown");
            return stringBuilder.toString();
        }
    }

    void checkSetupDone(String str) {
        if (!this.mSetupDone) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Illegal state for operation (");
            stringBuilder.append(str);
            stringBuilder.append("): IAB helper is not set up.");
            logError(stringBuilder.toString());
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("IAB helper is not set up. Can't perform operation: ");
            stringBuilder2.append(str);
            throw new IllegalStateException(stringBuilder2.toString());
        }
    }

    int getResponseCodeFromBundle(Bundle bundle) {
        Object obj = bundle.get("RESPONSE_CODE");
        if (obj == null) {
            logDebug("Bundle with null response code, assuming OK (known issue)");
            return 0;
        } else if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        } else {
            if (obj instanceof Long) {
                return (int) ((Long) obj).longValue();
            }
            logError("Unexpected type for bundle response code.");
            logError(obj.getClass().getName());
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected type for bundle response code: ");
            stringBuilder.append(obj.getClass().getName());
            throw new RuntimeException(stringBuilder.toString());
        }
    }

    int getResponseCodeFromIntent(Intent intent) {
        Object obj = intent.getExtras().get("RESPONSE_CODE");
        if (obj == null) {
            logError("Intent with no response code, assuming OK (known issue)");
            return 0;
        } else if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        } else {
            if (obj instanceof Long) {
                return (int) ((Long) obj).longValue();
            }
            logError("Unexpected type for intent response code.");
            logError(obj.getClass().getName());
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected type for intent response code: ");
            stringBuilder.append(obj.getClass().getName());
            throw new RuntimeException(stringBuilder.toString());
        }
    }

    void flagStartAsync(String str) throws IabAsyncInProgressException {
        synchronized (this.mAsyncInProgressLock) {
            if (this.mAsyncInProgress) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Can't start async operation (");
                stringBuilder.append(str);
                stringBuilder.append(") because another async operation (");
                stringBuilder.append(this.mAsyncOperation);
                stringBuilder.append(") is in progress.");
                throw new IabAsyncInProgressException(stringBuilder.toString());
            }
            this.mAsyncOperation = str;
            this.mAsyncInProgress = true;
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Starting async operation: ");
            stringBuilder2.append(str);
            logDebug(stringBuilder2.toString());
        }
    }

    void flagEndAsync() {
        synchronized (this.mAsyncInProgressLock) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Ending async operation: ");
            stringBuilder.append(this.mAsyncOperation);
            logDebug(stringBuilder.toString());
            this.mAsyncOperation = "";
            this.mAsyncInProgress = false;
            if (this.mDisposeAfterAsync) {
//                dispose();
            }
//            try {
//            } catch (IabAsyncInProgressException unused) {
//            }
        }
    }

    int queryPurchases(Inventory inventory, String str) throws JSONException, RemoteException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Querying owned items, item type: ");
        stringBuilder.append(str);
        logDebug(stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("Package name: ");
        stringBuilder.append(this.mContext.getPackageName());
        logDebug(stringBuilder.toString());
        int i = 0;
        String str2 = null;
        int i2 = 0;
        while (true) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("Calling getPurchases with continuation token: ");
            stringBuilder2.append(str2);
            logDebug(stringBuilder2.toString());
            Bundle purchases = this.mService.getPurchases(3, this.mContext.getPackageName(), str, str2);
            int responseCodeFromBundle = getResponseCodeFromBundle(purchases);
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("Owned items response: ");
            stringBuilder3.append(String.valueOf(responseCodeFromBundle));
            logDebug(stringBuilder3.toString());
            if (responseCodeFromBundle != 0) {
                StringBuilder stringBuilder4 = new StringBuilder();
                stringBuilder4.append("getPurchases() failed: ");
                stringBuilder4.append(getResponseDesc(responseCodeFromBundle));
                logDebug(stringBuilder4.toString());
                return responseCodeFromBundle;
            } else if (purchases.containsKey("INAPP_PURCHASE_ITEM_LIST") && purchases.containsKey("INAPP_PURCHASE_DATA_LIST") && purchases.containsKey("INAPP_DATA_SIGNATURE_LIST")) {
                ArrayList stringArrayList = purchases.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList stringArrayList2 = purchases.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList stringArrayList3 = purchases.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                int i3 = i2;
                for (i2 = 0; i2 < stringArrayList2.size(); i2++) {
                    String str3 = (String) stringArrayList2.get(i2);
                    String str4 = (String) stringArrayList3.get(i2);
                    String str5 = (String) stringArrayList.get(i2);
                    if (Security.verifyPurchase(this.mSignatureBase64, str3, str4)) {
                        StringBuilder stringBuilder5 = new StringBuilder();
                        stringBuilder5.append("Sku is owned: ");
                        stringBuilder5.append(str5);
                        logDebug(stringBuilder5.toString());
                        Purchase purchase = new Purchase(str, str3, str4);
                        if (TextUtils.isEmpty(purchase.getToken())) {
                            logWarn("BUG: empty/null token!");
                            StringBuilder stringBuilder6 = new StringBuilder();
                            stringBuilder6.append("Purchase data: ");
                            stringBuilder6.append(str3);
                            logDebug(stringBuilder6.toString());
                        }
                        inventory.addPurchase(purchase);
                    } else {
                        logWarn("Purchase signature verification **FAILED**. Not adding item.");
                        StringBuilder stringBuilder7 = new StringBuilder();
                        stringBuilder7.append("   Purchase data: ");
                        stringBuilder7.append(str3);
                        logDebug(stringBuilder7.toString());
                        stringBuilder7 = new StringBuilder();
                        stringBuilder7.append("   Signature: ");
                        stringBuilder7.append(str4);
                        logDebug(stringBuilder7.toString());
                        i3 = 1;
                    }
                }
                str2 = purchases.getString("INAPP_CONTINUATION_TOKEN");
                StringBuilder stringBuilder8 = new StringBuilder();
                stringBuilder8.append("Continuation token: ");
                stringBuilder8.append(str2);
                logDebug(stringBuilder8.toString());
                if (TextUtils.isEmpty(str2)) {
                    if (i3 != 0) {
                        i = IABHELPER_VERIFICATION_FAILED;
                    }
                    return i;
                }
                i2 = i3;
            } else {
                logError("Bundle returned from getPurchases() doesn't contain required fields.");
            }
        }
//        logError("Bundle returned from getPurchases() doesn't contain required fields.");
//        return IABHELPER_BAD_RESPONSE;
//        return 0;
    }

    int querySkuDetails(String str, Inventory inventory, List<String> list) throws RemoteException, JSONException {
        Iterator it;
        logDebug("Querying SKU details.");
        List<String> arrayList = new ArrayList<String>();
        arrayList.addAll(inventory.getAllOwnedSkus(str));
        if (list != null) {
            for (String str2 : list) {
                if (!arrayList.contains(str2)) {
                    arrayList.add(str2);
                }
            }
        }
        if (arrayList.size() == 0) {
            logDebug("queryPrices: nothing to do because there are no SKUs.");
            return 0;
        }
        Iterator it2;
        List<String> arrayList2 = new ArrayList<String>();
        int size = arrayList.size() / 20;
        int size2 = arrayList.size() % 20;
        for (int i = 0; i < size; i++) {
            List<String> arrayList3 = new ArrayList<String>();
            int i2 = i * 20;
            for (String add : arrayList.subList(i2, i2 + 20)) {
                arrayList3.add(add);
            }
            for (String kj : arrayList3)
                arrayList2.add(kj);
        }
        if (size2 != 0) {
            List<String> arrayList4 = new ArrayList<String>();
            size *= 20;
            for (String add2 : arrayList.subList(size, size2 + size)) {
                arrayList4.add(add2);
            }
            for (String i : arrayList4) {
                arrayList2.add(i);
            }
        }
        it = arrayList2.iterator();
        while (it.hasNext()) {
//            List<String> arrayList = (ArrayList<String>) it.next();
            Bundle bundle = new Bundle();
//            bundle.putStringArrayList(GET_SKU_DETAILS_ITEM_LIST, arrayList);
            Bundle skuDetails = this.mService.getSkuDetails(3, this.mContext.getPackageName(), str, bundle);
            if (skuDetails.containsKey("DETAILS_LIST")) {
                it2 = skuDetails.getStringArrayList("DETAILS_LIST").iterator();
                while (it2.hasNext()) {
                    SkuDetails skuDetails2 = new SkuDetails(str, (String) it2.next());
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Got sku details: ");
                    stringBuilder.append(skuDetails2);
                    logDebug(stringBuilder.toString());
                    inventory.addSkuDetails(skuDetails2);
                }
            } else {
                int responseCodeFromBundle = getResponseCodeFromBundle(skuDetails);
                if (responseCodeFromBundle != 0) {
                    StringBuilder stringBuilder2 = new StringBuilder();
                    stringBuilder2.append("getSkuDetails() failed: ");
                    stringBuilder2.append(getResponseDesc(responseCodeFromBundle));
                    logDebug(stringBuilder2.toString());
                    return responseCodeFromBundle;
                }
                logError("getSkuDetails() returned a bundle with neither an error nor a detail list.");
                return IABHELPER_BAD_RESPONSE;
            }
        }
        return 0;
    }

    void consumeAsyncInternal(List<Purchase> list, OnConsumeFinishedListener onConsumeFinishedListener, OnConsumeMultiFinishedListener onConsumeMultiFinishedListener) throws IabAsyncInProgressException {
        final Handler handler = new Handler();
        flagStartAsync("consume");
        final List<Purchase> list2 = list;
        final OnConsumeFinishedListener onConsumeFinishedListener2 = onConsumeFinishedListener;
        final OnConsumeMultiFinishedListener onConsumeMultiFinishedListener2 = onConsumeMultiFinishedListener;
        new Thread(new Runnable() {
            public void run() {
                final List arrayList = new ArrayList();
                for (Purchase purchase : list2) {
                    try {
                        IabHelper.this.consume(purchase);
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("Successful consume of sku ");
                        stringBuilder.append(purchase.getSku());
                        arrayList.add(new IabResult(0, stringBuilder.toString()));
                    } catch (IabException e) {
                        arrayList.add(e.getResult());
                    }
                }
                IabHelper.this.flagEndAsync();
                if (!(IabHelper.this.mDisposed || onConsumeFinishedListener2 == null)) {
                    handler.post(new Runnable() {
                        public void run() {
                            onConsumeFinishedListener2.onConsumeFinished((Purchase) list2.get(0), (IabResult) arrayList.get(0));
                        }
                    });
                }
                if (!IabHelper.this.mDisposed && onConsumeMultiFinishedListener2 != null) {
                    handler.post(new Runnable() {
                        public void run() {
                            onConsumeMultiFinishedListener2.onConsumeMultiFinished(list2, arrayList);
                        }
                    });
                }
            }
        }).start();
    }

    void logDebug(String str) {
        if (this.mDebugLog) {
            Log.d(this.mDebugTag, str);
        }
    }

    void logError(String str) {
        String str2 = this.mDebugTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("In-app billing error: ");
        stringBuilder.append(str);
        Log.e(str2, stringBuilder.toString());
    }

    void logWarn(String str) {
        String str2 = this.mDebugTag;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("In-app billing warning: ");
        stringBuilder.append(str);
        Log.w(str2, stringBuilder.toString());
    }
}
