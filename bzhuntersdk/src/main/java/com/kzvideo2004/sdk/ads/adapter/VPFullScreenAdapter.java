package com.kzvideo2004.sdk.ads.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bzvideo2004.sdk.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;
import com.kzvideo2004.sdk.ads.model.CampaignAds;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

public class VPFullScreenAdapter extends PagerAdapter {
    public Context context;
    ArrayList<CampaignAds> listAds = new ArrayList<>();
    AdClickListener mAdClickListener;

    public interface AdClickListener {
        void onAdClicked();
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public VPFullScreenAdapter(Context context2, ArrayList<CampaignAds> arrayList, AdClickListener adClickListener) {
        this.context = context2;
        this.listAds = arrayList;
        this.mAdClickListener = adClickListener;
    }

    public int getCount() {
        return this.listAds.size();
    }

    public Object instantiateItem(ViewGroup viewGroup, final int i) {
        View inflate = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_kz_ads_top, (ViewGroup) null);
        ((Button) inflate.findViewById(R.id.btnOpenLink)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (VPFullScreenAdapter.this.mAdClickListener != null) {
                    VPFullScreenAdapter.this.mAdClickListener.onAdClicked();
                }
                UtilsCombat.OpenBrower(VPFullScreenAdapter.this.context, VPFullScreenAdapter.this.listAds.get(i).getsLink());
            }
        });
        ((RatingBar) inflate.findViewById(R.id.rbRating)).setRating(this.listAds.get(i).getRating());
        ((TextView) inflate.findViewById(R.id.tvTitleAds)).setText(this.listAds.get(i).getsTitle());
        ((TextView) inflate.findViewById(R.id.tvAdsDes)).setText(this.listAds.get(i).getsDecrition());
        final LinearLayout linearLayout = (LinearLayout) inflate.findViewById(R.id.lrContainer);
        linearLayout.setVisibility(View.INVISIBLE);
        final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) inflate.findViewById(R.id.shimmer_container);
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmer();
        Glide.with(this.context).load(this.listAds.get(i).getsIcon()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                return false;
            }
        }).into((ImageView) inflate.findViewById(R.id.ivIconAds));
        Glide.with(this.context).load(this.listAds.get(i).getsImage()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout.setVisibility(View.VISIBLE);
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout.setVisibility(View.VISIBLE);
                return false;
            }
        }).into((ImageView) inflate.findViewById(R.id.ivAdsBig));
        viewGroup.addView(inflate);
        return inflate;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((LinearLayout) obj);
    }
}
