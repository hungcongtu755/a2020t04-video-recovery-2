package com.kzvideo2004.sdk.ads.funtion;

import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.kzvideo2004.sdk.ads.model.CONTAIN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Random;

public class UtilsCombat {
    public static void ShowToastShort(Context context, String str) {
        Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
    }

    public static void ShowToastLong(Context context, String str) {
        Toast.makeText(context, str, Toast.LENGTH_LONG).show();
    }

    public static void OpenMoreApp(Context context, String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("market://search?q=pub:" + str));
        context.startActivity(intent);
    }

    public static void OpenBrower(Context context, String str) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(str)));
        } catch (Exception unused) {
        }
    }

    public static boolean isPackageInstalled(String str, PackageManager packageManager) {
        try {
            return packageManager.getApplicationInfo(str, 0).enabled;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    public static void RateApp(Context context) {
        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + context.getPackageName())));
    }

    public static void SendFeedBack(Context context, String str, String str2) {
        String[] strArr = {str};
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setData(Uri.parse("mailto:"));
        intent.setType("message/rfc822");
        intent.putExtra("android.intent.extra.EMAIL", strArr);
        intent.putExtra("android.intent.extra.SUBJECT", str2);
        intent.putExtra("android.intent.extra.TEXT", "Enter your FeedBack");
        try {
            context.startActivity(Intent.createChooser(intent, "Send FeedBack..."));
        } catch (ActivityNotFoundException unused) {
            ShowToastShort(context, "There is no email client installed.");
        }
    }

    public static boolean isConnectionAvailable(Context context) {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected() || !activeNetworkInfo.isConnectedOrConnecting() || !activeNetworkInfo.isAvailable()) {
            return false;
        }
        return true;
    }

    public static void shareApp(Context context) {
        String packageName = context.getPackageName();
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", "Check out the App at: https://play.google.com/store/apps/details?id=" + packageName);
        intent.setType("text/plain");
        context.startActivity(intent);
    }

    public static boolean isMyServiceRunning(Class<?> cls, Context context) {
        for (ActivityManager.RunningServiceInfo runningServiceInfo : ((ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getRunningServices(Integer.MAX_VALUE)) {
            if (cls.getName().equals(runningServiceInfo.service.getClassName())) {
                Log.i("isMyServiceRunning?", "true");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", "false");
        return false;
    }

    public static void setLanguageAds(Context context) {
        if (PreferenceManager.getDefaultSharedPreferences(context).getBoolean("Frist_ads", true)) {
            PreferenceManager.getDefaultSharedPreferences(context).getString("lgAds", "en");
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(context).edit();
            edit.putBoolean("Frist_ads", false);
            if (Locale.getDefault().getLanguage().equalsIgnoreCase("vi")) {
                edit.putString("lgAds", "vi");
            }
            edit.commit();
        }
    }

    public static String getTextLanguage(String str, String str2) {
        String[] split = str.split(str2 + ":");
        if (split.length == 1) {
            split = str.split("en:");
        }
        if (split.length == 1) {
            return split[0];
        }
        return split[1].split(",,")[0];
    }

    public static float getRating(String str) {
        try {
            String[] split = str.split("rating:");
            if (split.length == 1) {
                return 4.3f;
            }
            return Float.valueOf(split[1].split(",,")[0]).floatValue();
        } catch (Exception unused) {
            return 4.3f;
        }
    }

    public static void getInfoApp(String str) {
        getTimeReload(str);
        getShowMax(str);
        getFlagAdmob(str);
        getAdxBanner(str);
        getID(str);
        getAdxFull(str);
        getAdxReward(str);
        getAdxNative(str);
        getAdxNativeBanner(str);
        getFbBanner(str);
        getFbFull(str);
        getFbNative(str);
        getFbNativeBanner(str);
        getFbReward(str);
        CONTAIN.contentAds = getContentAds(str);
    }

    public static boolean useList(String[] strArr, String str) {
        return Arrays.asList(strArr).contains(str);
    }

    public static String[] getContentAds(String str) {
        new ArrayList();
        String[] split = str.split("CONTENTADS:");
        try {
            if (split.length == 1) {
                return split;
            }
            String[] split2 = split[1].split(",,");
            if (split2.length == 1) {
                return split2[0].split(",");
            }
            return split2[1].split(",");
        } catch (Exception unused) {
            return split;
        }
    }

    public static void getFlagAdmob(String str) {
        try {
            String[] split = str.split("FLAGADMOB:");
            if (split.length != 1) {
                if (split[1].split(",,")[0].equals("true")) {
                    CONTAIN.FLAG_ADMOB = true;
                } else {
                    CONTAIN.FLAG_ADMOB = false;
                }
            }
        } catch (Exception unused) {
        }
    }

    public static void getID(String str) {
        try {
            String[] split = str.split("ADXID:");
            if (split.length != 1) {
                CONTAIN.App_ID_ADMOB = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getShowMax(String str) {
        try {
            String[] split = str.split("MAXSHOW:");
            if (split.length != 1) {
                CONTAIN.ShowMax = (long) Integer.valueOf(split[1].split(",,")[0]).intValue();
            }
        } catch (Exception unused) {
        }
    }

    public static void getTimeReload(String str) {
        try {
            String[] split = str.split("TIMERELOAD:");
            if (split.length != 1) {
                CONTAIN.TimeReload = (long) Integer.valueOf(split[1].split(",,")[0]).intValue();
            }
        } catch (Exception unused) {
        }
    }

    public static void getAdxFull(String str) {
        try {
            String[] split = str.split("ADXFULL:");
            if (split.length != 1) {
                CONTAIN.ADX_ID_FULL = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getAdxBanner(String str) {
        try {
            String[] split = str.split("ADXBANNER:");
            if (split.length != 1) {
                CONTAIN.ADX_ID_BANNER = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getAdxReward(String str) {
        try {
            String[] split = str.split("ADXREWARD:");
            if (split.length != 1) {
                CONTAIN.ADX_ID_REWARD = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getAdxNative(String str) {
        try {
            String[] split = str.split("ADXNATIVE:");
            if (split.length != 1) {
                CONTAIN.ADX_ID_NATIVE = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getAdxNativeBanner(String str) {
        try {
            String[] split = str.split("ADXNATIVEBANNER:");
            if (split.length != 1) {
                CONTAIN.ADX_ID_NATIVE_BANNER = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getFbFull(String str) {
        try {
            String[] split = str.split("FBFULL:");
            if (split.length != 1) {
                CONTAIN.FB_ID_FULL = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getFbBanner(String str) {
        try {
            String[] split = str.split("FBBANNER:");
            if (split.length != 1) {
                CONTAIN.FB_ID_BANNER = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getFbReward(String str) {
        try {
            String[] split = str.split("FBREWARD:");
            if (split.length != 1) {
                CONTAIN.FB_ID_REWARD = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getFbNative(String str) {
        try {
            String[] split = str.split("FBNATIVE:");
            if (split.length != 1) {
                CONTAIN.FB_ID_NATIVE = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static void getFbNativeBanner(String str) {
        try {
            String[] split = str.split("FBNATIVEBANNER:");
            if (split.length != 1) {
                CONTAIN.FB_ID_NATIVE_BANNER = split[1].split(",,")[0];
            }
        } catch (Exception unused) {
        }
    }

    public static int getRandom(int i) {
        return new Random().nextInt(i);
    }
}
