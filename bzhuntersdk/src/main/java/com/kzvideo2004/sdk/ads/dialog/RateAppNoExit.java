package com.kzvideo2004.sdk.ads.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import com.bzvideo2004.sdk.R;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;

public class RateAppNoExit extends Dialog {
    Context mContext;
    String mEmail;
    String mTitleEmail;

    public RateAppNoExit(Context context, String str, String str2) {
        super(context);
        this.mContext = context;
        this.mEmail = str;
        this.mTitleEmail = str2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.dialog_rate_app_no_exit);
        ((TextView) findViewById(R.id.btn_good)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(RateAppNoExit.this.mContext).edit();
                edit.putBoolean("Show_rate", true);
                edit.commit();
                UtilsCombat.RateApp(RateAppNoExit.this.mContext);
                UtilsCombat.ShowToastLong(RateAppNoExit.this.mContext, "Thanks for rate and review ^^ ");
                RateAppNoExit.this.dismiss();
            }
        });
        ((TextView) findViewById(R.id.btn_not_good)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(RateAppNoExit.this.mContext).edit();
                edit.putBoolean("Show_rate", true);
                edit.commit();
                RateAppNoExit.this.showFeedBackDialog();
                RateAppNoExit.this.dismiss();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showFeedBackDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext, R.style.DialogTheme);
        builder.setTitle(this.mContext.getString(R.string.title_dialog_feed_back));
        builder.setMessage(this.mContext.getString(R.string.message_dialog_feed_back));
        builder.setPositiveButton(this.mContext.getString((int)17039370), new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                UtilsCombat.SendFeedBack(RateAppNoExit.this.mContext, RateAppNoExit.this.mEmail, RateAppNoExit.this.mTitleEmail);
            }
        });
        builder.setNegativeButton(this.mContext.getString((int)17039360), new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                RateAppNoExit.this.dismiss();
            }
        });
        AlertDialog create = builder.create();
        create.setCanceledOnTouchOutside(false);
        create.setCancelable(false);
        create.show();
    }
}
