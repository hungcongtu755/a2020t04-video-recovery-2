package com.kzvideo2004.sdk.ads.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bzvideo2004.sdk.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;
import com.kzvideo2004.sdk.ads.model.CampaignAds;
import java.util.ArrayList;
import androidx.annotation.Nullable;

public class DialogExitApp extends Dialog {
    TextView btnNo;
    TextView btnOk;
    ArrayList<CampaignAds> list = new ArrayList<>();
    Context mContext;

    public DialogExitApp(Context context, ArrayList<CampaignAds> arrayList) {
        super(context);
        this.mContext = context;
        this.list = arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.dialog_more_app_exit);
        final int random = UtilsCombat.getRandom(this.list.size());
        this.btnOk = (TextView) findViewById(R.id.btn_yes);
        this.btnNo = (TextView) findViewById(R.id.btn_no);
        ((RatingBar) findViewById(R.id.rbRating)).setRating(this.list.get(random).getRating());
        findViewById(R.id.ivInfoAds).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!((Activity) DialogExitApp.this.mContext).isFinishing()) {
                    new DialogInfoAds(DialogExitApp.this.mContext).show();
                }
            }
        });
        ((Button) findViewById(R.id.btnOpenLink)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UtilsCombat.OpenBrower(DialogExitApp.this.mContext, DialogExitApp.this.list.get(random).getsLink());
            }
        });
        ((TextView) findViewById(R.id.tvTitleAds)).setText(this.list.get(random).getsTitle());
        ((TextView) findViewById(R.id.tvAdsDes)).setText(this.list.get(random).getsDecrition());
        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lrContainer);
        linearLayout.setVisibility(View.INVISIBLE);
        final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) findViewById(R.id.shimmer_container);
        shimmerFrameLayout.startShimmer();
        Glide.with(this.mContext.getApplicationContext()).load(this.list.get(random).getsIcon()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                return false;
            }
        }).into((ImageView) findViewById(R.id.ivIconAds));
        Glide.with(this.mContext).load(this.list.get(random).getsImage()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout.setVisibility(View.VISIBLE);
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout.setVisibility(View.VISIBLE);
                return false;
            }
        }).into((ImageView) findViewById(R.id.ivAdsBig));
        this.btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogExitApp.this.dismiss();
                ((Activity) DialogExitApp.this.mContext).finish();
            }
        });
        this.btnNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogExitApp.this.dismiss();
            }
        });
    }
}
