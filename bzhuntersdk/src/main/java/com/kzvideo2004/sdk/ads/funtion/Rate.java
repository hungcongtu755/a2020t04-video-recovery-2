package com.kzvideo2004.sdk.ads.funtion;

import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;

import com.bzvideo2004.sdk.R;
import com.kzvideo2004.sdk.ads.dialog.RateApp;
import com.kzvideo2004.sdk.ads.dialog.RateAppNoExit;


public class Rate {
    public static void Show(Context context) {
        try {
            if (!UtilsCombat.isConnectionAvailable(context)) {
                ((Activity) context).finish();
            } else if (!PreferenceManager.getDefaultSharedPreferences(context).getBoolean("Show_rate", false)) {
                RateApp rateApp = new RateApp(context, context.getString(R.string.email_feedback), context.getString(R.string.title_email));
                rateApp.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                rateApp.show();
            } else {
                ((Activity) context).finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            ((Activity) context).finish();
        }
    }

    public static void ShowNoExit(Context context) {
        try {
            if (UtilsCombat.isConnectionAvailable(context) && !PreferenceManager.getDefaultSharedPreferences(context).getBoolean("Show_rate", false)) {
                RateAppNoExit rateAppNoExit = new RateAppNoExit(context, context.getString(R.string.email_feedback), context.getString(R.string.title_email));
                rateAppNoExit.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                rateAppNoExit.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
