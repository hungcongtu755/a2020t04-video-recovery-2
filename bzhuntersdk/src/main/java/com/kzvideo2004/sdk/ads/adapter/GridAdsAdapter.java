package com.kzvideo2004.sdk.ads.adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bzvideo2004.sdk.R;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;
import com.kzvideo2004.sdk.ads.model.CampaignAds;
import java.util.ArrayList;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class GridAdsAdapter extends RecyclerView.Adapter<GridAdsAdapter.MyViewHolder> {
    Context context;
    ArrayList<CampaignAds> listCampaignAds = new ArrayList<>();
    BitmapDrawable placeholder;

    public GridAdsAdapter(Context context2, ArrayList<CampaignAds> arrayList) {
        this.context = context2;
        this.listCampaignAds = arrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        protected Button btnOpenLink;
        protected CardView cvAds;
        protected ImageView itemImage;
        protected RatingBar rbRating;
        protected TextView tvTitleAds;

        public MyViewHolder(View view) {
            super(view);
            this.itemImage = (ImageView) view.findViewById(R.id.ivImage);
            this.tvTitleAds = (TextView) view.findViewById(R.id.tvTitleAds);
            this.rbRating = (RatingBar) view.findViewById(R.id.rbRating);
            this.btnOpenLink = (Button) view.findViewById(R.id.btnOpenLink);
            this.cvAds = (CardView) view.findViewById(R.id.cvAds);
        }
    }

    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_kz_ads_horizontal, viewGroup, false));
    }

    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {
        final CampaignAds campaignAds = this.listCampaignAds.get(i);
        try {
            ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) Glide.with(this.context).load(campaignAds.getsIcon()).diskCacheStrategy(DiskCacheStrategy.ALL)).priority(Priority.HIGH)).centerCrop()).into(myViewHolder.itemImage);
        } catch (Exception e) {
            Context context2 = this.context;
            Toast.makeText(context2, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        myViewHolder.tvTitleAds.setText(campaignAds.getsTitle());
        myViewHolder.rbRating.setRating(campaignAds.getRating());
        myViewHolder.cvAds.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UtilsCombat.OpenBrower(GridAdsAdapter.this.context, campaignAds.getsLink());
            }
        });
        myViewHolder.btnOpenLink.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UtilsCombat.OpenBrower(GridAdsAdapter.this.context, campaignAds.getsLink());
            }
        });
    }

    public int getItemCount() {
        return this.listCampaignAds.size();
    }
}
