package com.kzvideo2004.sdk.ads.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bzvideo2004.sdk.R;
import com.kzvideo2004.sdk.ads.ads.AdsHelp;
import com.kzvideo2004.sdk.ads.funtion.SharePreferenceAds;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;

public class DialogUnlockForFree extends Dialog {
    Button btnRewardAds;
    String key;
    AdCloseRewardListener mAdCloseRewardListener;
    Context mContext;

    public interface AdCloseRewardListener {
        void onAdFail();

        void onAdSuccess();
    }

    public DialogUnlockForFree(Context context, AdCloseRewardListener adCloseRewardListener, String str) {
        super(context);
        this.mContext = context;
        this.mAdCloseRewardListener = adCloseRewardListener;
        this.key = str;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.dialog_reward_adx);
        setCanceledOnTouchOutside(true);
        setCancelable(false);
        this.btnRewardAds = (Button) findViewById(R.id.btnRewardAds);
        findViewById(R.id.btnRewardAds).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogUnlockForFree.this.btnRewardAds.setText(DialogUnlockForFree.this.mContext.getString(R.string.watch_reward_ads));
                if (UtilsCombat.isConnectionAvailable(DialogUnlockForFree.this.mContext)) {
                    AdsHelp.getInstance(DialogUnlockForFree.this.mContext).showReward(new AdsHelp.AdCloseRewardListener() {
                        public void onAdFail() {
                            DialogUnlockForFree.this.btnRewardAds.setText(DialogUnlockForFree.this.mContext.getString(R.string.retry_reward_ads));
                        }

                        public void onAdSuccess() {
                            SharePreferenceAds.getInstance(DialogUnlockForFree.this.mContext).setRewardTime(DialogUnlockForFree.this.key);
                            if (DialogUnlockForFree.this.mAdCloseRewardListener != null) {
                                DialogUnlockForFree.this.mAdCloseRewardListener.onAdSuccess();
                            }
                            Toast.makeText(DialogUnlockForFree.this.mContext, "Success...", (int)0).show();
                            DialogUnlockForFree.this.dismiss();
                        }
                    });
                } else {
                    Toast.makeText(DialogUnlockForFree.this.mContext, "No internet...", (int)0).show();
                }
            }
        });
        dismiss();
        findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogUnlockForFree.this.dismiss();
                if (DialogUnlockForFree.this.mAdCloseRewardListener != null) {
                    DialogUnlockForFree.this.mAdCloseRewardListener.onAdFail();
                }
            }
        });
    }
}
