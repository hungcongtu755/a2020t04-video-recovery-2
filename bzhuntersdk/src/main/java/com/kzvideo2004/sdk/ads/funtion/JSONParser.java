package com.kzvideo2004.sdk.ads.funtion;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JSONParser {
    static InputStream is = null;
    static String jSon = "";
    static JSONObject mjObj;
    static HttpURLConnection urlConnection;

    public static JSONObject makeHttpRequest(String str) {
        StringBuilder sb = new StringBuilder();
        try {
            urlConnection = (HttpURLConnection) new URL(str).openConnection();
            urlConnection.setReadTimeout(200000);
            urlConnection.setConnectTimeout(200000);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable th) {
            urlConnection.disconnect();
            throw th;
        }
        urlConnection.disconnect();
        jSon = sb.toString();
        try {
            if (jSon == null || jSon.equals("")) {
                return null;
            }
            mjObj = new JSONObject(jSon);
            return mjObj;
        } catch (JSONException unused) {
            return null;
        }
    }
}
