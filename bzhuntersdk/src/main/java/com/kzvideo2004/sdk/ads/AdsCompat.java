package com.kzvideo2004.sdk.ads;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bzvideo2004.sdk.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.kzvideo2004.sdk.ads.adapter.HorizontalAdapter;
import com.kzvideo2004.sdk.ads.ads.AdsHelp;
import com.kzvideo2004.sdk.ads.ads.FBHelper;
import com.kzvideo2004.sdk.ads.dialog.DialogExitApp;
import com.kzvideo2004.sdk.ads.dialog.DialogFullScreen;
import com.kzvideo2004.sdk.ads.dialog.DialogInfoAds;
import com.kzvideo2004.sdk.ads.dialog.DialogStoreAds;
import com.kzvideo2004.sdk.ads.dialog.DialogUnlockForFree;
import com.kzvideo2004.sdk.ads.funtion.KzAds;
import com.kzvideo2004.sdk.ads.funtion.Rate;
import com.kzvideo2004.sdk.ads.funtion.SharePreferenceAds;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;
import com.kzvideo2004.sdk.ads.model.CONTAIN;
import com.kzvideo2004.sdk.ads.model.CampaignAds;

import java.util.ArrayList;
import java.util.Collections;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AdsCompat {
    private static AdsCompat instance;
    static AdLoadData mAdLoadData;
    public static Context mContext;
    public static Activity activity;
    public static KzAds mKz;
    ArrayList<CampaignAds> list1 = new ArrayList<>();
    ArrayList<CampaignAds> listHorizontalAds = new ArrayList<>();
    ArrayList<CampaignAds> listcontentads = new ArrayList<>();
//    private AdCloseListener mAdCloseListener;
    ArrayList<CampaignAds> mListAds = new ArrayList<>();
    private boolean mRate = false;
    private long timeLoad = 0;

    public interface AdCloseListener {
        void onAdClosed();
    }

    public interface AdCloseRewardListener {
        void onAdFail();

        void onAdSuccess();
    }

    public interface AdLoadData {
        void onSuccess();
    }

//    public static AdsCompat getInstance(Context context) {
//        mContext = context;
//        if (instance == null) {
//            instance = new AdsCompat();
//        }
//        return instance;
//    }

    public static AdsCompat getInstance(Context context) {
        mContext = context;
//        activity = activity;
        if (instance == null) {
            instance = new AdsCompat();
        }
        return instance;
    }
    public void loadData() {
        this.mListAds.clear();
        this.list1.clear();
        this.listcontentads.clear();
        final FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        PackageManager packageManager = AdsCompat.mContext.getPackageManager();
        ArrayList<CampaignAds> arrayList = new ArrayList<CampaignAds>();
        CampaignAds cTmp = new CampaignAds();
        cTmp.setActive( mFirebaseRemoteConfig.getBoolean(CONTAIN.ADS_ACTIVE));
        cTmp.setsDecrition(mFirebaseRemoteConfig.getString(CONTAIN.ADS_DESCRIPTION));
        cTmp.setAdsInfo(mFirebaseRemoteConfig.getString(CONTAIN.ADS_INFO));
        cTmp.setsImage(mFirebaseRemoteConfig.getString(CONTAIN.ADS_IMAGE));
        cTmp.setShowRate( mFirebaseRemoteConfig.getBoolean(CONTAIN.ADS_SHOW_RATE));
        cTmp.setsLink(mFirebaseRemoteConfig.getString(CONTAIN.ADS_APP_LINK));
        cTmp.setType((int)mFirebaseRemoteConfig.getLong(CONTAIN.ADS_TYPE));
        cTmp.setsPackage(mContext.getPackageName());
        Log.d("Ads",cTmp.getAdsInfo());
        Log.d("Ads",cTmp.toString());
        arrayList.add(cTmp);
        AdsCompat.this.checkRate(arrayList, packageManager);
        if (arrayList.size() > 0) {
            AdsCompat.this.addData();
        }
        if (CONTAIN.styleAds == 2) {
            FBHelper.getInstance(AdsCompat.mContext).initFull();
        } else if (CONTAIN.styleAds == 3) {
            AdsHelp.getInstance(AdsCompat.mContext).initFull();
        }if (AdsCompat.mAdLoadData != null) {
            AdsCompat.mAdLoadData.onSuccess();
        }
    }

    public void loadData(AdLoadData adLoadData) {
        mAdLoadData = adLoadData;
        this.mListAds.clear();
        this.list1.clear();
        this.listcontentads.clear();
        this.listHorizontalAds.clear();
        PackageManager packageManager = AdsCompat.mContext.getPackageManager();
        ArrayList<CampaignAds> arrayList = new ArrayList<CampaignAds>();
        CampaignAds cTmp = new CampaignAds();
        cTmp.setActive(true);
        cTmp.setsDecrition("en:Test,,vn:Kiểm tra");
        cTmp.setAdsInfo("FLAGADMOB:false,,TIMERELOAD:40000,,MAXSHOW:2,,ADXID:,,ADXFULL:ca-app-pub-3940256099942544/1033173712,,ADXNATIVE:ca-app-pub-3940256099942544/2247696110,,ADXBANNER:ca-app-pub-3940256099942544/6300978111,,ADXNATIVEBANNER:ca-app-pub-3940256099942544/2247696110,,ADXREWARD:ca-app-pub-3940256099942544/5224354917,,FBFULL:637456636797114_639801379895973,,FBNATIVE:637456636797114_637457006797077,,FBBANNER:637456636797114_639801506562627,,FBNATIVEBANNER:,,FBREWARD:,,");
        cTmp.setsImage("en:https://i.imgur.com/d3SCNZZ.png,,vi:https://i.imgur.com/6RmBJbP.png");
        cTmp.setShowRate(true);
        cTmp.setsLink("http://bit.ly/2TCavwL");
        cTmp.setType(3);
        arrayList.add(cTmp);
        AdsCompat.this.checkRate(arrayList, packageManager);
        if (arrayList.size() > 0) {
            AdsCompat.this.addData();
        }
        if (CONTAIN.styleAds == 2) {
            FBHelper.getInstance(AdsCompat.mContext).initFull();
        } else if (CONTAIN.styleAds == 3) {
            AdsHelp.getInstance(AdsCompat.mContext).initFull();
        }if (AdsCompat.mAdLoadData != null) {
            AdsCompat.mAdLoadData.onSuccess();
        }
        // campaignAds.setsPackage(jSONObject2.getString("packNames"));
        //            campaignAds.setsDecrition(UtilsCombat.getTextLanguage(jSONObject2.getString("decription"), language));
        //            campaignAds.setsImage(UtilsCombat.getTextLanguage(jSONObject2.getString("image"), language));
        //            campaignAds.setRating(UtilsCombat.getRating(jSONObject2.getString("image_vn")));
        //            campaignAds.setsTitle(UtilsCombat.getTextLanguage(jSONObject2.getString("title"), language));
        //            campaignAds.setsIcon(UtilsCombat.getTextLanguage(jSONObject2.getString("icon"), language));
        //            campaignAds.setType(jSONObject2.getInt("type"));
        //            campaignAds.setsLink(jSONObject2.getString("link"));
        //            campaignAds.setShowRate(jSONObject2.getBoolean("report"));
        //            campaignAds.setActive(jSONObject2.getBoolean("Active"));
        //            campaignAds.setAdsInfo(jSONObject2.getString("title_vn"));

//        KzAds.getInstance(mContext).loadData(mContext, new KzAds.LoadDataListener() {
//            public void OnResult(ArrayList<CampaignAds> arrayList) {
//                if (UtilsCombat.isConnectionAvailable(AdsCompat.mContext)) {
//                    CONTAIN.FlagLoadedData = true;
//                }
//                PackageManager packageManager = AdsCompat.mContext.getPackageManager();
//                Log.d("Ads", arrayList.toString());
//                if (arrayList != null) {
//                    AdsCompat.this.checkRate(arrayList, packageManager);
//                }
//                if (arrayList.size() > 0) {
//                    AdsCompat.this.addData();
//                }
//                if (CONTAIN.styleAds == 2) {
//                    FBHelper.getInstance(AdsCompat.mContext).initFull();
//                } else if (CONTAIN.styleAds == 3) {
//                    AdsHelp.getInstance(AdsCompat.mContext).initFull();
//                }
//                if (AdsCompat.mAdLoadData != null) {
//                    AdsCompat.mAdLoadData.onSuccess();
//                }
//            }
//        });
    }

    private AdsCompat() {
    }

    public void showDialogExit() {
        if (CONTAIN.styleAds == 6) {
            Rate.Show(mContext);
            return;
        }
        ArrayList<CampaignAds> arrayList = this.listcontentads;
        if (arrayList == null || arrayList.size() <= 0) {
            Rate.Show(mContext);
        } else if (!this.mRate) {
            DialogExitApp dialogExitApp = new DialogExitApp(mContext, this.listcontentads);
            dialogExitApp.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
            dialogExitApp.show();
        } else if (!PreferenceManager.getDefaultSharedPreferences(mContext).getBoolean("Show_rate", false)) {
            Rate.Show(mContext);
        } else {
            DialogExitApp dialogExitApp2 = new DialogExitApp(mContext, this.listcontentads);
            dialogExitApp2.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
            dialogExitApp2.show();
        }
    }

    public void showReward(final AdCloseRewardListener fAdCloseRewardListener, String str) {
        if (SharePreferenceAds.getInstance(mContext).checkStatusAds(str)) {
            DialogUnlockForFree dialogUnlockForFree = new DialogUnlockForFree(mContext, new DialogUnlockForFree.AdCloseRewardListener() {
                public void onAdFail() {
                    AdCloseRewardListener adCloseRewardListener = fAdCloseRewardListener;
                    if (adCloseRewardListener != null) {
                        adCloseRewardListener.onAdFail();
                    }
                }

                public void onAdSuccess() {
                    AdCloseRewardListener adCloseRewardListener = fAdCloseRewardListener;
                    if (adCloseRewardListener != null) {
                        adCloseRewardListener.onAdSuccess();
                    }
                }
            }, str);
            dialogUnlockForFree.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
            dialogUnlockForFree.show();
        } else if (fAdCloseRewardListener != null) {
            fAdCloseRewardListener.onAdSuccess();
        }
    }

    public void showStoreAds(final AdCloseListener fAdCloseListener) {
//        this.mAdCloseListener = fAdCloseListener;
        ArrayList<CampaignAds> arrayList = this.mListAds;
        if (arrayList != null && arrayList.size() > 0) {
            DialogStoreAds dialogStoreAds = new DialogStoreAds(mContext, this.mListAds, new DialogStoreAds.AdCloseListener() {
                public void onAdClosed() {
                    AdCloseListener adCloseListener = fAdCloseListener;
                    if (adCloseListener != null) {
                        adCloseListener.onAdClosed();
                    }
                }
            });
            dialogStoreAds.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
            dialogStoreAds.show();
        } else if (fAdCloseListener != null) {
            fAdCloseListener.onAdClosed();
        }
        if (!UtilsCombat.isConnectionAvailable(mContext)) {
            Toast.makeText(mContext, "No internet...", (int)0).show();
        }
    }

    public void showFullScreen(final AdCloseListener fAdCloseListener, boolean z) {
//        this.mAdCloseListener = fAdCloseListener;
        if (!z) {
            ArrayList<CampaignAds> arrayList = this.listcontentads;
            if (arrayList != null && arrayList.size() > 0 && CONTAIN.CountCurrent < CONTAIN.ShowMax) {
                DialogFullScreen dialogFullScreen = new DialogFullScreen(mContext, this.listcontentads, new DialogFullScreen.AdCloseListener() {
                    public void onAdClosed() {
                        CONTAIN.CountCurrent++;
                        AdCloseListener adCloseListener = fAdCloseListener;
                        if (adCloseListener != null) {
                            adCloseListener.onAdClosed();
                        }
                    }
                });
                dialogFullScreen.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                dialogFullScreen.show();
            } else if (fAdCloseListener != null) {
                fAdCloseListener.onAdClosed();
            }
        } else if (CONTAIN.styleAds == 1) {
            ArrayList<CampaignAds> arrayList2 = this.listcontentads;
            if (arrayList2 != null && arrayList2.size() > 0 && CONTAIN.CountCurrent < CONTAIN.ShowMax && this.timeLoad + CONTAIN.TimeReload < System.currentTimeMillis()) {
                this.timeLoad = System.currentTimeMillis();
                DialogFullScreen dialogFullScreen2 = new DialogFullScreen(mContext, this.listcontentads, new DialogFullScreen.AdCloseListener() {
                    public void onAdClosed() {
                        CONTAIN.CountCurrent++;
                        AdCloseListener adCloseListener = fAdCloseListener;
                        if (adCloseListener != null) {
                            adCloseListener.onAdClosed();
                        }
                    }
                });
                dialogFullScreen2.getWindow().getAttributes().windowAnimations = R.style.DialogTheme;
                dialogFullScreen2.show();
            } else if (fAdCloseListener != null) {
                fAdCloseListener.onAdClosed();
            }
        } else if (CONTAIN.styleAds == 2) {
            FBHelper.getInstance(mContext).showInterstitialAd(new FBHelper.AdCloseListener() {
                public void onAdClosed() {
                    AdCloseListener adCloseListener = fAdCloseListener;
                    if (adCloseListener != null) {
                        adCloseListener.onAdClosed();
                    }
                }
            });
        } else if (CONTAIN.styleAds == 3) {
            AdsHelp.getInstance(mContext).showInterstitialAd(new AdsHelp.AdCloseListener() {
                public void onAdClosed() {
                    AdCloseListener adCloseListener = fAdCloseListener;
                    if (adCloseListener != null) {
                        adCloseListener.onAdClosed();
                    }
                }
            });
        } else if (CONTAIN.styleAds == 6 && fAdCloseListener != null) {
            fAdCloseListener.onAdClosed();
        }
    }

    /* access modifiers changed from: private */
    public void checkRate(ArrayList<CampaignAds> arrayList, PackageManager packageManager) {
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getsPackage().contains(mContext.getPackageName())) {
                this.mRate = arrayList.get(i).getShowRate();
                CONTAIN.styleAds = arrayList.get(i).getType();
                UtilsCombat.getInfoApp(arrayList.get(i).getAdsInfo());
            } else if (arrayList.get(i).getActive() && !UtilsCombat.isPackageInstalled(arrayList.get(i).getsPackage(), packageManager)) {
                this.mListAds.add(arrayList.get(i));
            }
        }
    }

    public void loadNative(boolean z) {
        try {
            FrameLayout frameLayout = (FrameLayout) ((Activity) mContext).findViewById(R.id.fl_adplaceholder);
            if (z) {
                if (CONTAIN.styleAds == 1) {
                    if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                        frameLayout.setVisibility(View.GONE);
                        return;
                    }
                    frameLayout.setVisibility(View.VISIBLE);
                    intNative(frameLayout);
                } else if (CONTAIN.styleAds == 2) {
                    FBHelper.getInstance(mContext).loadNative();
                } else if (CONTAIN.styleAds == 3) {
                    AdsHelp.getInstance(mContext).loadNative();
                }
            } else if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                frameLayout.setVisibility(View.GONE);
            } else {
                frameLayout.setVisibility(View.VISIBLE);
                intNative(frameLayout);
            }
        } catch (Exception unused) {
        }
    }

    public void loadHorizontalAds() {
        try {
            FrameLayout frameLayout = (FrameLayout) ((Activity) mContext).findViewById(R.id.flHorizontal);
            if (this.listHorizontalAds == null || this.listHorizontalAds.size() <= 0) {
                frameLayout.setVisibility(View.GONE);
                return;
            }
            frameLayout.setVisibility(View.VISIBLE);
            intHorizontal(frameLayout);
        } catch (Exception unused) {
        }
    }

    public void loadBanner(boolean z) {
        try {
            FrameLayout frameLayout = (FrameLayout) ((Activity) mContext).findViewById(R.id.fl_banner_container);
            if (z) {
                if (CONTAIN.styleAds == 1) {
                    if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                        frameLayout.setVisibility(View.GONE);
                        return;
                    }
                    frameLayout.setVisibility(View.VISIBLE);
                    intBanner(frameLayout);
                } else if (CONTAIN.styleAds == 2) {
                    FBHelper.getInstance(mContext).loadBanner();
                } else if (CONTAIN.styleAds == 3) {
                    AdsHelp.getInstance(mContext).loadBanner();
                }
            } else if (this.listcontentads != null && this.listcontentads.size() > 0) {
                frameLayout.setVisibility(View.VISIBLE);
                intBanner(frameLayout);
            }
        } catch (Exception unused) {
        }
    }

    public void loadNativeBanner(boolean z) {
        try {
            FrameLayout frameLayout = (FrameLayout) ((Activity) mContext).findViewById(R.id.fl_native_banner_ad_container);
            if (z) {
                if (CONTAIN.styleAds == 1) {
                    if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                        frameLayout.setVisibility(View.GONE);
                        return;
                    }
                    frameLayout.setVisibility(View.VISIBLE);
                    intNativeBanner(frameLayout);
                } else if (CONTAIN.styleAds == 2) {
                    FBHelper.getInstance(mContext).loadNativeBanner();
                } else if (CONTAIN.styleAds == 3) {
                    AdsHelp.getInstance(mContext).loadNativeBanner();
                }
            } else if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                frameLayout.setVisibility(View.GONE);
            } else {
                frameLayout.setVisibility(View.VISIBLE);
                intNativeBanner(frameLayout);
            }
        } catch (Exception unused) {
        }
    }

    public void loadFloating(Activity activity) {
        try {
            FrameLayout frameLayout = (FrameLayout) activity.findViewById(R.id.fl_floating);
            if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                frameLayout.setVisibility(View.GONE);
                return;
            }
            frameLayout.setVisibility(View.VISIBLE);
            intFloating(activity, frameLayout);
        } catch (Exception unused) {
        }
    }

    public void loadHorizontalAdsFragment(View view) {
        try {
            FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.flHorizontal);
            if (this.listHorizontalAds == null || this.listHorizontalAds.size() <= 0) {
                frameLayout.setVisibility(View.GONE);
                return;
            }
            frameLayout.setVisibility(View.VISIBLE);
            intHorizontal(frameLayout);
        } catch (Exception unused) {
        }
    }

    public void loadNativeFragment(View view, boolean z) {
        try {
            FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fl_adplaceholder);
            if (z) {
                if (CONTAIN.styleAds == 1) {
                    if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                        frameLayout.setVisibility(View.GONE);
                        return;
                    }
                    frameLayout.setVisibility(View.VISIBLE);
                    intNative(frameLayout);
                } else if (CONTAIN.styleAds == 2) {
                    FBHelper.getInstance(mContext).loadNativeFragment(view);
                } else if (CONTAIN.styleAds == 3) {
                    AdsHelp.getInstance(mContext).loadNativeFragment(view);
                }
            } else if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                frameLayout.setVisibility(View.GONE);
            } else {
                frameLayout.setVisibility(View.VISIBLE);
                intNative(frameLayout);
            }
        } catch (Exception unused) {
        }
    }

    public void loadBannerFragment(View view, boolean z) {
        try {
            FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fl_banner_container);
            if (z) {
                if (CONTAIN.styleAds == 1) {
                    if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                        frameLayout.setVisibility(View.GONE);
                        return;
                    }
                    frameLayout.setVisibility(View.VISIBLE);
                    intBanner(frameLayout);
                } else if (CONTAIN.styleAds == 2) {
                    FBHelper.getInstance(mContext).loadBannerFragment(view);
                } else if (CONTAIN.styleAds == 3) {
                    AdsHelp.getInstance(mContext).loadBannerFragment(view);
                }
            } else if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                frameLayout.setVisibility(View.GONE);
            } else {
                frameLayout.setVisibility(View.VISIBLE);
                intBanner(frameLayout);
            }
        } catch (Exception unused) {
        }
    }

    public void loadFloatingFragment(Activity activity, View view) {
        try {
            FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fl_floating);
            if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                frameLayout.setVisibility(View.GONE);
                return;
            }
            frameLayout.setVisibility(View.VISIBLE);
            intFloatingMore(activity, frameLayout);
        } catch (Exception unused) {
        }
    }

    public void loadNativeBannerFragment(View view, boolean z) {
        try {
            FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.fl_native_banner_ad_container);
            if (z) {
                if (CONTAIN.styleAds == 1) {
                    if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                        frameLayout.setVisibility(View.GONE);
                        return;
                    }
                    frameLayout.setVisibility(View.VISIBLE);
                    intNativeBanner(frameLayout);
                } else if (CONTAIN.styleAds == 2) {
                    FBHelper.getInstance(mContext).loadNativeBannerFragment(view);
                } else if (CONTAIN.styleAds == 3) {
                    AdsHelp.getInstance(mContext).loadNativeBannerFragment(view);
                }
            } else if (this.listcontentads == null || this.listcontentads.size() <= 0) {
                frameLayout.setVisibility(View.GONE);
            } else {
                frameLayout.setVisibility(View.VISIBLE);
                intNativeBanner(frameLayout);
            }
        } catch (Exception unused) {
        }
    }

    public void intNative(FrameLayout frameLayout) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from((Activity) mContext).inflate(R.layout.item_kz_ads_native, frameLayout, false);
        frameLayout.addView(linearLayout);
        final int random = UtilsCombat.getRandom(this.listcontentads.size());
        ((Button) linearLayout.findViewById(R.id.btnOpenLink)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UtilsCombat.OpenBrower(AdsCompat.mContext, AdsCompat.this.listcontentads.get(random).getsLink());
            }
        });
        linearLayout.findViewById(R.id.ivInfoAds).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    new DialogInfoAds(AdsCompat.mContext).show();
                } catch (Exception unused) {
                }
            }
        });
        ((RatingBar) linearLayout.findViewById(R.id.rbRating)).setRating(this.listcontentads.get(random).getRating());
        ((TextView) linearLayout.findViewById(R.id.tvTitleAds)).setText(this.listcontentads.get(random).getsTitle());
        ((TextView) linearLayout.findViewById(R.id.tvAdsDes)).setText(this.listcontentads.get(random).getsDecrition());
        final LinearLayout linearLayout2 = (LinearLayout) linearLayout.findViewById(R.id.lrContainer);
        linearLayout2.setVisibility(View.INVISIBLE);
        final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) linearLayout.findViewById(R.id.shimmer_container);
        shimmerFrameLayout.startShimmer();
        Glide.with(((Activity) mContext).getApplicationContext()).load(this.listcontentads.get(random).getsIcon()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                return false;
            }
        }).into((ImageView) linearLayout.findViewById(R.id.ivIconAds));
        Glide.with(mContext.getApplicationContext()).load(this.listcontentads.get(random).getsImage()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout2.setVisibility(View.VISIBLE);
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout2.setVisibility(View.VISIBLE);
                return false;
            }
        }).into((ImageView) linearLayout.findViewById(R.id.ivAdsBig));
    }

    private void intFloating(final Activity activity, FrameLayout frameLayout) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(activity).inflate(R.layout.item_kz_ads_floating, frameLayout, false);
        frameLayout.addView(linearLayout);
        final int random = UtilsCombat.getRandom(this.listcontentads.size());
        final LinearLayout linearLayout2 = (LinearLayout) linearLayout.findViewById(R.id.lrContainer);
        linearLayout2.setVisibility(View.INVISIBLE);
        linearLayout2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UtilsCombat.OpenBrower(activity, AdsCompat.this.listcontentads.get(random).getsLink());
            }
        });
        ((TextView) linearLayout.findViewById(R.id.tvTitleAds)).setText(this.listcontentads.get(random).getsTitle());
        final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) linearLayout.findViewById(R.id.shimmer_container);
        shimmerFrameLayout.startShimmer();
        Glide.with(activity.getApplicationContext()).load(this.listcontentads.get(random).getsIcon()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout2.setVisibility(View.VISIBLE);
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout2.setVisibility(View.VISIBLE);
                return false;
            }
        }).into((ImageView) linearLayout.findViewById(R.id.ivIconAds));
    }

    private void intFloatingMore(Activity activity, FrameLayout frameLayout) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(activity).inflate(R.layout.item_kz_ads_floating_more_app, frameLayout, false);
        frameLayout.addView(linearLayout);
        ((LinearLayout) linearLayout.findViewById(R.id.lrButton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdsCompat.this.showStoreAds(new AdCloseListener() {
                    public void onAdClosed() {
                    }
                });
            }
        });
    }

    private void intNativeBanner(FrameLayout frameLayout) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from((Activity) mContext).inflate(R.layout.item_kz_ads_native_banner, frameLayout, false);
        frameLayout.addView(linearLayout);
        final int random = UtilsCombat.getRandom(this.listcontentads.size());
        ((Button) linearLayout.findViewById(R.id.btnOpenLink)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UtilsCombat.OpenBrower(AdsCompat.mContext, AdsCompat.this.listcontentads.get(random).getsLink());
            }
        });
        linearLayout.findViewById(R.id.ivInfoAds).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    new DialogInfoAds(AdsCompat.mContext).show();
                } catch (Exception unused) {
                }
            }
        });
        ((RatingBar) linearLayout.findViewById(R.id.rbRating)).setRating(this.listcontentads.get(random).getRating());
        ((TextView) linearLayout.findViewById(R.id.tvTitleAds)).setText(this.listcontentads.get(random).getsTitle());
        ((TextView) linearLayout.findViewById(R.id.tvAdsDes)).setText(this.listcontentads.get(random).getsDecrition());
        final LinearLayout linearLayout2 = (LinearLayout) linearLayout.findViewById(R.id.lrContainer);
        linearLayout2.setVisibility(View.INVISIBLE);
        final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) linearLayout.findViewById(R.id.shimmer_container);
        shimmerFrameLayout.startShimmer();
        Glide.with(mContext.getApplicationContext()).load(this.listcontentads.get(random).getsIcon()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout2.setVisibility(View.VISIBLE);
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout2.setVisibility(View.VISIBLE);
                return false;
            }
        }).into((ImageView) linearLayout.findViewById(R.id.ivIconAds));
    }

    private void intBanner(FrameLayout frameLayout) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from((Activity) mContext).inflate(R.layout.item_kz_ads_banner, frameLayout, false);
        frameLayout.addView(linearLayout);
        final int random = UtilsCombat.getRandom(this.listcontentads.size());
        ImageView imageView = (ImageView) linearLayout.findViewById(R.id.ivAdsBig);
        ((Button) linearLayout.findViewById(R.id.btnOpenLink)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UtilsCombat.OpenBrower(AdsCompat.mContext, AdsCompat.this.listcontentads.get(random).getsLink());
            }
        });
        ((TextView) linearLayout.findViewById(R.id.tvTitleAds)).setText(this.listcontentads.get(random).getsTitle());
        ((TextView) linearLayout.findViewById(R.id.tvAdsDes)).setText(this.listcontentads.get(random).getsDecrition());
        final RelativeLayout relativeLayout = (RelativeLayout) linearLayout.findViewById(R.id.rlContainer);
        relativeLayout.setVisibility(View.INVISIBLE);
        final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) linearLayout.findViewById(R.id.shimmer_container_50);
        shimmerFrameLayout.startShimmer();
        Glide.with(mContext.getApplicationContext()).load(this.listcontentads.get(random).getsIcon()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                relativeLayout.setVisibility(View.GONE);
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                relativeLayout.setVisibility(View.VISIBLE);
                return false;
            }
        }).into((ImageView) linearLayout.findViewById(R.id.ivIconAds));
    }

    public void intHorizontal(FrameLayout frameLayout) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from((Activity) mContext).inflate(R.layout.item_kz_ads_horizontal_one, frameLayout, false);
        frameLayout.addView(linearLayout);
        linearLayout.findViewById(R.id.ivInfoAds).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    new DialogInfoAds(AdsCompat.mContext).show();
                } catch (Exception unused) {
                }
            }
        });
        RecyclerView recyclerView = (RecyclerView) linearLayout.findViewById(R.id.rvHorizontal);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        recyclerView.setAdapter(new HorizontalAdapter(mContext, this.listHorizontalAds));
    }

    public void addData() {
        Collections.shuffle(this.mListAds);
        for (int i = 0; i < this.mListAds.size(); i++) {
            if (CONTAIN.contentAds != null) {
                if (UtilsCombat.useList(CONTAIN.contentAds, this.mListAds.get(i).getsPackage())) {
                    this.listcontentads.add(this.mListAds.get(i));
                } else {
                    this.list1.add(this.mListAds.get(i));
                }
            }
        }
        ArrayList<CampaignAds> arrayList = this.listcontentads;
        if (arrayList == null || arrayList.size() != 0) {
            this.listHorizontalAds.addAll(this.listcontentads);
            this.listHorizontalAds.addAll(this.list1);
            return;
        }
        this.listcontentads = this.mListAds;
    }
}
