package com.kzvideo2004.sdk.ads.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bzvideo2004.sdk.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;
import com.kzvideo2004.sdk.ads.model.CampaignAds;

import java.util.ArrayList;

import androidx.annotation.Nullable;

public class DialogFullScreen extends Dialog {
    ImageView ivClose;
    ArrayList<CampaignAds> list = new ArrayList<>();
    AdCloseListener mAdCloseListener;
    Context mContext;
    int temp = 0;

    public interface AdCloseListener {
        void onAdClosed();
    }

    public DialogFullScreen(Context context, ArrayList<CampaignAds> arrayList, AdCloseListener adCloseListener) {
        super(context);
        this.mContext = context;
        this.list = arrayList;
        this.mAdCloseListener = adCloseListener;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.dialog_full_screen);
        setCancelable(false);
        this.ivClose = (ImageView) findViewById(R.id.ivClose);
        this.temp = UtilsCombat.getRandom(this.list.size());
        ((RatingBar) findViewById(R.id.rbRating)).setRating(this.list.get(this.temp).getRating());
        findViewById(R.id.ivInfoAds).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    if (!((Activity) DialogFullScreen.this.mContext).isFinishing()) {
                        new DialogInfoAds(DialogFullScreen.this.mContext).show();
                    }
                } catch (Exception unused) {
                }
            }
        });
        ((Button) findViewById(R.id.btnOpenLink)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (DialogFullScreen.this.mAdCloseListener != null) {
                    DialogFullScreen.this.mAdCloseListener.onAdClosed();
                }
                UtilsCombat.OpenBrower(DialogFullScreen.this.mContext, DialogFullScreen.this.list.get(DialogFullScreen.this.temp).getsLink());
            }
        });
        ((TextView) findViewById(R.id.tvTitleAds)).setText(this.list.get(this.temp).getsTitle());
        ((TextView) findViewById(R.id.tvAdsDes)).setText(this.list.get(this.temp).getsDecrition());
        final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lrContainer);
        linearLayout.setVisibility(View.INVISIBLE);
        final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) findViewById(R.id.shimmer_container);
        shimmerFrameLayout.startShimmer();
        Glide.with(this.mContext.getApplicationContext()).load(this.list.get(this.temp).getsIcon()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                return false;
            }
        }).into((ImageView) findViewById(R.id.ivIconAds));
        Glide.with(this.mContext).load(this.list.get(this.temp).getsImage()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
            public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout.setVisibility(View.VISIBLE);
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                shimmerFrameLayout.setVisibility(View.GONE);
                shimmerFrameLayout.stopShimmer();
                linearLayout.setVisibility(View.VISIBLE);
                return false;
            }
        }).into((ImageView) findViewById(R.id.ivAdsBig));
        this.ivClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogFullScreen.this.dismiss();
                if (DialogFullScreen.this.mAdCloseListener != null) {
                    DialogFullScreen.this.mAdCloseListener.onAdClosed();
                }
            }
        });
    }
}
