package com.kzvideo2004.sdk.ads.funtion;

import android.content.Context;
import android.os.AsyncTask;


import com.kzvideo2004.sdk.ads.model.CONTAIN;
import com.kzvideo2004.sdk.ads.model.CampaignAds;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class KzAds {
    private static KzAds instance;
    public static ArrayList<CampaignAds> listCampaign = new ArrayList<>();
    public static Context mContext;
    public static GetAdsData mGetAdsData;
    public static LoadDataListener mLoadDataListener;
    Context context;

    public interface LoadDataListener {
        void OnResult(ArrayList<CampaignAds> arrayList);
    }

    public static KzAds getInstance(Context context2) {
        mContext = context2;
        if (instance == null) {
            instance = new KzAds();
        }
        return instance;
    }

    private KzAds() {
    }

    public void loadData(Context context2, LoadDataListener loadDataListener) {
        try {
            this.context = context2;
            mLoadDataListener = loadDataListener;
            if (UtilsCombat.isConnectionAvailable(context2)) {
                cancelGetAdsData();
                mGetAdsData = new GetAdsData();
                mGetAdsData.execute(new Void[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetAdsData extends AsyncTask<Void, Void, String> {
        private GetAdsData() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... voidArr) {
            JSONObject makeHttpRequest = JSONParser.makeHttpRequest(CONTAIN.apiAds);
            if (makeHttpRequest == null) {
                return null;
            }
            try {
                KzAds.listCampaign = GetCampaign.getListAds(KzAds.this.context, makeHttpRequest);
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            super.onPostExecute(str);
            if (KzAds.mLoadDataListener != null) {
                KzAds.mLoadDataListener.OnResult(KzAds.listCampaign);
            }
        }
    }

    public void cancelGetAdsData() {
        GetAdsData getAdsData = mGetAdsData;
        if (getAdsData != null && getAdsData.getStatus() == AsyncTask.Status.RUNNING) {
            mGetAdsData.cancel(true);
            mGetAdsData = null;
        }
    }
}
