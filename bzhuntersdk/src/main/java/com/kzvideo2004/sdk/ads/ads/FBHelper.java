package com.kzvideo2004.sdk.ads.ads;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bzvideo2004.sdk.R;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdIconView;
import com.facebook.ads.AdListener;
import com.facebook.ads.AdOptionsView;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.MediaView;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.facebook.ads.NativeBannerAd;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.kzvideo2004.sdk.ads.model.CONTAIN;

import java.util.ArrayList;
import java.util.List;

public class FBHelper {
    private static FBHelper instance;
    static Context mContext;
    /* access modifiers changed from: private */
    public AdCloseListener adCloseListener;
    ShimmerFrameLayout containerShimmerBanner;
    AdView fbAdView;
    int flagFB = 0;
    /* access modifiers changed from: private */
    public InterstitialAd interstitialAd;
    /* access modifiers changed from: private */
    public NativeAd nativeAd;
    private NativeAdLayout nativeAdLayout;
    /* access modifiers changed from: private */
    public NativeBannerAd nativeBannerAd;
    private Dialog progressDialog;
    View rootViewNativeFragment;
    /* access modifiers changed from: private */
    public long timeLoad = 0;

    public interface AdCloseListener {
        void onAdClosed();
    }

    public static FBHelper getInstance(Context context) {
        mContext = context;
        if (instance == null) {
            instance = new FBHelper();
        }
        return instance;
    }

    private FBHelper() {
    }

    public void initFull() {
        if (!CONTAIN.FB_ID_FULL.equals("")) {
            InterstitialAd interstitialAd2 = this.interstitialAd;
            if (interstitialAd2 != null) {
                interstitialAd2.destroy();
            }
            this.interstitialAd = new InterstitialAd(mContext, CONTAIN.FB_ID_FULL);
            this.interstitialAd.setAdListener(new InterstitialAdListener() {
                public void onInterstitialDisplayed(Ad ad) {
                    Log.e(CONTAIN.TAG, "Interstitial ad displayed.");
                }

                public void onInterstitialDismissed(Ad ad) {
                    if (FBHelper.this.adCloseListener != null) {
                        FBHelper.this.adCloseListener.onAdClosed();
                        if (CONTAIN.CountCurrent < CONTAIN.ShowMax) {
                            FBHelper.this.interstitialAd.loadAd();
                            CONTAIN.CountCurrent++;
                        }
                    }
                }

                public void onError(Ad ad, AdError adError) {
                    String str = CONTAIN.TAG;
                    Log.e(str, "Interstitial ad failed to load: " + adError.getErrorMessage());
                    if (CONTAIN.CountCurrent < CONTAIN.ShowMax && ((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                        AdsHelp.getInstance(FBHelper.mContext).initFull();
                    }
                }

                public void onAdLoaded(Ad ad) {
                    Log.d(CONTAIN.TAG, "Interstitial onAdLoaded!");
                }

                public void onAdClicked(Ad ad) {
                    Log.d(CONTAIN.TAG, "Interstitial ad clicked!");
                }

                public void onLoggingImpression(Ad ad) {
                    Log.d(CONTAIN.TAG, "Interstitial ad impression logged!");
                }
            });
            this.interstitialAd.loadAd();
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null && !((Activity) context).isFinishing()) {
            AdsHelp.getInstance(mContext).initFull();
        }
    }

    public void showInterstitialAd(final AdCloseListener adCloseListener2) {
        this.adCloseListener = adCloseListener2;
        if (this.timeLoad + CONTAIN.TimeReload >= System.currentTimeMillis()) {
            adCloseListener2.onAdClosed();
        } else if (canShowInterstitialAd()) {
            this.interstitialAd.show();
            this.timeLoad = System.currentTimeMillis();
        } else {
            AdsHelp.getInstance(mContext).showInterstitialAd(new AdsHelp.AdCloseListener() {
                public void onAdClosed() {
                    adCloseListener2.onAdClosed();
                    long unused = FBHelper.this.timeLoad = System.currentTimeMillis();
                }
            });
        }
    }

    private boolean canShowInterstitialAd() {
        InterstitialAd interstitialAd2 = this.interstitialAd;
        return interstitialAd2 != null && interstitialAd2.isAdLoaded();
    }

    public void loadBanner() {
        if (!CONTAIN.FB_ID_BANNER.equals("")) {
            AdView adView = this.fbAdView;
            if (adView != null) {
                adView.destroy();
            }
            if (CONTAIN.styleBanner == 0) {
                this.containerShimmerBanner = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container_50);
                this.fbAdView = new AdView(mContext, CONTAIN.FB_ID_BANNER, AdSize.BANNER_HEIGHT_50);
            } else {
                this.containerShimmerBanner = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container_100);
                this.fbAdView = new AdView(mContext, CONTAIN.FB_ID_BANNER, AdSize.BANNER_HEIGHT_90);
            }
            this.containerShimmerBanner.setVisibility(View.VISIBLE);
            this.containerShimmerBanner.startShimmer();
            LinearLayout linearLayout = (LinearLayout) ((Activity) mContext).findViewById(R.id.banner_container);
            linearLayout.setVisibility(View.GONE);
            linearLayout.addView(this.fbAdView);
            this.fbAdView.setAdListener(new AdListener() {
                public void onAdClicked(Ad ad) {
                }

                public void onLoggingImpression(Ad ad) {
                }

                public void onError(Ad ad, AdError adError) {
                    String str = CONTAIN.TAG;
                    Log.e(str, "FB Banner ad failed to load: " + adError.getErrorMessage());
                    FBHelper.this.containerShimmerBanner.stopShimmer();
                    FBHelper.this.containerShimmerBanner.setVisibility(View.GONE);
                    if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                        ((Activity) FBHelper.mContext).findViewById(R.id.banner_container).setVisibility(View.GONE);
                        AdsHelp.getInstance(FBHelper.mContext).loadBanner();
                    }
                }

                public void onAdLoaded(Ad ad) {
                    Log.d(CONTAIN.TAG, "FB Banner ad is loaded and ready to be displayed");
                    FBHelper.this.containerShimmerBanner.stopShimmer();
                    FBHelper.this.containerShimmerBanner.setVisibility(View.GONE);
                    if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                        ((Activity) FBHelper.mContext).findViewById(R.id.banner_container).setVisibility(View.VISIBLE);
                        ((Activity) FBHelper.mContext).findViewById(R.id.spAds).setVisibility(View.VISIBLE);
                    }
                }
            });
            this.fbAdView.loadAd();
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null && !((Activity) context).isFinishing()) {
            AdsHelp.getInstance(mContext).loadBanner();
        }
    }

    public void loadBannerFragment(final View view) {
        if (!CONTAIN.FB_ID_BANNER.equals("")) {
            if (CONTAIN.styleBanner == 0) {
                this.containerShimmerBanner = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container_50);
                this.fbAdView = new AdView(mContext, CONTAIN.FB_ID_BANNER, AdSize.BANNER_HEIGHT_50);
            } else {
                this.containerShimmerBanner = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container_100);
                this.fbAdView = new AdView(mContext, CONTAIN.FB_ID_BANNER, AdSize.BANNER_HEIGHT_90);
            }
            this.containerShimmerBanner.setVisibility(View.VISIBLE);
            this.containerShimmerBanner.startShimmer();
            AdView adView = this.fbAdView;
            if (adView != null) {
                adView.destroy();
            }
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.banner_container);
            linearLayout.setVisibility(View.GONE);
            linearLayout.addView(this.fbAdView);
            this.fbAdView.setAdListener(new AdListener() {
                public void onAdClicked(Ad ad) {
                }

                public void onLoggingImpression(Ad ad) {
                }

                public void onError(Ad ad, AdError adError) {
                    String str = CONTAIN.TAG;
                    Log.e(str, "FB Banner ad failed to load: " + adError.getErrorMessage());
                    FBHelper.this.containerShimmerBanner.stopShimmer();
                    FBHelper.this.containerShimmerBanner.setVisibility(View.GONE);
                    view.findViewById(R.id.banner_container).setVisibility(View.GONE);
                    if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                        AdsHelp.getInstance(FBHelper.mContext).loadBannerFragment(view);
                    }
                }

                public void onAdLoaded(Ad ad) {
                    Log.d(CONTAIN.TAG, "FB Banner ad is loaded and ready to be displayed");
                    FBHelper.this.containerShimmerBanner.stopShimmer();
                    FBHelper.this.containerShimmerBanner.setVisibility(View.GONE);
                    view.findViewById(R.id.banner_container).setVisibility(View.VISIBLE);
                    if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                        ((Activity) FBHelper.mContext).findViewById(R.id.spAds).setVisibility(View.VISIBLE);
                    }
                }
            });
            this.fbAdView.loadAd();
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null && !((Activity) context).isFinishing()) {
            AdsHelp.getInstance(mContext).loadBannerFragment(view);
        }
    }

    public void loadNativeFragment(final View view) {
        if (!CONTAIN.FB_ID_NATIVE.equals("")) {
            NativeAd nativeAd2 = this.nativeAd;
            if (nativeAd2 != null) {
                nativeAd2.destroy();
            }
            this.rootViewNativeFragment = view;
            final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_container);
            final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.native_ad_container);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();
            this.nativeAd = new NativeAd(mContext, CONTAIN.FB_ID_NATIVE);
            this.nativeAd.setAdListener(new NativeAdListener() {
                public void onMediaDownloaded(Ad ad) {
                    Log.e(CONTAIN.TAG, "FB Native ad finished downloading all assets.");
                }

                public void onError(Ad ad, AdError adError) {
                    String str = CONTAIN.TAG;
                    Log.e(str, "FB Native ad failed to load: " + adError.getErrorMessage());
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    frameLayout.setVisibility(View.GONE);
                    if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                        AdsHelp.getInstance(FBHelper.mContext).loadNativeFragment(FBHelper.this.rootViewNativeFragment);
                    }
                }

                public void onAdLoaded(Ad ad) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (FBHelper.this.nativeAd == null || FBHelper.this.nativeAd != ad) {
                        frameLayout.setVisibility(View.GONE);
                        return;
                    }
                    try {
                        if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                            FBHelper.this.inflateAdFrament(FBHelper.this.nativeAd, view);
                            frameLayout.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception unused) {
                    }
                    Log.d(CONTAIN.TAG, "FB Native ad is loaded and ready to be displayed!");
                }

                public void onAdClicked(Ad ad) {
                    Log.d(CONTAIN.TAG, "FB Native ad clicked!");
                }

                public void onLoggingImpression(Ad ad) {
                    Log.d(CONTAIN.TAG, "FB Native ad impression logged!");
                }
            });
            this.nativeAd.loadAd();
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null && !((Activity) context).isFinishing()) {
            AdsHelp.getInstance(mContext).loadNativeFragment(view);
        }
    }

    public void loadNative() {
        if (!CONTAIN.FB_ID_NATIVE.equals("")) {
            NativeAd nativeAd2 = this.nativeAd;
            if (nativeAd2 != null) {
                nativeAd2.destroy();
            }
            final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container);
            final FrameLayout frameLayout = (FrameLayout) ((Activity) mContext).findViewById(R.id.native_ad_container);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();
            this.nativeAd = new NativeAd(mContext, CONTAIN.FB_ID_NATIVE);
            this.nativeAd.setAdListener(new NativeAdListener() {
                public void onMediaDownloaded(Ad ad) {
                    Log.e(CONTAIN.TAG, "FB Native ad finished downloading all assets.");
                }

                public void onError(Ad ad, AdError adError) {
                    String str = CONTAIN.TAG;
                    Log.e(str, "FB Native ad failed to load: " + adError.getErrorMessage());
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    frameLayout.setVisibility(View.GONE);
                    try {
                        if (((Activity) FBHelper.mContext) != null) {
                            AdsHelp.getInstance(FBHelper.mContext).loadNative();
                        }
                    } catch (Exception unused) {
                    }
                }

                public void onAdLoaded(Ad ad) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (FBHelper.this.nativeAd != null && FBHelper.this.nativeAd == ad) {
                        try {
                            if (((Activity) FBHelper.mContext) != null) {
                                FBHelper.this.inflateAd(FBHelper.this.nativeAd);
                                frameLayout.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception unused) {
                        }
                        Log.d(CONTAIN.TAG, "FB Native ad is loaded and ready to be displayed!");
                    }
                }

                public void onAdClicked(Ad ad) {
                    Log.d(CONTAIN.TAG, "FB Native ad clicked!");
                }

                public void onLoggingImpression(Ad ad) {
                    Log.d(CONTAIN.TAG, "FB Native ad impression logged!");
                }
            });
            this.nativeAd.loadAd();
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null) {
            AdsHelp.getInstance(context).loadNative();
        }
    }

    public void loadNativeBanner() {
        if (!CONTAIN.FB_ID_NATIVE_BANNER.equals("")) {
            NativeBannerAd nativeBannerAd2 = this.nativeBannerAd;
            if (nativeBannerAd2 != null) {
                nativeBannerAd2.destroy();
            }
            final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container);
            final FrameLayout frameLayout = (FrameLayout) ((Activity) mContext).findViewById(R.id.native_banner_ad_container);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();
            this.nativeBannerAd = new NativeBannerAd(mContext, CONTAIN.FB_ID_NATIVE_BANNER);
            this.nativeBannerAd.setAdListener(new NativeAdListener() {
                public void onMediaDownloaded(Ad ad) {
                    Log.e(CONTAIN.TAG, "NativeBanner ad finished downloading all assets.");
                }

                public void onError(Ad ad, AdError adError) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    frameLayout.setVisibility(View.GONE);
                    if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                        AdsHelp.getInstance(FBHelper.mContext).loadNativeBanner();
                        Log.e(CONTAIN.TAG, "NativeBanner ad failed to load");
                    }
                }

                public void onAdLoaded(Ad ad) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (FBHelper.this.nativeBannerAd != null && FBHelper.this.nativeBannerAd == ad) {
                        try {
                            if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                                FBHelper.this.inflateNativeBannerAd(FBHelper.this.nativeBannerAd);
                                frameLayout.setVisibility(View.VISIBLE);
                            }
                            Log.d(CONTAIN.TAG, "NativeBanner ad is loaded and ready to be displayed!");
                        } catch (Exception unused) {
                        }
                    }
                }

                public void onAdClicked(Ad ad) {
                    Log.d(CONTAIN.TAG, "NativeBanner ad clicked!");
                }

                public void onLoggingImpression(Ad ad) {
                    Log.d(CONTAIN.TAG, "NativeBanner ad impression logged!");
                }
            });
            this.nativeBannerAd.loadAd();
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null) {
            AdsHelp.getInstance(context).loadNativeBanner();
        }
    }

    public void loadNativeBannerFragment(final View view) {
        if (!CONTAIN.FB_ID_NATIVE_BANNER.equals("")) {
            NativeBannerAd nativeBannerAd2 = this.nativeBannerAd;
            if (nativeBannerAd2 != null) {
                nativeBannerAd2.destroy();
            }
            final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_container);
            final FrameLayout frameLayout = (FrameLayout) view.findViewById(R.id.native_banner_ad_container);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();
            this.nativeBannerAd = new NativeBannerAd(mContext, CONTAIN.FB_ID_NATIVE_BANNER);
            this.nativeBannerAd.setAdListener(new NativeAdListener() {
                public void onMediaDownloaded(Ad ad) {
                    Log.e(CONTAIN.TAG, "NativeBanner ad finished downloading all assets.");
                }

                public void onError(Ad ad, AdError adError) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    frameLayout.setVisibility(View.GONE);
                    if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                        AdsHelp.getInstance(FBHelper.mContext).loadNativeBannerFragment(view);
                    }
                }

                public void onAdLoaded(Ad ad) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (FBHelper.this.nativeBannerAd != null && FBHelper.this.nativeBannerAd == ad) {
                        try {
                            if (((Activity) FBHelper.mContext) != null && !((Activity) FBHelper.mContext).isFinishing()) {
                                FBHelper.this.inflateNativeBannerAdFragment(FBHelper.this.nativeBannerAd, view);
                                frameLayout.setVisibility(View.VISIBLE);
                                Log.d(CONTAIN.TAG, "NativeBanner ad is loaded and ready to be displayed!");
                            }
                        } catch (Exception unused) {
                        }
                        Log.d(CONTAIN.TAG, "NativeBanner ad is loaded and ready to be displayed!");
                    }
                }

                public void onAdClicked(Ad ad) {
                    Log.d(CONTAIN.TAG, "NativeBanner ad clicked!");
                }

                public void onLoggingImpression(Ad ad) {
                    Log.d(CONTAIN.TAG, "NativeBanner ad impression logged!");
                }
            });
            this.nativeBannerAd.loadAd();
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null && !((Activity) context).isFinishing()) {
            AdsHelp.getInstance(mContext).loadNativeBannerFragment(view);
        }
    }

    /* access modifiers changed from: private */
    public void inflateAd(NativeAd nativeAd2) {
        nativeAd2.unregisterView();
        this.nativeAdLayout = (NativeAdLayout) ((Activity) mContext).findViewById(R.id.native_ad_container);
        int i = 0;
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from((Activity) mContext).inflate(R.layout.item_fb_ads_native, this.nativeAdLayout, false);
        this.nativeAdLayout.addView(linearLayout);
        LinearLayout linearLayout2 = (LinearLayout) ((Activity) mContext).findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView((Activity) mContext, nativeAd2, this.nativeAdLayout);
        linearLayout2.removeAllViews();
        linearLayout2.addView(adOptionsView, 0);
        AdIconView adIconView = (AdIconView) linearLayout.findViewById(R.id.native_ad_icon);
        TextView textView = (TextView) linearLayout.findViewById(R.id.native_ad_title);
        MediaView mediaView = (MediaView) linearLayout.findViewById(R.id.native_ad_media);
        TextView textView2 = (TextView) linearLayout.findViewById(R.id.native_ad_sponsored_label);
        Button button = (Button) linearLayout.findViewById(R.id.native_ad_call_to_action);
        textView.setText(nativeAd2.getAdvertiserName());
        ((TextView) linearLayout.findViewById(R.id.native_ad_body)).setText(nativeAd2.getAdBodyText());
        ((TextView) linearLayout.findViewById(R.id.native_ad_social_context)).setText(nativeAd2.getAdSocialContext());
        if (!nativeAd2.hasCallToAction()) {
            i = 8;
        }
        button.setVisibility(i);
        button.setText(nativeAd2.getAdCallToAction());
        textView2.setText(nativeAd2.getSponsoredTranslation());
        ArrayList arrayList = new ArrayList();
        arrayList.add(textView);
        arrayList.add(button);
        nativeAd2.registerViewForInteraction((View) linearLayout, mediaView, (MediaView) adIconView, (List<View>) arrayList);
    }

    /* access modifiers changed from: private */
    public void inflateAdFrament(NativeAd nativeAd2, View view) {
        nativeAd2.unregisterView();
        this.nativeAdLayout = (NativeAdLayout) view.findViewById(R.id.native_ad_container);
        int i = 0;
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from((Activity) mContext).inflate(R.layout.item_fb_ads_native, this.nativeAdLayout, false);
        this.nativeAdLayout.addView(linearLayout);
        LinearLayout linearLayout2 = (LinearLayout) ((Activity) mContext).findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView((Activity) mContext, nativeAd2, this.nativeAdLayout);
        linearLayout2.removeAllViews();
        linearLayout2.addView(adOptionsView, 0);
        AdIconView adIconView = (AdIconView) linearLayout.findViewById(R.id.native_ad_icon);
        TextView textView = (TextView) linearLayout.findViewById(R.id.native_ad_title);
        MediaView mediaView = (MediaView) linearLayout.findViewById(R.id.native_ad_media);
        TextView textView2 = (TextView) linearLayout.findViewById(R.id.native_ad_sponsored_label);
        Button button = (Button) linearLayout.findViewById(R.id.native_ad_call_to_action);
        textView.setText(nativeAd2.getAdvertiserName());
        ((TextView) linearLayout.findViewById(R.id.native_ad_body)).setText(nativeAd2.getAdBodyText());
        ((TextView) linearLayout.findViewById(R.id.native_ad_social_context)).setText(nativeAd2.getAdSocialContext());
        if (!nativeAd2.hasCallToAction()) {
            i = 8;
        }
        button.setVisibility(i);
        button.setText(nativeAd2.getAdCallToAction());
        textView2.setText(nativeAd2.getSponsoredTranslation());
        ArrayList arrayList = new ArrayList();
        arrayList.add(textView);
        arrayList.add(button);
        nativeAd2.registerViewForInteraction((View) linearLayout, mediaView, (MediaView) adIconView, (List<View>) arrayList);
    }

    /* access modifiers changed from: private */
    public void inflateNativeBannerAd(NativeBannerAd nativeBannerAd2) {
        nativeBannerAd2.unregisterView();
        this.nativeAdLayout = (NativeAdLayout) ((Activity) mContext).findViewById(R.id.native_banner_ad_container);
        int i = 0;
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from((Activity) mContext).inflate(R.layout.item_fb_ads_native_banner, this.nativeAdLayout, false);
        this.nativeAdLayout.addView(linearLayout);
        RelativeLayout relativeLayout = (RelativeLayout) linearLayout.findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView((Activity) mContext, nativeBannerAd2, this.nativeAdLayout);
        relativeLayout.removeAllViews();
        relativeLayout.addView(adOptionsView, 0);
        TextView textView = (TextView) linearLayout.findViewById(R.id.native_ad_title);
        TextView textView2 = (TextView) linearLayout.findViewById(R.id.native_ad_social_context);
        TextView textView3 = (TextView) linearLayout.findViewById(R.id.native_ad_sponsored_label);
        AdIconView adIconView = (AdIconView) linearLayout.findViewById(R.id.native_icon_view);
        Button button = (Button) linearLayout.findViewById(R.id.native_ad_call_to_action);
        button.setText(nativeBannerAd2.getAdCallToAction());
        if (!nativeBannerAd2.hasCallToAction()) {
            i = 8;
        }
        button.setVisibility(i);
        textView.setText(nativeBannerAd2.getAdvertiserName());
        textView2.setText(nativeBannerAd2.getAdSocialContext());
        textView3.setText(nativeBannerAd2.getSponsoredTranslation());
        ArrayList arrayList = new ArrayList();
        arrayList.add(textView);
        arrayList.add(button);
        nativeBannerAd2.registerViewForInteraction((View) linearLayout, (MediaView) adIconView, (List<View>) arrayList);
    }

    /* access modifiers changed from: private */
    public void inflateNativeBannerAdFragment(NativeBannerAd nativeBannerAd2, View view) {
        nativeBannerAd2.unregisterView();
        this.nativeAdLayout = (NativeAdLayout) view.findViewById(R.id.native_banner_ad_container);
        int i = 0;
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from((Activity) mContext).inflate(R.layout.item_fb_ads_native_banner, this.nativeAdLayout, false);
        this.nativeAdLayout.addView(linearLayout);
        RelativeLayout relativeLayout = (RelativeLayout) linearLayout.findViewById(R.id.ad_choices_container);
        AdOptionsView adOptionsView = new AdOptionsView((Activity) mContext, nativeBannerAd2, this.nativeAdLayout);
        relativeLayout.removeAllViews();
        relativeLayout.addView(adOptionsView, 0);
        TextView textView = (TextView) linearLayout.findViewById(R.id.native_ad_title);
        TextView textView2 = (TextView) linearLayout.findViewById(R.id.native_ad_social_context);
        TextView textView3 = (TextView) linearLayout.findViewById(R.id.native_ad_sponsored_label);
        AdIconView adIconView = (AdIconView) linearLayout.findViewById(R.id.native_icon_view);
        Button button = (Button) linearLayout.findViewById(R.id.native_ad_call_to_action);
        button.setText(nativeBannerAd2.getAdCallToAction());
        if (!nativeBannerAd2.hasCallToAction()) {
            i = 8;
        }
        button.setVisibility(i);
        textView.setText(nativeBannerAd2.getAdvertiserName());
        textView2.setText(nativeBannerAd2.getAdSocialContext());
        textView3.setText(nativeBannerAd2.getSponsoredTranslation());
        ArrayList arrayList = new ArrayList();
        arrayList.add(textView);
        arrayList.add(button);
        nativeBannerAd2.registerViewForInteraction((View) linearLayout, (MediaView) adIconView, (List<View>) arrayList);
    }
}
