package com.kzvideo2004.sdk.ads.funtion;

import android.content.Context;
import android.preference.PreferenceManager;

import com.kzvideo2004.sdk.ads.model.CampaignAds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class GetCampaign {
    public static ArrayList<CampaignAds> getListAds(Context context, JSONObject jSONObject) throws JSONException {
        PreferenceManager.getDefaultSharedPreferences(context).getString("lgAds", "en");
        return getHome(jSONObject);
    }

    public static ArrayList<CampaignAds> getHome(JSONObject jSONObject) throws JSONException {
        ArrayList<CampaignAds> arrayList = new ArrayList<>();
        JSONArray jSONArray = jSONObject.getJSONObject("data").getJSONArray("pushs");
        String language = Locale.getDefault().getLanguage();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            CampaignAds campaignAds = new CampaignAds();
            campaignAds.setsPackage(jSONObject2.getString("packNames"));
            campaignAds.setsDecrition(UtilsCombat.getTextLanguage(jSONObject2.getString("decription"), language));
            campaignAds.setsImage(UtilsCombat.getTextLanguage(jSONObject2.getString("image"), language));
            campaignAds.setRating(UtilsCombat.getRating(jSONObject2.getString("image_vn")));
            campaignAds.setsTitle(UtilsCombat.getTextLanguage(jSONObject2.getString("title"), language));
            campaignAds.setsIcon(UtilsCombat.getTextLanguage(jSONObject2.getString("icon"), language));
            campaignAds.setType(jSONObject2.getInt("type"));
            campaignAds.setsLink(jSONObject2.getString("link"));
            campaignAds.setShowRate(jSONObject2.getBoolean("report"));
            campaignAds.setActive(jSONObject2.getBoolean("Active"));
            campaignAds.setAdsInfo(jSONObject2.getString("title_vn"));
            arrayList.add(campaignAds);
        }
        return arrayList;
    }
}
