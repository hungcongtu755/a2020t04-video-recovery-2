package com.kzvideo2004.sdk.ads.model;

public class CampaignAds {
    private boolean Active;
    private String Ads_info;
    private boolean ShowRate;
    private int iType;
    private int postion;
    private float rating;
    private String sDecrition;
    private String sIcon;
    private String sImage;
    private String sLink;
    private String sPackage;
    private String sTitle;

    public String getAdsInfo() {
        return this.Ads_info;
    }

    public void setAdsInfo(String str) {
        this.Ads_info = str;
    }

    public boolean getActive() {
        return this.Active;
    }

    public void setActive(boolean z) {
        this.Active = z;
    }

    public boolean getShowRate() {
        return this.ShowRate;
    }

    public void setShowRate(boolean z) {
        this.ShowRate = z;
    }

    public String getsPackage() {
        return this.sPackage;
    }

    public void setsPackage(String str) {
        this.sPackage = str;
    }

    public String getsImage() {
        return this.sImage;
    }

    public void setsImage(String str) {
        this.sImage = str;
    }

    public String getsTitle() {
        return this.sTitle;
    }

    public void setsTitle(String str) {
        this.sTitle = str;
    }

    public String getsDecrition() {
        return this.sDecrition;
    }

    public void setsDecrition(String str) {
        this.sDecrition = str;
    }

    public String getsLink() {
        return this.sLink;
    }

    public void setsLink(String str) {
        this.sLink = str;
    }

    public String getsIcon() {
        return this.sIcon;
    }

    public void setsIcon(String str) {
        this.sIcon = str;
    }

    public int getType() {
        return this.iType;
    }

    public void setType(int i) {
        this.iType = i;
    }

    public int getPostion() {
        return this.postion;
    }

    public void setPostion(int i) {
        this.postion = i;
    }

    public float getRating() {
        return this.rating;
    }

    public void setRating(float f) {
        this.rating = f;
    }


}
