package com.kzvideo2004.sdk.ads.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bzvideo2004.sdk.R;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;
import com.kzvideo2004.sdk.ads.model.CampaignAds;

import java.util.ArrayList;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.SingleItemRowHolder> {
    private ArrayList<CampaignAds> itemsList;
    public Context mContext;

    public HorizontalAdapter(Context context, ArrayList<CampaignAds> arrayList) {
        this.itemsList = arrayList;
        this.mContext = context;
    }

    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new SingleItemRowHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_kz_ads_horizontal, viewGroup, false));
    }

    public void onBindViewHolder(SingleItemRowHolder singleItemRowHolder, int i) {
        final CampaignAds campaignAds = this.itemsList.get(i);
        try {
            ((RequestBuilder) ((RequestBuilder) ((RequestBuilder) Glide.with(this.mContext).load(campaignAds.getsIcon()).diskCacheStrategy(DiskCacheStrategy.ALL)).priority(Priority.HIGH)).centerCrop()).into(singleItemRowHolder.itemImage);
        } catch (Exception e) {
            Context context = this.mContext;
            Toast.makeText(context, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        singleItemRowHolder.tvTitleAds.setText(campaignAds.getsTitle());
        singleItemRowHolder.rbRating.setRating(campaignAds.getRating());
        singleItemRowHolder.cvAds.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UtilsCombat.OpenBrower(HorizontalAdapter.this.mContext, campaignAds.getsLink());
            }
        });
        singleItemRowHolder.btnOpenLink.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UtilsCombat.OpenBrower(HorizontalAdapter.this.mContext, campaignAds.getsLink());
            }
        });
    }

    public int getItemCount() {
        return this.itemsList.size();
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {
        protected Button btnOpenLink;
        protected CardView cvAds;
        protected ImageView itemImage;
        protected RatingBar rbRating;
        protected TextView tvTitleAds;

        public SingleItemRowHolder(View view) {
            super(view);
            this.itemImage = (ImageView) view.findViewById(R.id.ivImage);
            this.tvTitleAds = (TextView) view.findViewById(R.id.tvTitleAds);
            this.rbRating = (RatingBar) view.findViewById(R.id.rbRating);
            this.btnOpenLink = (Button) view.findViewById(R.id.btnOpenLink);
            this.cvAds = (CardView) view.findViewById(R.id.cvAds);
        }
    }
}
