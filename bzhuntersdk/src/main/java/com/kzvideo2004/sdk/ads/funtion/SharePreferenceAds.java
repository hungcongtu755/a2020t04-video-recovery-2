package com.kzvideo2004.sdk.ads.funtion;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.ads.AdRequest;
import com.kzvideo2004.sdk.ads.model.CONTAIN;

public class SharePreferenceAds {
    private static SharePreferenceAds instance;
    private SharedPreferences.Editor editor = this.pre.edit();
    private Context mContext;
    private SharedPreferences pre;

    public interface AdCloseRewardListener {
        void onCanShow();

        void onNotShow();
    }

    private SharePreferenceAds(Context context) {
        this.mContext = context;
        this.pre = context.getSharedPreferences(AdRequest.LOGTAG, 4);
    }

    public static SharePreferenceAds getInstance(Context context) {
        if (instance == null) {
            instance = new SharePreferenceAds(context);
        }
        return instance;
    }

    public long getRewardTime(String str) {
        return this.pre.getLong(str, 0);
    }

    public void setRewardTime(String str) {
        this.editor.putLong(str, System.currentTimeMillis());
        this.editor.commit();
    }

    public boolean checkStatusAds(String str) {
        return getInstance(this.mContext).getRewardTime(str) + CONTAIN.TIMESHOWREWARD < System.currentTimeMillis();
    }

    public void checkShowAds(String str, AdCloseRewardListener adCloseRewardListener) {
        if (checkStatusAds(str)) {
            adCloseRewardListener.onCanShow();
        } else {
            adCloseRewardListener.onNotShow();
        }
    }
}
