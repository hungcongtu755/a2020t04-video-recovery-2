package com.kzvideo2004.sdk.ads.dialog;
import android.view.View;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.bzvideo2004.sdk.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.tabs.TabLayout;
import com.kzvideo2004.sdk.ads.adapter.CusViewPager;
import com.kzvideo2004.sdk.ads.adapter.GridAdsAdapter;
import com.kzvideo2004.sdk.ads.adapter.HorizontalAdapter;
import com.kzvideo2004.sdk.ads.adapter.VPFullScreenAdapter;
import com.kzvideo2004.sdk.ads.funtion.UtilsCombat;
import com.kzvideo2004.sdk.ads.model.CONTAIN;
import com.kzvideo2004.sdk.ads.model.CampaignAds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class DialogStoreAds extends Dialog {
    TabLayout indicator;
    ImageView ivClose;
    ArrayList<CampaignAds> list = new ArrayList<>();
    ArrayList<CampaignAds> list2 = new ArrayList<>();
    ArrayList<CampaignAds> list3 = new ArrayList<>();
    ArrayList<CampaignAds> list4 = new ArrayList<>();
    ArrayList<CampaignAds> listcontentads = new ArrayList<>();
    AdCloseListener mAdCloseListener;
    Context mContext;
    RecyclerView rv4;
    RecyclerView rvHorizontal;
    CusViewPager viewPager;

    public interface AdCloseListener {
        void onAdClosed();
    }

    public DialogStoreAds(Context context, ArrayList<CampaignAds> arrayList, AdCloseListener adCloseListener) {
        super(context, (int)16974125);
        this.mContext = context;
        this.list = arrayList;
        this.mAdCloseListener = adCloseListener;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.dialog_market_ads_vp);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.main_activity_root);
        addData();
        intView();
        intEvent();
        intData();
        intAds3();
        findViewById(R.id.ivInfoAds).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!((Activity) DialogStoreAds.this.mContext).isFinishing()) {
                    new DialogInfoAds(DialogStoreAds.this.mContext).show();
                }
            }
        });
    }

    public void intView() {
        this.viewPager = (CusViewPager) findViewById(R.id.viewPager);
        this.indicator = (TabLayout) findViewById(R.id.indicator);
        this.ivClose = (ImageView) findViewById(R.id.ivClose);
        this.rvHorizontal = (RecyclerView) findViewById(R.id.rvHorizontal);
        this.rv4 = (RecyclerView) findViewById(R.id.rv4);
    }

    public void intEvent() {
        this.ivClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogStoreAds.this.dismiss();
                if (DialogStoreAds.this.mAdCloseListener != null) {
                    DialogStoreAds.this.mAdCloseListener.onAdClosed();
                }
            }
        });
    }

    public void intData() {
        if (this.listcontentads.size() > 0) {
            findViewById(R.id.cvAds1).setVisibility(View.VISIBLE);
            this.viewPager.setAdapter(new VPFullScreenAdapter(this.mContext, this.listcontentads, new VPFullScreenAdapter.AdClickListener() {
                public void onAdClicked() {
                    if (DialogStoreAds.this.mAdCloseListener != null) {
                        DialogStoreAds.this.mAdCloseListener.onAdClosed();
                    }
                }
            }));
            this.indicator.setupWithViewPager(this.viewPager, true);
            new Timer().scheduleAtFixedRate(new SliderTimer(), 60000, 6000);
        } else {
            findViewById(R.id.cvAds1).setVisibility(View.GONE);
        }
        if (this.list2.size() > 0) {
            findViewById(R.id.lrAds2).setVisibility(View.VISIBLE);
            this.rvHorizontal.setHasFixedSize(true);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.mContext, (int)0, false);
            if (this.mContext.getResources().getBoolean(R.bool.is_right_to_left)) {
                linearLayoutManager.setStackFromEnd(true);
                linearLayoutManager.setReverseLayout(true);
            }
            this.rvHorizontal.setLayoutManager(linearLayoutManager);
            this.rvHorizontal.setAdapter(new HorizontalAdapter(this.mContext, this.list2));
        } else {
            findViewById(R.id.lrAds2).setVisibility(View.GONE);
        }
        if (this.list4.size() > 0) {
            findViewById(R.id.lrAds4).setVisibility(View.VISIBLE);
            this.rv4.setLayoutManager(new GridLayoutManager(this.mContext, 3));
            this.rv4.setAdapter(new GridAdsAdapter(this.mContext, this.list4));
            return;
        }
        findViewById(R.id.lrAds4).setVisibility(View.GONE);
    }

    private class SliderTimer extends TimerTask {
        private SliderTimer() {
        }

        public void run() {
            ((Activity) DialogStoreAds.this.mContext).runOnUiThread(new Runnable() {
                public void run() {
                    if (DialogStoreAds.this.viewPager.getCurrentItem() < DialogStoreAds.this.list.size() - 1) {
                        DialogStoreAds.this.viewPager.setCurrentItem(DialogStoreAds.this.viewPager.getCurrentItem() + 1);
                    } else {
                        DialogStoreAds.this.viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }

    public void addData() {
        Collections.shuffle(this.list);
        int i = 0;
        for (int i2 = 0; i2 < this.list.size(); i2++) {
            if (CONTAIN.contentAds != null) {
                if (UtilsCombat.useList(CONTAIN.contentAds, this.list.get(i2).getsPackage())) {
                    this.listcontentads.add(this.list.get(i2));
                } else if (i < 3) {
                    this.list2.add(this.list.get(i2));
                } else if (i == 3) {
                    this.list3.add(this.list.get(i2));
                } else {
                    this.list4.add(this.list.get(i2));
                }
            } else if (i < 3) {
                this.list2.add(this.list.get(i2));
            } else if (i == 3) {
                this.list3.add(this.list.get(i2));
            } else {
                this.list4.add(this.list.get(i2));
            }
            i++;
        }
    }

    public void intAds3() {
        ImageView imageView = (ImageView) findViewById(R.id.ivIconAds);
        TextView textView = (TextView) findViewById(R.id.tvTitleAds);
        TextView textView2 = (TextView) findViewById(R.id.tvAdsDes);
        Button button = (Button) findViewById(R.id.btnOpenLink);
        RatingBar ratingBar = (RatingBar) findViewById(R.id.rbRating);
        if (this.list3.size() > 0) {
            final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lrContainer);
            linearLayout.setVisibility(View.INVISIBLE);
            final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) findViewById(R.id.shimmer_container);
            shimmerFrameLayout.startShimmer();
            findViewById(R.id.lrAds3).setVisibility(View.VISIBLE);
            int random = UtilsCombat.getRandom(this.list3.size());
            final CampaignAds campaignAds = this.list3.get(random);
            try {
                Glide.with(this.mContext.getApplicationContext()).load(this.list3.get(random).getsIcon()).apply((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().centerCrop()).skipMemoryCache(true)).priority(Priority.HIGH)).dontAnimate()).dontTransform()).listener(new RequestListener<Drawable>() {
                    public boolean onLoadFailed(@Nullable GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                        shimmerFrameLayout.setVisibility(View.GONE);
                        shimmerFrameLayout.stopShimmer();
                        linearLayout.setVisibility(View.VISIBLE);
                        return false;
                    }

                    public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                        shimmerFrameLayout.setVisibility(View.GONE);
                        shimmerFrameLayout.stopShimmer();
                        linearLayout.setVisibility(View.VISIBLE);
                        return false;
                    }
                }).into(imageView);
            } catch (Exception e) {
                Context context = this.mContext;
                Toast.makeText(context, "Exception: " + e.getMessage(), (int)0).show();
            }
            textView.setText(campaignAds.getsTitle());
            textView2.setText(campaignAds.getsDecrition());
            ratingBar.setRating(campaignAds.getRating());
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    UtilsCombat.OpenBrower(DialogStoreAds.this.mContext, campaignAds.getsLink());
                }
            });
            linearLayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    UtilsCombat.OpenBrower(DialogStoreAds.this.mContext, campaignAds.getsLink());
                }
            });
            return;
        }
        findViewById(R.id.lrAds3).setVisibility(View.GONE);
    }
}
