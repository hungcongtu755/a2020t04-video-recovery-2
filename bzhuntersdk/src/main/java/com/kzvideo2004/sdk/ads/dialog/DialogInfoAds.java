package com.kzvideo2004.sdk.ads.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import com.bzvideo2004.sdk.R;

public class DialogInfoAds extends Dialog {
    Context mContext;

    public DialogInfoAds(Context context) {
        super(context);
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.dialog_info_ads);
        findViewById(R.id.ivClose).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DialogInfoAds.this.dismiss();
            }
        });
    }
}
