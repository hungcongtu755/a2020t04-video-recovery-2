package com.kzvideo2004.sdk.ads.ads;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bzvideo2004.sdk.R;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.kzvideo2004.sdk.ads.AdsCompat;
import com.kzvideo2004.sdk.ads.model.CONTAIN;

import androidx.annotation.NonNull;

public class AdsHelp {
    private static AdsHelp instance;
    static Context mContext;
    /* access modifiers changed from: private */
    public AdCloseListener adCloseListener;
    PublisherAdView adView;
    ProgressDialog barProgressDialog;
    ShimmerFrameLayout containerShimmerBanner;
    int countReward = 0;
    boolean flagReward = false;
    PublisherInterstitialAd mPublisherInterstitialAd;
    /* access modifiers changed from: private */
    public UnifiedNativeAd nativeAd;
    /* access modifiers changed from: private */
    public UnifiedNativeAd nativeBannerAd;
    private Dialog progressDialog;
    /* access modifiers changed from: private */
    public RewardedAd rewardedAd;
    long timeLoad = 0;

    public interface AdCloseListener {
        void onAdClosed();
    }

    public interface AdCloseRewardListener {
        void onAdFail();

        void onAdSuccess();
    }

    public static AdsHelp getInstance(Context context) {
        mContext = context;
        if (instance == null) {
            instance = new AdsHelp();
        }
        return instance;
    }

    private AdsHelp() {
    }

    public void showReward(final AdCloseRewardListener adCloseRewardListener) {
        this.barProgressDialog = new ProgressDialog(mContext);
        this.barProgressDialog.setCanceledOnTouchOutside(true);
        this.barProgressDialog.setCancelable(false);
        this.barProgressDialog.setMessage("Loading...");
        this.barProgressDialog.setProgressStyle(0);
        this.barProgressDialog.show();
        if (!CONTAIN.FlagLoadedData) {
            AdsCompat.getInstance(mContext).loadData(new AdsCompat.AdLoadData() {
                public void onSuccess() {
                    if (CONTAIN.ADX_ID_REWARD.equals("")) {
                        try {
                            if (AdsHelp.this.barProgressDialog != null && AdsHelp.this.barProgressDialog.isShowing()) {
                                AdsHelp.this.barProgressDialog.dismiss();
                            }
                        } catch (Exception unused) {
                        }
                        adCloseRewardListener.onAdSuccess();
                    } else if (AdsHelp.this.countReward < 3) {
                        RewardedAd unused2 = AdsHelp.this.rewardedAd = new RewardedAd(AdsHelp.mContext, CONTAIN.ADX_ID_REWARD);
                        if (AdsHelp.this.rewardedAd != null && !AdsHelp.this.rewardedAd.isLoaded()) {
                            AdsHelp.this.rewardedAd.loadAd(new PublisherAdRequest.Builder().build(), (RewardedAdLoadCallback) new RewardedAdLoadCallback() {
                                public void onRewardedAdLoaded() {
                                    try {
                                        if (AdsHelp.this.barProgressDialog != null && AdsHelp.this.barProgressDialog.isShowing()) {
                                            AdsHelp.this.barProgressDialog.dismiss();
                                        }
                                        AdsHelp.this.rewardedAd.show((Activity) AdsHelp.mContext, new RewardedAdCallback() {
                                            public void onRewardedAdOpened() {
                                                Log.d("KZADS", "Ad opened");
                                            }

                                            public void onRewardedAdClosed() {
                                                if (AdsHelp.this.flagReward) {
                                                    AdsHelp.this.flagReward = false;
                                                    adCloseRewardListener.onAdSuccess();
                                                    Log.d("KZADS", "onAdSuccess");
                                                    return;
                                                }
                                                adCloseRewardListener.onAdFail();
                                                Log.d("KZADS", "onAdFail");
                                            }

                                            public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                                                Log.d("KZADS", " User earned reward");
                                                AdsHelp.this.flagReward = true;
                                            }

                                            public void onRewardedAdFailedToShow(int i) {
                                                Log.d("KZADS", " Ad failed to display");
                                            }
                                        });
                                    } catch (Exception unused) {
                                    }
                                }

                                public void onRewardedAdFailedToLoad(int i) {
                                    AdsHelp.this.countReward++;
                                    try {
                                        if (AdsHelp.this.barProgressDialog != null && AdsHelp.this.barProgressDialog.isShowing()) {
                                            AdsHelp.this.barProgressDialog.dismiss();
                                        }
                                    } catch (Exception unused) {
                                    }
                                    adCloseRewardListener.onAdFail();
                                }
                            });
                        }
                    } else {
                        AdsHelp adsHelp = AdsHelp.this;
                        adsHelp.countReward = 0;
                        try {
                            if (adsHelp.barProgressDialog != null && AdsHelp.this.barProgressDialog.isShowing()) {
                                AdsHelp.this.barProgressDialog.dismiss();
                            }
                        } catch (Exception unused3) {
                        }
                        adCloseRewardListener.onAdSuccess();
                    }
                }
            });
        } else if (CONTAIN.ADX_ID_REWARD.equals("")) {
            try {
                if (this.barProgressDialog != null && this.barProgressDialog.isShowing()) {
                    this.barProgressDialog.dismiss();
                }
            } catch (Exception unused) {
            }
            adCloseRewardListener.onAdSuccess();
        } else if (this.countReward < 3) {
            this.rewardedAd = new RewardedAd(mContext, CONTAIN.ADX_ID_REWARD);
            RewardedAd rewardedAd2 = this.rewardedAd;
            if (rewardedAd2 != null && !rewardedAd2.isLoaded()) {
                this.rewardedAd.loadAd(new PublisherAdRequest.Builder().build(), (RewardedAdLoadCallback) new RewardedAdLoadCallback() {
                    public void onRewardedAdLoaded() {
                        try {
                            if (AdsHelp.this.barProgressDialog != null && AdsHelp.this.barProgressDialog.isShowing()) {
                                AdsHelp.this.barProgressDialog.dismiss();
                            }
                            AdsHelp.this.rewardedAd.show((Activity) AdsHelp.mContext, new RewardedAdCallback() {
                                public void onRewardedAdOpened() {
                                    Log.d("KZADS", "Ad opened");
                                }

                                public void onRewardedAdClosed() {
                                    if (AdsHelp.this.flagReward) {
                                        AdsHelp.this.flagReward = false;
                                        adCloseRewardListener.onAdSuccess();
                                        Log.d("KZADS", "onAdSuccess");
                                        return;
                                    }
                                    adCloseRewardListener.onAdFail();
                                    Log.d("KZADS", "onAdFail");
                                }

                                public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                                    Log.d("KZADS", " User earned reward");
                                    AdsHelp.this.flagReward = true;
                                }

                                public void onRewardedAdFailedToShow(int i) {
                                    Log.d("KZADS", " Ad failed to display");
                                    adCloseRewardListener.onAdFail();
                                }
                            });
                        } catch (Exception unused) {
                        }
                    }

                    public void onRewardedAdFailedToLoad(int i) {
                        AdsHelp.this.countReward++;
                        try {
                            if (AdsHelp.this.barProgressDialog != null && AdsHelp.this.barProgressDialog.isShowing()) {
                                AdsHelp.this.barProgressDialog.dismiss();
                            }
                        } catch (Exception unused) {
                        }
                        adCloseRewardListener.onAdFail();
                    }
                });
            }
        } else {
            this.countReward = 0;
            try {
                if (this.barProgressDialog != null && this.barProgressDialog.isShowing()) {
                    this.barProgressDialog.dismiss();
                }
            } catch (Exception unused2) {
            }
            adCloseRewardListener.onAdSuccess();
        }
    }

    public void initFull() {
        if (!CONTAIN.ADX_ID_FULL.equals("")) {
            if (CONTAIN.FLAG_ADMOB) {
                MobileAds.initialize(mContext, CONTAIN.App_ID_ADMOB);
            }
            this.mPublisherInterstitialAd = new PublisherInterstitialAd(mContext);
            this.mPublisherInterstitialAd.setAdUnitId(CONTAIN.ADX_ID_FULL);
            this.mPublisherInterstitialAd.setAdListener(new AdListener() {
                public void onAdClicked() {
                }

                public void onAdLeftApplication() {
                }

                public void onAdOpened() {
                }

                public void onAdLoaded() {
                    Log.e(CONTAIN.TAG, "ADX Full ads load  thanh cong ");
                }

                public void onAdFailedToLoad(int i) {
                    Log.e(CONTAIN.TAG, "ADX Full ads load duoc quang cao");
                }

                public void onAdClosed() {
                    Log.e(CONTAIN.TAG, "ADX Full ads dong quang cao");
                    if (AdsHelp.this.adCloseListener != null) {
                        AdsHelp.this.adCloseListener.onAdClosed();
                        if (CONTAIN.CountCurrent < CONTAIN.ShowMax) {
                            AdsHelp.this.loadInterstitialAd();
                            CONTAIN.CountCurrent++;
                        }
                    }
                }
            });
            loadInterstitialAd();
        }
    }

    /* access modifiers changed from: private */
    public void loadInterstitialAd() {
        PublisherInterstitialAd publisherInterstitialAd = this.mPublisherInterstitialAd;
        if (publisherInterstitialAd != null && !publisherInterstitialAd.isLoading() && !this.mPublisherInterstitialAd.isLoaded()) {
            this.mPublisherInterstitialAd.loadAd(new PublisherAdRequest.Builder().build());
        }
    }

    public void showInterstitialAd(final AdCloseListener adCloseListener2) {
        if (this.timeLoad + CONTAIN.TimeReload >= System.currentTimeMillis()) {
            adCloseListener2.onAdClosed();
        } else if (canShowInterstitialAd()) {
            this.adCloseListener = adCloseListener2;
            this.mPublisherInterstitialAd.show();
            Log.e(CONTAIN.TAG, "ADX Full ads da show quang cao");
            this.timeLoad = System.currentTimeMillis();
        } else {
            Log.e(CONTAIN.TAG, "ADX Full khong show dc chuyen qua KZADS");
            AdsCompat.getInstance(mContext).showFullScreen(new AdsCompat.AdCloseListener() {
                public void onAdClosed() {
                    adCloseListener2.onAdClosed();
                    AdsHelp.this.timeLoad = System.currentTimeMillis();
                }
            }, false);
        }
    }

    private void createDialogLoading(Context context) {
        if (context != null) {
            this.progressDialog = new Dialog(context, R.style.DialogCustomTheme);
            this.progressDialog.setContentView(R.layout.progress_layout_ads);
            this.progressDialog.setCanceledOnTouchOutside(false);
            this.progressDialog.show();
        }
    }

    private void cancelDialogLoading() {
        try {
            if (this.progressDialog != null && this.progressDialog.isShowing()) {
                this.progressDialog.dismiss();
            }
            this.progressDialog = null;
        } catch (Exception unused) {
        }
    }

    private boolean canShowInterstitialAd() {
        PublisherInterstitialAd publisherInterstitialAd = this.mPublisherInterstitialAd;
        return publisherInterstitialAd != null && publisherInterstitialAd.isLoaded();
    }

    public void loadBanner() {
        if (!CONTAIN.ADX_ID_BANNER.equals("")) {
            PublisherAdView publisherAdView = this.adView;
            if (publisherAdView != null) {
                publisherAdView.destroy();
            }
            if (CONTAIN.styleBanner == 0) {
                this.containerShimmerBanner = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container_50);
                this.adView = new PublisherAdView((Activity) mContext);
                this.adView.setAdSizes(AdSize.BANNER);
                this.adView.setAdUnitId(CONTAIN.ADX_ID_BANNER);
            } else {
                this.containerShimmerBanner = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container_100);
                this.adView = new PublisherAdView((Activity) mContext);
                this.adView.setAdSizes(AdSize.LARGE_BANNER);
                this.adView.setAdUnitId(CONTAIN.ADX_ID_BANNER);
            }
            this.containerShimmerBanner.setVisibility(View.VISIBLE);
            this.containerShimmerBanner.startShimmer();
            try {
                final LinearLayout linearLayout = (LinearLayout) ((Activity) mContext).findViewById(R.id.banner_container);
                linearLayout.setVisibility(View.GONE);
                linearLayout.addView(this.adView);
                this.adView.loadAd(new PublisherAdRequest.Builder().build());
                this.adView.setAdListener(new AdListener() {
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        Log.e(CONTAIN.TAG, "ADX Banner ads khong load dc quang cao");
                        linearLayout.setVisibility(View.GONE);
                        AdsHelp.this.containerShimmerBanner.stopShimmer();
                        AdsHelp.this.containerShimmerBanner.setVisibility(View.GONE);
                        if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing()) {
                            AdsCompat.getInstance(AdsHelp.mContext).loadBanner(false);
                        }
                    }

                    public void onAdLoaded() {
                        Log.e(CONTAIN.TAG, "ADX Banner ads  load xong ");
                        AdsHelp.this.containerShimmerBanner.stopShimmer();
                        AdsHelp.this.containerShimmerBanner.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.VISIBLE);
                        if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing()) {
                            ((Activity) AdsHelp.mContext).findViewById(R.id.spAds).setVisibility(View.VISIBLE);
                        }
                    }
                });
            } catch (Exception unused) {
            }
        } else {
            Context context = mContext;
            if (((Activity) context) != null && !((Activity) context).isFinishing()) {
                AdsCompat.getInstance(mContext).loadBanner(false);
            }
        }
    }

    public void loadBannerFragment(final View view) {
        if (!CONTAIN.ADX_ID_BANNER.equals("")) {
            if (CONTAIN.styleBanner == 0) {
                this.containerShimmerBanner = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_container_50);
                this.adView = new PublisherAdView((Activity) mContext);
                this.adView.setAdSizes(AdSize.BANNER);
                this.adView.setAdUnitId(CONTAIN.ADX_ID_BANNER);
            } else {
                this.containerShimmerBanner = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_container_100);
                this.adView = new PublisherAdView((Activity) mContext);
                this.adView.setAdSizes(AdSize.LARGE_BANNER);
                this.adView.setAdUnitId(CONTAIN.ADX_ID_BANNER);
            }
            this.containerShimmerBanner.setVisibility(View.VISIBLE);
            this.containerShimmerBanner.startShimmer();
            try {
                if (this.adView != null) {
                    this.adView.destroy();
                }
                this.adView = new PublisherAdView((Activity) mContext);
                this.adView.setAdSizes(AdSize.BANNER);
                this.adView.setAdUnitId(CONTAIN.ADX_ID_BANNER);
                final LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.banner_container);
                linearLayout.setVisibility(View.GONE);
                linearLayout.addView(this.adView);
                this.adView.loadAd(new PublisherAdRequest.Builder().build());
                this.adView.setAdListener(new AdListener() {
                    public void onAdFailedToLoad(int i) {
                        super.onAdFailedToLoad(i);
                        Log.e(CONTAIN.TAG, "ADX Banner ads khong load dc quang cao");
                        linearLayout.setVisibility(View.GONE);
                        AdsHelp.this.containerShimmerBanner.stopShimmer();
                        AdsHelp.this.containerShimmerBanner.setVisibility(View.GONE);
                        if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing()) {
                            AdsCompat.getInstance(AdsHelp.mContext).loadBannerFragment(view, false);
                        }
                    }

                    public void onAdLoaded() {
                        Log.e(CONTAIN.TAG, "ADX Banner ads  load dc quang cao");
                        AdsHelp.this.containerShimmerBanner.stopShimmer();
                        AdsHelp.this.containerShimmerBanner.setVisibility(View.GONE);
                        linearLayout.setVisibility(View.VISIBLE);
                        view.findViewById(R.id.spAds).setVisibility(View.VISIBLE);
                    }
                });
            } catch (Exception unused) {
            }
        } else {
            Context context = mContext;
            if (((Activity) context) != null && !((Activity) context).isFinishing()) {
                AdsCompat.getInstance(mContext).loadBannerFragment(view, false);
            }
        }
    }

    public void loadNative() {
        if (!CONTAIN.ADX_ID_NATIVE.equals("")) {
            final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();
            final FrameLayout mFrameLayout = (FrameLayout) ((Activity) mContext).findViewById(R.id.fl_adplaceholder);
            new AdLoader.Builder(mContext, CONTAIN.ADX_ID_NATIVE).forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    FrameLayout frameLayout;
                    if (AdsHelp.this.nativeAd != null) {
                        AdsHelp.this.nativeAd.destroy();
                    }
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    UnifiedNativeAd unused = AdsHelp.this.nativeAd = unifiedNativeAd;
                    if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing() && (frameLayout = mFrameLayout) != null) {
                        frameLayout.setVisibility(View.VISIBLE);
                        UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) ((Activity) AdsHelp.mContext).getLayoutInflater().inflate(R.layout.item_admob_native, (ViewGroup) null);
                        AdsHelp.this.populateUnifiedNativeAdView(unifiedNativeAd, unifiedNativeAdView);
                        frameLayout.removeAllViews();
                        frameLayout.addView(unifiedNativeAdView);
                    }
                }
            }).withAdListener(new AdListener() {
                public void onAdFailedToLoad(int i) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing()) {
                        AdsCompat.getInstance(AdsHelp.mContext).loadNative(false);
                    }
                }
            }).withNativeAdOptions(new NativeAdOptions.Builder().setVideoOptions(new VideoOptions.Builder().setStartMuted(false).build()).build()).build().loadAd(new PublisherAdRequest.Builder().build());
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null && !((Activity) context).isFinishing()) {
            AdsCompat.getInstance(mContext).loadNative(false);
        }
    }

    public void loadNativeFragment(final View view) {
        if (!CONTAIN.ADX_ID_NATIVE.equals("")) {
            final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_container);
            final FrameLayout mFrameLayout = (FrameLayout) view.findViewById(R.id.fl_adplaceholder);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();
            new AdLoader.Builder(mContext, CONTAIN.ADX_ID_NATIVE).forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    FrameLayout frameLayout;
                    if (AdsHelp.this.nativeAd != null) {
                        AdsHelp.this.nativeAd.destroy();
                    }
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    UnifiedNativeAd unused = AdsHelp.this.nativeAd = unifiedNativeAd;
                    if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing() && (frameLayout = mFrameLayout) != null) {
                        frameLayout.setVisibility(View.VISIBLE);
                        UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) ((Activity) AdsHelp.mContext).getLayoutInflater().inflate(R.layout.item_admob_native, (ViewGroup) null);
                        AdsHelp.this.populateUnifiedNativeAdView(unifiedNativeAd, unifiedNativeAdView);
                        frameLayout.removeAllViews();
                        frameLayout.addView(unifiedNativeAdView);
                    }
                }
            }).withAdListener(new AdListener() {
                public void onAdFailedToLoad(int i) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing()) {
                        AdsCompat.getInstance(AdsHelp.mContext).loadNativeFragment(view, false);
                    }
                }
            }).withNativeAdOptions(new NativeAdOptions.Builder().setVideoOptions(new VideoOptions.Builder().setStartMuted(false).build()).build()).build().loadAd(new PublisherAdRequest.Builder().build());
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null && !((Activity) context).isFinishing()) {
            AdsCompat.getInstance(mContext).loadNativeFragment(view, false);
        }
    }

    /* access modifiers changed from: private */
    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView((MediaView) unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.ad_call_to_action));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_app_icon));
        unifiedNativeAdView.setPriceView(unifiedNativeAdView.findViewById(R.id.ad_price));
        unifiedNativeAdView.setStarRatingView(unifiedNativeAdView.findViewById(R.id.ad_stars));
        unifiedNativeAdView.setStoreView(unifiedNativeAdView.findViewById(R.id.ad_store));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.GONE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getPrice() == null) {
            unifiedNativeAdView.getPriceView().setVisibility(View.GONE);
        } else {
            unifiedNativeAdView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) unifiedNativeAdView.getPriceView()).setText(unifiedNativeAd.getPrice());
        }
        if (unifiedNativeAd.getStore() == null) {
            unifiedNativeAdView.getStoreView().setVisibility(View.GONE);
        } else {
            unifiedNativeAdView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) unifiedNativeAdView.getStoreView()).setText(unifiedNativeAd.getStore());
        }
        if (unifiedNativeAd.getStarRating() == null) {
            unifiedNativeAdView.getStarRatingView().setVisibility(View.GONE);
        } else {
            ((RatingBar) unifiedNativeAdView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
            unifiedNativeAdView.getStarRatingView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.GONE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    /* access modifiers changed from: private */
    public void populateUnifiedNativeBannerAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.primary));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.row_two));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.cta));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.icon));
        unifiedNativeAdView.setStoreView(unifiedNativeAdView.findViewById(R.id.secondary));
        unifiedNativeAdView.setStarRatingView(unifiedNativeAdView.findViewById(R.id.rating_bar));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.VISIBLE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((Button) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getStore() == null) {
            unifiedNativeAdView.getStoreView().setVisibility(View.GONE);
        } else {
            unifiedNativeAdView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) unifiedNativeAdView.getStoreView()).setText(unifiedNativeAd.getStore());
        }
        if (unifiedNativeAd.getStarRating() == null) {
            unifiedNativeAdView.getStarRatingView().setVisibility(View.GONE);
        } else {
            ((RatingBar) unifiedNativeAdView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
            unifiedNativeAdView.getStarRatingView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    public void loadNativeBanner() {
        if (!CONTAIN.ADX_ID_NATIVE_BANNER.equals("")) {
            final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) ((Activity) mContext).findViewById(R.id.shimmer_container);
            final FrameLayout mFrameLayout = (FrameLayout) ((Activity) mContext).findViewById(R.id.fl_native_banner_ad_container);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();
            new AdLoader.Builder(mContext, CONTAIN.ADX_ID_NATIVE_BANNER).forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    FrameLayout frameLayout;
                    if (AdsHelp.this.nativeBannerAd != null) {
                        AdsHelp.this.nativeBannerAd.destroy();
                    }
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    UnifiedNativeAd unused = AdsHelp.this.nativeBannerAd = unifiedNativeAd;
                    if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing() && (frameLayout = mFrameLayout) != null) {
                        frameLayout.setVisibility(View.VISIBLE);
                        UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) ((Activity) AdsHelp.mContext).getLayoutInflater().inflate(R.layout.item_admob_native_banner, (ViewGroup) null);
                        AdsHelp adsHelp = AdsHelp.this;
                        adsHelp.populateUnifiedNativeBannerAdView(adsHelp.nativeBannerAd, unifiedNativeAdView);
                        frameLayout.removeAllViews();
                        frameLayout.addView(unifiedNativeAdView);
                    }
                }
            }).withAdListener(new AdListener() {
                public void onAdFailedToLoad(int i) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing()) {
                        AdsCompat.getInstance(AdsHelp.mContext).loadNativeBanner(false);
                    }
                }
            }).withNativeAdOptions(new NativeAdOptions.Builder().setVideoOptions(new VideoOptions.Builder().setStartMuted(false).build()).build()).build().loadAd(new PublisherAdRequest.Builder().build());
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null && !((Activity) context).isFinishing()) {
            AdsCompat.getInstance(mContext).loadNativeBanner(false);
        }
    }

    public void loadNativeBannerFragment(final View view) {
        if (!CONTAIN.ADX_ID_NATIVE_BANNER.equals("")) {
            final ShimmerFrameLayout shimmerFrameLayout = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_container);
            final FrameLayout mFrameLayout = (FrameLayout) view.findViewById(R.id.fl_native_banner_ad_container);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();
            new AdLoader.Builder(mContext, CONTAIN.ADX_ID_NATIVE_BANNER).forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                    if (AdsHelp.this.nativeBannerAd != null) {
                        AdsHelp.this.nativeBannerAd.destroy();
                    }
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    UnifiedNativeAd unused = AdsHelp.this.nativeBannerAd = unifiedNativeAd;
                    FrameLayout frameLayout = mFrameLayout;
                    if (frameLayout != null) {
                        frameLayout.setVisibility(View.VISIBLE);
                        UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) ((Activity) AdsHelp.mContext).getLayoutInflater().inflate(R.layout.item_admob_native_banner, (ViewGroup) null);
                        AdsHelp adsHelp = AdsHelp.this;
                        adsHelp.populateUnifiedNativeBannerAdView(adsHelp.nativeBannerAd, unifiedNativeAdView);
                        frameLayout.removeAllViews();
                        frameLayout.addView(unifiedNativeAdView);
                    }
                }
            }).withAdListener(new AdListener() {
                public void onAdFailedToLoad(int i) {
                    shimmerFrameLayout.stopShimmer();
                    shimmerFrameLayout.setVisibility(View.GONE);
                    if (((Activity) AdsHelp.mContext) != null && !((Activity) AdsHelp.mContext).isFinishing()) {
                        AdsCompat.getInstance(AdsHelp.mContext).loadNativeBannerFragment(view, false);
                    }
                }
            }).withNativeAdOptions(new NativeAdOptions.Builder().setVideoOptions(new VideoOptions.Builder().setStartMuted(false).build()).build()).build().loadAd(new PublisherAdRequest.Builder().build());
            return;
        }
        Context context = mContext;
        if (((Activity) context) != null && !((Activity) context).isFinishing()) {
            AdsCompat.getInstance(mContext).loadNativeBannerFragment(view, false);
        }
    }
}
