package com.bzvideo2004.sdk.demo;

import com.bzvideo2004.sdk.ads.venus.AdManager;
import com.bzvideo2004.sdk.ads.venus.AdManagerOptions;
import com.bzvideo2004.sdk.ads.venus.Boom;

public class AdDialog extends android.app.Dialog {
    android.content.Context context;

    public AdDialog(android.content.Context context2) {
        super(context2);
        this.context = context2;
    }

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showNativeAd(this);
    }

    public static void showNativeAd(android.app.Dialog context2) {
        AdManager.newInstance(context2.getContext(), AdManagerOptions.Builder.create().withAdNetwork(AdManagerOptions.AdNetwork.ADMOB).addAdOptions(AdManagerOptions.AdNetwork.ADMOB, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("ca-app-pub-3091789937370156/4453649168").withNativeAdLayout("zimba_admob_medium_banner_native_ad_layout").withNativeAdUnitId("ca-app-pub-3091789937370156/8881609644").build()).addAdOptions(AdManagerOptions.AdNetwork.FACEBOOK, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("fb_inter_id").withNativeAdLayout("fb_native_ad_layout").withNativeAdUnitId("fb_native_ad_id").build()).build()).requestNativeAd((android.view.ViewGroup) Boom.findViewById(context2, "zimba_container_ad_view"));
    }
}
