package com.bzvideo2004.sdk.demo;

import com.bzvideo2004.sdk.ads.venus.Boom;
import com.bzvideo2004.sdk.inapp.sub.ui.IABActivity;
import com.bzvideo2004.sdk.inapp.sub.view.BannerDisplayer;

public class MainActivity extends android.app.Activity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void initIab() {
        IABActivity.startIfNotSubs(this, "main");
    }

    private void startIab(String event) {
        IABActivity.startIfNotSubs(this, "main", event);
    }

    private void showBanner() {
        new BannerDisplayer((android.app.Activity) this, "zimba_sub_banner").bindData(java.util.Arrays.asList(new Integer[]{Integer.valueOf(Boom.getDrawableId(this, "vip_banner1")), Integer.valueOf(Boom.getDrawableId(this, "vip_banner2")), Integer.valueOf(Boom.getDrawableId(this, "vip_banner3")), Integer.valueOf(Boom.getDrawableId(this, "vip_banner4"))}));
    }
}
