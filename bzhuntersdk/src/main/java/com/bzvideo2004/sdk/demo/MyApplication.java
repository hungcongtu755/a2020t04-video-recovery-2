package com.bzvideo2004.sdk.demo;

import com.bzvideo2004.sdk.android.fcm.ImbaMessaging;

import androidx.multidex.MultiDex;

public class MyApplication extends android.app.Application {
    public void onCreate() {
        super.onCreate();
        initApp();
    }

    private void initApp() {
        com.google.firebase.FirebaseApp.initializeApp(this);
        com.google.android.gms.ads.MobileAds.initialize(this, "ca-app-pub-1854637948405034~1642878560");
        ImbaMessaging.initialize(this);
        com.facebook.ads.AudienceNetworkAds.initialize(this);
    }

    protected void attachBaseContext(android.content.Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
