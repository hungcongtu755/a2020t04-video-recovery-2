package com.bzvideo2004.sdk.ads.venus;

public class AdManager {
    private BaseAdManager adManager;
    private AdManagerOptions adManagerOptions;


    static class AdsOptions {
        static final int[] ads = new int[AdManagerOptions.AdNetwork.values().length];

        static {
            try {
                ads[AdManagerOptions.AdNetwork.ADMOB.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                ads[AdManagerOptions.AdNetwork.FACEBOOK.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    public static AdManager newInstance(android.content.Context context, AdManagerOptions adManagerOptions2) {
        return new AdManager(context, adManagerOptions2);
    }

    public AdManager(android.content.Context context, AdManagerOptions adManagerOptions2) {
        this.adManagerOptions = adManagerOptions2;
        int i = AdManager.AdsOptions.ads[adManagerOptions2.getAdNetwork().ordinal()];
        if (i == 1) {
            this.adManager = new AdmobAdManager(context);
        } else if (i == 2) {
            this.adManager = new FbAdManager(context);
        }
    }

    public AdManagerOptions getAdManagerOptions() {
        return this.adManagerOptions;
    }

    public void requestInterAd() {
        if (!android.text.TextUtils.isEmpty(this.adManagerOptions.getAdOptions().getInterAdUnitId())) {
            this.adManager.requestInterAd(this.adManagerOptions.getAdOptions().getInterAdUnitId());
        }
    }

    public boolean isInterAdLoaded() {
        return this.adManager.isInterAdLoaded();
    }

    public void showInterAd() {
        this.adManager.showInterAd();
    }

    public void requestNativeAd(android.view.ViewGroup viewGroup) {
        if (viewGroup == null) {
            android.util.Log.e("ADS", "native parent is null");
        } else if (!android.text.TextUtils.isEmpty(this.adManagerOptions.getAdOptions().getNativeAdLayout()) && !android.text.TextUtils.isEmpty(this.adManagerOptions.getAdOptions().getNativeAdUnitId())) {
            this.adManager.requestNativeAd(viewGroup, this.adManagerOptions.getAdOptions().getNativeAdLayout(), this.adManagerOptions.getAdOptions().getNativeAdUnitId());
        }
    }

    public boolean isNativeAdLoaded() {
        return this.adManager.isNativeAdLoaded();
    }

    public void setAdListener(AdListener adListener) {
        this.adManager.setAdListener(adListener);
    }

    public void destroy() {
        this.adManager.destroy();
    }
}
