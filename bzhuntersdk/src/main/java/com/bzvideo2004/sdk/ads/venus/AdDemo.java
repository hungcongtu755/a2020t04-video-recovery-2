package com.bzvideo2004.sdk.ads.venus;

import androidx.fragment.app.Fragment;

public class AdDemo {
    public static void showAd(android.app.Activity context) {
        AdManager.newInstance(context, initAdManager()).requestNativeAd((android.view.ViewGroup) Boom.findViewById(context, "fl_adplaceholder"));
    }

    public static void showAd(Fragment context) {
        AdManager.newInstance(context.getContext(), initAdManager()).requestNativeAd((android.view.ViewGroup) Boom.findViewById(context.getContext(), context.getView(), "fl_adplaceholder"));
    }

    public static void showNativeAd(android.app.Activity context) {
        AdManager.newInstance(context, AdManagerOptions.Builder.create().withAdNetwork(AdManagerOptions.AdNetwork.ADMOB).addAdOptions(AdManagerOptions.AdNetwork.ADMOB, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("admob_inter_id").withNativeAdLayout("admob_native_ad_layout").withNativeAdUnitId("admob_native_ad_id").build()).addAdOptions(AdManagerOptions.AdNetwork.FACEBOOK, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("fb_inter_id").withNativeAdLayout("fb_native_ad_layout").withNativeAdUnitId("fb_native_ad_id").build()).build()).requestNativeAd((android.view.ViewGroup) Boom.findViewById(context, "fl_adplaceholder"));
    }

    public static void showNativeAd(Fragment context) {
        AdManager.newInstance(context.getContext(), AdManagerOptions.Builder.create().withAdNetwork(AdManagerOptions.AdNetwork.ADMOB).addAdOptions(AdManagerOptions.AdNetwork.ADMOB, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("admob_inter_id").withNativeAdLayout("admob_native_ad_layout").withNativeAdUnitId("admob_native_ad_id").build()).addAdOptions(AdManagerOptions.AdNetwork.FACEBOOK, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("fb_inter_id").withNativeAdLayout("fb_native_ad_layout").withNativeAdUnitId("fb_native_ad_id").build()).build()).requestNativeAd((android.view.ViewGroup) Boom.findViewById(context.getContext(), context.getView(), "fl_adplaceholder"));
    }

    public static void showNativeAd(android.app.Dialog context) {
        AdManager.newInstance(context.getContext(), AdManagerOptions.Builder.create().withAdNetwork(AdManagerOptions.AdNetwork.ADMOB).addAdOptions(AdManagerOptions.AdNetwork.ADMOB, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("admob_inter_id").withNativeAdLayout("admob_native_ad_layout").withNativeAdUnitId("admob_native_ad_id").build()).addAdOptions(AdManagerOptions.AdNetwork.FACEBOOK, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("fb_inter_id").withNativeAdLayout("fb_native_ad_layout").withNativeAdUnitId("fb_native_ad_id").build()).build()).requestNativeAd((android.view.ViewGroup) Boom.findViewById(context, "fl_adplaceholder"));
    }

    public static void showInter(AdManager adManager) {
        if (adManager.isInterAdLoaded()) {
            adManager.showInterAd();
        }
    }

    public static AdManagerOptions initAdManager() {
        return AdManagerOptions.Builder.create().withAdNetwork(AdManagerOptions.AdNetwork.ADMOB).addAdOptions(AdManagerOptions.AdNetwork.ADMOB, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("admob_inter_id").withNativeAdLayout("admob_native_ad_layout").withNativeAdUnitId("admob_native_ad_id").build()).addAdOptions(AdManagerOptions.AdNetwork.FACEBOOK, AdManagerOptions.AdOptions.Builder.create().enabled(true).withInterAdUnitId("fb_inter_id").withNativeAdLayout("fb_native_ad_layout").withNativeAdUnitId("fb_native_ad_id").build()).build();
    }
}
