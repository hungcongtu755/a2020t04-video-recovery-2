package com.bzvideo2004.sdk.ads.venus;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

public class Boom {
    private android.content.Context context;
    private android.view.View view;

    public Boom(android.content.Context context2) {
        this.context = context2;
    }

    public Boom(android.content.Context context2, android.view.View view2) {
        this.context = context2;
        this.view = view2;
    }

    public static Boom bind(android.app.Activity activity) {
        return new Boom(activity);
    }

    public static Boom bind(android.app.Activity activity, String layout) {
        Boom boom = bind(activity);
        activity.setContentView(getLayoutId(activity, layout));
        return boom;
    }

    public static Boom bind(android.content.Context context2, android.view.View view2) {
        return new Boom(context2, view2);
    }

    public <T extends android.view.View> T findViewById(String name) {
        android.content.Context context2 = this.context;
        if (context2 != null) {
            android.view.View view2 = this.view;
            if (view2 != null) {
                return view2.findViewById(getId(context2, name));
            }
            if (context2 instanceof android.app.Activity) {
                return ((android.app.Activity) context2).findViewById(getId(context2, name));
            }
            throw new NullPointerException("Call Boom.bind(context, view) first!");
        }
        throw new NullPointerException("Call Boom.bind(activity) first!");
    }

    private static int getId(android.content.Context context2, String type, String name) {
        return context2.getResources().getIdentifier(name, type, context2.getPackageName());
    }

    public static <T extends android.view.View> T findViewById(android.app.Activity activity, String name) {
        if (activity != null) {
            return activity.findViewById(getId(activity, name));
        }
        throw new NullPointerException("Activity is null!");
    }

    public static <T extends android.view.View> T findViewById(Fragment fragment, String name) {
        if (fragment != null) {
            return fragment.getView().findViewById(getId(fragment.getContext(), name));
        }
        throw new NullPointerException("Fragment is null!");
    }

    public static <T extends android.view.View> T findViewById(android.app.Dialog dialog, String name) {
        if (dialog != null) {
            return dialog.findViewById(getId(dialog.getContext(), name));
        }
        throw new NullPointerException("Dialog is null!");
    }

    public static <T extends android.view.View> T findViewById(android.content.Context context2, android.view.View view2, String name) {
        if (context2 == null) {
            throw new NullPointerException("Context is null!");
        } else if (view2 != null) {
            return view2.findViewById(getId(context2, name));
        } else {
            throw new NullPointerException("View is null!");
        }
    }

    public static int getLayoutId(android.content.Context context2, String name) {
        return getId(context2, "layout", name);
    }

    public static int getId(android.content.Context context2, String name) {
        return getId(context2, "id", name);
    }

    public static int getStringId(android.content.Context context2, String name) {
        return getId(context2, "string", name);
    }

    public static int getColorId(android.content.Context context2, String name) {
        return getId(context2, "color", name);
    }

    public static int getDrawableId(android.content.Context context2, String name) {
        return getId(context2, "drawable", name);
    }

    public static int getDimenId(android.content.Context context2, String name) {
        return getId(context2, "dimen", name);
    }

    public static int getStyleableId(android.content.Context context2, String name) {
        return getId(context2, "styleable", name);
    }

    public static String getStringById(android.content.Context context2, int resId) {
        return context2.getResources().getString(resId);
    }

    public static String getStringById(android.content.Context context2, String name) {
        return context2.getResources().getString(getStringId(context2, name));
    }

    public static int getColorById(android.content.Context context2, int resId) {
        return ContextCompat.getColor(context2, resId);
    }

    public static int getColorById(android.content.Context context2, String name) {
        return ContextCompat.getColor(context2, getColorId(context2, name));
    }

    public static android.graphics.drawable.Drawable getDrawableById(android.content.Context context2, int resId) {
        return ContextCompat.getDrawable(context2, resId);
    }

    public static android.graphics.drawable.Drawable getDrawableById(android.content.Context context2, String name) {
        return ContextCompat.getDrawable(context2, getDrawableId(context2, name));
    }

    public static float getDimenById(android.content.Context context2, int resId) {
        return context2.getResources().getDimension(resId);
    }

    public static float getDimenById(android.content.Context context2, String name) {
        return context2.getResources().getDimension(getDimenId(context2, name));
    }

    public static boolean isResourceExists(android.content.Context context2, int resId) {
        boolean z = false;
        try {
            if (context2.getResources().getResourceName(resId) != null) {
                z = true;
            }
            return z;
        } catch (android.content.res.Resources.NotFoundException e) {
            return false;
        }
    }

    public static final int[] getResourceDeclareStyleableIntArray(android.content.Context context2, String packageR, String name) {
        java.lang.reflect.Field[] fields2;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(packageR);
            sb.append(".R$styleable");
            for (java.lang.reflect.Field f : Class.forName(sb.toString()).getFields()) {
                if (f.getName().equals(name)) {
                    return (int[]) f.get(null);
                }
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return null;
    }
}
