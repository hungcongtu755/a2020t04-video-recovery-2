package com.bzvideo2004.sdk.ads.venus;

public abstract class BaseAdManager {
    protected AdListener adListener;
    protected android.content.Context context;

    public abstract void destroy();

    public abstract boolean isInterAdLoaded();

    public abstract boolean isNativeAdLoaded();

    public abstract void requestInterAd(String str);

    public abstract void requestNativeAd(android.view.ViewGroup viewGroup, String str, String str2);

    public abstract boolean shouldShowAd();

    public abstract void showInterAd();

    public BaseAdManager(android.content.Context context2) {
        this.context = context2;
    }

    public void setAdListener(AdListener adListener2) {
        this.adListener = adListener2;
    }

    protected <T extends android.view.View> T findViewById(android.view.View view, String resId) {
        return view.findViewById(getId(this.context, resId));
    }

    protected int getLayoutId(String layout) {
        return getId(this.context, "layout", layout);
    }

    protected int getId(android.content.Context context2, String id) {
        return getId(context2, "id", id);
    }

    protected static int getId(android.content.Context context2, String type, String name) {
        return context2.getResources().getIdentifier(name, type, context2.getPackageName());
    }
}
