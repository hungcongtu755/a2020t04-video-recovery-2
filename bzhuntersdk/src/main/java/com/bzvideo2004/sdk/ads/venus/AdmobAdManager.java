package com.bzvideo2004.sdk.ads.venus;

import android.content.Context;
import android.util.Log;

public class AdmobAdManager extends BaseAdManager {
    private Boom boom;
    private com.google.android.gms.ads.InterstitialAd mInterstitialAd;
    public com.google.android.gms.ads.formats.UnifiedNativeAd mUnifiedNativeAd;
    private android.view.View nativeAdView;

    public AdmobAdManager(android.content.Context context) {
        super(context);
    }

    public void requestInterAd(String adUnitId) {
        if (!shouldShowAd() || android.text.TextUtils.isEmpty(adUnitId)) {
            if (this.adListener != null) {
                this.adListener.onWillNotRequest();
            }
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("req interstitial ad = ");
        sb.append(adUnitId);
        android.util.Log.d("ADS", sb.toString());
        this.mInterstitialAd = new com.google.android.gms.ads.InterstitialAd(this.context);
        this.mInterstitialAd.setAdUnitId(adUnitId);
        if (this.adListener != null) {
            this.mInterstitialAd.setAdListener(new com.google.android.gms.ads.AdListener() {
                public void onAdClosed() {
                    AdmobAdManager.this.adListener.onAdClosed();
                }

                public void onAdFailedToLoad(int var1) {
                    AdmobAdManager.this.adListener.onAdFailedToLoad();
                }

                public void onAdOpened() {
                    AdmobAdManager.this.adListener.onAdOpened();
                }

                public void onAdLoaded() {
                    AdmobAdManager.this.adListener.onAdLoaded();
                }

                public void onAdClicked() {
                    AdmobAdManager.this.adListener.onAdClicked();
                }

                public void onAdImpression() {
                    AdmobAdManager.this.adListener.onAdImpression();
                }
            });
        }
        this.mInterstitialAd.loadAd(new com.google.android.gms.ads.AdRequest.Builder().build());
    }

    public boolean isInterAdLoaded() {
        com.google.android.gms.ads.InterstitialAd interstitialAd = this.mInterstitialAd;
        return interstitialAd != null && interstitialAd.isLoaded();
    }

    public void showInterAd() {
        if (shouldShowAd()) {
            com.google.android.gms.ads.InterstitialAd interstitialAd = this.mInterstitialAd;
            if (interstitialAd != null && interstitialAd.isLoaded()) {
                android.util.Log.d("ADS", "show interstitial ad");
                this.mInterstitialAd.show();
            }
        }
    }

    public void requestNativeAd(final android.view.ViewGroup parent, String nativeLayoutName, String adUnitId) {
        String str = "ADS";
        Log.d(str,"Request load Ads ID:"+ adUnitId);
        if (parent == null) {
            android.util.Log.e(str, "viewGroup is null");
        } else if (!shouldShowAd() || android.text.TextUtils.isEmpty(adUnitId) || android.text.TextUtils.isEmpty(nativeLayoutName)) {
            if (this.adListener != null) {
                this.adListener.onWillNotRequest();
            }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("req native ad = ");
            sb.append(adUnitId);
            android.util.Log.d(str, sb.toString());
            this.nativeAdView = ((android.view.LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(getLayoutId(nativeLayoutName), null);
            this.boom = Boom.bind(this.context, this.nativeAdView);
            com.google.android.gms.ads.AdLoader.Builder builder = new com.google.android.gms.ads.AdLoader.Builder(this.context, adUnitId);
            builder.forUnifiedNativeAd(new com.google.android.gms.ads.formats.UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                public void onUnifiedNativeAdLoaded(com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd) {
                    android.util.Log.d("ADS", "unifiedNativeAd loaded");
                    AdmobAdManager.this.mUnifiedNativeAd = unifiedNativeAd;
                    com.google.android.gms.ads.formats.UnifiedNativeAdView adView = new com.google.android.gms.ads.formats.UnifiedNativeAdView(AdmobAdManager.this.context);
                    AdmobAdManager.this.populateUnifiedNativeAdView(unifiedNativeAd, adView);
                    parent.removeAllViews();
                    parent.addView(adView);
                }
            });
            builder.withNativeAdOptions(new com.google.android.gms.ads.formats.NativeAdOptions.Builder().setVideoOptions(new com.google.android.gms.ads.VideoOptions.Builder().setStartMuted(true).build()).build());
            if (this.adListener != null) {
                builder.withAdListener(new com.google.android.gms.ads.AdListener() {
                    public void onAdClosed() {
                        AdmobAdManager.this.adListener.onAdClosed();
                    }

                    public void onAdFailedToLoad(int var1) {
                        AdmobAdManager.this.adListener.onAdFailedToLoad();
                    }

                    public void onAdOpened() {
                        AdmobAdManager.this.adListener.onAdOpened();
                    }

                    public void onAdLoaded() {
                        AdmobAdManager.this.adListener.onAdLoaded();
                    }

                    public void onAdClicked() {
                        AdmobAdManager.this.adListener.onAdClicked();
                    }

                    public void onAdImpression() {
                        AdmobAdManager.this.adListener.onAdImpression();
                    }
                });
            }
            builder.build().loadAd(new com.google.android.gms.ads.AdRequest.Builder().build());
        }
    }

    public boolean isNativeAdLoaded() {
        return this.mUnifiedNativeAd != null;
    }

    public void destroy() {
        com.google.android.gms.ads.formats.UnifiedNativeAd unifiedNativeAd = this.mUnifiedNativeAd;
        if (unifiedNativeAd != null) {
            unifiedNativeAd.destroy();
        }
    }

    /* access modifiers changed from: private */
    public void populateUnifiedNativeAdView(com.google.android.gms.ads.formats.UnifiedNativeAd nativeAd, com.google.android.gms.ads.formats.UnifiedNativeAdView adView) {
        adView.addView(this.nativeAdView, new android.widget.FrameLayout.LayoutParams(-1, -2));
        android.view.View mediaView = this.boom.findViewById("zimba_ad_banner");
        if (mediaView == null || !(mediaView instanceof com.google.android.gms.ads.formats.MediaView)) {
            adView.setImageView(mediaView);
        } else {
            adView.setMediaView((com.google.android.gms.ads.formats.MediaView) mediaView);
        }
        adView.setHeadlineView(this.boom.findViewById("zimba_ad_headline"));
        adView.setBodyView(this.boom.findViewById("zimba_ad_body"));
        adView.setCallToActionView(this.boom.findViewById("zimba_ad_call_to_action"));
        adView.setIconView(this.boom.findViewById("zimba_ad_icon"));
        ((android.widget.TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        if (adView.getBodyView() != null) {
            ((android.widget.TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        ((android.widget.Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        if (!(adView.getIconView() == null || nativeAd.getIcon() == null)) {
            ((android.widget.ImageView) adView.getIconView()).setImageDrawable(nativeAd.getIcon().getDrawable());
            adView.getIconView().setBackgroundColor(-1);
        }
        if (!(adView.getImageView() == null || nativeAd.getImages() == null || nativeAd.getImages().size() <= 0)) {
            ((android.widget.ImageView) adView.getImageView()).setImageDrawable(((com.google.android.gms.ads.formats.NativeAd.Image) nativeAd.getImages().get(0)).getDrawable());
        }
        adView.setNativeAd(nativeAd);
        android.util.Log.d("ADS", "native ad populated");
    }

    private void populateAppInstallAdView(com.google.android.gms.ads.formats.NativeAppInstallAd nativeAppInstallAd, com.google.android.gms.ads.formats.NativeAppInstallAdView adView) {
        adView.addView(this.nativeAdView, new android.widget.FrameLayout.LayoutParams(-1, -2));
        adView.setImageView(this.boom.findViewById("zimba_ad_banner"));
        adView.setHeadlineView(this.boom.findViewById("zimba_ad_headline"));
        adView.setBodyView(this.boom.findViewById("zimba_ad_body"));
        adView.setCallToActionView(this.boom.findViewById("zimba_ad_call_to_action"));
        adView.setIconView(this.boom.findViewById("zimba_ad_icon"));
        ((android.widget.TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        if (adView.getBodyView() != null) {
            ((android.widget.TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        }
        ((android.widget.Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        adView.getCallToActionView().setBackgroundColor(android.graphics.Color.parseColor("#109D58"));
        if (adView.getIconView() != null) {
            ((android.widget.ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon().getDrawable());
            adView.getIconView().setBackgroundColor(-1);
        }
        if (adView.getImageView() != null) {
            ((android.widget.ImageView) adView.getImageView()).setImageDrawable(((com.google.android.gms.ads.formats.NativeAd.Image) nativeAppInstallAd.getImages().get(0)).getDrawable());
        }
        adView.setNativeAd(nativeAppInstallAd);
        android.util.Log.d("ADS", "app install ad populated");
    }

    private void populateContentAdView(com.google.android.gms.ads.formats.NativeContentAd nativeContentAd, com.google.android.gms.ads.formats.NativeContentAdView adView) {
        adView.addView(this.nativeAdView, new android.widget.FrameLayout.LayoutParams(-1, -2));
        adView.setImageView(this.boom.findViewById("zimba_ad_banner"));
        adView.setHeadlineView(this.boom.findViewById("zimba_ad_headline"));
        adView.setBodyView(this.boom.findViewById("zimba_ad_body"));
        adView.setCallToActionView(this.boom.findViewById("zimba_ad_call_to_action"));
        adView.setLogoView(this.boom.findViewById("zimba_ad_icon"));
        ((android.widget.TextView) adView.getHeadlineView()).setText(nativeContentAd.getHeadline());
        if (adView.getBodyView() != null) {
            ((android.widget.TextView) adView.getBodyView()).setText(nativeContentAd.getBody());
        }
        ((android.widget.TextView) adView.getCallToActionView()).setText(nativeContentAd.getCallToAction());
        adView.getCallToActionView().setBackgroundColor(android.graphics.Color.parseColor("#1089E9"));
        if (adView.getImageView() != null) {
            java.util.List<com.google.android.gms.ads.formats.NativeAd.Image> images = nativeContentAd.getImages();
            if (images.size() > 0) {
                ((android.widget.ImageView) adView.getImageView()).setImageDrawable(((com.google.android.gms.ads.formats.NativeAd.Image) images.get(0)).getDrawable());
            }
        }
        com.google.android.gms.ads.formats.NativeAd.Image logoImage = nativeContentAd.getLogo();
        if (!(logoImage == null || adView.getLogoView() == null)) {
            ((android.widget.ImageView) adView.getLogoView()).setImageDrawable(logoImage.getDrawable());
            adView.getLogoView().setBackgroundColor(-1);
        }
        adView.setNativeAd(nativeContentAd);
        android.util.Log.d("ADS", "content ad populated");
    }

    public boolean shouldShowAd() {
        return true;
    }
}
