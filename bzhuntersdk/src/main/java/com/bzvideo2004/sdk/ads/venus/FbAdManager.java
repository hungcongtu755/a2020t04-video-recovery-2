package com.bzvideo2004.sdk.ads.venus;

import android.content.Context;

public class FbAdManager extends BaseAdManager {
    private Boom boom;
    private com.facebook.ads.InterstitialAd mInterstitialAd;
    public com.facebook.ads.NativeAd mNativeAd;
    private android.view.View nativeAdView;

    public FbAdManager(android.content.Context context) {
        super(context);
    }

    public void requestInterAd(String adUnitId) {
        if (!shouldShowAd() || android.text.TextUtils.isEmpty(adUnitId)) {
            if (this.adListener != null) {
                this.adListener.onWillNotRequest();
            }
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("req fb interstitial ad = ");
        sb.append(adUnitId);
        android.util.Log.d("ADS", sb.toString());
        this.mInterstitialAd = new com.facebook.ads.InterstitialAd(this.context, adUnitId);
        if (this.adListener != null) {
            this.mInterstitialAd.setAdListener(new com.facebook.ads.InterstitialAdListener() {
                public void onInterstitialDisplayed(com.facebook.ads.Ad ad) {
                    FbAdManager.this.adListener.onAdOpened();
                }

                public void onInterstitialDismissed(com.facebook.ads.Ad ad) {
                    FbAdManager.this.adListener.onAdClosed();
                }

                public void onError(com.facebook.ads.Ad ad, com.facebook.ads.AdError adError) {
                    FbAdManager.this.adListener.onAdFailedToLoad();
                }

                public void onAdLoaded(com.facebook.ads.Ad ad) {
                    FbAdManager.this.adListener.onAdLoaded();
                }

                public void onAdClicked(com.facebook.ads.Ad ad) {
                    FbAdManager.this.adListener.onAdClicked();
                }

                public void onLoggingImpression(com.facebook.ads.Ad ad) {
                    FbAdManager.this.adListener.onAdImpression();
                }
            });
        }
        this.mInterstitialAd.loadAd();
    }

    public boolean isInterAdLoaded() {
        if (shouldShowAd()) {
            com.facebook.ads.InterstitialAd interstitialAd = this.mInterstitialAd;
            if (interstitialAd != null && interstitialAd.isAdLoaded()) {
                return true;
            }
        }
        return false;
    }

    public void showInterAd() {
        if (shouldShowAd()) {
            com.facebook.ads.InterstitialAd interstitialAd = this.mInterstitialAd;
            if (interstitialAd != null && interstitialAd.isAdLoaded()) {
                android.util.Log.d("ADS", "show fb interstitial ad");
                this.mInterstitialAd.show();
            }
        }
    }

    public void requestNativeAd(final android.view.ViewGroup viewGroup, String nativeLayoutName, String adUnitId) {
        String str = "ADS";
        if (viewGroup == null) {
            android.util.Log.e(str, "viewGroup is null");
        } else if (!shouldShowAd() || android.text.TextUtils.isEmpty(adUnitId) || android.text.TextUtils.isEmpty(nativeLayoutName)) {
            if (this.adListener != null) {
                this.adListener.onWillNotRequest();
            }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("req fb native ad = ");
            sb.append(adUnitId);
            android.util.Log.d(str, sb.toString());
            this.nativeAdView = ((android.view.LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(getLayoutId(nativeLayoutName), null);
            this.boom = Boom.bind(this.context, this.nativeAdView);
            this.mNativeAd = new com.facebook.ads.NativeAd(this.context, adUnitId);
            this.mNativeAd.setAdListener(new com.facebook.ads.NativeAdListener() {
                public void onMediaDownloaded(com.facebook.ads.Ad ad) {
                    android.util.Log.e("ADS", "Native ad finished downloading all assets.");
                }

                public void onError(com.facebook.ads.Ad ad, com.facebook.ads.AdError adError) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Native ad failed to load: ");
                    sb.append(adError.getErrorMessage());
                    android.util.Log.e("ADS", sb.toString());
                    if (FbAdManager.this.adListener != null) {
                        FbAdManager.this.adListener.onAdFailedToLoad();
                    }
                }

                public void onAdLoaded(com.facebook.ads.Ad ad) {
                    android.util.Log.d("ADS", "Native ad is loaded and ready to be displayed!");
                    if (FbAdManager.this.adListener != null) {
                        FbAdManager.this.adListener.onAdLoaded();
                    }
                    if (FbAdManager.this.mNativeAd != null && FbAdManager.this.mNativeAd == ad) {
                        com.facebook.ads.NativeAdLayout nativeAdLayout = new com.facebook.ads.NativeAdLayout(FbAdManager.this.context);
                        FbAdManager fbAdManager = FbAdManager.this;
                        fbAdManager.populateNativeAdView(nativeAdLayout, fbAdManager.mNativeAd);
                        viewGroup.removeAllViews();
                        viewGroup.addView(nativeAdLayout, new android.widget.FrameLayout.LayoutParams(-1, -1));
                    }
                }

                public void onAdClicked(com.facebook.ads.Ad ad) {
                    android.util.Log.d("ADS", "Native ad clicked!");
                    if (FbAdManager.this.adListener != null) {
                        FbAdManager.this.adListener.onAdClicked();
                    }
                }

                public void onLoggingImpression(com.facebook.ads.Ad ad) {
                    android.util.Log.d("ADS", "Native ad impression logged!");
                    if (FbAdManager.this.adListener != null) {
                        FbAdManager.this.adListener.onAdImpression();
                    }
                }
            });
            this.mNativeAd.loadAd();
        }
    }

    public boolean isNativeAdLoaded() {
        if (shouldShowAd()) {
            com.facebook.ads.NativeAd nativeAd = this.mNativeAd;
            if (nativeAd != null && nativeAd.isAdLoaded()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void populateNativeAdView(com.facebook.ads.NativeAdLayout nativeAdLayout, com.facebook.ads.NativeAd nativeAd) {
        nativeAd.unregisterView();
        nativeAdLayout.addView(this.nativeAdView, new android.widget.FrameLayout.LayoutParams(-1, -2));
        android.widget.LinearLayout linearLayout = (android.widget.LinearLayout) this.boom.findViewById("zimba_ad_choices_container");
        com.facebook.ads.AdOptionsView adOptionsView = new com.facebook.ads.AdOptionsView(this.context, nativeAd, nativeAdLayout);
        linearLayout.removeAllViews();
        int i = 0;
        linearLayout.addView(adOptionsView, 0);
        com.facebook.ads.AdIconView adIconView = (com.facebook.ads.AdIconView) this.boom.findViewById("zimba_ad_icon");
        android.widget.TextView adHeadline = (android.widget.TextView) this.boom.findViewById("zimba_ad_headline");
        android.view.View mediaView = this.boom.findViewById("zimba_ad_banner");
        android.widget.TextView adSocialContext = (android.widget.TextView) this.boom.findViewById("zimba_ad_social_context");
        android.widget.TextView adBody = (android.widget.TextView) this.boom.findViewById("zimba_ad_body");
        android.widget.TextView sponsoredLabel = (android.widget.TextView) this.boom.findViewById("zimba_ad_sponsored_label");
        android.widget.Button callToAction = (android.widget.Button) this.boom.findViewById("zimba_ad_call_to_action");
        adHeadline.setText(nativeAd.getAdvertiserName());
        if (adBody != null) {
            adBody.setText(nativeAd.getAdBodyText());
        }
        if (adSocialContext != null) {
            adSocialContext.setText(nativeAd.getAdSocialContext());
        }
        if (!nativeAd.hasCallToAction()) {
            i = 4;
        }
        callToAction.setVisibility(i);
        callToAction.setText(nativeAd.getAdCallToAction());
        if (sponsoredLabel != null) {
            sponsoredLabel.setText(nativeAd.getSponsoredTranslation());
        }
        java.util.ArrayList arrayList = new java.util.ArrayList();
        arrayList.add(adHeadline);
        arrayList.add(callToAction);
        if (mediaView == null || !(mediaView instanceof com.facebook.ads.MediaView)) {
            nativeAd.registerViewForInteraction(this.nativeAdView, (com.facebook.ads.MediaView) adIconView, (java.util.List<android.view.View>) arrayList);
        } else {
            nativeAd.registerViewForInteraction(this.nativeAdView, (com.facebook.ads.MediaView) mediaView, (com.facebook.ads.MediaView) adIconView, (java.util.List<android.view.View>) arrayList);
        }
    }

    public void destroy() {
        com.facebook.ads.InterstitialAd interstitialAd = this.mInterstitialAd;
        if (interstitialAd != null) {
            interstitialAd.destroy();
        }
    }

    public boolean shouldShowAd() {
        return true;
    }
}
