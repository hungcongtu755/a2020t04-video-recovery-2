package com.bzvideo2004.sdk.ads.venus;

public class ConsentActivity extends android.app.Activity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(Boom.getLayoutId(this, "zimba_consent_activity"));
        int type = getIntent().getIntExtra("type", 0);
        String title = getIntent().getStringExtra("title");
        android.view.View btnBack = Boom.findViewById((android.app.Activity) this, "zimba_back");
        android.widget.TextView txtTitle = (android.widget.TextView) Boom.findViewById((android.app.Activity) this, "zimba_title");
        ((android.webkit.WebView) Boom.findViewById((android.app.Activity) this, "zimba_webview")).loadUrl(type == 0 ? "https://sites.google.com/view/com-search-by-image" : "https://sites.google.com/view/com-search-by-image");
        txtTitle.setText(title);
        btnBack.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(android.view.View view) {
                ConsentActivity.this.finish();
            }
        });
    }

    public static void showPrivacyPolicy(android.content.Context context) {
        startActivity(context, 0, "Privacy Policy");
    }

    public static void showUserAgreement(android.content.Context context) {
        startActivity(context, 1, "User Agreement");
    }

    private static void startActivity(android.content.Context context, int type, String title) {
        android.content.Intent intent = new android.content.Intent(context, ConsentActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("title", title);
        context.startActivity(intent);
    }
}
