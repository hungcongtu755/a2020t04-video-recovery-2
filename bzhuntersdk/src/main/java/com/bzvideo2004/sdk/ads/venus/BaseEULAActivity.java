package com.bzvideo2004.sdk.ads.venus;

public abstract class BaseEULAActivity extends android.app.Activity {
    protected AdManager adManager;
    protected AdManagerOptions adManagerOptions;
    protected Boom boom;
    private android.widget.Button btnContinue;
    public android.view.View btnSkip;
    protected android.content.SharedPreferences eula;
    public boolean isInterAdClicked;
    private boolean isInterAdShowed;
    private android.widget.ProgressBar loading;
    private android.view.ViewGroup nativeAdContainer;
    private android.view.ViewGroup splashAdView;
    private android.view.View splashView;
    private android.widget.TextView txtAgree;
    public android.widget.TextView txtCountDown;
    public android.widget.TextView txtSkipAds;

    protected abstract AdManagerOptions initAdManagerOptions();

    public void onCreate(android.os.Bundle bundle) {
        super.onCreate(bundle);
        this.boom = Boom.bind((android.app.Activity) this, "zimba_eula_activity");
        this.eula = getSharedPreferences("app_eula", 0);
        this.splashAdView = (android.view.ViewGroup) this.boom.findViewById("zimba_splash_ad_view");
        this.nativeAdContainer = (android.view.ViewGroup) this.boom.findViewById("zimba_container_ad_view");
        this.splashView = this.boom.findViewById("zimba_splash_view");
        initView();
        this.adManagerOptions = initAdManagerOptions();
        this.adManager = new AdManager(this, this.adManagerOptions);
        this.adManager.requestNativeAd(this.nativeAdContainer);
        this.adManager.requestInterAd();
        this.adManager.setAdListener(new AdListener() {
            public void onAdClicked() {
                BaseEULAActivity.this.isInterAdClicked = true;
            }
        });
    }

    protected void onResume() {
        super.onResume();
        if (this.isInterAdShowed || this.isInterAdClicked) {
            startMainActivity();
        }
        this.isInterAdShowed = false;
        this.isInterAdClicked = false;
    }

    protected boolean willSkipAd() {
        return false;
    }

    private void initView() {
        this.btnContinue = (android.widget.Button) this.boom.findViewById("zimba_btn_continue");
        this.txtAgree = (android.widget.TextView) this.boom.findViewById("zimba_txt_agree");
        this.loading = (android.widget.ProgressBar) this.boom.findViewById("zimba_loading_bar");
        this.txtCountDown = (android.widget.TextView) this.boom.findViewById("zimba_txt_countdown");
        this.txtSkipAds = (android.widget.TextView) this.boom.findViewById("zimba_txt_skip_ads");
        this.btnSkip = this.boom.findViewById("zimba_btn_skip");
        this.loading.setVisibility((int)8);
        this.btnContinue.setVisibility((int)8);
        this.txtAgree.setVisibility((int)8);
        this.btnSkip.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(android.view.View view) {
                BaseEULAActivity.this.startMainActivity();
            }
        });
        this.txtSkipAds.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(android.view.View view) {
                BaseEULAActivity.this.startMainActivity();
            }
        });
        if (this.eula.getBoolean("user_accept_eula", false)) {
            waitForAd();
            return;
        }
        this.loading.setVisibility((int)8);
        this.btnContinue.setVisibility((int)0);
        this.txtAgree.setVisibility((int)0);
        this.btnContinue.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(android.view.View view) {
                BaseEULAActivity.this.eula.edit().putBoolean("user_accept_eula", true).apply();
                BaseEULAActivity.this.waitForAd();
            }
        });
        createTextLink();
    }

    /* access modifiers changed from: private */
    public void waitForAd() {
        this.loading.setVisibility((int)0);
        this.btnContinue.setVisibility((int)8);
        this.txtAgree.setVisibility((int)8);
        new android.os.Handler().postDelayed(new Runnable() {
            public void run() {
                BaseEULAActivity.this.nextStep();
            }
        }, 5000);
    }

    /* access modifiers changed from: private */
    public void nextStep() {
        if (willSkipAd() || this.adManager == null) {
            startMainActivity();
        } else if (AdManagerOptions.AdType.INTERSTITIAL == this.adManagerOptions.getPreferAdType()) {
            boolean showInter = showInter();
            boolean showNative = false;
            if (!showInter) {
                showNative = showNative();
            }
            if (!showInter && !showNative) {
                startMainActivity();
            }
        } else if (AdManagerOptions.AdType.NATIVE == this.adManagerOptions.getPreferAdType()) {
            boolean showNative2 = showNative();
            boolean showInter2 = false;
            if (!showNative2) {
                showInter2 = showInter();
            }
            if (!showInter2 && !showNative2) {
                startMainActivity();
            }
        }
    }

    private boolean showInter() {
        AdManager adManager2 = this.adManager;
        if (adManager2 == null || !adManager2.isInterAdLoaded()) {
            return false;
        }
        this.adManager.showInterAd();
        this.isInterAdShowed = true;
        return true;
    }

    private boolean showNative() {
        AdManager adManager2 = this.adManager;
        if (adManager2 == null || !adManager2.isNativeAdLoaded()) {
            return false;
        }
        this.splashView.setVisibility((int)8);
        this.splashAdView.setVisibility((int)0);
        this.txtSkipAds.setVisibility((int)8);
        this.btnSkip.setVisibility((int)8);
        this.txtCountDown.setVisibility((int)0);
        android.os.CountDownTimer r2 = new android.os.CountDownTimer(com.facebook.login.widget.ToolTipPopup.DEFAULT_POPUP_DISPLAY_TIME, 1000) {
            public void onTick(long j) {
                android.widget.TextView access$300 = BaseEULAActivity.this.txtCountDown;
                StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(j / 1000);
                access$300.setText(sb.toString());
            }

            public void onFinish() {
                BaseEULAActivity.this.txtSkipAds.setVisibility((int)0);
                BaseEULAActivity.this.btnSkip.setVisibility((int)0);
                BaseEULAActivity.this.txtCountDown.setVisibility((int)8);
            }
        };
        r2.start();
        return true;
    }

    protected void createTextLink() {
        String str = "By continuing you agree to our Privacy Policy";
        android.text.SpannableString spannableString = new android.text.SpannableString(str);
        String str3 = "Privacy Policy";
        int indexOf = str.indexOf("");
        int indexOf2 = str.indexOf(str3);
        new android.text.style.ClickableSpan() {
            public void onClick(android.view.View view) {
                ConsentActivity.showUserAgreement(BaseEULAActivity.this);
            }
        };
        spannableString.setSpan(new android.text.style.ClickableSpan() {
            public void onClick(android.view.View view) {
                ConsentActivity.showPrivacyPolicy(BaseEULAActivity.this);
            }
        }, indexOf2, str3.length() + indexOf2, 33);
        this.txtAgree.setLinksClickable(true);
        this.txtAgree.setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
        this.txtAgree.setText(spannableString, android.widget.TextView.BufferType.SPANNABLE);
    }

    protected void startMainActivity() {
        startActivity(new android.content.Intent(this, this.adManagerOptions.getMainClass()));
        finish();
    }

    public void onBackPressed() {
    }
}
