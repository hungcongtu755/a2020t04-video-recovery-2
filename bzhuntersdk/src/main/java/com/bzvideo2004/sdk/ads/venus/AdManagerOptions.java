package com.bzvideo2004.sdk.ads.venus;

public class AdManagerOptions {
    public java.util.HashMap<AdManagerOptions.AdNetwork, AdManagerOptions.AdOptions> adOptions;
    public Class mainClass;
    public AdManagerOptions.AdType preferAdType;
    /* access modifiers changed from: private */
    public AdManagerOptions.AdNetwork selectedAdNetwork;

    public enum AdNetwork {
        ADMOB("ga"),
        FACEBOOK("fb");
        
        private String net;

        private AdNetwork(String net2) {
            this.net = net2;
        }

        public String value() {
            return this.net;
        }
    }

    public static class AdOptions {
        public AdManagerOptions.AdType adType;
        public String interAdUnitId;
        public boolean isEnabled;
        public String nativeAdLayout;
        public String nativeAdUnitId;

        public static class Builder {
            private AdManagerOptions.AdType adType = AdManagerOptions.AdType.INTERSTITIAL;
            private String interAdUnitId;
            private boolean isEnabled = true;
            private String nativeAdLayout;
            private String nativeAdUnitId;

            public Builder() {
                String str = "";
                this.interAdUnitId = str;
                this.nativeAdUnitId = str;
                this.nativeAdLayout = str;
            }

            public static AdManagerOptions.AdOptions.Builder create() {
                return new AdManagerOptions.AdOptions.Builder();
            }

            public AdManagerOptions.AdOptions.Builder enabled(boolean isEnabled2) {
                this.isEnabled = isEnabled2;
                return this;
            }

            public AdManagerOptions.AdOptions.Builder withAdType(AdManagerOptions.AdType adType2) {
                this.adType = adType2;
                return this;
            }

            public AdManagerOptions.AdOptions.Builder withInterAdUnitId(String interAdUnitId2) {
                this.interAdUnitId = interAdUnitId2;
                return this;
            }

            public AdManagerOptions.AdOptions.Builder withNativeAdUnitId(String nativeAdUnitId2) {
                this.nativeAdUnitId = nativeAdUnitId2;
                return this;
            }

            public AdManagerOptions.AdOptions.Builder withNativeAdLayout(String nativeAdLayout2) {
                this.nativeAdLayout = nativeAdLayout2;
                return this;
            }

            public AdManagerOptions.AdOptions build() {
                AdManagerOptions.AdOptions adOptions = new AdManagerOptions.AdOptions();
                adOptions.isEnabled = this.isEnabled;
                adOptions.adType = this.adType;
                adOptions.interAdUnitId = this.interAdUnitId;
                adOptions.nativeAdUnitId = this.nativeAdUnitId;
                adOptions.nativeAdLayout = this.nativeAdLayout;
                return adOptions;
            }
        }

        public String getInterAdUnitId() {
            return this.interAdUnitId;
        }

        public String getNativeAdUnitId() {
            return this.nativeAdUnitId;
        }

        public String getNativeAdLayout() {
            return this.nativeAdLayout;
        }

        public boolean isEnabled() {
            return this.isEnabled;
        }

        public AdManagerOptions.AdType getAdType() {
            return this.adType;
        }
    }

    public enum AdType {
        INTERSTITIAL("it"),
        NATIVE("nt");
        
        private String type;

        private AdType(String type2) {
            this.type = type2;
        }

        public String value() {
            return this.type;
        }
    }

    public static final class Builder {
        private java.util.HashMap<AdManagerOptions.AdNetwork, AdManagerOptions.AdOptions> adOptions = new java.util.HashMap<>();
        private Class mainClass;
        private AdManagerOptions.AdType preferAdType = AdManagerOptions.AdType.NATIVE;
        private AdManagerOptions.AdNetwork selectedAdNetwork = AdManagerOptions.AdNetwork.ADMOB;
        public static AdManagerOptions.Builder create() {
            return new AdManagerOptions.Builder();
        }
        public AdManagerOptions.Builder withAdNetwork(AdManagerOptions.AdNetwork adNetwork) {
            this.selectedAdNetwork = adNetwork;
            return this;
        }
        public AdManagerOptions.Builder preferAdType(AdManagerOptions.AdType adType) {
            this.preferAdType = adType;
            return this;
        }
        public AdManagerOptions.Builder addAdOptions(AdManagerOptions.AdNetwork adNetwork, AdManagerOptions.AdOptions adOptions2) {
            this.adOptions.put(adNetwork, adOptions2);
            return this;
        }

        public AdManagerOptions.Builder withMainClass(Class clazz) {
            this.mainClass = clazz;
            return this;
        }

        public AdManagerOptions build() {
            AdManagerOptions options = new AdManagerOptions();
            options.adOptions = this.adOptions;
            options.selectedAdNetwork = this.selectedAdNetwork;
            options.preferAdType = this.preferAdType;
            options.mainClass = this.mainClass;
            return options;
        }
    }

    public AdManagerOptions.AdOptions getAdOptions() {
        return (AdManagerOptions.AdOptions) this.adOptions.get(this.selectedAdNetwork);
    }

    public AdManagerOptions.AdNetwork getAdNetwork() {
        return this.selectedAdNetwork;
    }
    public AdManagerOptions.AdType getPreferAdType() {
        return this.preferAdType;
    }
    public Class getMainClass() {
        return this.mainClass;
    }
}
