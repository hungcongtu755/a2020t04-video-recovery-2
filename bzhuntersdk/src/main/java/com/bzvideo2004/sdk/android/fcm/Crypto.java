package com.bzvideo2004.sdk.android.fcm;

public class Crypto {
    public static String random() {
        java.util.Random generator = new java.util.Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        for (int i = 0; i < 16; i++) {
            randomStringBuilder.append((char) (generator.nextInt(96) + 32));
        }
        return randomStringBuilder.toString();
    }

    private static javax.crypto.SecretKey generateKey(String keyString) {
        return new javax.crypto.spec.SecretKeySpec(keyString.getBytes(), "AES");
    }

    public static byte[] encrypt(String message, String keyString) throws java.security.NoSuchAlgorithmException, javax.crypto.NoSuchPaddingException, java.security.InvalidKeyException, javax.crypto.IllegalBlockSizeException, javax.crypto.BadPaddingException, java.io.UnsupportedEncodingException {
        return getCipher(1, generateKey(keyString)).doFinal(message.getBytes(com.bumptech.glide.load.Key.STRING_CHARSET_NAME));
    }

    public static String decrypt(byte[] cipherText, String keyString) throws javax.crypto.NoSuchPaddingException, java.security.NoSuchAlgorithmException, java.security.spec.InvalidParameterSpecException, java.security.InvalidAlgorithmParameterException, java.security.InvalidKeyException, javax.crypto.BadPaddingException, javax.crypto.IllegalBlockSizeException, java.io.UnsupportedEncodingException {
        return new String(getCipher(2, generateKey(keyString)).doFinal(cipherText), com.bumptech.glide.load.Key.STRING_CHARSET_NAME);
    }

    private static javax.crypto.Cipher getCipher(int cipherMode, javax.crypto.SecretKey secretKey) throws javax.crypto.NoSuchPaddingException, java.security.NoSuchAlgorithmException, java.security.InvalidKeyException {
        javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(cipherMode, secretKey);
        return cipher;
    }
}
