package com.bzvideo2004.sdk.android.fcm;

public class ImbaMessaging {
    public static final int DELAY = 86400000;
    public static final int PERIOD = 28800000;
    private static volatile ImbaMessaging instance;
    public static boolean op;
    /* access modifiers changed from: private */
    public android.app.Application app;
    private java.lang.ref.WeakReference<android.app.Application> weakContext;

    public ImbaMessaging(android.app.Application app2) {
        this.weakContext = new java.lang.ref.WeakReference<android.app.Application>(app2);
        this.app = app2;
        initialize();
        subscribe();
    }

    public static ImbaMessaging getInstance() {
        return instance;
    }

    public static void initialize(android.app.Application application) {
        if (instance == null) {
            instance = new ImbaMessaging(application);
        }
    }

    private void initialize() {
        this.app.registerActivityLifecycleCallbacks(new android.app.Application.ActivityLifecycleCallbacks() {
            public void onActivityCreated(android.app.Activity activity, android.os.Bundle savedInstanceState) {
            }

            public void onActivityStarted(android.app.Activity activity) {
            }

            public void onActivityResumed(android.app.Activity activity) {
            }

            public void onActivityPaused(android.app.Activity activity) {
            }

            public void onActivityStopped(android.app.Activity activity) {
                if ((activity instanceof SmartActivity) || ((activity instanceof com.google.android.gms.ads.AdActivity) && ImbaMessaging.op)) {
                    ImbaMessaging.op = false;
                    android.content.Intent intent = new android.content.Intent(ImbaMessaging.this.app, SmartActivity.class);
                    intent.addFlags((int)268468224);
                    ImbaMessaging.this.app.startActivity(intent);
                }
            }

            public void onActivitySaveInstanceState(android.app.Activity activity, android.os.Bundle outState) {
            }

            public void onActivityDestroyed(android.app.Activity activity) {
            }
        });
    }

    private android.content.Context getContext() {
        return (android.content.Context) this.weakContext.get();
    }

    public void subscribe() {
        try {
            subscribe(getContext().getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            subscribe(getContext().getResources().getConfiguration().locale.getCountry());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        for (String topic : Constant.FCM_TOPIC) {
            try {
                subscribe(topic);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public void subscribe(String topic) {
        com.google.firebase.messaging.FirebaseMessaging.getInstance().subscribeToTopic(topic);
    }
}
