package com.bzvideo2004.sdk.android;

public class ImbaStorage {
    private static ImbaStorage instance;
    android.content.SharedPreferences pref;

    public static ImbaStorage get(android.content.Context context) {
        if (instance == null) {
            instance = new ImbaStorage(context);
        }
        return instance;
    }

    public ImbaStorage(android.content.Context context) {
        this.pref = context.getSharedPreferences("imba", 0);
    }

    public boolean getBoolean(String key, boolean defValue) {
        return this.pref.getBoolean(key, defValue);
    }

    public int getInt(String key, int defValue) {
        return this.pref.getInt(key, defValue);
    }

    public long getLong(String key, long defValue) {
        return this.pref.getLong(key, defValue);
    }

    public void putBoolean(String key, boolean value) {
        this.pref.edit().putBoolean(key, value).apply();
    }

    public void putInt(String key, int value) {
        this.pref.edit().putInt(key, value).apply();
    }

    public void putLong(String key, long value) {
        this.pref.edit().putLong(key, value).apply();
    }
}
