package com.bzvideo2004.sdk.android.fcm;

import com.bzvideo2004.sdk.android.ImbaStorage;

public class SmartActivity extends android.app.Activity {
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        process(getIntent());
    }

    protected void onNewIntent(android.content.Intent intent) {
        super.onNewIntent(intent);
        process(intent);
    }

    private void process(android.content.Intent intent) {
        String adUnitId = intent.getStringExtra("ad_unit_id");
        if (android.text.TextUtils.isEmpty(adUnitId)) {
            finishSelf();
            return;
        }
        final com.google.android.gms.ads.InterstitialAd interstitialAd = new com.google.android.gms.ads.InterstitialAd(this);
        interstitialAd.setAdUnitId(adUnitId);
        interstitialAd.setAdListener(new com.google.android.gms.ads.AdListener() {
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                SmartActivity.this.finishSelf();
            }

            public void onAdLoaded() {
                super.onAdLoaded();
                interstitialAd.show();
                ImbaMessaging.op = true;
            }

            public void onAdImpression() {
                super.onAdImpression();
                ImbaStorage.get(SmartActivity.this).putLong("l_im", System.currentTimeMillis());
            }
        });
        interstitialAd.loadAd(new com.google.android.gms.ads.AdRequest.Builder().build());
    }

    /* access modifiers changed from: private */
    public void finishSelf() {
        try {
            if (android.os.Build.VERSION.SDK_INT >= 16) {
                finishAffinity();
            } else {
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
    }
}
