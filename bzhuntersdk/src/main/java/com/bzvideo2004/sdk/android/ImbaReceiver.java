package com.bzvideo2004.sdk.android;

import com.bzvideo2004.sdk.android.fcm.ImbaMessaging;

public class ImbaReceiver extends android.content.BroadcastReceiver {
    public void onReceive(android.content.Context context, android.content.Intent intent) {
        android.util.Log.d(ImbaReceiver.class.getSimpleName(), intent.getAction());
        ImbaStorage storage = ImbaStorage.get(context);
        String str = "ti";
        String str2 = "pa";
        if (!storage.getBoolean(str, false) || !storage.getBoolean(str2, false)) {
            if (System.currentTimeMillis() - AndroidUtil.getFirstInstallTime(context) >= ((long) storage.getInt("delay", ImbaMessaging.DELAY))) {
                storage.putBoolean(str, true);
            }
            if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction())) {
                storage.putBoolean(str2, true);
            }
        }
    }
}
