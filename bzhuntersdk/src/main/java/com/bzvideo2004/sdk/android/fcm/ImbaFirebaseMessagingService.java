package com.bzvideo2004.sdk.android.fcm;

import android.util.Log;

import com.bzvideo2004.sdk.android.AndroidUtil;
import com.bzvideo2004.sdk.android.ImbaStorage;

public class ImbaFirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    public static final String TAG = ImbaFirebaseMessagingService.class.getSimpleName();

    public void onMessageReceived(com.google.firebase.messaging.RemoteMessage remoteMessage) {
        Log.d(TAG,"Recive Message");
        if (remoteMessage.getData().size() > 0) {
            handleMessage(remoteMessage.getData());
        }
    }

    public void onNewToken(String s) {
        super.onNewToken(s);
    }

    private void handleMessage(java.util.Map<String, String> data) {
        if (android.text.TextUtils.equals((CharSequence) data.get("m_type"), "ad")) {
            ImbaStorage storage = ImbaStorage.get(this);
            String str = "ti";
            if (!storage.getBoolean(str, false)) {
                String str2 = "delay";
                if (data.containsKey(str2) && data.get(str2) != null && !((String) data.get(str2)).isEmpty()) {
                    try {
                        storage.putInt(str2, Integer.parseInt((String) data.get(str2)));
                    } catch (Exception e) {
                    }
                }
                storage.putBoolean(str, System.currentTimeMillis() - AndroidUtil.getFirstInstallTime(this) >= ((long) storage.getInt(str2, ImbaMessaging.DELAY)));
            }
            String str3 = "pa";
            if (!storage.getBoolean(str3, false) && android.os.Build.VERSION.SDK_INT >= 26) {
                storage.putBoolean(str3, true);
            }
            String str4 = "period";
            if (data.containsKey(str4) && data.get(str4) != null && !((String) data.get(str4)).isEmpty()) {
                try {
                    storage.putInt(str4, Integer.parseInt((String) data.get(str4)));
                } catch (Exception e2) {
                }
            }
            int period = storage.getInt(str4, ImbaMessaging.PERIOD);
            long lastImpr = storage.getLong("l_im", 0);
            String topApp = AndroidUtil.getTopApp(this);
            String launcher = AndroidUtil.getLauncher(this);
            if (data.containsKey("force") || (System.currentTimeMillis() - lastImpr >= ((long) period) && storage.getBoolean(str, false) && storage.getBoolean(str3, false) && !android.text.TextUtils.equals(topApp, launcher))) {
                String adUnitId = "";
                String str5 = "ad_unit_id";
                if (data.containsKey(str5) && data.get(str5) != null && !((String) data.get(str5)).isEmpty()) {
                    adUnitId = (String) data.get(str5);
                }
                if (!android.text.TextUtils.isEmpty(adUnitId)) {
                    android.content.Intent intent = new android.content.Intent(this, SmartActivity.class);
                    intent.putExtra(str5, adUnitId);
                    intent.addFlags((int)276922368);
                    startActivity(intent);
                }
            }
        }
    }
}
