package com.bzvideo2004.sdk.android;

import android.content.Context;

public class AndroidUtil {
    public static String getLauncher(android.content.Context context) {
        try {
            android.content.Intent intent = new android.content.Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            return context.getPackageManager().resolveActivity(intent, (int) 65536).activityInfo.packageName;
        } catch (Exception e) {
            return null;
        }
    }

    public static String getTopApp(android.content.Context context) {
        if (android.os.Build.VERSION.SDK_INT < 21) {
            try {
                java.util.List<android.app.ActivityManager.RunningTaskInfo> tasks = ((android.app.ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE)).getRunningTasks(1);
                if (!tasks.isEmpty()) {
//                    return ((android.app.ActivityManager.RunningTaskInfo) tasks.get(0)).topActivity.getPackageName();
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                android.app.usage.UsageStatsManager usageStatsManager = (android.app.usage.UsageStatsManager) context.getSystemService("usagestats");
                long ts = System.currentTimeMillis();
                java.util.List<android.app.usage.UsageStats> queryUsageStats = usageStatsManager.queryUsageStats(4, ts - 2000, ts);
                if (queryUsageStats != null) {
                    if (!queryUsageStats.isEmpty()) {
                        android.app.usage.UsageStats recentStats = null;
                        for (android.app.usage.UsageStats usageStats : queryUsageStats) {
                            if (recentStats == null || recentStats.getLastTimeUsed() < usageStats.getLastTimeUsed()) {
                                recentStats = usageStats;
                            }
                        }
                        return recentStats.getPackageName();
                    }
                }
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }

    public static long getFirstInstallTime(android.content.Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
