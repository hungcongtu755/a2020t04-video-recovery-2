package com.bzvideo2004.sdk.inapp.sub.util;

public class CertificationVerifier {
    private String cert;
    private android.content.Context context;
    private String realCert = "8edb3d87208eee6d24c0a42fc5438de";

    public boolean isCertVerfied() {
        return true;
    }

    public CertificationVerifier(android.content.Context context2) {
        this.context = context2;
        this.cert = getCert();
    }

    public String getCert() {
        android.content.pm.PackageInfo packageInfo;
        java.security.cert.CertificateFactory certificateFactory;
        java.security.cert.X509Certificate x509Certificate;
        try {
            packageInfo = this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), (int)64);
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            packageInfo = null;
        }
        java.io.ByteArrayInputStream byteArrayInputStream = new java.io.ByteArrayInputStream(packageInfo.signatures[0].toByteArray());
        try {
            certificateFactory = java.security.cert.CertificateFactory.getInstance("X509");
        } catch (Exception e2) {
            e2.printStackTrace();
            certificateFactory = null;
        }
        try {
            x509Certificate = (java.security.cert.X509Certificate) certificateFactory.generateCertificate(byteArrayInputStream);
        } catch (Exception e3) {
            e3.printStackTrace();
            x509Certificate = null;
        }
        try {
            return sha1(java.security.MessageDigest.getInstance("SHA1").digest(x509Certificate.getEncoded()));
        } catch (java.security.NoSuchAlgorithmException e4) {
            e4.printStackTrace();
            return null;
        } catch (java.security.cert.CertificateEncodingException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    private String sha1(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (int i = 0; i < bArr.length; i++) {
            String hexString = Integer.toHexString(bArr[i]);
            int length = hexString.length();
            if (length == 1) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(com.facebook.appevents.AppEventsConstants.EVENT_PARAM_VALUE_NO);
                sb2.append(hexString);
                hexString = sb2.toString();
            }
            if (length > 2) {
                hexString = hexString.substring(length - 2, length);
            }
            sb.append(hexString.toUpperCase());
            if (i < bArr.length - 1) {
                sb.append(':');
            }
        }
        return sb.toString();
    }

    public String md5(String str) {
        try {
            java.security.MessageDigest instance = java.security.MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer("");
            for (byte b : digest) {
                stringBuffer.append(Integer.toHexString(b & Byte.MAX_VALUE));
            }
            return stringBuffer.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "d1b96e456dadad14b10aa3feda978a91";
        }
    }
}
