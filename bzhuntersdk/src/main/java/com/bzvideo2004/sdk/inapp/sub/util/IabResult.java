package com.bzvideo2004.sdk.inapp.sub.util;

public class IabResult {
    String mMessage;
    int mResponse;

    public IabResult(int response, String message) {
        this.mResponse = response;
        if (message == null || message.trim().length() == 0) {
            this.mMessage = IabHelper.getResponseDesc(response);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(message);
        sb.append(" (response: ");
        sb.append(IabHelper.getResponseDesc(response));
        sb.append(")");
        this.mMessage = sb.toString();
    }

    public int getResponse() {
        return this.mResponse;
    }

    public String getMessage() {
        return this.mMessage;
    }

    public boolean isSuccess() {
        return this.mResponse == 0;
    }

    public boolean isFailure() {
        return !isSuccess();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IabResult: ");
        sb.append(getMessage());
        return sb.toString();
    }
}
