package com.bzvideo2004.sdk.inapp.sub.util;

public class PremiumConfig extends BaseSharedPrefs {
    private static PremiumConfig instance;
    private android.content.Context context;

    public static PremiumConfig get(android.content.Context context2) {
        if (instance == null) {
            synchronized (PremiumConfig.class) {
                if (instance == null) {
                    instance = new PremiumConfig(context2);
                }
            }
        }
        return instance;
    }

    private PremiumConfig(android.content.Context context2) {
        android.content.Context applicationContext = context2.getApplicationContext();
        if (applicationContext != null) {
            context2 = applicationContext;
        }
        this.context = context2;
    }

    public android.content.SharedPreferences getSharedPref() {
        return this.context.getSharedPreferences("premium", 0);
    }

    public void setSubscribed(boolean subscribed) {
        putBoolean("k_ps", subscribed);
    }

    public boolean isSubscribed() {
        boolean isSubscribed = getBoolean("k_ps", false);
        StringBuilder sb = new StringBuilder();
        sb.append("isSubscribed: ");
        sb.append(isSubscribed);
        android.util.Log.i("PremiumConfig", sb.toString());
        return isSubscribed;
    }

    public boolean isNotSubscribed() {
        return !isSubscribed();
    }

    public void setFakeAccount(boolean z) {
        putBoolean("k_f_a", z);
    }

    public boolean isFakeAccount() {
        return getBoolean("k_f_a", false);
    }
}
