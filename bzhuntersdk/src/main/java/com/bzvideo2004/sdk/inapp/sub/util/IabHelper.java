package com.bzvideo2004.sdk.inapp.sub.util;

import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;

import org.json.JSONException;

import java.util.ArrayList;

public class IabHelper {
    public static final int BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE = 3;
    public static final int BILLING_RESPONSE_RESULT_DEVELOPER_ERROR = 5;
    public static final int BILLING_RESPONSE_RESULT_ERROR = 6;
    public static final int BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED = 7;
    public static final int BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED = 8;
    public static final int BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE = 4;
    public static final int BILLING_RESPONSE_RESULT_OK = 0;
    public static final int BILLING_RESPONSE_RESULT_SERVICE_UNAVAILABLE = 2;
    public static final int BILLING_RESPONSE_RESULT_USER_CANCELED = 1;
    public static final String GET_SKU_DETAILS_ITEM_LIST = "ITEM_ID_LIST";
    public static final String GET_SKU_DETAILS_ITEM_TYPE_LIST = "ITEM_TYPE_LIST";
    public static final int IABHELPER_BAD_RESPONSE = -1002;
    public static final int IABHELPER_ERROR_BASE = -1000;
    public static final int IABHELPER_INVALID_CONSUMPTION = -1010;
    public static final int IABHELPER_MISSING_TOKEN = -1007;
    public static final int IABHELPER_REMOTE_EXCEPTION = -1001;
    public static final int IABHELPER_SEND_INTENT_FAILED = -1004;
    public static final int IABHELPER_SUBSCRIPTIONS_NOT_AVAILABLE = -1009;
    public static final int IABHELPER_SUBSCRIPTION_UPDATE_NOT_AVAILABLE = -1011;
    public static final int IABHELPER_UNKNOWN_ERROR = -1008;
    public static final int IABHELPER_UNKNOWN_PURCHASE_RESPONSE = -1006;
    public static final int IABHELPER_USER_CANCELLED = -1005;
    public static final int IABHELPER_VERIFICATION_FAILED = -1003;
    public static final String INAPP_CONTINUATION_TOKEN = "INAPP_CONTINUATION_TOKEN";
    public static final String ITEM_TYPE_INAPP = "inapp";
    public static final String ITEM_TYPE_SUBS = "subs";
    public static final String RESPONSE_BUY_INTENT = "BUY_INTENT";
    public static final String RESPONSE_CODE = "RESPONSE_CODE";
    public static final String RESPONSE_GET_SKU_DETAILS_LIST = "DETAILS_LIST";
    public static final String RESPONSE_INAPP_ITEM_LIST = "INAPP_PURCHASE_ITEM_LIST";
    public static final String RESPONSE_INAPP_PURCHASE_DATA = "INAPP_PURCHASE_DATA";
    public static final String RESPONSE_INAPP_PURCHASE_DATA_LIST = "INAPP_PURCHASE_DATA_LIST";
    public static final String RESPONSE_INAPP_SIGNATURE = "INAPP_DATA_SIGNATURE";
    public static final String RESPONSE_INAPP_SIGNATURE_LIST = "INAPP_DATA_SIGNATURE_LIST";
    boolean mAsyncInProgress = false;
    private final Object mAsyncInProgressLock = new Object();
    String mAsyncOperation = "";
    android.content.Context mContext;
    boolean mDebugLog = false;
    String mDebugTag = "IMBA";
    boolean mDisposeAfterAsync = false;
    boolean mDisposed = false;
    IabHelper.OnIabPurchaseFinishedListener mPurchaseListener;
    String mPurchasingItemType;
    int mRequestCode;
    com.android.vending.billing.IInAppBillingService mService;
    android.content.ServiceConnection mServiceConn;
    boolean mSetupDone = false;
    String mSignatureBase64 = null;
    boolean mSubscriptionUpdateSupported = false;
    boolean mSubscriptionsSupported = false;

    public static class IabAsyncInProgressException extends Exception {
        public IabAsyncInProgressException(String message) {
            super(message);
        }
    }

    public interface OnConsumeFinishedListener {
        void onConsumeFinished(Purchase purchase, IabResult iabResult);
    }

    public interface OnConsumeMultiFinishedListener {
        void onConsumeMultiFinished(java.util.List<Purchase> list, java.util.List<IabResult> list2);
    }

    public interface OnIabPurchaseFinishedListener {
        void onIabPurchaseFinished(IabResult iabResult, Purchase purchase);
    }

    public interface OnIabSetupFinishedListener {
        void onIabSetupFinished(IabResult iabResult);
    }

    public interface QueryInventoryFinishedListener {
        void onQueryInventoryFinished(IabResult iabResult, Inventory inventory);
    }

    public IabHelper(android.content.Context ctx, String base64PublicKey) {
        this.mContext = ctx.getApplicationContext();
        this.mSignatureBase64 = base64PublicKey;
        logDebug("IAB helper created.");
    }

    public void enableDebugLogging(boolean enable, String tag) {
        checkNotDisposed();
        this.mDebugLog = enable;
        this.mDebugTag = tag;
    }

    public void enableDebugLogging(boolean enable) {
        checkNotDisposed();
        this.mDebugLog = enable;
    }

    public void startSetup(final IabHelper.OnIabSetupFinishedListener listener) {
        checkNotDisposed();
        if (!this.mSetupDone) {
            logDebug("Starting in-app billing setup.");
            this.mServiceConn = new android.content.ServiceConnection() {
                public void onServiceDisconnected(android.content.ComponentName name) {
                    IabHelper.this.logDebug("Billing service disconnected.");
                    IabHelper.this.mService = null;
                }

                public void onServiceConnected(android.content.ComponentName name, android.os.IBinder service) {
                    String str = "subs";
                    if (!IabHelper.this.mDisposed) {
                        IabHelper.this.logDebug("Billing service connected.");
                        IabHelper.this.mService = com.android.vending.billing.IInAppBillingService.Stub.asInterface(service);
                        String packageName = IabHelper.this.mContext.getPackageName();
                        try {
                            IabHelper.this.logDebug("Checking for in-app billing 3 support.");
                            int response = IabHelper.this.mService.isBillingSupported(3, packageName, "inapp");
                            if (response != 0) {
                                if (listener != null) {
                                    listener.onIabSetupFinished(new IabResult(response, "Error checking for billing v3 support."));
                                }
                                IabHelper.this.mSubscriptionsSupported = false;
                                IabHelper.this.mSubscriptionUpdateSupported = false;
                                return;
                            }
                            IabHelper iabHelper = IabHelper.this;
                            StringBuilder sb = new StringBuilder();
                            sb.append("In-app billing version 3 supported for ");
                            sb.append(packageName);
                            iabHelper.logDebug(sb.toString());
                            if (IabHelper.this.mService.isBillingSupported(5, packageName, str) == 0) {
                                IabHelper.this.logDebug("Subscription re-signup AVAILABLE.");
                                IabHelper.this.mSubscriptionUpdateSupported = true;
                            } else {
                                IabHelper.this.logDebug("Subscription re-signup not available.");
                                IabHelper.this.mSubscriptionUpdateSupported = false;
                            }
                            if (IabHelper.this.mSubscriptionUpdateSupported) {
                                IabHelper.this.mSubscriptionsSupported = true;
                            } else {
                                int response2 = IabHelper.this.mService.isBillingSupported(3, packageName, str);
                                if (response2 == 0) {
                                    IabHelper.this.logDebug("Subscriptions AVAILABLE.");
                                    IabHelper.this.mSubscriptionsSupported = true;
                                } else {
                                    IabHelper iabHelper2 = IabHelper.this;
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("Subscriptions NOT AVAILABLE. Response: ");
                                    sb2.append(response2);
                                    iabHelper2.logDebug(sb2.toString());
                                    IabHelper.this.mSubscriptionsSupported = false;
                                    IabHelper.this.mSubscriptionUpdateSupported = false;
                                }
                            }
                            IabHelper.this.mSetupDone = true;
                            IabHelper.OnIabSetupFinishedListener onIabSetupFinishedListener = listener;
                            if (onIabSetupFinishedListener != null) {
                                onIabSetupFinishedListener.onIabSetupFinished(new IabResult(0, "Setup successful."));
                            }
                        } catch (android.os.RemoteException e) {
                            IabHelper.OnIabSetupFinishedListener onIabSetupFinishedListener2 = listener;
                            if (onIabSetupFinishedListener2 != null) {
                                onIabSetupFinishedListener2.onIabSetupFinished(new IabResult(-1001, "RemoteException while setting up in-app billing."));
                            }
                            e.printStackTrace();
                        }
                    }
                }
            };
            android.content.Intent serviceIntent = new android.content.Intent("com.android.vending.billing.InAppBillingService.BIND");
            serviceIntent.setPackage("com.android.vending");
            java.util.List<android.content.pm.ResolveInfo> intentServices = this.mContext.getPackageManager().queryIntentServices(serviceIntent, 0);
            if (intentServices != null && !intentServices.isEmpty()) {
                this.mContext.bindService(serviceIntent, this.mServiceConn, (int)1);
            } else if (listener != null) {
                listener.onIabSetupFinished(new IabResult(3, "Billing service unavailable on device."));
            }
        } else {
            throw new IllegalStateException("IAB helper is already set up.");
        }
    }

    public void dispose() throws IabHelper.IabAsyncInProgressException {
        synchronized (this.mAsyncInProgressLock) {
            if (this.mAsyncInProgress) {
                StringBuilder sb = new StringBuilder();
                sb.append("Can't dispose because an async operation (");
                sb.append(this.mAsyncOperation);
                sb.append(") is in progress.");
                throw new IabHelper.IabAsyncInProgressException(sb.toString());
            }
        }
        logDebug("Disposing.");
        this.mSetupDone = false;
        if (this.mServiceConn != null) {
            logDebug("Unbinding from service.");
            android.content.Context context = this.mContext;
            if (context != null) {
                context.unbindService(this.mServiceConn);
            }
        }
        this.mDisposed = true;
        this.mContext = null;
        this.mServiceConn = null;
        this.mService = null;
        this.mPurchaseListener = null;
    }

    public void disposeWhenFinished() {
        synchronized (this.mAsyncInProgressLock) {
            if (this.mAsyncInProgress) {
                logDebug("Will dispose after async operation finishes.");
                this.mDisposeAfterAsync = true;
            } else {
                try {
                    dispose();
                } catch (IabHelper.IabAsyncInProgressException e) {
                }
            }
        }
    }

    private void checkNotDisposed() {
        if (this.mDisposed) {
            throw new IllegalStateException("IabHelper was disposed of, so it cannot be used.");
        }
    }

    public boolean subscriptionsSupported() {
        checkNotDisposed();
        return this.mSubscriptionsSupported;
    }

    public void launchPurchaseFlow(android.app.Activity act, String sku, int requestCode, IabHelper.OnIabPurchaseFinishedListener listener) throws IabHelper.IabAsyncInProgressException {
        launchPurchaseFlow(act, sku, requestCode, listener, "");
    }

    public void launchPurchaseFlow(android.app.Activity act, String sku, int requestCode, IabHelper.OnIabPurchaseFinishedListener listener, String extraData) throws IabHelper.IabAsyncInProgressException {
        launchPurchaseFlow(act, sku, "inapp", null, requestCode, listener, extraData);
    }

    public void launchSubscriptionPurchaseFlow(android.app.Activity act, String sku, int requestCode, IabHelper.OnIabPurchaseFinishedListener listener) throws IabHelper.IabAsyncInProgressException {
        launchSubscriptionPurchaseFlow(act, sku, requestCode, listener, "");
    }

    public void launchSubscriptionPurchaseFlow(android.app.Activity act, String sku, int requestCode, IabHelper.OnIabPurchaseFinishedListener listener, String extraData) throws IabHelper.IabAsyncInProgressException {
        launchPurchaseFlow(act, sku, "subs", null, requestCode, listener, extraData);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x009f A[Catch:{ SendIntentException -> 0x0146, RemoteException -> 0x011c }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c7 A[Catch:{ SendIntentException -> 0x0146, RemoteException -> 0x011c }] */
    public void launchPurchaseFlow(android.app.Activity act, String sku, String itemType, java.util.List<String> oldSkus, int requestCode, IabHelper.OnIabPurchaseFinishedListener listener, String extraData) throws IabHelper.IabAsyncInProgressException {
        android.os.Bundle buyIntentBundle;
        int response;
        String str = sku;
        String str2 = itemType;
        int i = requestCode;
        IabHelper.OnIabPurchaseFinishedListener onIabPurchaseFinishedListener = listener;
        checkNotDisposed();
        String str3 = "launchPurchaseFlow";
        checkSetupDone(str3);
        flagStartAsync(str3);
        if (!str2.equals("subs") || this.mSubscriptionsSupported) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("Constructing buy intent for ");
                sb.append(str);
                sb.append(", item type: ");
                sb.append(str2);
                logDebug(sb.toString());
                if (oldSkus != null) {
                    if (!oldSkus.isEmpty()) {
                        if (!this.mSubscriptionUpdateSupported) {
                            IabResult r = new IabResult(-1011, "Subscription updates are not available.");
                            flagEndAsync();
                            if (onIabPurchaseFinishedListener != null) {
                                onIabPurchaseFinishedListener.onIabPurchaseFinished(r, null);
                            }
                            return;
                        }
                        buyIntentBundle = this.mService.getBuyIntentToReplaceSkus(5, this.mContext.getPackageName(), oldSkus, sku, itemType, extraData);
                        response = getResponseCodeFromBundle(buyIntentBundle);
                        if (response == 0) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Unable to buy item, Error response: ");
                            sb2.append(getResponseDesc(response));
                            logError(sb2.toString());
                            flagEndAsync();
                            IabResult result = new IabResult(response, "Unable to buy item");
                            if (onIabPurchaseFinishedListener != null) {
                                onIabPurchaseFinishedListener.onIabPurchaseFinished(result, null);
                            }
                            return;
                        }
                        android.app.PendingIntent pendingIntent = (android.app.PendingIntent) buyIntentBundle.getParcelable("BUY_INTENT");
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Launching buy intent for ");
                        sb3.append(str);
                        sb3.append(". Request code: ");
                        sb3.append(i);
                        logDebug(sb3.toString());
                        this.mRequestCode = i;
                        this.mPurchaseListener = onIabPurchaseFinishedListener;
                        this.mPurchasingItemType = str2;
                        act.startIntentSenderForResult(pendingIntent.getIntentSender(), requestCode, new android.content.Intent(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue(), Integer.valueOf(0).intValue());
                        return;
                    }
                }
                buyIntentBundle = this.mService.getBuyIntent(3, this.mContext.getPackageName(), sku, itemType, extraData);
                response = getResponseCodeFromBundle(buyIntentBundle);
                if (response == 0) {
                }
            } catch (android.content.IntentSender.SendIntentException e) {
                StringBuilder sb4 = new StringBuilder();
                sb4.append("SendIntentException while launching purchase flow for sku ");
                sb4.append(str);
                logError(sb4.toString());
                e.printStackTrace();
                flagEndAsync();
                IabResult result2 = new IabResult(-1004, "Failed to send intent.");
                if (onIabPurchaseFinishedListener != null) {
                    onIabPurchaseFinishedListener.onIabPurchaseFinished(result2, null);
                }
            } catch (android.os.RemoteException e2) {
                StringBuilder sb5 = new StringBuilder();
                sb5.append("RemoteException while launching purchase flow for sku ");
                sb5.append(str);
                logError(sb5.toString());
                e2.printStackTrace();
                flagEndAsync();
                IabResult result3 = new IabResult(-1001, "Remote exception while starting purchase flow");
                if (onIabPurchaseFinishedListener != null) {
                    onIabPurchaseFinishedListener.onIabPurchaseFinished(result3, null);
                }
            }
        } else {
            IabResult r2 = new IabResult(-1009, "Subscriptions are not available.");
            flagEndAsync();
            if (onIabPurchaseFinishedListener != null) {
                onIabPurchaseFinishedListener.onIabPurchaseFinished(r2, null);
            }
        }
    }

    public boolean handleActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        if (requestCode != this.mRequestCode) {
            return false;
        }
        checkNotDisposed();
        checkSetupDone("handleActivityResult");
        flagEndAsync();
        if (data == null) {
            logError("Null data in IAB activity result.");
            IabResult result = new IabResult(-1002, "Null data in IAB result");
            IabHelper.OnIabPurchaseFinishedListener onIabPurchaseFinishedListener = this.mPurchaseListener;
            if (onIabPurchaseFinishedListener != null) {
                onIabPurchaseFinishedListener.onIabPurchaseFinished(result, null);
            }
            return true;
        }
        int responseCode = getResponseCodeFromIntent(data);
        String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
        String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");
        if (resultCode == -1 && responseCode == 0) {
            logDebug("Successful resultcode from purchase activity.");
            StringBuilder sb = new StringBuilder();
            sb.append("Purchase data: ");
            sb.append(purchaseData);
            logDebug(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Data signature: ");
            sb2.append(dataSignature);
            logDebug(sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            String str = "Extras: ";
            sb3.append(str);
            sb3.append(data.getExtras());
            logDebug(sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Expected item type: ");
            sb4.append(this.mPurchasingItemType);
            logDebug(sb4.toString());
            if (purchaseData == null || dataSignature == null) {
                logError("BUG: either purchaseData or dataSignature is null.");
                StringBuilder sb5 = new StringBuilder();
                sb5.append(str);
                sb5.append(data.getExtras().toString());
                logDebug(sb5.toString());
                IabResult result2 = new IabResult(-1008, "IAB returned null purchaseData or dataSignature");
                IabHelper.OnIabPurchaseFinishedListener onIabPurchaseFinishedListener2 = this.mPurchaseListener;
                if (onIabPurchaseFinishedListener2 != null) {
                    onIabPurchaseFinishedListener2.onIabPurchaseFinished(result2, null);
                }
                return true;
            }
            try {
                Purchase purchase = new Purchase(this.mPurchasingItemType, purchaseData, dataSignature);
                String sku = purchase.getSku();
                if (!Security.verifyPurchase(this.mSignatureBase64, purchaseData, dataSignature)) {
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("Purchase signature verification FAILED for sku ");
                    sb6.append(sku);
                    logError(sb6.toString());
                    StringBuilder sb7 = new StringBuilder();
                    sb7.append("Signature verification failed for sku ");
                    sb7.append(sku);
                    IabResult result3 = new IabResult(-1003, sb7.toString());
                    if (this.mPurchaseListener != null) {
                        this.mPurchaseListener.onIabPurchaseFinished(result3, purchase);
                    }
                    return true;
                }
                logDebug("Purchase signature successfully verified.");
                IabHelper.OnIabPurchaseFinishedListener onIabPurchaseFinishedListener3 = this.mPurchaseListener;
                if (onIabPurchaseFinishedListener3 != null) {
                    onIabPurchaseFinishedListener3.onIabPurchaseFinished(new IabResult(0, "Success"), purchase);
                }
            } catch (org.json.JSONException e) {
                String str2 = "Failed to parse purchase data.";
                logError(str2);
                e.printStackTrace();
                IabResult result4 = new IabResult(-1002, str2);
                IabHelper.OnIabPurchaseFinishedListener onIabPurchaseFinishedListener4 = this.mPurchaseListener;
                if (onIabPurchaseFinishedListener4 != null) {
                    onIabPurchaseFinishedListener4.onIabPurchaseFinished(result4, null);
                }
                return true;
            }
        } else if (resultCode == -1) {
            StringBuilder sb8 = new StringBuilder();
            sb8.append("Result code was OK but in-app billing response was not OK: ");
            sb8.append(getResponseDesc(responseCode));
            logDebug(sb8.toString());
            if (this.mPurchaseListener != null) {
                this.mPurchaseListener.onIabPurchaseFinished(new IabResult(responseCode, "Problem purchashing item."), null);
            }
        } else if (resultCode == 0) {
            StringBuilder sb9 = new StringBuilder();
            sb9.append("Purchase canceled - Response: ");
            sb9.append(getResponseDesc(responseCode));
            logDebug(sb9.toString());
            IabResult result5 = new IabResult(-1005, "User canceled.");
            IabHelper.OnIabPurchaseFinishedListener onIabPurchaseFinishedListener5 = this.mPurchaseListener;
            if (onIabPurchaseFinishedListener5 != null) {
                onIabPurchaseFinishedListener5.onIabPurchaseFinished(result5, null);
            }
        } else {
            StringBuilder sb10 = new StringBuilder();
            sb10.append("Purchase failed. Result code: ");
            sb10.append(Integer.toString(resultCode));
            sb10.append(". Response: ");
            sb10.append(getResponseDesc(responseCode));
            logError(sb10.toString());
            IabResult result6 = new IabResult(-1006, "Unknown purchase response.");
            IabHelper.OnIabPurchaseFinishedListener onIabPurchaseFinishedListener6 = this.mPurchaseListener;
            if (onIabPurchaseFinishedListener6 != null) {
                onIabPurchaseFinishedListener6.onIabPurchaseFinished(result6, null);
            }
        }
        return true;
    }

    public Inventory queryInventory() throws IabException {
        return queryInventory(true, null, null);
    }

    public Inventory queryInventory(boolean querySkuDetails, java.util.List<String> moreItemSkus, java.util.List<String> moreSubsSkus) throws IabException {
        String str = "subs";
        String str2 = "inapp";
        checkNotDisposed();
        checkSetupDone("queryInventory");
        try {
            Inventory inv = new Inventory();
            int r = queryPurchases(inv, str2);
            if (r == 0) {
                if (querySkuDetails) {
                    int r2 = querySkuDetails(str2, inv, moreItemSkus);
                    if (r2 != 0) {
                        throw new IabException(r2, "Error refreshing inventory (querying prices of items).");
                    }
                }
                if (this.mSubscriptionsSupported) {
                    int r3 = queryPurchases(inv, str);
                    if (r3 != 0) {
                        throw new IabException(r3, "Error refreshing inventory (querying owned subscriptions).");
                    } else if (querySkuDetails) {
                        int r4 = querySkuDetails(str, inv, moreSubsSkus);
                        if (r4 != 0) {
                            throw new IabException(r4, "Error refreshing inventory (querying prices of subscriptions).");
                        }
                    }
                }
                return inv;
            }
            throw new IabException(r, "Error refreshing inventory (querying owned items).");
        } catch (android.os.RemoteException e) {
            throw new IabException(-1001, "Remote exception while refreshing inventory.", e);
        } catch (org.json.JSONException e2) {
            throw new IabException(-1002, "Error parsing JSON response while refreshing inventory.", e2);
        }
    }

    public void queryInventoryAsync(boolean querySkuDetails, java.util.List<String> moreItemSkus, java.util.List<String> moreSubsSkus, IabHelper.QueryInventoryFinishedListener listener) throws IabHelper.IabAsyncInProgressException {
        final android.os.Handler handler = new android.os.Handler();
        checkNotDisposed();
        checkSetupDone("queryInventory");
        flagStartAsync("refresh inventory");
        final boolean z = querySkuDetails;
        final java.util.List<String> list = moreItemSkus;
        final java.util.List<String> list2 = moreSubsSkus;
        final IabHelper.QueryInventoryFinishedListener queryInventoryFinishedListener = listener;
        Runnable r0 = new Runnable() {
            public void run() {
                IabResult result = new IabResult(0, "Inventory refresh successful.");
                Inventory inv = null;
                try {
                    inv = IabHelper.this.queryInventory(z, list, list2);
                } catch (IabException ex) {
                    result = ex.getResult();
                }
                IabHelper.this.flagEndAsync();
                final IabResult result_f = result;
                final Inventory inv_f = inv;
                if (!IabHelper.this.mDisposed && queryInventoryFinishedListener != null) {
                    handler.post(new Runnable() {
                        public void run() {
                            queryInventoryFinishedListener.onQueryInventoryFinished(result_f, inv_f);
                        }
                    });
                }
            }
        };
        new Thread(r0).start();
    }

    public void queryInventoryAsync(IabHelper.QueryInventoryFinishedListener listener) throws IabHelper.IabAsyncInProgressException {
        queryInventoryAsync(true, null, null, listener);
    }

    void consume(Purchase itemInfo) throws IabException {
        checkNotDisposed();
        checkSetupDone("consume");
        if (itemInfo.mItemType.equals("inapp")) {
            try {
                String token = itemInfo.getToken();
                String sku = itemInfo.getSku();
                if (token == null || token.equals("")) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Can't consume ");
                    sb.append(sku);
                    sb.append(". No token.");
                    logError(sb.toString());
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("PurchaseInfo is missing token for sku: ");
                    sb2.append(sku);
                    sb2.append(" ");
                    sb2.append(itemInfo);
                    throw new IabException(-1007, sb2.toString());
                }
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Consuming sku: ");
                sb3.append(sku);
                sb3.append(", token: ");
                sb3.append(token);
                logDebug(sb3.toString());
                int response = this.mService.consumePurchase(3, this.mContext.getPackageName(), token);
                if (response == 0) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("Successfully consumed sku: ");
                    sb4.append(sku);
                    logDebug(sb4.toString());
                    return;
                }
                StringBuilder sb5 = new StringBuilder();
                sb5.append("Error consuming consuming sku ");
                sb5.append(sku);
                sb5.append(". ");
                sb5.append(getResponseDesc(response));
                logDebug(sb5.toString());
                StringBuilder sb6 = new StringBuilder();
                sb6.append("Error consuming sku ");
                sb6.append(sku);
                throw new IabException(response, sb6.toString());
            } catch (android.os.RemoteException e) {
                StringBuilder sb7 = new StringBuilder();
                sb7.append("Remote exception while consuming. PurchaseInfo: ");
                sb7.append(itemInfo);
                throw new IabException(-1001, sb7.toString(), e);
            }
        } else {
            StringBuilder sb8 = new StringBuilder();
            sb8.append("Items of type '");
            sb8.append(itemInfo.mItemType);
            sb8.append("' can't be consumed.");
            throw new IabException(-1010, sb8.toString());
        }
    }

    public void consumeAsync(Purchase purchase, IabHelper.OnConsumeFinishedListener listener) throws IabHelper.IabAsyncInProgressException {
        checkNotDisposed();
        checkSetupDone("consume");
        java.util.List<Purchase> purchases = new java.util.ArrayList<>();
        purchases.add(purchase);
        consumeAsyncInternal(purchases, listener, null);
    }

    public void consumeAsync(java.util.List<Purchase> purchases, IabHelper.OnConsumeMultiFinishedListener listener) throws IabHelper.IabAsyncInProgressException {
        checkNotDisposed();
        checkSetupDone("consume");
        consumeAsyncInternal(purchases, null, listener);
    }

    public static String getResponseDesc(int code) {
        String str = "/";
        String[] iab_msgs = "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned".split(str);
        String[] iabhelper_msgs = "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt".split(str);
        if (code <= -1000) {
            int index = -1000 - code;
            if (index >= 0 && index < iabhelper_msgs.length) {
                return iabhelper_msgs[index];
            }
            StringBuilder sb = new StringBuilder();
            sb.append(String.valueOf(code));
            sb.append(":Unknown IAB Helper Error");
            return sb.toString();
        } else if (code >= 0 && code < iab_msgs.length) {
            return iab_msgs[code];
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(String.valueOf(code));
            sb2.append(":Unknown");
            return sb2.toString();
        }
    }

    void checkSetupDone(String operation) {
        if (!this.mSetupDone) {
            StringBuilder sb = new StringBuilder();
            sb.append("Illegal state for operation (");
            sb.append(operation);
            sb.append("): IAB helper is not set up.");
            logError(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("IAB helper is not set up. Can't perform operation: ");
            sb2.append(operation);
            throw new IllegalStateException(sb2.toString());
        }
    }

    int getResponseCodeFromBundle(android.os.Bundle b) {
        Object o = b.get("RESPONSE_CODE");
        if (o == null) {
            logDebug("Bundle with null response code, assuming OK (known issue)");
            return 0;
        } else if (o instanceof Integer) {
            return ((Integer) o).intValue();
        } else {
            if (o instanceof Long) {
                return (int) ((Long) o).longValue();
            }
            logError("Unexpected type for bundle response code.");
            logError(o.getClass().getName());
            StringBuilder sb = new StringBuilder();
            sb.append("Unexpected type for bundle response code: ");
            sb.append(o.getClass().getName());
            throw new RuntimeException(sb.toString());
        }
    }

    int getResponseCodeFromIntent(android.content.Intent i) {
        Object o = i.getExtras().get("RESPONSE_CODE");
        if (o == null) {
            logError("Intent with no response code, assuming OK (known issue)");
            return 0;
        } else if (o instanceof Integer) {
            return ((Integer) o).intValue();
        } else {
            if (o instanceof Long) {
                return (int) ((Long) o).longValue();
            }
            logError("Unexpected type for intent response code.");
            logError(o.getClass().getName());
            StringBuilder sb = new StringBuilder();
            sb.append("Unexpected type for intent response code: ");
            sb.append(o.getClass().getName());
            throw new RuntimeException(sb.toString());
        }
    }

    void flagStartAsync(String operation) throws IabHelper.IabAsyncInProgressException {
        synchronized (this.mAsyncInProgressLock) {
            if (!this.mAsyncInProgress) {
                this.mAsyncOperation = operation;
                this.mAsyncInProgress = true;
                StringBuilder sb = new StringBuilder();
                sb.append("Starting async operation: ");
                sb.append(operation);
                logDebug(sb.toString());
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Can't start async operation (");
                sb2.append(operation);
                sb2.append(") because another async operation (");
                sb2.append(this.mAsyncOperation);
                sb2.append(") is in progress.");
                throw new IabHelper.IabAsyncInProgressException(sb2.toString());
            }
        }
    }

    void flagEndAsync() {
        synchronized (this.mAsyncInProgressLock) {
            StringBuilder sb = new StringBuilder();
            sb.append("Ending async operation: ");
            sb.append(this.mAsyncOperation);
            logDebug(sb.toString());
            this.mAsyncOperation = "";
            this.mAsyncInProgress = false;
            if (this.mDisposeAfterAsync) {
                try {
                    dispose();
                } catch (IabHelper.IabAsyncInProgressException e) {
                }
            }
        }
    }

    int queryPurchases(Inventory inv, String itemType) throws JSONException, RemoteException {
        // Query purchases
        logDebug("Querying owned items, item type: " + itemType);
        logDebug("Package name: " + mContext.getPackageName());
        boolean verificationFailed = false;
        String continueToken = null;

        do {
            logDebug("Calling getPurchases with continuation token: " + continueToken);
            Bundle ownedItems = mService.getPurchases(3, mContext.getPackageName(),
                    itemType, continueToken);

            int response = getResponseCodeFromBundle(ownedItems);
            logDebug("Owned items response: " + String.valueOf(response));
            if (response != BILLING_RESPONSE_RESULT_OK) {
                logDebug("getPurchases() failed: " + getResponseDesc(response));
                return response;
            }
            if (!ownedItems.containsKey(RESPONSE_INAPP_ITEM_LIST)
                    || !ownedItems.containsKey(RESPONSE_INAPP_PURCHASE_DATA_LIST)
                    || !ownedItems.containsKey(RESPONSE_INAPP_SIGNATURE_LIST)) {
                logError("Bundle returned from getPurchases() doesn't contain required fields.");
                return IABHELPER_BAD_RESPONSE;
            }

            ArrayList<String> ownedSkus = ownedItems.getStringArrayList(
                    RESPONSE_INAPP_ITEM_LIST);
            ArrayList<String> purchaseDataList = ownedItems.getStringArrayList(
                    RESPONSE_INAPP_PURCHASE_DATA_LIST);
            ArrayList<String> signatureList = ownedItems.getStringArrayList(
                    RESPONSE_INAPP_SIGNATURE_LIST);

            for (int i = 0; i < purchaseDataList.size(); ++i) {
                String purchaseData = purchaseDataList.get(i);
                String signature = signatureList.get(i);
                String sku = ownedSkus.get(i);
                if (Security.verifyPurchase(mSignatureBase64, purchaseData, signature)) {
                    logDebug("Sku is owned: " + sku);
                    Purchase purchase = new Purchase(itemType, purchaseData, signature);

                    if (TextUtils.isEmpty(purchase.getToken())) {
                        logWarn("BUG: empty/null token!");
                        logDebug("Purchase data: " + purchaseData);
                    }

                    // Record ownership and token
                    inv.addPurchase(purchase);
                }
                else {
                    logWarn("Purchase signature verification **FAILED**. Not adding item.");
                    logDebug("   Purchase data: " + purchaseData);
                    logDebug("   Signature: " + signature);
                    verificationFailed = true;
                }
            }

            continueToken = ownedItems.getString(INAPP_CONTINUATION_TOKEN);
            logDebug("Continuation token: " + continueToken);
        } while (!TextUtils.isEmpty(continueToken));

        return verificationFailed ? IABHELPER_VERIFICATION_FAILED : BILLING_RESPONSE_RESULT_OK;
    }

    int querySkuDetails(String itemType, Inventory inv, java.util.List<String> moreSkus) throws android.os.RemoteException, org.json.JSONException {
        String str = itemType;
        Inventory inventory = inv;
        logDebug("Querying SKU details.");
        java.util.ArrayList<String> skuList = new java.util.ArrayList<>();
        skuList.addAll(inventory.getAllOwnedSkus(str));
        if (moreSkus != null) {
            for (String sku : moreSkus) {
                if (!skuList.contains(sku)) {
                    skuList.add(sku);
                }
            }
        }
        if (skuList.size() == 0) {
            logDebug("queryPrices: nothing to do because there are no SKUs.");
            return 0;
        }
        java.util.ArrayList<java.util.ArrayList<String>> packs = new java.util.ArrayList<>();
        int n = skuList.size() / 20;
        int mod = skuList.size() % 20;
        for (int i = 0; i < n; i++) {
            java.util.ArrayList<String> tempList = new java.util.ArrayList<>();
            for (String s : skuList.subList(i * 20, (i * 20) + 20)) {
                tempList.add(s);
            }
            packs.add(tempList);
        }
        if (mod != 0) {
            java.util.ArrayList<String> tempList2 = new java.util.ArrayList<>();
            for (String s2 : skuList.subList(n * 20, (n * 20) + mod)) {
                tempList2.add(s2);
            }
            packs.add(tempList2);
        }
        java.util.Iterator it = packs.iterator();
        while (it.hasNext()) {
            java.util.ArrayList<String> skuPartList = (java.util.ArrayList) it.next();
            android.os.Bundle querySkus = new android.os.Bundle();
            querySkus.putStringArrayList("ITEM_ID_LIST", skuPartList);
            android.os.Bundle skuDetails = this.mService.getSkuDetails(3, this.mContext.getPackageName(), str, querySkus);
            String str2 = "DETAILS_LIST";
            if (!skuDetails.containsKey(str2)) {
                int response = getResponseCodeFromBundle(skuDetails);
                if (response != 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("getSkuDetails() failed: ");
                    sb.append(getResponseDesc(response));
                    logDebug(sb.toString());
                    return response;
                }
                logError("getSkuDetails() returned a bundle with neither an error nor a detail list.");
                return -1002;
            }
            java.util.Iterator it2 = skuDetails.getStringArrayList(str2).iterator();
            while (it2.hasNext()) {
                SkuDetails d = new SkuDetails(str, (String) it2.next());
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Got sku details: ");
                sb2.append(d);
                logDebug(sb2.toString());
                inventory.addSkuDetails(d);
                str = itemType;
            }
            str = itemType;
        }
        return 0;
    }

    void consumeAsyncInternal(java.util.List<Purchase> purchases, IabHelper.OnConsumeFinishedListener singleListener, IabHelper.OnConsumeMultiFinishedListener multiListener) throws IabHelper.IabAsyncInProgressException {
        final android.os.Handler handler = new android.os.Handler();
        flagStartAsync("consume");
        final java.util.List<Purchase> list = purchases;
        final IabHelper.OnConsumeFinishedListener onConsumeFinishedListener = singleListener;
        final IabHelper.OnConsumeMultiFinishedListener onConsumeMultiFinishedListener = multiListener;
        Runnable r0 = new Runnable() {
            public void run() {
                final java.util.List<IabResult> results = new java.util.ArrayList<>();
                for (Purchase purchase : list) {
                    try {
                        IabHelper.this.consume(purchase);
                        StringBuilder sb = new StringBuilder();
                        sb.append("Successful consume of sku ");
                        sb.append(purchase.getSku());
                        results.add(new IabResult(0, sb.toString()));
                    } catch (IabException ex) {
                        results.add(ex.getResult());
                    }
                }
                IabHelper.this.flagEndAsync();
                if (!IabHelper.this.mDisposed && onConsumeFinishedListener != null) {
                    handler.post(new Runnable() {
                        public void run() {
                            onConsumeFinishedListener.onConsumeFinished((Purchase) list.get(0), (IabResult) results.get(0));
                        }
                    });
                }
                if (!IabHelper.this.mDisposed && onConsumeMultiFinishedListener != null) {
                    handler.post(new Runnable() {
                        public void run() {
                            onConsumeMultiFinishedListener.onConsumeMultiFinished(list, results);
                        }
                    });
                }
            }
        };
        new Thread(r0).start();
    }

    void logDebug(String msg) {
        if (this.mDebugLog) {
            android.util.Log.d(this.mDebugTag, msg);
        }
    }

    void logError(String msg) {
        String str = this.mDebugTag;
        StringBuilder sb = new StringBuilder();
        sb.append("In-app billing error: ");
        sb.append(msg);
        android.util.Log.e(str, sb.toString());
    }

    void logWarn(String msg) {
        String str = this.mDebugTag;
        StringBuilder sb = new StringBuilder();
        sb.append("In-app billing warning: ");
        sb.append(msg);
        android.util.Log.w(str, sb.toString());
    }
}
