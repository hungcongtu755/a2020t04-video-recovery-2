package com.bzvideo2004.sdk.inapp.sub.util;

public class Inventory {
    java.util.Map<String, Purchase> mPurchaseMap = new java.util.HashMap();
    java.util.Map<String, SkuDetails> mSkuMap = new java.util.HashMap();

    Inventory() {
    }

    public SkuDetails getSkuDetails(String sku) {
        return (SkuDetails) this.mSkuMap.get(sku);
    }

    public Purchase getPurchase(String sku) {
        return (Purchase) this.mPurchaseMap.get(sku);
    }

    public boolean hasPurchase(String sku) {
        return this.mPurchaseMap.containsKey(sku);
    }

    public boolean hasDetails(String sku) {
        return this.mSkuMap.containsKey(sku);
    }

    public void erasePurchase(String sku) {
        if (this.mPurchaseMap.containsKey(sku)) {
            this.mPurchaseMap.remove(sku);
        }
    }

    java.util.List<String> getAllOwnedSkus() {
        return new java.util.ArrayList(this.mPurchaseMap.keySet());
    }

    java.util.List<String> getAllOwnedSkus(String itemType) {
        java.util.List<String> result = new java.util.ArrayList<>();
        for (Purchase p : this.mPurchaseMap.values()) {
            if (p.getItemType().equals(itemType)) {
                result.add(p.getSku());
            }
        }
        return result;
    }

    public java.util.List<Purchase> getAllPurchases() {
        return new java.util.ArrayList(this.mPurchaseMap.values());
    }

    public java.util.List<SkuDetails> getAllSkuDetails(String itemType) {
        java.util.List<SkuDetails> result = new java.util.ArrayList<>();
        for (SkuDetails details : this.mSkuMap.values()) {
            if (details.getItemType().equals(itemType)) {
                result.add(details);
            }
        }
        return result;
    }

    void addSkuDetails(SkuDetails d) {
        this.mSkuMap.put(d.getSku(), d);
    }

    void addPurchase(Purchase p) {
        this.mPurchaseMap.put(p.getSku(), p);
    }
}
