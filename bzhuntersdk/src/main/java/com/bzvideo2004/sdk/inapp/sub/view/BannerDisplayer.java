package com.bzvideo2004.sdk.inapp.sub.view;

import com.bzvideo2004.sdk.ads.venus.Boom;

import androidx.fragment.app.Fragment;

public class BannerDisplayer {
    public static final String TAG = "IMBA";
    private Banner banner;
    private BannerLoader<Integer, android.widget.ImageView> bannerLoader;
    private android.content.Context context;

    public BannerDisplayer(android.app.Activity activity, String id) {
        this.context = activity;
        this.banner = (Banner) Boom.findViewById(activity, id);
        init();
    }

    public BannerDisplayer(Fragment fragment, String id) {
        this.context = fragment.getContext();
        this.banner = (Banner) Boom.findViewById(fragment, id);
        init();
    }

    public BannerDisplayer(android.content.Context context2, android.view.View view, String id) {
        this.context = context2;
        this.banner = (Banner) Boom.findViewById(context2, view, id);
        init();
    }

    private void init() {
        this.bannerLoader = new BannerLoader<Integer, android.widget.ImageView>() {
            public void bindView(android.content.Context context, Integer id, android.widget.ImageView imageView) {
                imageView.setImageBitmap(android.graphics.BitmapFactory.decodeResource(context.getResources(), id.intValue()));
            }

            public android.widget.ImageView getView(android.content.Context context) {
                return new android.widget.ImageView(context);
            }
        };
        this.banner.setBannerLoader(this.bannerLoader);
        this.banner.setPageTransformer(new ScaleCenterTransformer());
        this.banner.setOnBannerTouchListener(new Banner.BannerTouchListener() {
            public void onTouch() {
                android.util.Log.d("IMBA", "banner touched");
            }
        });
    }

    public void bindData(java.util.List<Integer> imageList) {
        this.banner.setData(imageList);
    }
}
