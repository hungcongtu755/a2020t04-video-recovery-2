package com.bzvideo2004.sdk.inapp.sub.ui;

import com.bzvideo2004.sdk.ads.venus.ConsentActivity;
import com.bzvideo2004.sdk.inapp.sub.util.BillingUtils;
import com.bzvideo2004.sdk.inapp.sub.util.IabHelper;
import com.bzvideo2004.sdk.inapp.sub.util.IabResult;
import com.bzvideo2004.sdk.inapp.sub.util.Inventory;
import com.bzvideo2004.sdk.inapp.sub.util.Purchase;
import com.bzvideo2004.sdk.inapp.sub.util.SkuDetails;

public class IABDialogActivity extends BaseIabActivity implements android.view.View.OnClickListener {
    public static final int RC_REQUEST = 10001;
    private android.widget.TextView btnBuy;
    private android.widget.TextView btnRestore;
    private String currentSubsSku;
    private java.util.List<SkuDetails> skuDetailsList = new java.util.ArrayList();
    private android.widget.TextView tvExplain;

    class PrivacyPolicySpan extends android.text.style.ClickableSpan {
        PrivacyPolicySpan() {
        }

        public void onClick(android.view.View view) {
            ConsentActivity.showPrivacyPolicy(IABDialogActivity.this);
        }

        public void updateDrawState(android.text.TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(android.graphics.Color.parseColor("#ffb7b7b7"));
        }
    }

    public static void start(android.content.Context context, int type, String from) {
        android.content.Intent intent = new android.content.Intent(context, IABDialogActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("from", from);
        if (!(context instanceof android.app.Activity)) {
            intent.addFlags((int)335544320);
        }
        context.startActivity(intent);
    }

    public static void start(android.content.Context context, int type, String from, int requestCode) {
        android.content.Intent intent = new android.content.Intent(context, IABDialogActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("from", from);
        if (context instanceof android.app.Activity) {
            ((android.app.Activity) context).startActivityForResult(intent, requestCode);
            return;
        }
        intent.addFlags((int)335544320);
        context.startActivity(intent);
    }

    protected void onCreate(android.os.Bundle bundle) {
        super.onCreate(bundle);
        setContentView(getLayoutId(this, "zimba_iab_dialog"));
        initUI();
    }

    private void initUI() {
        android.view.View btnClose = findViewById(this, "zimba_dialog_close");
        btnClose.setOnClickListener(this);
        btnClose.setAlpha(0.0f);
        btnClose.setVisibility((int)0);
        btnClose.animate().alpha(1.0f).setDuration(500).setStartDelay(5000);
        this.btnBuy = (android.widget.TextView) findViewById(this, "zimba_quit_buy_btn");
        this.btnBuy.setOnClickListener(this);
        this.btnRestore = (android.widget.TextView) findViewById(this, "zimba_quit_restore_tv");
        this.btnRestore.getPaint().setFlags(8);
        this.btnRestore.getPaint().setAntiAlias(true);
        this.btnRestore.setOnClickListener(this);
        this.tvExplain = (android.widget.TextView) findViewById(this, "zimba_quit_explain");
    }

    public void onClick(android.view.View view) {
        java.util.List list;
        int id = view.getId();
        if (id == getId(this, "zimba_quit_buy_btn")) {
            if (this.selectedSkuDetails == null) {
                android.widget.Toast.makeText(this, getStringById(this, "zimba_premium_error_reason"), (int)1).show();
            } else if (canDoSubscribe()) {
                logEventClickBuy();
                String payload = "";
                if (android.text.TextUtils.isEmpty(this.currentSubsSku) || this.currentSubsSku.equals(this.selectedSkuDetails.getSku())) {
                    list = null;
                } else {
                    java.util.List<String> oldSkus = new java.util.ArrayList<>();
                    oldSkus.add(this.currentSubsSku);
                    list = oldSkus;
                }
                String str = "IMBA";
                android.util.Log.d(str, "Launching purchase flow for subscription.");
                try {
                    this.mHelper.launchPurchaseFlow(this, this.selectedSkuDetails.getSku(), "subs", list, 10001, this.mPurchaseFinishedListener, payload);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    android.util.Log.e(str, "Error launching purchase flow. Another async operation in progress.");
                }
            }
        } else if (id == getId(this, "zimba_quit_restore_tv")) {
            if (this.mHelper != null) {
                try {
                    this.mHelper.queryInventoryAsync(true, null, defSubsSkus, new IabHelper.QueryInventoryFinishedListener() {
                        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                            String str = "IMBA";
                            if (IABDialogActivity.this.mHelper == null) {
                                android.util.Log.d(str, "Helper is null");
                            } else if (result.isFailure()) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Failed to query inventory: ");
                                sb.append(result);
                                android.util.Log.d(str, sb.toString());
                            } else {
                                android.util.Log.d(str, "Query inventory was successful.");
                                IABDialogActivity iABDialogActivity = IABDialogActivity.this;
                                iABDialogActivity.isPremium = BillingUtils.isOwnedSubs(inventory, iABDialogActivity);
                                android.util.Log.d(str, "Initial inventory query finished; enabling main UI.");
                                if (IABDialogActivity.this.isPremium) {
                                    IABDialogActivity iABDialogActivity2 = IABDialogActivity.this;
                                    android.widget.Toast.makeText(iABDialogActivity2, BaseIabActivity.getStringById(iABDialogActivity2, "zimba_premium_sub_success"), (int)0).show();
                                    IABDialogActivity.this.doFinish();
                                }
                            }
                        }
                    });
                } catch (Exception e2) {
                }
            }
        } else {
            if (id == getId(this, "zimba_dialog_close") && !ClickUtils.isJustClick()) {
                doFinish();
            }
        }
    }

    private boolean canDoSubscribe() {
        if (!android.text.TextUtils.isEmpty(this.currentSubsSku)) {
            SkuDetails skuDetails = null;
            java.util.Iterator it = this.skuDetailsList.iterator();
            while (true) {
                if (it.hasNext()) {
                    SkuDetails details = (SkuDetails) it.next();
                    if (android.text.TextUtils.equals(this.currentSubsSku, details.getSku())) {
                        skuDetails = details;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!(skuDetails == null || this.selectedSkuDetails == null)) {
                if (android.text.TextUtils.equals(skuDetails.getSku(), this.selectedSkuDetails.getSku())) {
                    android.widget.Toast.makeText(this, getStringById(this, "zimba_premium_error_current_sub_plan"), (int)1).show();
                    return false;
                } else if (SkuDetails.getSubPeriod(this.selectedSkuDetails.getSubscriptionPeriod()) < SkuDetails.getSubPeriod(skuDetails.getSubscriptionPeriod())) {
                    android.widget.Toast.makeText(this, getStringById(this, "zimba_premium_error_unable_downgrade"), (int)1).show();
                    return false;
                }
            }
        }
        return true;
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: private */
    public void doFinish() {
        logEventClose();
        setResult(-1);
        finish();
    }

    public void OnSetupFinished(IabResult result) {
    }

    public void OnQueryInventoryFinished(IabResult result, Inventory inventory) {
        this.skuDetailsList = inventory.getAllSkuDetails("subs");
        this.currentSubsSku = BillingUtils.getOwnedSubsSku(inventory);
        this.isPremium = BillingUtils.isOwnedSubs(inventory, this);
        StringBuilder sb = new StringBuilder();
        sb.append("User ");
        sb.append(this.isPremium ? "HAS" : "DOES NOT HAVE");
        sb.append(" subscription.");
        android.util.Log.d("IMBA", sb.toString());
        if (result.getResponse() == 0) {
            inventoryQueryTask(inventory);
        }
    }

    public void OnPurchaseFinished(IabResult result, Purchase purchase) {
        if (purchase.getItemType().equals("subs")) {
            android.widget.Toast.makeText(this, getStringById(this, "zimba_premium_sub_success"), (int)0).show();
            doFinish();
        }
    }

    public final void inventoryQueryTask(Inventory inventory) {
        updateBtnRestoreState();
        selectSubsPlan();
        updateBtnBuyState();
        updateExplainText();
    }

    private void selectSubsPlan() {
        if (!this.skuDetailsList.isEmpty()) {
            int size = this.skuDetailsList.size();
            this.selectedSkuDetails = (SkuDetails) this.skuDetailsList.get(0);
            for (int i = 0; i < size; i++) {
                SkuDetails skuDetails = (SkuDetails) this.skuDetailsList.get(i);
                if (skuDetails.getPriceAmountMicros() > this.selectedSkuDetails.getPriceAmountMicros()) {
                    this.selectedSkuDetails = skuDetails;
                }
            }
        }
    }

    private void updateBtnBuyState() {
        if (this.isPremium) {
            this.btnBuy.setText(getStringById(this, "zimba_upgrade_subscription"));
            return;
        }
        String string = getStringById(this, "zimba_price_none");
        if (this.selectedSkuDetails != null) {
            string = String.valueOf(BillingUtils.getFreeTrialPeriod(this.selectedSkuDetails.getFreeTrialPeriod()));
        }
        this.btnBuy.setText(getStringById(this, "zimba_free_trial_for_days", string));
    }

    private void updateBtnRestoreState() {
        this.btnRestore.setVisibility(this.isPremium ? (int)8 : (int)0);
    }

    private void updateExplainText() {
        StringBuilder sb = new StringBuilder();
        if (this.isPremium) {
            sb.append(getStringById(this, "zimba_premium_explain_upgrade"));
        } else {
            String str = "zimba_price_none";
            String string = getStringById(this, str);
            String string2 = getStringById(this, str);
            if (this.selectedSkuDetails != null) {
                string2 = this.selectedSkuDetails.getPrice();
                if ("P1M".equals(this.selectedSkuDetails.getSubscriptionPeriod())) {
                    string = getStringById(this, "zimba_monthly");
                } else {
                    if ("P6M".equals(this.selectedSkuDetails.getSubscriptionPeriod())) {
                        string = getStringById(this, "zimba_six_monthly");
                    } else {
                        if ("P3M".equals(this.selectedSkuDetails.getSubscriptionPeriod())) {
                            string = getStringById(this, "zimba_three_monthly");
                        } else {
                            if ("P1Y".equals(this.selectedSkuDetails.getSubscriptionPeriod())) {
                                string = getStringById(this, "zimba_yearly");
                            }
                        }
                    }
                }
            }
            sb.append(getStringById(this, "zimba_premium_explain_price", string2, string));
        }
        sb.append(getStringById(this, "zimba_premium_explain_cancel"));
        String string3 = getStringById(this, "zimba_privacy_policy");
        sb.append(" ");
        sb.append(string3);
        android.text.SpannableString spannableString = new android.text.SpannableString(sb.toString());
        spannableString.setSpan(new IABDialogActivity.PrivacyPolicySpan(), sb.length() - string3.length(), sb.length(), 33);
        this.tvExplain.setText(spannableString);
    }

    public String IabSource() {
        return "dialog";
    }

    protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        StringBuilder sb = new StringBuilder();
        sb.append("onActivityResult(");
        sb.append(requestCode);
        String str = ",";
        sb.append(str);
        sb.append(resultCode);
        sb.append(str);
        sb.append(data);
        String str2 = "IMBA";
        android.util.Log.d(str2, sb.toString());
        if (this.mHelper == null || this.mHelper.handleActivityResult(requestCode, resultCode, data)) {
            android.util.Log.d(str2, "onActivityResult handled by IABUtil.");
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
