package com.bzvideo2004.sdk.inapp.sub.util;

public class IabBroadcastReceiver extends android.content.BroadcastReceiver {
    public static final String ACTION = "com.android.vending.billing.PURCHASES_UPDATED";
    private final IabBroadcastReceiver.IabBroadcastListener mListener;

    public interface IabBroadcastListener {
        void receivedBroadcast();
    }

    public IabBroadcastReceiver(IabBroadcastReceiver.IabBroadcastListener listener) {
        this.mListener = listener;
    }

    public void onReceive(android.content.Context context, android.content.Intent intent) {
        IabBroadcastReceiver.IabBroadcastListener iabBroadcastListener = this.mListener;
        if (iabBroadcastListener != null) {
            iabBroadcastListener.receivedBroadcast();
        }
    }
}
