package com.bzvideo2004.sdk.inapp.sub.ui;
import com.bzvideo2004.sdk.R;
import com.bzvideo2004.sdk.inapp.sub.util.IabBroadcastReceiver;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.provider.FontsContractCompat;

public abstract class BaseIabActivity extends android.app.Activity implements IabBroadcastReceiver.IabBroadcastListener {
    public static final String TAG = "IMBA";
    protected static final String base64EncodedPublicKey = "";
    protected static java.util.List<String> defSubsSkus = new java.util.ArrayList();
    protected String intentEvent;
    protected String intentFrom;
    protected int intentType;
    protected boolean isPremium = false;
    protected boolean mAutoRenewEnabled = false;
    protected com.bzvideo2004.sdk.inapp.sub.util.IabBroadcastReceiver mBroadcastReceiver;
    protected com.google.firebase.analytics.FirebaseAnalytics mFirebaseAnalytics;
    com.bzvideo2004.sdk.inapp.sub.util.IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new com.bzvideo2004.sdk.inapp.sub.util.IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(com.bzvideo2004.sdk.inapp.sub.util.IabResult result, com.bzvideo2004.sdk.inapp.sub.util.Inventory inventory) {
            String str = "IMBA";
            android.util.Log.d(str, "Query inventory finished.");
            if (BaseIabActivity.this.mHelper == null) {
                android.util.Log.d(str, "Helper is null");
            } else if (result.isFailure()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to query inventory: ");
                sb.append(result);
                android.util.Log.d(str, sb.toString());
            } else {
                android.util.Log.d(str, "Query inventory was successful.");
                BaseIabActivity baseIabActivity = BaseIabActivity.this;
                baseIabActivity.isPremium = com.bzvideo2004.sdk.inapp.sub.util.BillingUtils.isOwnedSubs(inventory, baseIabActivity);
                android.util.Log.d(str, "Initial inventory query finished; enabling main UI.");
                BaseIabActivity.this.OnQueryInventoryFinished(result, inventory);
            }
        }
    };
    protected com.bzvideo2004.sdk.inapp.sub.util.IabHelper mHelper;
    com.bzvideo2004.sdk.inapp.sub.util.IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new com.bzvideo2004.sdk.inapp.sub.util.IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(com.bzvideo2004.sdk.inapp.sub.util.IabResult result, com.bzvideo2004.sdk.inapp.sub.util.Purchase purchase) {
            StringBuilder sb = new StringBuilder();
            sb.append("Purchase finished: ");
            sb.append(result);
            sb.append(", purchase: ");
            sb.append(purchase);
            String str = "IMBA";
            android.util.Log.d(str, sb.toString());
            if (BaseIabActivity.this.mHelper == null) {
                android.util.Log.d(str, "Helper is null");
                return;
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("result res: ");
            sb2.append(result.getResponse());
            android.util.Log.d(str, sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("result mess: ");
            sb3.append(result.getMessage());
            android.util.Log.d(str, sb3.toString());
            if (result.isFailure()) {
                BaseIabActivity.this.logEventPurchaseFailed(result.getResponse(), result.getMessage());
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Error purchasing: ");
                sb4.append(result);
                android.util.Log.d(str, sb4.toString());
            } else if (!new com.bzvideo2004.sdk.inapp.sub.util.CertificationVerifier(BaseIabActivity.this).isCertVerfied()) {
                BaseIabActivity.this.logEventPurchaseFailed(-69, "Authenticity verification failed");
                android.util.Log.d(str, "Error purchasing. Authenticity verification failed.");
            } else {
                android.util.Log.d(str, "Purchase successful.");
                BaseIabActivity.this.logEventPurchase(purchase);
                if (purchase.getItemType().equals("subs")) {
                    android.util.Log.d(str, "subscription purchased.");
                    BaseIabActivity baseIabActivity = BaseIabActivity.this;
                    baseIabActivity.isPremium = true;
                    baseIabActivity.mAutoRenewEnabled = purchase.isAutoRenewing();
                    com.bzvideo2004.sdk.inapp.sub.util.PremiumConfig.get(BaseIabActivity.this).setSubscribed(BaseIabActivity.this.isPremium);
                }
                BaseIabActivity.this.OnPurchaseFinished(result, purchase);
            }
        }
    };
    protected com.bzvideo2004.sdk.inapp.sub.util.SkuDetails selectedSkuDetails;

    public abstract String IabSource();

    public abstract void OnPurchaseFinished(com.bzvideo2004.sdk.inapp.sub.util.IabResult iabResult, com.bzvideo2004.sdk.inapp.sub.util.Purchase purchase);

    public abstract void OnQueryInventoryFinished(com.bzvideo2004.sdk.inapp.sub.util.IabResult iabResult, com.bzvideo2004.sdk.inapp.sub.util.Inventory inventory);

    public abstract void OnSetupFinished(com.bzvideo2004.sdk.inapp.sub.util.IabResult iabResult);

    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.intentType = getIntent().getIntExtra("type", 0);
        this.intentFrom = getIntent().getStringExtra("from");
        this.intentEvent = getIntent().getStringExtra(NotificationCompat.CATEGORY_EVENT);
        this.mFirebaseAnalytics = com.google.firebase.analytics.FirebaseAnalytics.getInstance(this);
        defSubsSkus = java.util.Arrays.asList(getResources().getStringArray(R.array.zimba_sub_packages));
        String str = "IMBA";
        android.util.Log.d(str, "Creating IAB helper.");
        this.mHelper = new com.bzvideo2004.sdk.inapp.sub.util.IabHelper(this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq3BJxEmyLvJ2OyfSgKO/Bc8DsaE73J3CBMl5QM6paW1u14rJED22p3bsJswGUn8DVEhzyQPkzQtKCRW2oMrxOEycu4WxaaMqPUFs4EDc/cznt8WK02u7gq5YYI79gEr6a3ZEDmbUC7wjYfCTadxolmXhfw1LK72t6WqzgAyALFXheL6iovUhBm+xCgoqy9bLSTsESLE2y/aXQtKGuNkDwuTxm8E9deFI6e33Kxo5bSJfLHJVH01hl4/BX05BBGKEB55HBRrtaqJcboBZdNx/ot+tg2j+YBlMCGo+ncgJFb3Xi0OdxonZuAN0heu7MqpC/kKRwjzfT5PJyLVtOyklRQIDAQAB");
        this.mHelper.enableDebugLogging(true);
        android.util.Log.d(str, "Starting setup.");
        this.mHelper.startSetup(new com.bzvideo2004.sdk.inapp.sub.util.IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(com.bzvideo2004.sdk.inapp.sub.util.IabResult result) {
                String str = "IMBA";
                android.util.Log.d(str, "Setup finished.");
                if (!result.isSuccess()) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Problem setting up in-app billing: ");
                    sb.append(result);
                    android.util.Log.d(str, sb.toString());
                } else if (BaseIabActivity.this.mHelper == null) {
                    android.util.Log.d(str, "Helper is null");
                } else {
                    BaseIabActivity baseIabActivity = BaseIabActivity.this;
                    baseIabActivity.mBroadcastReceiver = new com.bzvideo2004.sdk.inapp.sub.util.IabBroadcastReceiver(baseIabActivity);
                    android.content.IntentFilter broadcastFilter = new android.content.IntentFilter("com.android.vending.billing.PURCHASES_UPDATED");
                    BaseIabActivity baseIabActivity2 = BaseIabActivity.this;
                    baseIabActivity2.registerReceiver(baseIabActivity2.mBroadcastReceiver, broadcastFilter);
                    android.util.Log.d(str, "Setup successful. Querying inventory.");
                    try {
                        BaseIabActivity.this.mHelper.queryInventoryAsync(true, null, BaseIabActivity.defSubsSkus, BaseIabActivity.this.mGotInventoryListener);
                    } catch (com.bzvideo2004.sdk.inapp.sub.util.IabHelper.IabAsyncInProgressException e) {
                        android.util.Log.d(str, "Error querying inventory. Another async operation in progress.");
                    }
                    BaseIabActivity.this.OnSetupFinished(result);
                }
            }
        });
        logEventOpen();
    }

    public void receivedBroadcast() {
        String str = "IMBA";
        android.util.Log.d(str, "Received broadcast notification. Querying inventory.");
        try {
            this.mHelper.queryInventoryAsync(true, null, defSubsSkus, this.mGotInventoryListener);
        } catch (com.bzvideo2004.sdk.inapp.sub.util.IabHelper.IabAsyncInProgressException e) {
            android.util.Log.d(str, "Error querying inventory. Another async operation in progress.");
        }
    }

    public void onDestroy() {
        super.onDestroy();
        com.bzvideo2004.sdk.inapp.sub.util.IabBroadcastReceiver iabBroadcastReceiver = this.mBroadcastReceiver;
        if (iabBroadcastReceiver != null) {
            unregisterReceiver(iabBroadcastReceiver);
        }
        android.util.Log.d("IMBA", "Destroying helper.");
        com.bzvideo2004.sdk.inapp.sub.util.IabHelper iabHelper = this.mHelper;
        if (iabHelper != null) {
            iabHelper.disposeWhenFinished();
            this.mHelper = null;
        }
    }

    public static <T extends android.view.View> T findViewById(android.app.Activity activity, String name) {
        if (activity != null) {
            return activity.findViewById(getId(activity, name));
        }
        throw new NullPointerException("Activity null!");
    }

    public static <T extends android.view.View> T findViewById(android.content.Context context, android.view.View view, String name) {
        if (context == null) {
            throw new NullPointerException("Context null!");
        } else if (view != null) {
            return view.findViewById(getId(context, name));
        } else {
            throw new NullPointerException("View null!");
        }
    }

    public static int getLayoutId(android.content.Context context, String name) {
        return getId(context, "layout", name);
    }

    private static int getId(android.content.Context context, String type, String name) {
        return context.getResources().getIdentifier(name, type, context.getPackageName());
    }

    public static int getId(android.content.Context context, String name) {
        return getId(context, "id", name);
    }

    public static int getStringId(android.content.Context context, String name) {
        return getId(context, "string", name);
    }

    public static String getStringById(android.content.Context context, String name) {
        return context.getResources().getString(getStringId(context, name));
    }

    public static String getStringById(android.content.Context context, String name, Object... formatArgs) {
        return context.getResources().getString(getStringId(context, name), formatArgs);
    }

    public static int getDrawableId(android.content.Context context, String name) {
        return getId(context, "drawable", name);
    }

    public static android.graphics.drawable.Drawable getDrawableById(android.content.Context context, int resId) {
        return ContextCompat.getDrawable(context, resId);
    }

    public static android.graphics.drawable.Drawable getDrawableById(android.content.Context context, String name) {
        return ContextCompat.getDrawable(context, getDrawableId(context, name));
    }

    protected void logEventClickBuy() {
        android.os.Bundle bundle = new android.os.Bundle();
        com.bzvideo2004.sdk.inapp.sub.util.SkuDetails skuDetails = this.selectedSkuDetails;
        if (skuDetails != null) {
            bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_ID, skuDetails.getSku());
            bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_NAME, this.selectedSkuDetails.getTitle());
            bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.PRICE, this.selectedSkuDetails.getPrice());
            bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT_TYPE, this.selectedSkuDetails.getItemType());
            bundle.putString("trial_period", this.selectedSkuDetails.getFreeTrialPeriod());
        }
        bundle.putString("source", IabSource());
        bundle.putString("intent_from", this.intentFrom);
        bundle.putString("intent_event", this.intentEvent);
        this.mFirebaseAnalytics.logEvent("checkout", bundle);
    }

    protected void logEventPurchase(com.bzvideo2004.sdk.inapp.sub.util.Purchase purchase) {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_ID, purchase.getSku());
        bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT_TYPE, purchase.getItemType());
        bundle.putString("source", IabSource());
        bundle.putString("auto_renewming", String.valueOf(purchase.isAutoRenewing()));
        this.mFirebaseAnalytics.logEvent(com.google.firebase.analytics.FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, bundle);
    }

    protected void logEventPurchaseFailed(int code, String message) {
        android.os.Bundle bundle = new android.os.Bundle();
        com.bzvideo2004.sdk.inapp.sub.util.SkuDetails skuDetails = this.selectedSkuDetails;
        if (skuDetails != null) {
            bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_ID, skuDetails.getSku());
            bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.ITEM_NAME, this.selectedSkuDetails.getTitle());
            bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.PRICE, this.selectedSkuDetails.getPrice());
            bundle.putString(com.google.firebase.analytics.FirebaseAnalytics.Param.CONTENT_TYPE, this.selectedSkuDetails.getItemType());
            bundle.putString("trial_period", this.selectedSkuDetails.getFreeTrialPeriod());
        }
        bundle.putString("source", IabSource());
        bundle.putString("intent_from", this.intentFrom);
        bundle.putString("intent_event", this.intentEvent);
        bundle.putString(FontsContractCompat.Columns.RESULT_CODE, String.valueOf(code));
        bundle.putString("result_message", message);
        this.mFirebaseAnalytics.logEvent("purchase_failed", bundle);
    }

    protected void logEventClose() {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString("premium", String.valueOf(this.isPremium));
        bundle.putString("source", IabSource());
        bundle.putString("intent_from", this.intentFrom);
        bundle.putString("intent_event", this.intentEvent);
        this.mFirebaseAnalytics.logEvent("iab_close", bundle);
    }

    protected void logEventOpen() {
        android.os.Bundle bundle = new android.os.Bundle();
        bundle.putString("source", IabSource());
        bundle.putString("intent_from", this.intentFrom);
        bundle.putString("intent_event", this.intentEvent);
        this.mFirebaseAnalytics.logEvent("iab_open", bundle);
    }
}
