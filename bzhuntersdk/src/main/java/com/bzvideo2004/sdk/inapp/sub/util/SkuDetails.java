package com.bzvideo2004.sdk.inapp.sub.util;

public class SkuDetails {
    private static java.util.Map<String, Integer> subPeriodList;
    private final String mDescription;
    private final String mFreeTrialPeriod;
    private final String mIntroductoryPrice;
    private final String mIntroductoryPriceAmountMicros;
    private final String mIntroductoryPriceCycles;
    private final String mIntroductoryPricePeriod;
    private final String mItemType;
    private final String mJson;
    private final String mPrice;
    private final long mPriceAmountMicros;
    private long mPriceAmountMicrosOriginal;
    private final String mPriceCurrencyCode;
    private String mPriceOriginal;
    private final String mSku;
    private final String mSubscriptionPeriod;
    private final String mTitle;
    private final String mType;

    public static int getSubPeriod(String period) {
        if (subPeriodList == null) {
            subPeriodList = new java.util.HashMap();
            subPeriodList.put("P1M", Integer.valueOf(1));
            subPeriodList.put("P3M", Integer.valueOf(3));
            subPeriodList.put("P6M", Integer.valueOf(6));
            subPeriodList.put("P1Y", Integer.valueOf(12));
        }
        if (subPeriodList.containsKey(period)) {
            return ((Integer) subPeriodList.get(period)).intValue();
        }
        return 0;
    }

    public SkuDetails(String jsonSkuDetails) throws org.json.JSONException {
        this("inapp", jsonSkuDetails);
    }

    public SkuDetails(String itemType, String jsonSkuDetails) throws org.json.JSONException {
        this.mItemType = itemType;
        this.mJson = jsonSkuDetails;
        org.json.JSONObject o = new org.json.JSONObject(this.mJson);
        this.mSku = o.optString("productId");
        this.mType = o.optString("type");
        this.mPrice = o.optString(com.google.firebase.analytics.FirebaseAnalytics.Param.PRICE);
        this.mPriceAmountMicros = o.optLong("price_amount_micros");
        this.mPriceCurrencyCode = o.optString("price_currency_code");
        this.mTitle = o.optString("title");
        this.mDescription = o.optString("description");
        this.mSubscriptionPeriod = o.optString("subscriptionPeriod");
        this.mFreeTrialPeriod = o.optString("freeTrialPeriod");
        this.mIntroductoryPrice = o.optString("introductoryPrice");
        this.mIntroductoryPriceAmountMicros = o.optString("introductoryPriceAmountMicros");
        this.mIntroductoryPricePeriod = o.optString("introductoryPricePeriod");
        this.mIntroductoryPriceCycles = o.optString("introductoryPriceCycles");
    }

    public String getItemType() {
        return this.mItemType;
    }

    public String getSku() {
        return this.mSku;
    }

    public String getType() {
        return this.mType;
    }

    public String getPrice() {
        return this.mPrice;
    }

    public long getPriceAmountMicros() {
        return this.mPriceAmountMicros;
    }

    public String getPriceCurrencyCode() {
        return this.mPriceCurrencyCode;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public String getPriceOriginal() {
        return this.mPriceOriginal;
    }

    public void setPriceOriginal(String off) {
        this.mPriceOriginal = off;
    }

    public String getSubscriptionPeriod() {
        return this.mSubscriptionPeriod;
    }

    public String getFreeTrialPeriod() {
        return this.mFreeTrialPeriod;
    }

    public String getIntroductoryPrice() {
        return this.mIntroductoryPrice;
    }

    public String getIntroductoryPriceAmountMicros() {
        return this.mIntroductoryPriceAmountMicros;
    }

    public String getIntroductoryPricePeriod() {
        return this.mIntroductoryPricePeriod;
    }

    public String getIntroductoryPriceCycles() {
        return this.mIntroductoryPriceCycles;
    }

    public long getPriceAmountMicrosOriginal() {
        return this.mPriceAmountMicrosOriginal;
    }

    public void setPriceAmountMicrosOriginal(long priceAmountMicrosOriginal) {
        this.mPriceAmountMicrosOriginal = priceAmountMicrosOriginal;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SkuDetails:");
        sb.append(this.mJson);
        return sb.toString();
    }
}
