package com.bzvideo2004.sdk.inapp.sub.util;

public class Purchase {
    String mDeveloperPayload;
    boolean mIsAutoRenewing;
    String mItemType;
    String mOrderId;
    String mOriginalJson;
    String mPackageName;
    int mPurchaseState;
    long mPurchaseTime;
    String mSignature;
    String mSku;
    String mToken;

    public Purchase(String itemType, String jsonPurchaseInfo, String signature) throws org.json.JSONException {
        this.mItemType = itemType;
        this.mOriginalJson = jsonPurchaseInfo;
        org.json.JSONObject o = new org.json.JSONObject(this.mOriginalJson);
        this.mOrderId = o.optString("orderId");
        this.mPackageName = o.optString("packageName");
        this.mSku = o.optString("productId");
        this.mPurchaseTime = o.optLong("purchaseTime");
        this.mPurchaseState = o.optInt("purchaseState");
        this.mDeveloperPayload = o.optString("developerPayload");
        this.mToken = o.optString("token", o.optString("purchaseToken"));
        this.mIsAutoRenewing = o.optBoolean("autoRenewing");
        this.mSignature = signature;
    }

    public String getItemType() {
        return this.mItemType;
    }

    public String getOrderId() {
        return this.mOrderId;
    }

    public String getPackageName() {
        return this.mPackageName;
    }

    public String getSku() {
        return this.mSku;
    }

    public long getPurchaseTime() {
        return this.mPurchaseTime;
    }

    public int getPurchaseState() {
        return this.mPurchaseState;
    }

    public String getDeveloperPayload() {
        return this.mDeveloperPayload;
    }

    public String getToken() {
        return this.mToken;
    }

    public String getOriginalJson() {
        return this.mOriginalJson;
    }

    public String getSignature() {
        return this.mSignature;
    }

    public boolean isAutoRenewing() {
        return this.mIsAutoRenewing;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PurchaseInfo(type:");
        sb.append(this.mItemType);
        sb.append("):");
        sb.append(this.mOriginalJson);
        return sb.toString();
    }
}
