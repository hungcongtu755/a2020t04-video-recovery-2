package com.bzvideo2004.sdk.inapp.sub.view;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

public class ScaleCenterTransformer implements ViewPager.PageTransformer {
    private float scale = 0.85f;

    public void transformPage(@NonNull android.view.View view, float f) {
        int width = view.getWidth();
        view.setPivotY((float) view.getHeight());
        view.setPivotX((float) (width / 2));
        if (f < -1.0f) {
            view.setScaleX(this.scale);
            view.setScaleY(this.scale);
            view.setPivotX((float) width);
        } else if (f > 1.0f) {
            view.setPivotX(0.0f);
            view.setScaleX(this.scale);
            view.setScaleY(this.scale);
        } else if (f < 0.0f) {
            float f2 = f + 1.0f;
            float f3 = this.scale;
            float f22 = (f2 * (1.0f - f3)) + f3;
            view.setScaleX(f22);
            view.setScaleY(f22);
            view.setPivotX(((float) width) * (((-f) * 0.5f) + 0.5f));
        } else {
            float f32 = 1.0f - f;
            float f4 = this.scale;
            float f42 = ((1.0f - f4) * f32) + f4;
            view.setScaleX(f42);
            view.setScaleY(f42);
            view.setPivotX(((float) width) * f32 * 0.5f);
        }
    }
}
