package com.bzvideo2004.sdk.inapp.sub.ui;

public class ClickUtils {
    private static long lastClick;

    public static boolean isJustClick() {
        if (System.currentTimeMillis() - lastClick < 1000 && System.currentTimeMillis() - lastClick > 0) {
            return true;
        }
        lastClick = System.currentTimeMillis();
        return false;
    }
}
