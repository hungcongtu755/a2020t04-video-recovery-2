package com.bzvideo2004.sdk.inapp.sub.view;

public class BannerScroller extends android.widget.Scroller {
    private int duration = 800;

    public BannerScroller(android.content.Context context) {
        super(context);
    }

    public void startScroll(int startX, int startY, int dx, int dy, int duration2) {
        super.startScroll(startX, startY, dx, dy, this.duration);
    }

    public void startScroll(int startX, int startY, int dx, int dy) {
        super.startScroll(startX, startY, dx, dy, this.duration);
    }

    public void extendDuration(int duration2) {
        this.duration = duration2;
    }
}
