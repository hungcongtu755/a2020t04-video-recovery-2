package com.bzvideo2004.sdk.inapp.sub.util;

public class BillingUtils {
    public static boolean isOwnedSubs(Inventory inventory, android.content.Context context) {
        boolean isOwnedSubs = isOwnedSubs(inventory);
        boolean isDebuggable = (context.getApplicationInfo().flags & 2) != 0;
        StringBuilder sb = new StringBuilder();
        sb.append("According to Inventory, Subscribe state:");
        sb.append(isOwnedSubs);
        String str = "IMBA";
        android.util.Log.i(str, sb.toString());
        if (isOwnedSubs && PremiumConfig.get(context).isFakeAccount()) {
            android.util.Log.i(str, "Fail verified subscription, due to FAKE_ACCOUNT");
            isOwnedSubs = false;
        }
        CertificationVerifier certificationVerifier = new CertificationVerifier(context);
        if (!isDebuggable && isOwnedSubs && !certificationVerifier.isCertVerfied()) {
            android.util.Log.i(str, "Fail verified subscription, due to ERROR_SIGNATURE");
            isOwnedSubs = false;
        }
        PremiumConfig.get(context).setSubscribed(isOwnedSubs);
        return isOwnedSubs;
    }

    private static boolean isOwnedSubs(Inventory inventory) {
        java.util.List ownedSkus = inventory.getAllOwnedSkus("subs");
        return (ownedSkus == null || ownedSkus.size() == 0) ? false : true;
    }

    public static java.util.ArrayList<String> getAutoRenewOwnedSkus(Inventory inventory) {
        java.util.ArrayList<String> arrayList = new java.util.ArrayList<>();
        java.util.List<String> ownedSkus = inventory.getAllOwnedSkus("subs");
        if (ownedSkus == null || ownedSkus.size() == 0) {
            return arrayList;
        }
        for (String sku : ownedSkus) {
            if (inventory.getPurchase(sku).isAutoRenewing()) {
                arrayList.add(sku);
            }
        }
        return arrayList;
    }

    public static String getOwnedSubsSku(Inventory inventory) {
        java.util.List<String> ownedSkus = inventory.getAllOwnedSkus("subs");
        if (ownedSkus == null || ownedSkus.size() == 0) {
            return "";
        }
        for (String str : ownedSkus) {
            if (inventory.getPurchase(str).isAutoRenewing()) {
                return str;
            }
        }
        return (String) ownedSkus.get(0);
    }

    public static int getFreeTrialPeriod(String str) {
        if (android.text.TextUtils.isEmpty(str) || !str.startsWith("P")) {
            return 0;
        }
        java.util.regex.Matcher matcher = java.util.regex.Pattern.compile("\\d(D|W|M)").matcher(str);
        java.util.ArrayList<String> arrayList = new java.util.ArrayList<>();
        while (matcher.find()) {
            arrayList.add(matcher.group());
        }
        int i = 0;
        try {
            java.util.Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                String str2 = (String) it.next();
                int intValue = Integer.valueOf(str2.substring(0, str2.length() - 1)).intValue();
                if (str2.endsWith("D")) {
                    i += intValue * 1;
                } else if (str2.endsWith("W")) {
                    i += intValue * 7;
                } else if (str2.endsWith("M")) {
                    i += intValue * 30;
                }
            }
            return i;
        } catch (Exception e) {
            return 0;
        }
    }
}
