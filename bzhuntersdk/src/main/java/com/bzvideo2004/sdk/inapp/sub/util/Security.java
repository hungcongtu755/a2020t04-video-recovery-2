package com.bzvideo2004.sdk.inapp.sub.util;

public class Security {
    private static final String KEY_FACTORY_ALGORITHM = "RSA";
    private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";
    private static final String TAG = "IABUtil/Security";

    public static boolean verifyPurchase(String base64PublicKey, String signedData, String signature) {
        if (!android.text.TextUtils.isEmpty(signedData) && !android.text.TextUtils.isEmpty(base64PublicKey) && !android.text.TextUtils.isEmpty(signature)) {
            return verify(generatePublicKey(base64PublicKey), signedData, signature);
        }
        android.util.Log.e(TAG, "Purchase verification failed: missing data.");
        return false;
    }

    public static java.security.PublicKey generatePublicKey(String encodedPublicKey) {
        try {
            return java.security.KeyFactory.getInstance(KEY_FACTORY_ALGORITHM).generatePublic(new java.security.spec.X509EncodedKeySpec(android.util.Base64.decode(encodedPublicKey, 0)));
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (java.security.spec.InvalidKeySpecException e2) {
            android.util.Log.e(TAG, "Invalid key specification.");
            throw new IllegalArgumentException(e2);
        }
    }

    public static boolean verify(java.security.PublicKey publicKey, String signedData, String signature) {
        String str = TAG;
        try {
            byte[] signatureBytes = android.util.Base64.decode(signature, 0);
            try {
                java.security.Signature sig = java.security.Signature.getInstance(SIGNATURE_ALGORITHM);
                sig.initVerify(publicKey);
                sig.update(signedData.getBytes());
                if (sig.verify(signatureBytes)) {
                    return true;
                }
                android.util.Log.e(str, "Signature verification failed.");
                return false;
            } catch (java.security.NoSuchAlgorithmException e) {
                android.util.Log.e(str, "NoSuchAlgorithmException.");
                return false;
            } catch (java.security.InvalidKeyException e2) {
                android.util.Log.e(str, "Invalid key specification.");
                return false;
            } catch (java.security.SignatureException e3) {
                android.util.Log.e(str, "Signature exception.");
                return false;
            }
        } catch (IllegalArgumentException e4) {
            android.util.Log.e(str, "Base64 decoding failed.");
            return false;
        }
    }
}
