package com.bzvideo2004.sdk.inapp.sub.view;

import android.content.Context;

import com.bzvideo2004.sdk.ads.venus.Boom;

import androidx.vectordrawable.graphics.drawable.PathInterpolatorCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class Banner extends android.widget.FrameLayout implements ViewPager.OnPageChangeListener {
    public static float defHToWRatio = 0.58f;
    public static int defPagerMargin = 10;
    public static int defSpace = 40;
    public static float defWToScreenRatio = 0.77f;
    public static int defaultScrollDuration = 800;
    private int bannerBottomMargin;
    /* access modifiers changed from: private */
    public Banner.BannerClickListener bannerClickListener;
    /* access modifiers changed from: private */
    public int bannerCurrentIndex;
    private int bannerDefaultHeight;
    private int bannerDefaultIndicator;
    private int bannerDefaultWidth;
    /* access modifiers changed from: private */
    public BannerLoader bannerLoader;
    private int bannerMargin;
    private int bannerSelectHeight;
    private int bannerSelectIndicator;
    private int bannerSelectWidth;
    private Banner.BannerTouchListener bannerTouchListener;
    private android.content.Context context;
    private int currentIndicatorIndex;
    /* access modifiers changed from: private */
    public int delay;
    /* access modifiers changed from: private */
    public int endlessViewCount;
    private float hToWRatio;
    /* access modifiers changed from: private */
    public android.os.Handler handler;
    private android.widget.LinearLayout indicatorLayout;
    private int indicatorSpace;
    private boolean isAutoPlay;
    /* access modifiers changed from: private */
    public boolean isMultiplePage;
    private boolean isShowIndicator;
    /* access modifiers changed from: private */
    public java.util.List<Object> mImageList;
    /* access modifiers changed from: private */
    public java.util.ArrayList<android.view.View> mImageViewList;
    private int pageSpacing;
    private Banner.BannerPagerChangedListener pagerChangedListener;
    /* access modifiers changed from: private */
    public final Runnable runnable;
    private int scrollDuration;
    private Banner.ScrollPagerAdapter scrollPagerAdapter;
    private int viewCount;
    /* access modifiers changed from: private */
    public ViewPager viewPager;
    private float wToScreenRatio;

    public interface BannerClickListener {
        void onClick(int i);
    }

    public interface BannerPagerChangedListener {
        void onPageScrollStateChanged(int i);

        void onPageScrolled(int i, float f, int i2);

        void onPageSelected(int i);
    }

    public interface BannerTouchListener {
        void onTouch();
    }

    private static class Logger {
        public static final boolean LOG = true;

        private Logger() {
        }

        public static void c(String tag, String msg) {
            android.util.Log.d(tag, msg);
        }

        public static void d(String tag, String msg) {
            android.util.Log.e(tag, msg);
        }
    }

    class ScrollPagerAdapter extends PagerAdapter {
        public boolean isViewFromObject(android.view.View view, Object obj) {
            return view == obj;
        }

        private ScrollPagerAdapter() {
        }

        public int getCount() {
            return Banner.this.mImageList.size();
        }

        public Object instantiateItem(android.view.ViewGroup viewGroup, final int i) {
            if (Banner.this.bannerLoader != null) {
                while (Banner.this.mImageViewList.size() <= i) {
                    Banner.this.mImageViewList.add(null);
                }
                android.view.View view = (android.view.View) Banner.this.mImageViewList.get(i);
                if (view == null) {
                    view = Banner.this.bannerLoader.getView(Banner.this.viewPager.getContext());
                    Banner.this.bannerLoader.bindView(Banner.this.viewPager.getContext(), Banner.this.mImageList.get(i), view);
                    Banner.this.mImageViewList.set(i, view);
                    view.setOnClickListener(new OnClickListener() {
                        public void onClick(android.view.View view) {
                            if (Banner.this.bannerClickListener != null) {
                                Banner.this.bannerClickListener.onClick(Banner.this.getNextIndex(i));
                            }
                        }
                    });
                }
                viewGroup.addView(view);
                return view;
            }
            throw new RuntimeException("[Banner] --> The mBannerLoader is not null");
        }

        public void destroyItem(android.view.ViewGroup viewGroup, int i, Object obj) {
            viewGroup.removeView((android.view.View) obj);
        }
    }

    public Banner(android.content.Context context2) {
        this(context2, null);
    }

    public Banner(android.content.Context context2, android.util.AttributeSet attributeSet) {
        this(context2, attributeSet, 0);
    }

    public Banner(android.content.Context context2, android.util.AttributeSet attributeSet, int defStyleAttr) {
        super(context2, attributeSet, defStyleAttr);
        this.handler = new android.os.Handler();
        this.mImageViewList = new java.util.ArrayList<>();
        this.mImageList = new java.util.ArrayList();
        this.bannerCurrentIndex = 1;
        this.currentIndicatorIndex = 0;
        this.hToWRatio = defHToWRatio;
        this.wToScreenRatio = defWToScreenRatio;
        this.runnable = new Runnable() {
            public void run() {
                Banner banner = Banner.this;
                banner.bannerCurrentIndex = banner.bannerCurrentIndex + 1;
                if (Banner.this.isMultiplePage) {
                    if (Banner.this.bannerCurrentIndex == Banner.this.endlessViewCount - 1) {
                        Banner.this.viewPager.setCurrentItem(2, false);
                        Banner.this.handler.post(Banner.this.runnable);
                        return;
                    }
                    Banner.this.viewPager.setCurrentItem(Banner.this.bannerCurrentIndex);
                    Banner.this.handler.postDelayed(Banner.this.runnable, (long) Banner.this.delay);
                } else if (Banner.this.bannerCurrentIndex == Banner.this.endlessViewCount) {
                    Banner.this.viewPager.setCurrentItem(1, false);
                    Banner.this.handler.post(Banner.this.runnable);
                } else {
                    Banner.this.viewPager.setCurrentItem(Banner.this.bannerCurrentIndex);
                    Banner.this.handler.postDelayed(Banner.this.runnable, (long) Banner.this.delay);
                }
            }
        };
        this.context = context2;
        inflate(context2, Boom.getLayoutId(context2, "zimba_sub_banner_layout"), this);
        readAttributes(context2, attributeSet, defStyleAttr);
        initUI();
        initScroller();
    }

    private void readAttributes(android.content.Context context2, android.util.AttributeSet attributeSet, int defStyleAttr) {
        this.isAutoPlay = true;
        this.isMultiplePage = true;
        this.isShowIndicator = true;
        this.delay = PathInterpolatorCompat.MAX_NUM_POINTS;
        this.scrollDuration = defaultScrollDuration;
        this.indicatorSpace = defSpace;
        this.pageSpacing = dpToPx(context2, 11.0f);
        this.bannerSelectWidth = dpToPx(context2, 12.0f);
        this.bannerDefaultWidth = dpToPx(context2, 12.0f);
        this.bannerSelectHeight = dpToPx(context2, 2.0f);
        this.bannerDefaultHeight = dpToPx(context2, 2.0f);
        this.bannerMargin = dpToPx(context2, 6.0f);
        this.bannerBottomMargin = dpToPx(context2, 10.0f);
        this.bannerSelectIndicator = Boom.getDrawableId(context2, "zimba_shape_banner_select_indicator");
        this.bannerDefaultIndicator = Boom.getDrawableId(context2, "zimba_shape_banner_default_indicator");
    }

    private void initUI() {
        this.viewPager = (ViewPager) Boom.findViewById(this.context, this, "zimba_banner_view_pager");
        this.indicatorLayout = (android.widget.LinearLayout) Boom.findViewById(this.context, this, "zimba_indicator_layout");
        initLayout();
        initMultiplePager();
    }

    public void setBannerHtoWRatio(float ratio) {
        this.hToWRatio = ratio;
    }

    public void setBannerWidthToScreenRatio(float ratio) {
        this.wToScreenRatio = ratio;
    }

    private void initMultiplePager() {
        if (this.isMultiplePage) {
            setClipChildren(false);
            setLayerType(1, null);
            LayoutParams layoutParams = (LayoutParams) this.viewPager.getLayoutParams();
            int b2 = (int) (((float) getScreenWidth(this.context)) * this.wToScreenRatio);
            int i2 = (int) (((float) b2) * this.hToWRatio);
            layoutParams.width = b2;
            layoutParams.height = i2;
            this.viewPager.setLayoutParams(layoutParams);
            setPagerMargin(this.pageSpacing);
            setOffscreenPageLimit(2);
        }
    }

    private void initLayout() {
        if (this.isShowIndicator) {
            this.indicatorLayout.setVisibility((int)0);
        }
        int a2 = dpToPx(this.context, 16.0f);
        LayoutParams layoutParams = (LayoutParams) this.indicatorLayout.getLayoutParams();
        if (this.isMultiplePage) {
            int i = this.indicatorSpace;
            int i2 = this.pageSpacing;
            layoutParams.leftMargin = i + i2 + a2;
            layoutParams.rightMargin = i + i2 + a2;
            layoutParams.bottomMargin = this.bannerBottomMargin;
        } else {
            layoutParams.leftMargin = a2;
            layoutParams.rightMargin = a2;
            layoutParams.bottomMargin = this.bannerBottomMargin;
        }
        this.indicatorLayout.setLayoutParams(layoutParams);
    }

    private void initScroller() {
        try {
            java.lang.reflect.Field declaredField = ViewPager.class.getDeclaredField("mScroller");
            declaredField.setAccessible(true);
            BannerScroller scroller = new BannerScroller(this.viewPager.getContext());
            scroller.extendDuration(this.scrollDuration);
            declaredField.set(this.viewPager, scroller);
        } catch (Exception e2) {
            Banner.Logger.d("Banner", e2.getMessage());
        }
    }

    public boolean dispatchTouchEvent(android.view.MotionEvent motionEvent) {
        if (this.isAutoPlay) {
            int action = motionEvent.getAction();
            String str = "Banner";
            if (action == 1 || action == 3 || action == 4) {
                startAutoPlay();
                Banner.BannerTouchListener bannerTouchListener2 = this.bannerTouchListener;
                if (bannerTouchListener2 != null) {
                    bannerTouchListener2.onTouch();
                }
                Banner.Logger.c(str, "dispatchTouchEvent: Start auto play");
            } else if (action == 0) {
                stopAutoPlay();
                Banner.Logger.c(str, "dispatchTouchEvent: Stop auto play");
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public void setPagerMargin(int i2) {
        ViewPager viewPager2 = this.viewPager;
        if (viewPager2 != null && this.isMultiplePage) {
            viewPager2.setPageMargin(i2);
        }
    }

    public Banner setBannerLoader(BannerLoader bannerLoader2) {
        this.bannerLoader = bannerLoader2;
        return this;
    }

    public Banner setOffscreenPageLimit(int limit) {
        ViewPager viewPager2 = this.viewPager;
        if (viewPager2 != null) {
            viewPager2.setOffscreenPageLimit(limit);
        }
        return this;
    }

    public Banner setPageTransformer(ViewPager.PageTransformer pageTransformer) {
        try {
            if (this.viewPager != null) {
                this.viewPager.setPageTransformer(true, pageTransformer);
            }
        } catch (Exception e) {
            Banner.Logger.d("Banner", "Please set the PageTransformer class");
        }
        return this;
    }

    public void setData(java.util.List<?> list) {
        String str = "Banner";
        if (list == null || list.isEmpty()) {
            Banner.Logger.d(str, "The image data set is empty.");
            return;
        }
        this.mImageList.clear();
        this.mImageViewList.clear();
        this.viewCount = list.size();
        android.view.View findViewWithTag = findViewWithTag("only_one_pager");
        if (findViewWithTag != null) {
            removeView(findViewWithTag);
        }
        if (list.size() == 1) {
            bindData(list.get(0));
            return;
        }
        initIndicator(this.viewCount);
        if (this.isMultiplePage) {
            int i = this.viewCount;
            this.endlessViewCount = i + 4;
            this.mImageList.add(list.get(i - 2));
            this.mImageList.add(list.get(this.viewCount - 1));
            this.mImageList.addAll(list);
            this.mImageList.add(list.get(0));
            this.mImageList.add(list.get(1));
        } else {
            int i2 = this.viewCount;
            this.endlessViewCount = i2 + 2;
            this.mImageList.add(list.get(i2 - 1));
            this.mImageList.addAll(list);
            this.mImageList.add(list.get(0));
        }
        initViewPager();
        StringBuilder sb = new StringBuilder();
        sb.append("loadImagePaths: banner：");
        sb.append(this.mImageList);
        Banner.Logger.c(str, sb.toString());
    }

    private void initViewPager() {
        if (this.isMultiplePage) {
            this.bannerCurrentIndex = 2;
        } else {
            this.bannerCurrentIndex = 1;
        }
        if (this.scrollPagerAdapter == null) {
            this.scrollPagerAdapter = new Banner.ScrollPagerAdapter();
            this.viewPager.addOnPageChangeListener(this);
        }
        this.viewPager.setAdapter(this.scrollPagerAdapter);
        this.viewPager.setFocusable(true);
        this.viewPager.setCurrentItem(this.bannerCurrentIndex, false);
        if (this.isAutoPlay) {
            startAutoPlay();
        }
    }

    public int getBannerCurrentIndex() {
        return getNextIndex(this.bannerCurrentIndex);
    }

    /* access modifiers changed from: private */
    public int getNextIndex(int i2) {
        if (this.isMultiplePage) {
            if (i2 == 1) {
                return this.viewCount - 1;
            }
            if (i2 == 2 || this.endlessViewCount - 2 == i2) {
                return 0;
            }
            return i2 - 2;
        } else if (i2 == 0) {
            return this.viewCount - 1;
        } else {
            if (i2 == this.endlessViewCount - 1) {
                return 0;
            }
            return i2 - 1;
        }
    }

    public void startAutoPlay() {
        if (this.isAutoPlay && this.viewCount > 1) {
            this.handler.removeCallbacks(this.runnable);
            this.handler.postDelayed(this.runnable, (long) this.delay);
        }
    }

    public void stopAutoPlay() {
        if (this.isAutoPlay && this.viewCount > 1) {
            this.handler.removeCallbacks(this.runnable);
        }
    }

    public void onPageScrollStateChanged(int i2) {
        Banner.BannerPagerChangedListener bannerPagerChangedListener = this.pagerChangedListener;
        if (bannerPagerChangedListener != null) {
            bannerPagerChangedListener.onPageScrollStateChanged(i2);
        }
        if (i2 == 1) {
            if (this.isMultiplePage) {
                int i = this.bannerCurrentIndex;
                if (i == 1) {
                    this.viewPager.setCurrentItem(this.endlessViewCount - 3, false);
                } else if (i == this.endlessViewCount - 2) {
                    this.viewPager.setCurrentItem(2, false);
                }
            } else {
                int i3 = this.bannerCurrentIndex;
                if (i3 == 0) {
                    this.viewPager.setCurrentItem(this.endlessViewCount - 2, false);
                } else if (i3 == this.endlessViewCount - 1) {
                    this.viewPager.setCurrentItem(1, false);
                }
            }
        }
    }

    public void onPageScrolled(int i2, float f2, int i3) {
        Banner.BannerPagerChangedListener bannerPagerChangedListener = this.pagerChangedListener;
        if (bannerPagerChangedListener != null) {
            bannerPagerChangedListener.onPageScrolled(getNextIndex(i2), f2, i3);
        }
    }

    public void onPageSelected(int i2) {
        this.bannerCurrentIndex = i2;
        setSelectedIndicator(getNextIndex(i2));
        Banner.BannerPagerChangedListener bannerPagerChangedListener = this.pagerChangedListener;
        if (bannerPagerChangedListener != null) {
            bannerPagerChangedListener.onPageSelected(getNextIndex(i2));
        }
    }

    private void initIndicator(int viewCount2) {
        if (this.isShowIndicator) {
            this.indicatorLayout.removeAllViews();
            while (viewCount2 > 0) {
                android.view.View view = new android.view.View(this.context);
                android.widget.LinearLayout.LayoutParams layoutParams = new android.widget.LinearLayout.LayoutParams(this.bannerDefaultWidth, this.bannerDefaultHeight);
                layoutParams.setMargins(this.bannerMargin, 0, 0, 0);
                view.setLayoutParams(layoutParams);
                view.setBackgroundResource(this.bannerDefaultIndicator);
                this.indicatorLayout.addView(view);
                viewCount2--;
            }
        }
    }

    private void setSelectedIndicator(int index) {
        if (this.isShowIndicator && index <= this.indicatorLayout.getChildCount() - 1) {
            android.view.View childAt = this.indicatorLayout.getChildAt(this.currentIndicatorIndex);
            android.view.ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            layoutParams.width = this.bannerDefaultWidth;
            layoutParams.height = this.bannerDefaultHeight;
            childAt.setLayoutParams(layoutParams);
            childAt.setBackgroundResource(this.bannerDefaultIndicator);
            android.view.View childAt2 = this.indicatorLayout.getChildAt(index);
            android.view.ViewGroup.LayoutParams layoutParams2 = childAt2.getLayoutParams();
            layoutParams2.width = this.bannerSelectWidth;
            layoutParams2.height = this.bannerSelectHeight;
            childAt2.setLayoutParams(layoutParams2);
            childAt2.setBackgroundResource(this.bannerSelectIndicator);
            this.currentIndicatorIndex = index;
        }
    }

    private void bindData(Object obj) {
        android.view.View banner = this.bannerLoader.getView(this.viewPager.getContext());
        this.bannerLoader.bindView(this.viewPager.getContext(), obj, banner);
        banner.setOnClickListener(new OnClickListener() {
            public void onClick(android.view.View view) {
                if (Banner.this.bannerClickListener != null) {
                    Banner.this.bannerClickListener.onClick(0);
                }
            }
        });
        banner.setTag("only_one_pager");
        addView(banner, new LayoutParams(-1, -1));
    }

    public static int dpToPx(android.content.Context context2, float dp) {
        return (int) ((context2.getResources().getDisplayMetrics().density * dp) + 0.5f);
    }

    public static int getScreenWidth(android.content.Context context2) {
        android.util.DisplayMetrics displayMetrics = new android.util.DisplayMetrics();
        ((android.view.WindowManager) context2.getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public void setOnBannerClickListener(Banner.BannerClickListener bannerClickListenerVar) {
        this.bannerClickListener = bannerClickListenerVar;
    }

    public void setBannerPagerChangedListener(Banner.BannerPagerChangedListener pagerChangedListener2) {
        this.pagerChangedListener = pagerChangedListener2;
    }

    public void setOnBannerTouchListener(Banner.BannerTouchListener bannerTouchListenerVar) {
        this.bannerTouchListener = bannerTouchListenerVar;
    }
}
