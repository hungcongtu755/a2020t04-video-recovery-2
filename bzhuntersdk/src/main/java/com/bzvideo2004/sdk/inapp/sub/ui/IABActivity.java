package com.bzvideo2004.sdk.inapp.sub.ui;

import android.widget.ImageView;

import com.bzvideo2004.sdk.ads.venus.ConsentActivity;
import com.bzvideo2004.sdk.inapp.sub.util.BillingUtils;
import com.bzvideo2004.sdk.inapp.sub.util.IabHelper;
import com.bzvideo2004.sdk.inapp.sub.util.IabResult;
import com.bzvideo2004.sdk.inapp.sub.util.Inventory;
import com.bzvideo2004.sdk.inapp.sub.util.PremiumConfig;
import com.bzvideo2004.sdk.inapp.sub.util.Purchase;
import com.bzvideo2004.sdk.inapp.sub.util.SkuDetails;
import com.bzvideo2004.sdk.inapp.sub.view.Banner;
import com.bzvideo2004.sdk.inapp.sub.view.BannerLoader;
import com.bzvideo2004.sdk.inapp.sub.view.ScaleCenterTransformer;

import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class IABActivity extends BaseIabActivity implements android.view.View.OnClickListener {
    public static final int RC_REQUEST = 10001;
    private static int featureColumnCount = 1;
    private Banner banner;
    private BannerLoader<Integer, ImageView> bannerLoader = new BannerLoader<Integer, android.widget.ImageView>() {
        public void bindView(android.content.Context context, Integer id, android.widget.ImageView imageView) {
            imageView.setImageBitmap(android.graphics.BitmapFactory.decodeResource(IABActivity.this.getResources(), id.intValue()));
        }

        public android.widget.ImageView getView(android.content.Context context) {
            return new android.widget.ImageView(context);
        }
    };
    private android.widget.TextView btnBuy;
    private android.widget.TextView btnRestore;
    private String currentSubsSku;
    /* access modifiers changed from: private */
    public java.util.List<Integer> featureDrawable;
    /* access modifiers changed from: private */
    public java.util.List<Integer> featureName;
    private java.util.List<Integer> imageList;
    private java.util.ArrayList<String> ownedSubsList;
    public java.util.List<IABActivity.SubPriceItem> priceItemList = new java.util.ArrayList();
    private android.widget.LinearLayout priceLayout;
    private java.util.List<SkuDetails> skuDetailsList = new java.util.ArrayList();
    private android.widget.TextView tvExplain;

    class FeatureAdapter extends RecyclerView.Adapter<IABActivity.FeatureViewHolder> {
        private FeatureAdapter() {
        }

        public IABActivity.FeatureViewHolder onCreateViewHolder(android.view.ViewGroup viewGroup, int i) {
            return new IABActivity.FeatureViewHolder(android.view.LayoutInflater.from(viewGroup.getContext()).inflate(BaseIabActivity.getLayoutId(viewGroup.getContext(), "zimba_premium_sub_activity_feature_item"), viewGroup, false));
        }

        public void onBindViewHolder(IABActivity.FeatureViewHolder viewHolder, int i) {
            viewHolder.icon.setImageResource(((Integer) IABActivity.this.featureDrawable.get(i)).intValue());
            viewHolder.name.setText(((Integer) IABActivity.this.featureName.get(i)).intValue());
        }

        public int getItemCount() {
            return IABActivity.this.featureDrawable.size();
        }
    }

    class FeatureViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        android.widget.ImageView icon;
        android.widget.TextView name;

        public FeatureViewHolder(android.view.View view) {
            super(view);
            this.icon = (android.widget.ImageView) BaseIabActivity.findViewById(view.getContext(), view, "zimba_feature_description");
            this.name = (android.widget.TextView) BaseIabActivity.findViewById(view.getContext(), view, "zimba_feature_name");
        }
    }

    public final class InventoryFinishedTask implements Runnable {
        private final IABActivity context;
        private final Inventory inventory;

        public InventoryFinishedTask(IABActivity activity, Inventory inventory2) {
            this.context = activity;
            this.inventory = inventory2;
        }

        public void run() {
            this.context.inventoryFinishedTask(this.inventory);
        }
    }

    public final class InventoryQueriedTask implements Runnable {
        private final IABActivity context;
        private final Inventory inventory;

        public InventoryQueriedTask(IABActivity premiumSubActivity, Inventory inventory2) {
            this.context = premiumSubActivity;
            this.inventory = inventory2;
        }

        public void run() {
            this.context.inventoryQueryTask(this.inventory);
        }
    }

    class PriceItemSelectedTask implements Runnable {
        private final IABActivity.SubPriceItem priceItem;

        public PriceItemSelectedTask(IABActivity.SubPriceItem priceItem2) {
            this.priceItem = priceItem2;
        }

        public void run() {
            this.priceItem.setSelected(false);
        }
    }

    class PrivacyPolicySpan extends android.text.style.ClickableSpan {
        PrivacyPolicySpan() {
        }

        public void onClick(android.view.View view) {
            ConsentActivity.showPrivacyPolicy(IABActivity.this);
        }

        public void updateDrawState(android.text.TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(android.graphics.Color.parseColor("#ffb7b7b7"));
        }
    }

    public final class SelectedPriceItemTask implements Runnable {
        private final IABActivity activity;
        private final int index;

        public SelectedPriceItemTask(IABActivity premiumSubActivity, int index2) {
            this.activity = premiumSubActivity;
            this.index = index2;
        }

        public void run() {
            this.activity.setSelectedPriceItem(this.index);
        }
    }

    public class SubPriceItem {
        private final float defaultScale = 1.0f;
        int index;
        android.widget.ImageView ivHot;
        android.widget.TextView ivSaleOff;
        private final float selectedScale = 1.24f;
        android.widget.TextView tvOriginalPrice;
        android.widget.TextView tvPrice;
        android.widget.TextView tvTime;
        android.view.View view;

        SubPriceItem(android.view.View view2, int i2) {
            this.view = view2;
            this.index = i2;
            this.tvPrice = (android.widget.TextView) BaseIabActivity.findViewById(view2.getContext(), view2, "zimba_tv_price");
            this.tvOriginalPrice = (android.widget.TextView) BaseIabActivity.findViewById(view2.getContext(), view2, "zimba_tv_original_price");
            this.tvOriginalPrice.getPaint().setFlags(16);
            this.tvOriginalPrice.getPaint().setAntiAlias(true);
            this.tvTime = (android.widget.TextView) BaseIabActivity.findViewById(view2.getContext(), view2, "zimba_tv_time");
            this.ivHot = (android.widget.ImageView) BaseIabActivity.findViewById(view2.getContext(), view2, "zimba_iv_hot_corner");
            this.ivSaleOff = (android.widget.TextView) BaseIabActivity.findViewById(view2.getContext(), view2, "zimba_tv_off_corner");
        }

        public android.view.View getView() {
            return this.view;
        }

        public void bindView(final SkuDetails skuDetails) {
            this.view.setOnClickListener(new android.view.View.OnClickListener() {
                public void onClick(android.view.View view) {
                    IABActivity.SubPriceItem subPriceItem = IABActivity.SubPriceItem.this;
                    subPriceItem.onSelected(skuDetails, subPriceItem.view);
                }
            });
            this.tvPrice.setText(!android.text.TextUtils.isEmpty(skuDetails.getPrice()) ? skuDetails.getPrice() : BaseIabActivity.getStringById(this.view.getContext(), "zimba_price_none"));
            String str = "P1Y";
            if ("P1M".equals(skuDetails.getSubscriptionPeriod())) {
                this.tvTime.setText(BaseIabActivity.getStringById(this.view.getContext(), "zimba_monthly"));
            } else {
                if ("P3M".equals(skuDetails.getSubscriptionPeriod())) {
                    this.tvTime.setText(BaseIabActivity.getStringById(this.view.getContext(), "zimba_three_monthly"));
                } else {
                    if ("P6M".equals(skuDetails.getSubscriptionPeriod())) {
                        this.tvTime.setText(BaseIabActivity.getStringById(this.view.getContext(), "zimba_six_monthly"));
                    } else if (str.equals(skuDetails.getSubscriptionPeriod())) {
                        this.tvTime.setText(BaseIabActivity.getStringById(this.view.getContext(), "zimba_yearly"));
                    }
                }
            }
            if (str.equals(skuDetails.getSubscriptionPeriod())) {
                this.ivHot.setVisibility((int)0);
            }
            if (!android.text.TextUtils.isEmpty(skuDetails.getPriceOriginal())) {
                this.ivHot.setVisibility((int)8);
                this.ivSaleOff.setVisibility((int)0);
                this.ivSaleOff.setText(BaseIabActivity.getStringById(this.view.getContext(), "zimba_price_percent_off", IABActivity.getPercent(skuDetails.getPriceAmountMicros(), skuDetails.getPriceAmountMicrosOriginal())));
                this.tvOriginalPrice.setVisibility((int)0);
                this.tvOriginalPrice.setText(skuDetails.getPriceOriginal());
                return;
            }
            this.ivSaleOff.setVisibility((int)8);
            this.tvOriginalPrice.setVisibility((int)8);
        }

        public final void onSelected(SkuDetails skuDetails, android.view.View view2) {
            IABActivity iABActivity = IABActivity.this;
            iABActivity.selectedSkuDetails = skuDetails;
            iABActivity.setPriceViewSelected(view2, true);
        }

        public void setSelected(boolean playAnim) {
            if (!this.view.isSelected()) {
                this.view.setSelected(true);
                this.view.setPivotX((float) getViewWidth());
                android.view.View view2 = this.view;
                view2.setPivotY((float) view2.getHeight());
                if (playAnim) {
                    this.view.animate().scaleX(1.24f).scaleY(1.24f).start();
                    return;
                }
                this.view.setScaleX(1.24f);
                this.view.setScaleY(1.24f);
            }
        }

        public void setUnSelected(boolean playAnim) {
            if (this.view.isSelected()) {
                this.view.setSelected(false);
                this.view.setPivotX((float) getViewWidth());
                android.view.View view2 = this.view;
                view2.setPivotY((float) view2.getHeight());
                if (playAnim) {
                    this.view.animate().scaleX(1.0f).scaleY(1.0f).start();
                    return;
                }
                this.view.setScaleX(1.0f);
                this.view.setScaleY(1.0f);
            }
        }

        private int getViewWidth() {
            int size = IABActivity.this.priceItemList.size();
            int i = this.index;
            if (i == 0) {
                return this.view.getWidth();
            }
            if (i == size - 1) {
                return 0;
            }
            return this.view.getWidth() / 2;
        }
    }

    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId(this, "zimba_iab_activity"));
        initUI();
    }

    private void initUI() {
        initFeatureLayout();
        this.banner = (Banner) findViewById(this, "zimba_sub_banner");
        this.banner.setBannerLoader(this.bannerLoader);
        this.banner.setPageTransformer(new ScaleCenterTransformer());
        this.banner.setData(this.imageList);
        this.banner.setOnBannerTouchListener(new Banner.BannerTouchListener() {
            public void onTouch() {
                android.util.Log.d("IMBA", "banner touched");
            }
        });
        this.priceLayout = (android.widget.LinearLayout) findViewById(this, "zimba_price_layout");
        this.btnRestore = (android.widget.TextView) findViewById(this, "zimba_restore_tv");
        this.btnRestore.getPaint().setFlags(8);
        this.btnRestore.getPaint().setAntiAlias(true);
        this.btnRestore.setOnClickListener(this);
        this.btnBuy = (android.widget.TextView) findViewById(this, "zimba_buy_btn");
        this.btnBuy.setOnClickListener(this);
        ((android.widget.TextView) findViewById(this, "zimba_premium_feature_title")).setText(getStringById(this, "zimba_premium_features"));
        this.tvExplain = (android.widget.TextView) findViewById(this, "zimba_explain");
        this.tvExplain.setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
        android.view.View btnClose = findViewById(this, "zimba_close_iv");
        btnClose.setOnClickListener(this);
        btnClose.setAlpha(0.0f);
        btnClose.setVisibility((int)0);
        btnClose.animate().alpha(1.0f).setDuration(500).setStartDelay(5000);
        initPriceLayout();
    }

    private void initFeatureLayout() {
        this.imageList = new java.util.ArrayList();
        this.imageList.add(Integer.valueOf(getDrawableId(this, "zimba_premium_sub_banner1")));
        this.imageList.add(Integer.valueOf(getDrawableId(this, "zimba_premium_sub_banner2")));
        this.imageList.add(Integer.valueOf(getDrawableId(this, "zimba_premium_sub_banner3")));
        this.imageList.add(Integer.valueOf(getDrawableId(this, "zimba_premium_sub_banner4")));
        this.featureName = new java.util.ArrayList();
        this.featureName.add(Integer.valueOf(getStringId(this, "zimba_premium_sub_content_1")));
        this.featureName.add(Integer.valueOf(getStringId(this, "zimba_premium_sub_content_2")));
        this.featureName.add(Integer.valueOf(getStringId(this, "zimba_premium_sub_content_3")));
        this.featureName.add(Integer.valueOf(getStringId(this, "zimba_premium_sub_content_4")));
        this.featureDrawable = new java.util.ArrayList();
        this.featureDrawable.add(Integer.valueOf(getDrawableId(this, "zimba_premium_sub_content_icon_1")));
        this.featureDrawable.add(Integer.valueOf(getDrawableId(this, "zimba_premium_sub_content_icon_2")));
        this.featureDrawable.add(Integer.valueOf(getDrawableId(this, "zimba_premium_sub_content_icon_3")));
        this.featureDrawable.add(Integer.valueOf(getDrawableId(this, "zimba_premium_sub_content_icon_4")));
        RecyclerView recyclerView = (RecyclerView) findViewById(this, "zimba_premium_feature_recycler_view");
        recyclerView.setLayoutManager(new GridLayoutManager(this, featureColumnCount));
        recyclerView.setAdapter(new IABActivity.FeatureAdapter());
    }

    private void initPriceLayout() {
        this.priceLayout.removeAllViews();
        android.view.LayoutInflater from = android.view.LayoutInflater.from(this);
        for (int i = 0; i < 1; i++) {
            android.view.View inflate = from.inflate(getLayoutId(this, "zimba_premium_sub_activity_price_item"), this.priceLayout, false);
            IABActivity.SubPriceItem priceItem = new IABActivity.SubPriceItem(inflate, i);
            priceItem.view.setEnabled(false);
            if (i == 1) {
                priceItem.tvPrice.setText(getStringById(this, "zimba_monthly"));
            } else if (i == 0) {
                priceItem.tvPrice.setText(getStringById(this, "zimba_yearly"));
                priceItem.tvPrice.post(new IABActivity.PriceItemSelectedTask(priceItem));
            } else {
                priceItem.tvPrice.setText(getStringById(this, "zimba_six_monthly"));
            }
            this.priceLayout.addView(inflate);
        }
        updateBtnBuyState();
        updateBtnRestoreState();
        updateExplainText();
    }

    public static void start(android.content.Context context, String str) {
        start(context, str, "");
    }

    public static void startIfNotSubs(android.content.Context context, String from) {
        startIfNotSubs(context, from, "");
    }

    public static void startIfNotSubs(android.content.Context context, String from, String event) {
        if (!PremiumConfig.get(context).isSubscribed()) {
            start(context, from, event);
        }
    }

    public static void start(android.content.Context context, String from, String event) {
        android.content.Intent intent = new android.content.Intent(context, IABActivity.class);
        intent.putExtra("from", from);
        intent.putExtra(NotificationCompat.CATEGORY_EVENT, event);
        if (!(context instanceof android.app.Activity)) {
            intent.addFlags((int)268435456);
        }
        context.startActivity(intent);
    }

    protected void onActivityResult(int requestCode, int resultCode, android.content.Intent data) {
        StringBuilder sb = new StringBuilder();
        sb.append("onActivityResult(");
        sb.append(requestCode);
        String str = ",";
        sb.append(str);
        sb.append(resultCode);
        sb.append(str);
        sb.append(data);
        String str2 = "IMBA";
        android.util.Log.d(str2, sb.toString());
        if (this.mHelper == null || this.mHelper.handleActivityResult(requestCode, resultCode, data)) {
            android.util.Log.d(str2, "onActivityResult handled by IABUtil.");
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && requestCode == 256) {
            finish();
        }
    }

    public void OnSetupFinished(IabResult result) {
    }

    public void OnQueryInventoryFinished(IabResult result, Inventory inventory) {
        this.skuDetailsList = inventory.getAllSkuDetails("subs");
        this.currentSubsSku = BillingUtils.getOwnedSubsSku(inventory);
        StringBuilder sb = new StringBuilder();
        sb.append("current subs sku=");
        sb.append(this.currentSubsSku);
        String str = "IMBA";
        android.util.Log.d(str, sb.toString());
        this.ownedSubsList = BillingUtils.getAutoRenewOwnedSkus(inventory);
        this.isPremium = BillingUtils.isOwnedSubs(inventory, this);
        StringBuilder sb2 = new StringBuilder();
        sb2.append("User ");
        sb2.append(this.isPremium ? "HAS" : "DOES NOT HAVE");
        sb2.append(" subscription.");
        android.util.Log.d(str, sb2.toString());
        if (result.getResponse() == 0) {
            inventoryQueryTask(inventory);
        }
    }

    public void OnPurchaseFinished(IabResult result, Purchase purchase) {
        StringBuilder sb = new StringBuilder();
        sb.append("Purchase success = ");
        sb.append(purchase.getSku());
        android.util.Log.d("IMBA", sb.toString());
        if (purchase.getItemType().equals("subs")) {
            android.widget.Toast.makeText(this, getStringById(this, "zimba_premium_sub_success"), (int)0).show();
            onBackPressed();
        }
    }

    public void inventoryFinished(int response, Inventory inventory) {
        StringBuilder sb = new StringBuilder();
        sb.append("Query inventory finished. response:");
        sb.append(response);
        String str = "IMBA";
        android.util.Log.i(str, sb.toString());
        if (response != 0) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Failed to query inventory: ");
            sb3.append(response);
            android.util.Log.e(str, sb3.toString());
            return;
        }
        new Thread(new IABActivity.InventoryFinishedTask(this, inventory)).start();
    }

    public final void inventoryFinishedTask(Inventory inventory) {
        android.util.Log.i("IMBA", "Query inventory was successful.");
        this.isPremium = BillingUtils.isOwnedSubs(inventory, this);
        new Thread(new IABActivity.InventoryQueriedTask(this, inventory)).start();
    }

    public final void inventoryQueryTask(Inventory inventory) {
        this.isPremium = BillingUtils.isOwnedSubs(inventory, this);
        updateBtnRestoreState();
        if (this.isPremium) {
            this.ownedSubsList = BillingUtils.getAutoRenewOwnedSkus(inventory);
        }
        updatePriceLayout();
        updateBtnBuyState();
        updateExplainText();
    }

    private void updatePriceLayout() {
        if (!this.skuDetailsList.isEmpty()) {
            this.priceLayout.removeAllViews();
            android.view.LayoutInflater from = android.view.LayoutInflater.from(this);
            int size = this.skuDetailsList.size();
            int index = -1;
            for (int i = 0; i < size; i++) {
                SkuDetails skuDetails = (SkuDetails) this.skuDetailsList.get(i);
                android.view.View inflate = from.inflate(getLayoutId(this, "zimba_premium_sub_activity_price_item"), this.priceLayout, false);
                IABActivity.SubPriceItem priceItem = new IABActivity.SubPriceItem(inflate, i);
                priceItem.bindView(skuDetails);
                this.priceLayout.addView(inflate);
                this.priceItemList.add(priceItem);
                if ("P1Y".equals(skuDetails.getSubscriptionPeriod())) {
                    index = i;
                }
            }
            if (this.selectedSkuDetails == null && index != -1) {
                this.selectedSkuDetails = (SkuDetails) this.skuDetailsList.get(index);
                this.priceLayout.post(new IABActivity.SelectedPriceItemTask(this, index));
            }
        }
    }

    public final void setSelectedPriceItem(int index) {
        ((IABActivity.SubPriceItem) this.priceItemList.get(index)).setSelected(false);
    }

    public void setPriceViewSelected(android.view.View view, boolean selected) {
        for (IABActivity.SubPriceItem priceItem : this.priceItemList) {
            if (priceItem.getView() == view) {
                priceItem.setSelected(selected);
            } else {
                priceItem.setUnSelected(selected);
            }
        }
        updateBtnBuyState();
        updateExplainText();
    }

    private void updateBtnBuyState() {
        if (this.isPremium) {
            this.btnBuy.setText(getStringById(this, "zimba_upgrade_subscription"));
            return;
        }
        String string = getStringById(this, "zimba_price_none");
        if (this.selectedSkuDetails != null) {
            string = String.valueOf(BillingUtils.getFreeTrialPeriod(this.selectedSkuDetails.getFreeTrialPeriod()));
        }
        this.btnBuy.setText(getStringById(this, "zimba_free_trial_for_days", string));
    }

    private void updateBtnRestoreState() {
        this.btnRestore.setVisibility(this.isPremium ? (int)8 : (int)0);
    }

    private void updateExplainText() {
        StringBuilder sb = new StringBuilder();
        if (this.isPremium) {
            sb.append(getStringById(this, "zimba_premium_explain_upgrade"));
        } else {
            String str = "zimba_price_none";
            String string = getStringById(this, str);
            String string2 = getStringById(this, str);
            if (this.selectedSkuDetails != null) {
                string2 = this.selectedSkuDetails.getPrice();
                if ("P1M".equals(this.selectedSkuDetails.getSubscriptionPeriod())) {
                    string = getStringById(this, "zimba_monthly");
                } else {
                    if ("P6M".equals(this.selectedSkuDetails.getSubscriptionPeriod())) {
                        string = getStringById(this, "zimba_six_monthly");
                    } else {
                        if ("P1Y".equals(this.selectedSkuDetails.getSubscriptionPeriod())) {
                            string = getStringById(this, "zimba_yearly");
                        }
                    }
                }
            }
            sb.append(getStringById(this, "zimba_premium_explain_price", string2, string));
        }
        sb.append(getStringById(this, "zimba_premium_explain_cancel"));
        String string3 = getStringById(this, "zimba_privacy_policy");
        sb.append(" ");
        sb.append(string3);
        android.text.SpannableString spannableString = new android.text.SpannableString(sb.toString());
        spannableString.setSpan(new android.text.style.UnderlineSpan(), sb.length() - string3.length(), sb.length(), 33);
        spannableString.setSpan(new IABActivity.PrivacyPolicySpan(), sb.length() - string3.length(), sb.length(), 33);
        this.tvExplain.setText(spannableString);
    }

    protected void onStart() {
        super.onStart();
        this.banner.startAutoPlay();
    }

    protected void onStop() {
        super.onStop();
        this.banner.stopAutoPlay();
    }

    public void onClick(android.view.View view) {
        java.util.List list;
        int id = view.getId();
        if (id == getId(this, "zimba_buy_btn")) {
            if (this.selectedSkuDetails == null) {
                android.widget.Toast.makeText(this, getStringById(this, "zimba_premium_error_reason"), (int)1).show();
            } else if (canDoSubscribe()) {
                logEventClickBuy();
                String payload = "";
                if (android.text.TextUtils.isEmpty(this.currentSubsSku) || this.currentSubsSku.equals(this.selectedSkuDetails.getSku())) {
                    list = null;
                } else {
                    java.util.List<String> oldSkus = new java.util.ArrayList<>();
                    oldSkus.add(this.currentSubsSku);
                    list = oldSkus;
                }
                String str = "IMBA";
                android.util.Log.d(str, "Launching purchase flow for subscription.");
                try {
                    this.mHelper.launchPurchaseFlow(this, this.selectedSkuDetails.getSku(), "subs", list, 10001, this.mPurchaseFinishedListener, payload);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    android.util.Log.e(str, "Error launching purchase flow. Another async operation in progress.");
                }
            }
        } else if (id == getId(this, "zimba_restore_tv")) {
            if (this.mHelper != null) {
                try {
                    this.mHelper.queryInventoryAsync(true, null, defSubsSkus, new IabHelper.QueryInventoryFinishedListener() {
                        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                            String str = "IMBA";
                            if (IABActivity.this.mHelper == null) {
                                android.util.Log.d(str, "Helper is null");
                            } else if (result.isFailure()) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Failed to query inventory: ");
                                sb.append(result);
                                android.util.Log.d(str, sb.toString());
                            } else {
                                android.util.Log.d(str, "Query inventory was successful.");
                                IABActivity iABActivity = IABActivity.this;
                                iABActivity.isPremium = BillingUtils.isOwnedSubs(inventory, iABActivity);
                                android.util.Log.d(str, "Initial inventory query finished; enabling main UI.");
                                if (IABActivity.this.isPremium) {
                                    IABActivity iABActivity2 = IABActivity.this;
                                    android.widget.Toast.makeText(iABActivity2, BaseIabActivity.getStringById(iABActivity2, "zimba_premium_sub_success"), (int)0).show();
                                    IABActivity.this.onBackPressed();
                                }
                            }
                        }
                    });
                } catch (Exception e2) {
                }
            }
        } else {
            if (id == getId(this, "zimba_close_iv") && !ClickUtils.isJustClick()) {
                onBackPressed();
            }
        }
    }

    private boolean canDoSubscribe() {
        if (!android.text.TextUtils.isEmpty(this.currentSubsSku)) {
            SkuDetails skuDetails = null;
            java.util.Iterator it = this.skuDetailsList.iterator();
            while (true) {
                if (it.hasNext()) {
                    SkuDetails details = (SkuDetails) it.next();
                    if (android.text.TextUtils.equals(this.currentSubsSku, details.getSku())) {
                        skuDetails = details;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!(skuDetails == null || this.selectedSkuDetails == null)) {
                if (android.text.TextUtils.equals(skuDetails.getSku(), this.selectedSkuDetails.getSku())) {
                    android.widget.Toast.makeText(this, getStringById(this, "zimba_premium_error_current_sub_plan"), (int)1).show();
                    return false;
                } else if (SkuDetails.getSubPeriod(this.selectedSkuDetails.getSubscriptionPeriod()) < SkuDetails.getSubPeriod(skuDetails.getSubscriptionPeriod())) {
                    android.widget.Toast.makeText(this, getStringById(this, "zimba_premium_error_unable_downgrade"), (int)1).show();
                    return false;
                }
            }
        }
        return true;
    }

    public void onBackPressed() {
        logEventClose();
        if (!this.isPremium) {
            if (!"click_check_more".equals(this.intentEvent)) {
                if (!ClickUtils.isJustClick()) {
                    IABDialogActivity.start(this, 1, this.intentFrom, 256);
                    return;
                }
                return;
            }
        }
        super.onBackPressed();
    }

    public static String getPercent(long a, long b) {
        java.math.BigDecimal valueOf = java.math.BigDecimal.valueOf(a);
        java.math.BigDecimal valueOf2 = java.math.BigDecimal.valueOf(b);
        return valueOf2.subtract(valueOf).divide(valueOf2, 2, 4).multiply(new java.math.BigDecimal("100")).setScale(0, 7).toString();
    }

    public String IabSource() {
        return "activity";
    }
}
