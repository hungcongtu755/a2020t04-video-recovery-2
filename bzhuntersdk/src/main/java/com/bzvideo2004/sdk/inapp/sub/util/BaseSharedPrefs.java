package com.bzvideo2004.sdk.inapp.sub.util;

public abstract class BaseSharedPrefs {
    private android.content.SharedPreferences.Editor editor;
    private android.content.SharedPreferences sharedPref;

    public abstract android.content.SharedPreferences getSharedPref();

    public boolean bf() {
        return true;
    }

    private android.content.SharedPreferences get() {
        if (!bf()) {
            this.sharedPref = null;
            return getSharedPref();
        }
        if (this.sharedPref == null) {
            this.sharedPref = getSharedPref();
        }
        return this.sharedPref;
    }

    public void editor() {
        this.editor = get().edit();
    }

    public void reset() {
        this.editor.apply();
        this.editor = null;
    }

    public String getString(String str, String str2) {
        return get().getString(str, str2);
    }

    public void putInString(String str, String str2) {
        android.content.SharedPreferences.Editor editor2 = this.editor;
        if (editor2 != null) {
            editor2.putString(str, str2);
        } else {
            get().edit().putString(str, str2).apply();
        }
    }

    public int getInt(String str, int i) {
        return get().getInt(str, i);
    }

    public void putInt(String str, int i) {
        android.content.SharedPreferences.Editor editor2 = this.editor;
        if (editor2 != null) {
            editor2.putInt(str, i);
        } else {
            get().edit().putInt(str, i).apply();
        }
    }

    public boolean getBoolean(String str, boolean z) {
        return get().getBoolean(str, z);
    }

    public void putBoolean(String str, boolean z) {
        android.content.SharedPreferences.Editor editor2 = this.editor;
        if (editor2 != null) {
            editor2.putBoolean(str, z);
        } else {
            get().edit().putBoolean(str, z).apply();
        }
    }

    public long getLong(String str, long j) {
        return get().getLong(str, j);
    }

    public void putLong(String str, long j) {
        android.content.SharedPreferences.Editor editor2 = this.editor;
        if (editor2 != null) {
            editor2.putLong(str, j);
        } else {
            get().edit().putLong(str, j).apply();
        }
    }

    public float getFloat(String str, float f) {
        return get().getFloat(str, f);
    }

    public void putFloat(String str, float f) {
        android.content.SharedPreferences.Editor editor2 = this.editor;
        if (editor2 != null) {
            editor2.putFloat(str, f);
        } else {
            get().edit().putFloat(str, f).apply();
        }
    }

    public java.util.Set<String> getStringSet(String str, java.util.Set<String> set) {
        return get().getStringSet(str, set);
    }

    public void putStringSet(String str, java.util.Set<String> set) {
        android.content.SharedPreferences.Editor editor2 = this.editor;
        if (editor2 != null) {
            editor2.putStringSet(str, set);
        } else {
            get().edit().putStringSet(str, set).apply();
        }
    }

    public void remove(String str) {
        android.content.SharedPreferences.Editor editor2 = this.editor;
        if (editor2 != null) {
            editor2.remove(str);
        } else {
            get().edit().remove(str).apply();
        }
    }

    public boolean contains(String str) {
        return get().contains(str);
    }

    public void clear() {
        android.content.SharedPreferences.Editor editor2 = this.editor;
        if (editor2 != null) {
            editor2.clear();
        } else {
            get().edit().clear().apply();
        }
    }
}
