package com.bzvideo2004.sdk.inapp.sub.view;

public interface BannerLoader<T, V extends android.view.View> {
    void bindView(android.content.Context context, T t, V v);

    V getView(android.content.Context context);
}
